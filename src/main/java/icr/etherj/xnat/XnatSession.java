/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class XnatSession implements XnatEntity, Comparable<XnatSession>,
	XnatAssessorContainer, XnatScanContainer
{
	private final Map<String,XnatAssessor> assessors = new TreeMap<>();
	private final String date;
	private final String id;
	private String label = "";
	private final Set<XnatAssessorContainer.Listener> assessorListeners =
		new HashSet<>();
	private final Set<XnatScanContainer.Listener> scanListeners =
		new HashSet<>();
	private final String modality;
	private final Map<String,XnatScan> scans = new TreeMap<>();
	private final XnatId xnatId;

	public XnatSession(XnatId xnatId, String label, String date, String modality)
	{
		this.xnatId = xnatId;
		this.id = xnatId.getSessionId();
		this.label = label;
		this.date = date;
		this.modality = modality;
	}

	public XnatAssessor add(XnatAssessor assessor)
	{
		XnatAssessor result = assessors.put(assessor.getId(), assessor);
		fireAssessorAdded(assessor);
		return result;
	}

	public XnatScan add(XnatScan scan)
	{
		XnatScan result = scans.put(scan.getId(), scan);
		fireScanAdded(scan);
		return result;
	}

	public boolean addXnatAssessorContainerListener(
		XnatAssessorContainer.Listener listener)
	{
		return assessorListeners.add(listener);
	}

	public boolean addXnatScanContainerListener(
		XnatScanContainer.Listener listener)
	{
		return scanListeners.add(listener);
	}

	@Override
	public int compareTo(XnatSession other)
	{
		int result = date.compareTo(other.date);
		if (result != 0)
		{
			return result;
		}
		result = modality.compareTo(other.modality);
		return (result != 0) ? result : label.compareTo(other.label);
	}

	public List<XnatAssessor> getAssessors()
	{
		List<XnatAssessor> list = new ArrayList<>();
		list.addAll(assessors.values());
		return list;
	}

	/**
	 *
	 * @return
	 */
	public String getDate()
	{
		return date;
	}

	@Override
	public String getId()
	{
		return id;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 *
	 * @return
	 */
	public String getModality()
	{
		return modality;
	}

	@Override
	public String getType()
	{
		return XnatEntity.Session;
	}

	@Override
	public XnatId getXnatId()
	{
		return xnatId;
	}

	public XnatScan remove(String scanId)
	{
		XnatScan result = scans.remove(scanId);
		if (result != null)
		{
			fireScanRemoved(result);
		}
		return result;
	}

	public XnatAssessor remove(XnatAssessor assessor)
	{
		XnatAssessor result = assessors.remove(assessor.getId());
		fireAssessorRemoved(assessor);
		return result;
	}

	public XnatScan remove(XnatScan scan)
	{
		XnatScan result = scans.remove(scan.getId());
		fireScanRemoved(scan);
		return result;
	}

	public boolean removeXnatAssessorContainerListener(
		XnatAssessorContainer.Listener listener)
	{
		return assessorListeners.remove(listener);
	}

	public boolean removeXnatScanContainerListener(
		XnatScanContainer.Listener listener)
	{
		return scanListeners.remove(listener);
	}

	private void fireAssessorAdded(XnatAssessor assessor)
	{
		XnatAssessorContainer.Event e =
			new XnatAssessorContainer.Event(this, assessor);
		for (XnatAssessorContainer.Listener l : assessorListeners)
		{
			l.assessorAdded(e);
		}
	}

	private void fireAssessorRemoved(XnatAssessor assessor)
	{
		XnatAssessorContainer.Event e =
			new XnatAssessorContainer.Event(this, assessor);
		for (XnatAssessorContainer.Listener l : assessorListeners)
		{
			l.assessorRemoved(e);
		}
	}

	private void fireScanAdded(XnatScan scan)
	{
		XnatScanContainer.Event e = new XnatScanContainer.Event(this, scan);
		for (XnatScanContainer.Listener l : scanListeners)
		{
			l.scanAdded(e);
		}
	}

	private void fireScanRemoved(XnatScan scan)
	{
		XnatScanContainer.Event e = new XnatScanContainer.Event(this, scan);
		for (XnatScanContainer.Listener l : scanListeners)
		{
			l.scanRemoved(e);
		}
	}

}
