/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class XnatImageAnnotationCollection extends XnatAssessor
	implements XnatAssessorContainer
{
	private final Map<String,XnatImageAnnotation> annotations = new HashMap<>();
	private final String regionSetId;
	private XnatRegionSet regionSet;
	private final String uid;

	public XnatImageAnnotationCollection(XnatId xnatId, String label, String uid,
		String regionSetId)
	{
		super(xnatId, label);
		this.uid = uid;
		this.regionSetId = regionSetId;
	}

	public XnatImageAnnotation addImageAnnotation(XnatImageAnnotation ia)
	{
		XnatImageAnnotation value = annotations.put(ia.getId(), ia);
		return value;
	}

	public XnatImageAnnotation getImageAnnotation(String id)
	{
		return annotations.get(id);
	}

	public List<XnatImageAnnotation> getImageAnnotations()
	{
		List<XnatImageAnnotation> list = new ArrayList<>();
		list.addAll(annotations.values());
		return list;
	}

	/**
	 * @return the regionSetId
	 */
	public String getRegionSetId()
	{
		return regionSetId;
	}

	/**
	 * @return the regionSet
	 */
	public XnatRegionSet getRegionSet()
	{
		return regionSet;
	}

	@Override
	public String getType()
	{
		return XnatEntity.ImageAnnotationCollection;
	}

	/**
	 * @return the uid
	 */
	public String getUid()
	{
		return uid;
	}

	public XnatImageAnnotation removeImageAnnotation(String id)
	{
		XnatImageAnnotation value = annotations.remove(id);
		return value;
	}

	public XnatImageAnnotation removeImageAnnotation(XnatImageAnnotation ia)
	{
		return removeImageAnnotation(ia.getId());
	}

	/**
	 * @param regionSet the regionSet to set
	 */
	public void setRegionSet(XnatRegionSet regionSet)
		throws IllegalArgumentException
	{
		if (!regionSet.getId().equals(regionSetId))
		{
			throw new IllegalArgumentException("RegionSet ID mismatch");
		}
		this.regionSet = regionSet;
	}

}
