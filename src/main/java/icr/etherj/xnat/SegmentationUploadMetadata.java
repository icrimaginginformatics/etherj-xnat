/** *******************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package icr.etherj.xnat;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.FunctionalGroupsFrame;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import java.io.File;

/**
 *
 * @author jamesd
 */
public class SegmentationUploadMetadata extends AbstractUploadMetadata<Segmentation>
	implements UploadMetadata<Segmentation>
{
	/**
	 *
	 * @param seg
	 * @param file
	 */
	public SegmentationUploadMetadata(Segmentation seg, File file)
	{
		super(seg, "SEG", file, "RoiCollection_"+Uids.createShortUnique(),
			new XnatUploadableFile(file, "out", "SEG", "DICOM", "EXTERNAL", "DICOM Segmentation"));
		setLabel("SEG_"+IodUtils.getDate(seg)+"_"+
			IodUtils.getTime(seg));
		buildUidSets(seg);
	}

	@Override
	public String getName()
	{
		return IodUtils.getName(getUploadItem());
	}

	@Override
	public String getRootServerPath()
	{
		StudyUidContainer container = getPreferredContainer();
		return "/data/archive/projects/"+container.getProjectId()+
			"/subjects/"+container.getSubjectId()+
			"/experiments/"+container.getSessionId();
	}

	@Override
	public String getTypePath()
	{
		return "/assessors/";
	}
	@Override
	public String getUid()
	{
		return getUploadItem().getSopInstanceUid();
	}

	private void buildUidSets(Segmentation seg)
	{
		addStudyUid(seg.getGeneralStudyModule().getStudyInstanceUid());
		CommonInstanceReferenceModule cirm = seg.getCommonInstanceReferenceModule();
		for (ReferencedSeries refSeries : cirm.getReferencedSeriesList())
		{
			addSeriesUid(refSeries.getSeriesInstanceUid());
			for (ReferencedInstance refInst :
				refSeries.getReferencedInstanceList())
			{
				addSopInstanceUid(refInst.getReferencedSopInstanceUid());
			}
		}

		// Derivation image module
		MultiframeFunctionalGroupsModule mfgm =
			seg.getMultiframeFunctionalGroupsModule();
		SegmentationPerFrameFunctionalGroups segPffg = 
			(SegmentationPerFrameFunctionalGroups) mfgm.getPerFrameFunctionalGroups();
		for (FunctionalGroupsFrame frame : segPffg.getFrameList())
		{
			SegmentationFunctionalGroupsFrame segFrame =
				(SegmentationFunctionalGroupsFrame) frame;
			for (DerivationImage derivImage : segFrame.getDerivationImages())
			{
				for (SourceImage image : derivImage.getSourceImageList())
				{
					// Disabled for now as dcmjs is broken, it creates an internal
					// multiframe image whose UID appears here
//					addSopInstanceUid(image.getReferencedSopInstanceUid());
				}
			}
		}
	}

}
