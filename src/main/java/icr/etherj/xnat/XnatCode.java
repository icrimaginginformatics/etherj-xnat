/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.AbstractExceptionCode;

/**
 *
 * @author jamesd
 */
public final class XnatCode extends AbstractExceptionCode
{
	public static final XnatCode Success = new XnatCode("Success", "00000");
	// 01 Network
	public static final XnatCode IO = new XnatCode("IO", "01001");
	public static final XnatCode MalformedUrl = new XnatCode("Malformed URL",
		"01002");
	public static final XnatCode SocketTimeout = new XnatCode("Socket timeout",
		"01003");
	public static final XnatCode UnknownService = new XnatCode("Unknown service",
		"01004");
	public static final XnatCode Protocol = new XnatCode("Protocol", "01005");
	public static final XnatCode UnknownHost = new XnatCode("Unknown host",
		"01006");
	public static final XnatCode Connect = new XnatCode("Connect", "01007");
	public static final XnatCode SSL = new XnatCode("SSL", "01008");
	public static final XnatCode NoSuchAlgorithm = new XnatCode(
		"No such algorithm", "01101");
	public static final XnatCode KeyManagement = new XnatCode("Key management",
		"01102");
	// 02 XML
	public static final XnatCode XML = new XnatCode("XML", "02001");
	public static final XnatCode ParserConfiguration = new XnatCode(
		"Parser configuration", "02002");
	public static final XnatCode SAX = new XnatCode("SAX", "02003");
	// 04 General
	public static final XnatCode IllegalArgument = new XnatCode(
		"Illegal argument", "04001");
	public static final XnatCode IllegalState = new XnatCode("Illegal state",
		"04002");
	public static final XnatCode UnknownEncoding = new XnatCode(
		"Unknown encoding", "04003");
	// 03 - HTTP
	public static final XnatCode HttpInvalid = new XnatCode(
		"Invalid HTTP response", "03001");
	public static final XnatCode HttpNotImplementedYet = new XnatCode(
		"HTTP not implemented yet", "03002");
	public static final XnatCode HttpBadRequest = new XnatCode(
		"HTTP 400 bad request", "03400");
	public static final XnatCode HttpUnauthorised = new XnatCode(
		"HTTP 401 unauthorised", "03401");
	public static final XnatCode HttpForbidden = new XnatCode(
		"HTTP 403 forbidden", "03403");
	public static final XnatCode HttpNotFound = new XnatCode("HTTP 404 not found",
		"03404");
	public static final XnatCode HttpMethodNotAllowed = new XnatCode(
		"HTTP 405 method not allowed", "03405");
	public static final XnatCode HttpConflict = new XnatCode(
		"HTTP 409 conflict", "03409");
	public static final XnatCode HttpUnprocessableEntity = new XnatCode(
		"HTTP 422 unprocessable entity", "03422");
	public static final XnatCode HttpInternalError = new XnatCode(
		"HTTP 500 internal error", "03500");
	public static final XnatCode HttpGatewayTimeout = new XnatCode(
		"HTTP 504 gateway timeout", "03504");
	// 04 - Data Source
	public static final XnatCode UnknownTag = new XnatCode("Unknown tag",
		"04001");
	public static final XnatCode FileNotFound = new XnatCode("File not found",
		"04002");
	public static final XnatCode FileSize = new XnatCode("File size", "04003");
	public static final XnatCode InvalidFileUri = new XnatCode("Invalid file URI",
		"04004");
	public static final XnatCode NoUniqueResult = new XnatCode("No unique result",
		"04005");
	public static final XnatCode UidMismatch = new XnatCode("UID mismatch",
		"04006");
	public static final XnatCode CacheCreate = new XnatCode("Cache create",
		"04007");
	public static final XnatCode CacheDelete = new XnatCode("Cache delete",
		"04008");
	public static final XnatCode CacheRead = new XnatCode("Cache read", "04009");
	public static final XnatCode InvalidXnatId = new XnatCode("Invalid XNAT ID",
		"04010");
	// 05 - Other
	public static final XnatCode Unsupported = new XnatCode("Unsupported",
		"05001");

	@Override
	public String toString()
	{
		return getMessage();
	}

	private XnatCode(String message, String code)
	{
		super(message, code);
	}
}
