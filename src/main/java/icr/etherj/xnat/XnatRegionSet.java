/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class XnatRegionSet extends XnatAssessor implements XnatAssessorContainer
{
	private final String originalType;
	private final Map<String,XnatRegion> regions = new HashMap<>();
	private int schemaRegionCount = 0;

	public XnatRegionSet(XnatId xnatId, String label, String originalType)
	{
		super(xnatId, label);
		this.originalType = originalType;
	}

	public XnatRegion addRegion(XnatRegion region)
	{
		XnatRegion value = regions.put(region.getId(), region);
		return value;
	}

	public XnatRegion getRegion(String id)
	{
		return regions.get(id);
	}

	public int getRegionCount()
	{
		return regions.size();
	}

	public List<XnatRegion> getRegions()
	{
		List<XnatRegion> list = new ArrayList<>();
		list.addAll(regions.values());
		return list;
	}

	/**
	 * @return the originalType
	 */
	public String getOriginalType()
	{
		return originalType;
	}

	public int getSchemaRegionCount()
	{
		return schemaRegionCount;
	}

	@Override
	public String getType()
	{
		return XnatEntity.RegionSet;
	}

	public XnatRegion removeRegion(String id)
	{
		XnatRegion value = regions.remove(id);
		return value;
	}

	public XnatRegion removeRegion(XnatRegion region)
	{
		return removeRegion(region.getId());
	}

	public void setSchemaRegionCount(int count)
	{
		schemaRegionCount = count;
	}

}
