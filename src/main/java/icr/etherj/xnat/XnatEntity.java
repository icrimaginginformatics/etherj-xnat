/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

/**
 *
 * @author jamesd
 */
public interface XnatEntity
{
	public static final String AimInstance = "AIM instance";
	public static final String AnnotationRole = "AnnotationRoleEntity";
	public static final String Assessor = "assessor";
	public static final String AssessorContainer = "AssessorContainer";
	public static final String Calculation = "CalculationEntity";
	public static final String Container = "container";
	public static final String ImageAnnotationCollection = "iac";
	public static final String ImageAnnotation = "ia";
	public static final String ImageObservation = "ImagingObservationEntity";
	public static final String ImagePhysical = "ImagingPhysicalEntity";
	public static final String Inference = "InferenceEntity";
	public static final String Project = "project";
	public static final String Markup = "MarkupEntity";
	public static final String RegionSet = "regionset";
	public static final String Region = "region";
	public static final String RtStruct = "rtstruct";
	public static final String Scan = "scan";
	public static final String Segmentation = "SegmentationEntity";
	public static final String Session = "session";
	public static final String Subject = "subject";
	public static final String TaskContext = "TaskContextEntity";

	/**
	 *
	 * @return
	 */
	public String getId();

	/**
	 *
	 * @return
	 */
	public String getType();

	/**
	 *
	 * @return
	 */
	public XnatId getXnatId();
}
