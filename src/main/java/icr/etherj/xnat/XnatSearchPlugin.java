/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchPlugin;
import icr.etherj.search.SearchSpecification;
import java.util.List;

/**
 *
 * @author jamesd
 * @param <T>
 */
public interface XnatSearchPlugin<T>
{
	static final String Equals = "=";
	static final String LessThan = "&lt;";
	static final String Like = "LIKE";

	static final String AssessorSessionId = "imageSession_ID";
	static final String ID = "ID";
	static final String Label = "Label";
	static final String Name = "Name";
	static final String Project = "project";
	static final String ProjectIdHeader = "ProjectId";
	static final String StringType = "string";
	static final String SessionIdHeader = "SessionId";
	static final String SubjectId = "subject_ID";
	static final String SubjectIdHeader = "SubjectId";
	static final String Uid = "UID";

	/**
	 *
	 * @param xsc
	 * @param spec
	 * @return
	 * @throws icr.etherj.xnat.XnatException
	 */
	boolean exists(XnatServerConnection xsc, SearchSpecification spec)
		throws XnatException;

	/**
	 *
	 * @param xsc
	 * @param spec
	 * @return
	 * @throws icr.etherj.xnat.XnatException
	 */
	List<T> search(XnatServerConnection xsc, SearchSpecification spec)
		throws XnatException;

	/**
	 *
	 */
	public static class Helper
	{
		/**
		 *
		 * @param sb
		 * @param schemaField
		 * @param crit
		 * @return
		 */
		public StringBuilder addCriterion(StringBuilder sb, String schemaField,
			SearchCriterion crit)
		{
			return addCriterion(sb, schemaField, crit.createComparatorSql(),
				crit.getValue());
		}

		/**
		 *
		 * @param sb
		 * @param schemaField
		 * @param comparator
		 * @param value
		 * @return
		 */
		public StringBuilder addCriterion(StringBuilder sb, String schemaField,
			String comparator, String value)
		{
			sb.append("		<xdat:criteria override_value_formatting=\"0\">\n");
			sb.append("			<xdat:schema_field>").append(schemaField)
				.append("</xdat:schema_field>\n");
			sb.append("			<xdat:comparison_type>").append(comparator)
				.append("</xdat:comparison_type>\n");
			if (comparator.equals(Like))
			{
				sb.append("			<xdat:value>%").append(value).append("%</xdat:value>\n");
			}
			else
			{
				sb.append("			<xdat:value>").append(value).append("</xdat:value>\n");
			}
			sb.append("		</xdat:criteria>\n");
			return sb;
		}

		/**
		 *
		 * @param sb
		 * @param name
		 * @return
		 */
		public StringBuilder addRootElement(StringBuilder sb, String name)
		{
			sb.append("	<xdat:root_element_name>").append(name)
				.append("</xdat:root_element_name>\n");
			return sb;
		}

		/**
		 *
		 * @param sb
		 * @param name
		 * @param fieldId
		 * @param seq
		 * @param type
		 * @param header
		 * @return
		 */
		public StringBuilder addSearchField(StringBuilder sb, String name,
			String fieldId, int seq, String type, String header)
		{
			sb.append("	<xdat:search_field>\n");
			sb.append("		<xdat:element_name>").append(name)
				.append("</xdat:element_name>\n");
			sb.append("		<xdat:field_ID>").append(fieldId).append("</xdat:field_ID>\n");
			sb.append("		<xdat:sequence>").append(seq).append("</xdat:sequence>\n");
			sb.append("		<xdat:type>").append(type).append("</xdat:type>\n");
			sb.append("		<xdat:header>").append(header).append("</xdat:header>\n");
			sb.append("	</xdat:search_field>\n");
			return sb;
		}

		public StringBuilder createInitialXml(String desc)
		{
			StringBuilder sb = new StringBuilder(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			sb.append("<xdat:search ID=\"\"")
				.append(" allow-diff-columns=\"0\"")
				.append(" secure=\"false\"")
				.append(" brief-description=\"").append(desc).append("\"")
				.append(" xmlns:xdat=\"http://nrg.wustl.edu/security\"")
				.append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
			return sb;
		}

		/**
		 *
		 * @param rs
		 * @return
		 */
		public XnatId xnatIdFromResultSet(XnatResultSet rs)
		{
			return xnatIdFromResultSet(rs, 0);
		}

		/**
		 *
		 * @param rs
		 * @param idx
		 * @return
		 */
		public XnatId xnatIdFromResultSet(XnatResultSet rs, int idx)
		{
			int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
			int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
			int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
			return new XnatId(rs.get(idx, projIdx), rs.get(idx, subjIdIdx),
				rs.get(idx, sessionIdx));
		}

	}
}
