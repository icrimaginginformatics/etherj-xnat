/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.User;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.xnat.metadata.IacRoiCollectionDataMCT;
import icr.etherj.xnat.metadata.MetadataComplexType;
import icr.etherj.xnat.schema.Provenance;
import icr.etherj.xnat.schema.Provenance.ProcessStep.Library;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class IacUploadHelper
	implements XnatUploader.Helper<ImageAnnotationCollection>
{

	@Override
	public List<MetadataComplexType> createAuxiliaryMctList(
		UploadMetadata<ImageAnnotationCollection> metadata)
	{
		List<MetadataComplexType> mctList = new ArrayList<>();
		return mctList;
	}

	@Override
	public MetadataComplexType createComplexType(
		UploadMetadata<ImageAnnotationCollection> metadata)
	{
		return new IacRoiCollectionDataMCT(metadata);
	}

	@Override
	public boolean exists(UploadMetadata<ImageAnnotationCollection> metadata,
		XnatDataSource dataSource) throws XnatException
	{
		StudyUidContainer container = metadata.getPreferredContainer();
		XnatToolkit toolkit = XnatToolkit.getToolkit();
		SearchSpecification spec = toolkit.createSearchSpecification();
		SearchCriterion projCrit = toolkit.createSearchCriterion(XnatTag.ProjectID,
			SearchCriterion.Equal, container.getProjectId());
		spec.addCriterion(projCrit);
		SearchCriterion subjCrit = toolkit.createSearchCriterion(XnatTag.SubjectID,
			SearchCriterion.Equal, container.getSubjectId());
		spec.addCriterion(subjCrit);
		SearchCriterion sessionCrit = toolkit.createSearchCriterion(XnatTag.SessionID,
			SearchCriterion.Equal, container.getSessionId());
		spec.addCriterion(sessionCrit);
		SearchCriterion labelCrit = toolkit.createSearchCriterion(XnatTag.Label,
			SearchCriterion.Equal, metadata.getLabel());
		spec.addCriterion(labelCrit);

		return dataSource.exists(ImageAnnotationCollection.class, spec);
	}

	@Override
	public boolean existsInProject(
		UploadMetadata<ImageAnnotationCollection> metadata,
		XnatDataSource dataSource) throws XnatException
	{
		StudyUidContainer container = metadata.getPreferredContainer();
		XnatToolkit toolkit = XnatToolkit.getToolkit();
		SearchSpecification spec = toolkit.createSearchSpecification();
		SearchCriterion projCrit = toolkit.createSearchCriterion(XnatTag.ProjectID,
			SearchCriterion.Equal, container.getProjectId());
		spec.addCriterion(projCrit);
		SearchCriterion labelCrit = toolkit.createSearchCriterion(XnatTag.Label,
			SearchCriterion.Equal, metadata.getLabel());
		spec.addCriterion(labelCrit);

		return dataSource.exists(ImageAnnotationCollection.class, spec);
	}

	@Override
	public boolean populate(UploadMetadata<ImageAnnotationCollection> metadata)
	{
//		if (!(metadata instanceof IacUploadMetadata))
//		{
//			throw new IllegalArgumentException(
//				"Supplied UploadMetadata not an IacUploaMetadata");
//		}
//		createProvenance(metadata);
		return true;
	}
	
	private Provenance.ProcessStep createAimProcessStep(
		IacUploadMetadata metadata)
	{
		ImageAnnotationCollection iac = metadata.getUploadItem();
		String aimTimeStamp = "1900-01-01T00:00:00";//iac.getDateTime();
		String aimCvs = Provenance.UNKNOWN;
		User user = iac.getUser();
		String aimUser = new StringBuilder()
			.append("Name: ")
			.append((user.getName() != null)
				? user.getName()
				: Provenance.UNKNOWN)
			.append(Provenance.SEPARATOR)
			.append("Login Name: ")
			.append((user.getLoginName() != null)
				? user.getLoginName()
				: Provenance.UNKNOWN)
			.append(Provenance.SEPARATOR)
			.append("Role In Trial: ")
			.append((user.getRoleInTrial()!= null)
				? user.getRoleInTrial()
				: Provenance.UNKNOWN)
			.append(Provenance.SEPARATOR)
			.append("Number In Role: ")
			.append((user.getNumberWithinRoleOfClinicalTrial() >= 0)
				? user.getNumberWithinRoleOfClinicalTrial()
				: Provenance.UNKNOWN)
			.toString();
		String aimMachine = Provenance.UNKNOWN;
		Provenance.ProcessStep aimSourceStep = new Provenance.ProcessStep(
			new Provenance.ProcessStep.Program("Some AIM software", iac.getAimVersion(), Provenance.UNKNOWN),
			aimTimeStamp, aimCvs, aimUser, aimMachine,
			new Provenance.ProcessStep.Platform(),
			new Provenance.ProcessStep.Compiler(),
			new ArrayList<Library>());
		
		return aimSourceStep;
	}

	private Provenance.ProcessStep createDicomProcessStep(
		IacUploadMetadata metadata)
	{
		String version = Provenance.UNKNOWN;
		String timeStamp = "1900-01-01T00:00:00";
		String cvs = Provenance.UNKNOWN;
		String user = Provenance.UNKNOWN;
		String machine = Provenance.UNKNOWN;
		Provenance.ProcessStep step = new Provenance.ProcessStep(
			new Provenance.ProcessStep.Program("Some DICOM software", version,
				Provenance.UNKNOWN),
			timeStamp, cvs, user, machine,
			new Provenance.ProcessStep.Platform(),
			new Provenance.ProcessStep.Compiler(),
			new ArrayList<Library>());
		
		return step;
	}

	private Provenance.ProcessStep createProcessStep(IacUploadMetadata metadata)
	{
		String timeStamp = "1900-01-01T00:00:00";
		String cvs = Provenance.UNKNOWN;
		String user = Provenance.UNKNOWN;
		String machine = Provenance.UNKNOWN;
		Provenance.ProcessStep step = new Provenance.ProcessStep(
			new Provenance.ProcessStep.Program(getClass().getSimpleName(),
				"-current", Provenance.UNKNOWN),
			timeStamp, cvs, user, machine,
			new Provenance.ProcessStep.Platform(),
			new Provenance.ProcessStep.Compiler(),
			new ArrayList<Library>());
		
		return step;
	}

	private void createProvenance(UploadMetadata<ImageAnnotationCollection> metadata)
	{
		Provenance provenance = new Provenance();
		// Provenance applies to the AIM file itself or also to data it references?
		// i.e. is DICOM step really necessary?
//		provenance.addProcessStep(createDicomProcessStep(metadata));
//		provenance.addProcessStep(createAimProcessStep(metadata));
//		provenance.addProcessStep(createProcessStep(metadata));
		metadata.setProvenance(provenance);
	}

}
