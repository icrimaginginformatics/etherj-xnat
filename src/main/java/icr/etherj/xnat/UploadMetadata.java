/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import com.google.common.collect.SetMultimap;
import icr.etherj.xnat.schema.Provenance;
import icr.etherj.xnat.schema.Resource;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jamesd
 * @param <T>
 */
public interface UploadMetadata<T>
{

	/**
	 *
	 * @param uid
	 * @return
	 */
	boolean addSeriesUid(String uid);

	/**
	 *
	 * @param uid
	 * @return
	 */
	boolean addSopInstanceUid(String uid);

	/**
	 *
	 * @param uid
	 * @return
	 */
	boolean addStudyUid(String uid);

	/**
	 * @return the accessionId
	 */
	String getAccessionId();

	/**
	 *
	 * @return
	 */
	List<XnatUploadableResource> getAuxiliaryResources();

	Set<Class<?>> getAuxClasses();
	List<AuxMetadata> getAuxMetadata(Class<?> klass);

	/**
	 *
	 * @return
	 */
	int getCopyNumber();

	/**
	 *
	 * @param container
	 * @return
	 */
	XnatDependencyChecker.DependencyMaps getDependencyMaps(
		StudyUidContainer container);

	/**
	 *
	 * @return
	 */
	File getFile();

	/**
	 *
	 * @return
	 */
	List<Resource> getInList();

	/**
	 *
	 * @return
	 */
	String getLabel();

	/**
	 *
	 * @return
	 */
	String getName();

	/**
	 *
	 * @return
	 */
	List<Resource> getOutList();

	/**
	 *
	 * @return
	 */
	UploadMetadata getParent();

	/**
	 * @return the prefContainer
	 */
	StudyUidContainer getPreferredContainer();

	/**
	 *
	 * @return
	 */
	XnatUploadableResource getPrimaryResource();

	/**
	 * @return the provenance
	 */
	Provenance getProvenance();

	/**
	 *
	 * @return
	 */
	String getRootServerPath();

	/**
	 * @return the seriesUids
	 */
	Set<String> getSeriesUids();

	/**
	 * @return the sopInstUids
	 */
	Set<String> getSopInstanceUids();

	/**
	 * @return the studyUidContainers
	 */
	SetMultimap<String, StudyUidContainer> getStudyUidContainers();

	/**
	 * @return the studyUids
	 */
	Set<String> getStudyUids();

	/**
	 *
	 * @return
	 */
	String getType();

	/**
	 *
	 * @return
	 */
	String getTypePath();

	/**
	 *
	 * @return
	 */
	String getUid();

	/**
	 *
	 * @return
	 */
	T getUploadItem();

	/**
	 * @return the version
	 */
	String getVersion();

	/**
	 *
	 * @return
	 */
	int incrementCopyNumber();

	/**
	 *
	 * @param container
	 * @param dependencyMaps
	 */
	void setDependencyMaps(StudyUidContainer container,
		XnatDependencyChecker.DependencyMaps dependencyMaps);

	/**
	 *
	 * @param label
	 */
	void setLabel(String label);

	/**
	 * @param container the preferred container to set
	 */
	void setPreferredContainer(StudyUidContainer container) throws IllegalArgumentException;

	/**
	 * @param provenance the provenance to set
	 */
	void setProvenance(Provenance provenance);

	/**
	 * @param studyUidContainers the studyUidContainers to set
	 */
	void setStudyUidContainers(SetMultimap<String, StudyUidContainer> studyUidContainers);

	/**
	 * @param version the version to set
	 */
	void setVersion(String version);
	
}
