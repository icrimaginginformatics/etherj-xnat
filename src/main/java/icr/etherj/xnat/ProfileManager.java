/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import com.google.common.collect.ImmutableSortedMap;
import icr.etherj.Xml;
import icr.etherj.crypto.CryptoException;
import icr.etherj.crypto.CryptoToolkit;
import icr.etherj.crypto.KeyStore;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.locks.ReentrantLock;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 *
 * @author jamesd
 */
public class ProfileManager
{
	public static final int OK = 0;
	public static final int NoDirectory = 1;
	public static final int NoStore = 2;
	public static final int StoreExists = 3;

	private static final Logger logger =
		LoggerFactory.getLogger(ProfileManager.class);

	private static final String FILE_KEYSTORE = "xnatskeystore.eks";
	private static final String FILE_PROFILES = "xnatprofiles.xml";
	private static final String ATTR_CURRENT = "current";
	private static final String ATTR_INSTANCE = "instance";
	private static final String ATTR_NAME = "name";
	private static final String ATTR_PORT = "port";
	private static final String ATTR_SERVER = "server";
	private static final String ATTR_SSL = "ssl";
	private static final String ATTR_USERNAME = "username";
	private static final String NODE_PROFILE = "profile";
	private static final String NODE_ROOT = "etherj_xnat_profiles";
	private static final String NODE_TEXT = "#text";

	private String currentProfileName = "";
	private KeyStore keyStore;
	private SortedMap<String,Profile> profiles = new TreeMap<>();
	private final ReentrantLock lock = new ReentrantLock();

	/**
	 *
	 * @throws CryptoException
	 */
	public ProfileManager() throws CryptoException
	{
		keyStore = CryptoToolkit.getToolkit().createKeyStore();
	}

	/**
	 *
	 * @param keyStore
	 */
	public ProfileManager(KeyStore keyStore)
	{
		this.keyStore = keyStore;
	}

	/**
	 *
	 * @param profile
	 * @return
	 */
	public Profile addProfile(Profile profile)
	{
		Profile added;
		try
		{
			lock.lock();
			added = profiles.put(profile.getProfileName(), profile);
			if (profiles.size() == 1)
			{
				currentProfileName = profile.getProfileName();
			}
		}
		finally
		{
			lock.unlock();
		}
		return added;
	}

	/**
	 *
	 */
	public void clear()
	{
		try
		{
			lock.lock();
			profiles.clear();
			currentProfileName = "";
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public boolean containsProfile(String name)
	{
		return profiles.containsKey(name);
	}

	/**
	 *
	 * @param path
	 * @param password
	 * @return
	 * @throws IOException
	 */
	public int createStore(File path, String password) throws IOException
	{
		return createStore(path, password, false);
	}

	/**
	 *
	 * @param path
	 * @param password
	 * @param force
	 * @return
	 * @throws IOException
	 */
	public int createStore(File path, String password, boolean force)
		throws IOException
	{
		try
		{
			lock.lock();
			if (!path.isDirectory())
			{
				if (!path.mkdirs())
				{
					logger.warn("Could not create store directory: "+path.getPath());
					return NoDirectory;
				}
			}
			File ksFile = new File(path, FILE_KEYSTORE);
			if (ksFile.isFile() && !force)
			{
				logger.info("Store exists. Not overwriting");
				return StoreExists;
			}
			XmlStorage storage = new XmlStorage();
			storage.store(path, password);
			logger.info("Created keystore: "+ksFile.getPath());
			return OK;
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * @return the current Profile
	 */
	public Profile getCurrentProfile()
	{
		return getProfile(currentProfileName);
	}

	/**
	 * @return the currentProfileName
	 */
	public String getCurrentProfileName()
	{
		return currentProfileName;
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Profile getProfile(String name)
	{
		return profiles.get(name);
	}

	/**
	 *
	 * @return
	 */
	public int getProfileCount()
	{
		return profiles.size();
	}

	/**
	 *
	 * @return
	 */
	public Set<String> getProfileNames()
	{
		return profiles.keySet();
	}

	/**
	 *
	 * @return
	 */
	public SortedMap<String,Profile> getProfiles()
	{
		return ImmutableSortedMap.copyOf(profiles);
	}

	/**
	 *
	 * @param path
	 * @param password
	 * @throws FileNotFoundException
	 * @throws IOException if an I/O error occurs
	 * @throws icr.etherj.crypto.CryptoException
	 */
	public void load(File path, String password)
		throws FileNotFoundException, IOException, CryptoException
	{
		try
		{
			lock.lock();
			XmlStorage storage = new XmlStorage();
			storage.load(path, password);
			keyStore = storage.getKeyStore();
			currentProfileName = storage.getCurrentProfileName();
			profiles = storage.getProfiles();
			Set<String> names = profiles.keySet();
			Set<String> noPassword = new TreeSet<>();
			for (String name : names)
			{
				Profile profile = profiles.get(name);
				String key = keyStore.getKey(name);
				if (key == null)
				{
					noPassword.add(name);
					continue;
				}
				profile.setPassword(key);
			}
			for (String name : noPassword)
			{
				profiles.remove(name);
			}
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 *
	 * @param profile
	 * @return
	 */
	public Profile removeProfile(Profile profile)
	{
		return removeProfile(profile.getProfileName());
	}

	/**
	 *
	 * @param profileName
	 * @return
	 */
	public Profile removeProfile(String profileName)
	{
		Profile removed;
		try
		{
			lock.lock();
			removed = profiles.remove(profileName);
			if (profiles.isEmpty())
			{
				currentProfileName = "";
			}
		}
		finally
		{
			lock.unlock();
		}
		return removed;
	}

	/**
	 * @param profileName the currentProfileName to set
	 * @return 
	 */
	public boolean setCurrentProfileName(String profileName)
	{
		boolean value;
		try
		{
			lock.lock();
			if (profileName == null)
			{
				throw new IllegalArgumentException("Profile name must not be null");
			}
			value = profiles.containsKey(profileName);
			if (value)
			{
				this.currentProfileName = profileName;
			}
		}
		finally
		{
			lock.unlock();
		}
		return value;
	}

	/**
	 *
	 * @param profiles
	 */
	public void setProfiles(Map<String,Profile> profiles)
	{
		try
		{
			lock.lock();
			this.profiles.clear();
			this.profiles.putAll(profiles);
			currentProfileName = (profiles.size() == 1)
				? this.profiles.firstKey()
				: "";
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 *
	 * @param path
	 * @param password
	 * @throws IOException if an I/O error occurs
	 */
	public void store(File path, String password) throws IOException
	{
		try
		{
			lock.lock();
			keyStore.clear();
			for (String key : profiles.keySet())
			{
				keyStore.setKey(key, profiles.get(key).getPassword());
			}
			XmlStorage storage = new XmlStorage();
			storage.store(path, password);
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 *
	 * @param path
	 * @return
	 */
	public int storeExists(File path)
	{
		if (!path.isDirectory() || !path.canRead())
		{
			logger.warn("Path must be a readable directory: "+path.getPath());
			return NoDirectory;
		}
		File ksFile = new File(path, FILE_KEYSTORE);
		if (!ksFile.isFile() || !ksFile.canRead())
		{
			return NoStore;
		}
		return OK;
	}

	private class XmlStorage
	{
		private String innerCurrentName = "";
		private KeyStore innerKeyStore;
		private SortedMap<String,Profile> innerProfiles = new TreeMap<>();

		/**
		 * @return the currentProfileName
		 */
		public String getCurrentProfileName()
		{
			return innerCurrentName;
		}

		/**
		 * @return the keyStore
		 */
		public KeyStore getKeyStore()
		{
			return innerKeyStore;
		}

		/**
		 * @return the innerProfiles
		 */
		public SortedMap<String,Profile> getProfiles()
		{
			return innerProfiles;
		}

		public void load(File path, String password)
			throws FileNotFoundException, IOException, CryptoException
		{
			try
			{
				lock.lock();
				if (!path.isDirectory() || !path.canRead())
				{
					throw new IOException("Path must be a readable directory: "+
						path.getPath());
				}
				KeyStore newKs = loadKeyStore(path, password);
				SortedMap<String,Profile> newProfiles = loadProfiles(path);
				innerKeyStore = newKs;
				innerProfiles = newProfiles;
			}
			finally
			{
				lock.unlock();
			}
		}


		public void store(File path, String password) throws IOException
		{
			try
			{
				lock.lock();
				if (!path.isDirectory() || !path.canWrite())
				{
					throw new IOException("Path must be a writable directory: "+
						path.getPath());
				}
				storeKeyStore(path, password);
				storeProfiles(path);
			}
			finally
			{
				lock.unlock();
			}
		}

		private void appendProfile(Document doc, Element parent, Profile profile)
		{
			Element profileElem = doc.createElement(NODE_PROFILE);
			profileElem.setAttribute(ATTR_NAME, profile.getProfileName());
			profileElem.setAttribute(ATTR_SERVER, profile.getServer());
			profileElem.setAttribute(ATTR_PORT, String.valueOf(profile.getPort()));
			profileElem.setAttribute(ATTR_INSTANCE, profile.getInstance());
			profileElem.setAttribute(ATTR_SSL, String.valueOf(profile.isUsingSsl()));
			profileElem.setAttribute(ATTR_USERNAME, profile.getUserName());
			parent.appendChild(profileElem);
		}

		private KeyStore loadKeyStore(File dir, String password)
			throws FileNotFoundException, IOException, CryptoException
		{
			File ksFile = new File(dir, FILE_KEYSTORE);
			KeyStore ks;
			try (FileInputStream fis = new FileInputStream(ksFile))
			{
				ks = CryptoToolkit.getToolkit().createKeyStore();
				ks.load(fis, password);
			}
			return ks;
		}

		private SortedMap<String,Profile> loadProfiles(File dir)
			throws FileNotFoundException, IOException
		{
			File file = new File(dir, FILE_PROFILES);
			SortedMap<String,Profile> loadedProfiles = new TreeMap<>();
			String currName = "";
			try
			{
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(new FileInputStream(file));
				Element rootNode = doc.getDocumentElement();
				if (!rootNode.getNodeName().equals(NODE_ROOT))
				{
					throw new IllegalArgumentException(
						"Incorrect doc type: "+rootNode.getNodeName());
				}
				rootNode.normalize();
				NamedNodeMap attrs = rootNode.getAttributes();
				currName = Xml.getAttrStr(attrs, ATTR_CURRENT, "");
				NodeList childNodes = rootNode.getChildNodes();
				for (int i=0; i<childNodes.getLength(); i++)
				{
					Node node = childNodes.item(i);
					switch (node.getNodeName())
					{
						case NODE_TEXT:
							continue;

						case NODE_PROFILE:
							parseProfile(node, loadedProfiles);
							break;

						default:
					}
				}
			}
			catch (ParserConfigurationException | SAXException ex)
			{
				throw new IOException(ex);
			}
			if (currName.isEmpty() && (loadedProfiles.size() > 0))
			{
				currName = loadedProfiles.get(
					loadedProfiles.firstKey()).getProfileName();
			}
			innerCurrentName = currName;
			return loadedProfiles;
		}

		private void parseProfile(Node profileNode,
			SortedMap<String, Profile> loadedProfiles)
		{
			NamedNodeMap attrs = profileNode.getAttributes();
			Profile profile = new Profile(Xml.getAttrStr(attrs, ATTR_NAME, ""));
			profile.setServer(Xml.getAttrStr(attrs, ATTR_SERVER, ""));
			profile.setPort(Xml.getAttrInt(attrs, ATTR_PORT, 80));
			profile.setInstance(Xml.getAttrStr(attrs, ATTR_INSTANCE, ""));
			profile.setUsingSsl(Xml.getAttrBoolean(attrs, ATTR_SSL, false));
			profile.setUserName(Xml.getAttrStr(attrs, ATTR_USERNAME, ""));
			if (!profile.getProfileName().isEmpty() ||
				 !profile.getServer().isEmpty() ||
				 !profile.getInstance().isEmpty() ||
				 !profile.getUserName().isEmpty())
			{
				loadedProfiles.put(profile.getProfileName(), profile);
			}
			else
			{
				logger.warn("Ignoring invalid profile");
			}
		}

		private void storeKeyStore(File dir, String password) throws IOException
		{
			File ksFile = new File(dir, FILE_KEYSTORE);
			try (FileOutputStream fos = new FileOutputStream(ksFile))
			{
				keyStore.store(fos, password);
			}
			catch (CryptoException ex)
			{
				throw new IOException("Error storing keystore", ex);
			}
		}

		private void storeProfiles(File dir) throws IOException
		{
			Document doc = null;
			try
			{
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.newDocument();
				Element root = doc.createElement(NODE_ROOT);
				root.setAttribute(ATTR_CURRENT, currentProfileName);
				doc.appendChild(root);
				for (Entry<String,Profile> entry : profiles.entrySet())
				{
					appendProfile(doc, root, entry.getValue());
				}

				File file = new File(dir, FILE_PROFILES);
				TransformerFactory transformerFactory =
					TransformerFactory.newInstance();
				try
				{
					transformerFactory.setAttribute("indent-number", 3);
				}
				catch (IllegalArgumentException ex)
				{
					// MATLAB uses Saxon 6.5 instead of the standard JDK Xalan
					logger.warn("Attribute 'indent-number' not supported by TransformerFactory: "+
						transformerFactory.getClass().getCanonicalName());
				}
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(file);
				transformer.transform(source, result);
			}
			catch (ParserConfigurationException | TransformerException ex)
			{
				throw new IOException("Cannot create Document", ex);
			}
		}

	}

}
