/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.app;

import icr.etherj.concurrent.CallbackTaskResult;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import javax.swing.SwingWorker;

/**
 *
 * @author jamesd
 * @param <T>
 * @param <R>
 */
public class FunctionConsumerCallbackTask<T,R> extends SwingWorker<R,Void>
{
	private final Consumer<CallbackTaskResult<R>> consumer;
	private final ThrowingFunction<T,R> function;
	private final T t;

	/**
	 *
	 * @param function
	 * @param consumer
	 * @param t
	 */
	public FunctionConsumerCallbackTask(ThrowingFunction<T,R> function,
		Consumer<CallbackTaskResult<R>> consumer, T t)
	{
		super();
		this.function = function;
		this.consumer = consumer;
		this.t = t;
	}

	@Override
	protected R doInBackground() throws Exception
	{
		try
		{
			return function.apply(t);
		}
		catch (RuntimeException ex)
		{
			Throwable cause = ex.getCause();
			if (cause != null)
			{
				throw new Exception(ex);
			}
		}
		return null;
	}
	
	@Override
	protected void done()
	{
		CallbackTaskResult<R> result;
		try
		{
			R r = get();
			result = new CallbackTaskResult<>(r);
		}
		catch (InterruptedException | ExecutionException ex)
		{
			result = new CallbackTaskResult<>(ex);
		}
		consumer.accept(result);
	}

}
