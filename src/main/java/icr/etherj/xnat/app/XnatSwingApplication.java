/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.app;

import icr.etherj.ExceptionCode;
import icr.etherj.concurrent.CallbackTaskResult;
import icr.etherj.crypto.CryptoCode;
import icr.etherj.crypto.CryptoException;
import icr.etherj.ui.DropDownButton;
import icr.etherj.ui.PasswordDialog;
import icr.etherj.xnat.Profile;
import icr.etherj.xnat.ProfileManager;
import icr.etherj.xnat.XnatCode;
import icr.etherj.xnat.XnatDataSource;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatResultSet;
import icr.etherj.xnat.XnatServerConnection;
import icr.etherj.xnat.XnatToolkit;
import icr.etherj.xnat.ui.ProfileManagerDialog;
import icr.etherj.xnat.ui.ProjectSelector;
import icr.etherj.xnat.ui.XnatUiToolkit;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class XnatSwingApplication
{
	private static final Logger logger = LoggerFactory.getLogger(
		XnatSwingApplication.class);

	private static final int PASSWORD_OK = 0;
	private static final int PASSWORD_CANCEL = 1;
	private static final int PASSWORD_ERROR = 2;

	private final ApplicationState appState = new ApplicationState();
	private JMenuItem changePassItem;
	private JFrame frame;
	private boolean ignoreProjectUpdate = false;
	private ExecutorService netIoExec = Executors.newSingleThreadExecutor();
	private String password;
	private JMenu passwordMenu;
	private final String productName;
	private DropDownButton profileButton;
	private JMenuItem profileItem;
	private final ProfileManager profileManager;
	private JPopupMenu profilePopupMenu;
	private ProjectSelector projectSelector;
	private final SortedMap<String,XnatProject> projects = new TreeMap<>();
	private XnatDataSource xds;
	private XnatServerConnection xsc;
	private XnatToolkit xnatToolkit = XnatToolkit.getToolkit();
	private XnatUiToolkit xnatUiToolkit = XnatUiToolkit.getToolkit();
	
	/**
	 *
	 * @param productName
	 * @throws CryptoException
	 * @throws IllegalArgumentException
	 */
	public XnatSwingApplication(String productName)
		throws CryptoException, IllegalArgumentException
	{
		if ((productName == null) || productName.isEmpty())
		{
			throw new IllegalArgumentException("Product name must not be null or empty");
		}
		if (productName.contains(" "))
		{
			throw new IllegalArgumentException("Product name must not contain a space");
		}
		this.productName = productName;
		profileManager = new ProfileManager();
	}

	/**
	 *
	 * @return
	 */
	public String getAppPath()
	{
		String os = System.getProperty("os.name");
		StringBuilder sb = new StringBuilder(System.getProperty("user.home"))
			.append(File.separator);
		if (os.startsWith("Windows"))
		{
			sb.append("AppData")
				.append(File.separator).append("Local")
				.append(File.separator).append(productName)
				.append(File.separator);
		}
		else
		{
			sb.append(".").append(productName.toLowerCase())
				.append(File.separator);
		}
		return sb.toString();
	}

	/**
	 *
	 * @return
	 */
	public JFrame getFrame()
	{
		return frame;
	}

	/**
	 * @return the netIoExec
	 */
	public ExecutorService getNetworkExecutorService()
	{
		return netIoExec;
	}

	/**
	 *
	 * @return
	 */
	public final String getProductName()
	{
		return productName;
	}

	/**
	 * @return the profileManager
	 */
	public ProfileManager getProfileManager()
	{
		return profileManager;
	}

	/**
	 *
	 * @return
	 */
	public ProjectSelector getProjectSelector()
	{
		return projectSelector;
	}

	/**
	 * @return the xnatToolkit
	 */
	public XnatToolkit getXnatToolkit()
	{
		return xnatToolkit;
	}

	/**
	 * @return the xnatUiToolkit
	 */
	public XnatUiToolkit getXnatUiToolkit()
	{
		return xnatUiToolkit;
	}

	/**
	 *
	 * @param event
	 */
	public void onProjectSelectionChanged(PropertyChangeEvent event)
	{}

	/**
	 *
	 * @throws IOException
	 */
	public void onReadConfig() throws IOException
	{}

	/**
	 *
	 */
	public void onShowUi()
	{}

	/**
	 *
	 */
	public void onStartupComplete()
	{}

	/**
	 *
	 * @param event
	 */
	public void onStateChanged(PropertyChangeEvent event)
	{}

	/**
	 *
	 */
	public void onUiCreation()
	{}

	/**
	 *
	 * @param event
	 */
	public void onWindowClosing(WindowEvent event)
	{
	}

	/**
	 *
	 */
	public void onWriteConfig()
	{}

	/**
	 * @param netIoExec the netIoExec to set
	 */
	public void setNetworkExecutorService(ExecutorService netIoExec)
	{
		this.netIoExec = netIoExec;
	}

	/**
	 * @param xnatToolkit the xnatToolkit to set
	 */
	public void setXnatToolkit(XnatToolkit xnatToolkit)
	{
		this.xnatToolkit = xnatToolkit;
	}

	/**
	 * @param xnatUiToolkit the xnatUiToolkit to set
	 */
	public void setXnatUiToolkit(XnatUiToolkit xnatUiToolkit)
	{
		this.xnatUiToolkit = xnatUiToolkit;
	}

	/**
	 *
	 * @throws CryptoException
	 * @throws IOException
	 */
	public final void startup() throws CryptoException, IOException
	{
		logger.info(productName+" starting up");
		appState.addPropertyChangeListener(this::stateChanged);
		logger.info("Reading configuration");
		onReadConfig();
		createAndShowUi();
		appState.changeState(ApplicationState.NoKeystore);
		switch (readProfiles())
		{
			case PASSWORD_ERROR:
				System.exit(0);
			case PASSWORD_CANCEL:
				return;
			default:
		}
		updateProfilePopupMenu();
		setProfile(profileManager.getCurrentProfileName());
		onStartupComplete();
	}

	/* **************************************************************************
	 *	Protected methods
	 ***************************************************************************/

	/**
	 *
	 * @return
	 */
	protected XnatServerConnection getConnection()
	{
		return xsc;
	}

	/**
	 *
	 * @param ex
	 */
	protected void handleNetIoException(Exception ex)
	{
		if (ex instanceof InterruptedException)
		{
			logger.info("Connection cancelled");
			return;
		}
		// All the following are error conditions, null out the failed connection
		disconnect();
		Throwable cause = ex.getCause();
		// If it isn't an XnatException something worse has happened
		if (!(cause instanceof XnatException))
		{
			logger.error("Error connecting to server", cause);
			JOptionPane.showMessageDialog(frame,
				"Error connecting to server: "+cause.getMessage(),
				productName+" - Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		XnatException exXnat = (XnatException) cause;
		ExceptionCode code = exXnat.getCode();
		Profile profile = profileManager.getCurrentProfile();
		if (code.equals(XnatCode.UnknownHost))
		{
			logger.error("Unknown host", exXnat);
			JOptionPane.showMessageDialog(frame,
				"Unknown host: "+profile.getServer(),
				productName+" - Error", JOptionPane.ERROR_MESSAGE);
		}
		else if (code.equals(XnatCode.Connect))
		{
			logger.error("Error connecting to server", exXnat);
			JOptionPane.showMessageDialog(frame,
				"Connection refused: "+profile.getServerUrl(),
				productName+" - Error", JOptionPane.ERROR_MESSAGE);
		}
		else if (code.equals(XnatCode.HttpUnauthorised))
		{
			logger.error("Login credentials invalid", exXnat);
			JOptionPane.showMessageDialog(frame,
				"Login credentials invalid for\n\n"+profile.getServerUrl(),
				productName+" - Error", JOptionPane.ERROR_MESSAGE);
		}
		else if (code.equals(XnatCode.HttpForbidden))
		{
			logger.error("HTTP Forbidden", exXnat);
			JOptionPane.showMessageDialog(frame,
				"HTTP Forbidden for\n\n"+profile.getServerUrl()+
					"\n\nPassword expired?",
				productName+" - Error", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			logger.error("Error creating server connection", exXnat);
			JOptionPane.showMessageDialog(frame,
				"Error creating server connection: "+exXnat.getMessage()+
					"\n\tState: "+exXnat.getCode(),
				productName+" - Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 *
	 * @param menu
	 */
	protected void insertProfileItems(JMenu menu)
	{
		menu.add(profileItem);
		menu.add(passwordMenu);
	}

	/**
	 *
	 * @param toolbar
	 */
	protected void insertProfileItems(JToolBar toolbar)
	{
		toolbar.add(profileButton);
		toolbar.add(projectSelector);
	}

	/* **************************************************************************
	 *	Private methods
	 ***************************************************************************/

	private void changePassword()
	{
		String[] oldAndNew = PasswordDialog.showChangePasswordDialog(frame);
		if ((oldAndNew == null) || (oldAndNew.length != 2))
		{
			return;
		}
		// Verify old password
		if (!validatePassword(oldAndNew[0]))
		{
			JOptionPane.showMessageDialog(frame, 
				"Invalid password", productName, JOptionPane.ERROR_MESSAGE);
			SwingUtilities.invokeLater(() -> changePassword());
			return;
		}
		// Update on-disk storage to use new password
		password = oldAndNew[1];
		storeProfiles(password);
		logger.info("Keystore password changed");
	}

	private boolean connectToServer() throws XnatException
	{
		Profile profile = profileManager.getCurrentProfile();
		xsc = xnatToolkit.createServerConnection(profile.getServerUrl(),
			profile.getUserName(), profile.getPassword());
		xsc.open();
		String version = xsc.getServerVersion();
		logger.info("Connected to "+xsc.getServerUrl().toString()+
			" running XNAT "+version);
		xds = xnatToolkit.createDataSource(xsc);
		return true;
	}

	private void connectToServerDone(CallbackTaskResult<Boolean> result)
	{
		frame.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		// Successful profile connection/change
		storeProfiles(password);
		// Pull the list of projects
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		SupplierConsumerCallbackTask<XnatResultSet> task =
			new SupplierConsumerCallbackTask<>(
				this::fetchProjectList,
				this::fetchProjectListDone);
		netIoExec.submit(task);
	}

	private void createAndShowUi()
	{
		try
		{
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException | InstantiationException |
			IllegalAccessException | UnsupportedLookAndFeelException ex)
		{
			logger.warn("Cannot load System L&F", ex);
		}
		frame = new JFrame(productName);
		List<Image> imageList = new ArrayList<>();
		imageList.add(new ImageIcon(getClass().getResource(
			"/icr/etherj/xnat/resources/Xnat16.png")).getImage());
		imageList.add(new ImageIcon(getClass().getResource(
			"/icr/etherj/xnat/resources/Xnat24.png")).getImage());
		imageList.add(new ImageIcon(getClass().getResource(
			"/icr/etherj/xnat/resources/Xnat32.png")).getImage());
		frame.setIconImages(imageList);
		frame.setLayout(new BorderLayout());
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent we)
			{
				windowClosingActions(we);
			}
		});

		createMenuItems();
		createToolbarItems();

		// Create UI in overridable method
		logger.info("Creating UI");
		onUiCreation();

		frame.pack();
		frame.setVisible(true);

		// Any actions after showing the UI
		logger.info("Performing post-UI actions");
		onShowUi();
	}

	private void createMenuItems()
	{
		profileItem = new JMenuItem("Profile Manager...");
		profileItem.addActionListener((ActionEvent) -> profileManager());
		passwordMenu = new JMenu("Password");
		changePassItem = new JMenuItem("Change...");
		changePassItem.addActionListener((ActionEvent) -> changePassword());
		JMenuItem resetPassItem = new JMenuItem("Reset...");
		resetPassItem.addActionListener((ActionEvent) -> resetPassword());
		passwordMenu.add(changePassItem);
		passwordMenu.add(resetPassItem);
	}

	private void createToolbarItems()
	{
		ImageIcon icon = new ImageIcon(
			getClass().getResource("/icr/etherj/xnat/resources/Xnat24.png"));
		profilePopupMenu = new JPopupMenu();
		profileButton = new DropDownButton(icon, profilePopupMenu);
		profileButton.addActionListener((ActionEvent e) -> profileManager());

		projectSelector = new ProjectSelector();
		projectSelector.addPropertyChangeListener(ProjectSelector.SELECTION,
			this::projectSelectionChanged);
	}

	private void disconnect()
	{
		if ((xsc != null) && xsc.isOpen())
		{
			logger.info("Closing connection to "+xsc.getServerUrl().toString());
			xsc.close();
		}
		xsc = null;
		appState.changeState(ApplicationState.Disconnected);
	}

	private XnatResultSet fetchProjectList() throws XnatException
	{
		XnatResultSet xrs = xnatToolkit.createResultSet(
			xsc.getDocument("/data/archive/projects?format=xml"));
		return xrs;
	}

	private void fetchProjectListDone(CallbackTaskResult<XnatResultSet> result)
	{
		frame.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			appState.changeState(ApplicationState.Disconnected);
			handleNetIoException(result.getException());
			return;
		}
		appState.changeState(ApplicationState.Authenticated);
		projects.clear();
		XnatResultSet xrs = result.get();
		int idIdx = xrs.getColumnIndex("ID");
		int nameIdx = xrs.getColumnIndex("name");
		if ((idIdx < 0) || (nameIdx < 0))
		{
			logger.warn("Invalid result set: missing columns");
			projectSelector.setProjects(new ArrayList<>());
			return;
		}
		int nProjects = xrs.getRowCount();
		for (int i=0; i<nProjects; i++)
		{
			String id = xrs.get(i, idIdx);
			projects.put(id, new XnatProject(id, xrs.get(i, nameIdx)));
		}
		List<XnatProject> projectList = new ArrayList<>();
		projectList.addAll(projects.values());
		Collections.sort(projectList);
	
		ignoreProjectUpdate = true;
		projectSelector.setProjects(projectList);
		ignoreProjectUpdate = false;
		appState.changeState(ApplicationState.AuthWithProjects);
	}

	private void profileManager()
	{
		logger.info("Profile manager selected");
		ProfileManagerDialog profileDialog;
		try
		{
			profileDialog = new ProfileManagerDialog(frame, profileManager);
		}
		catch (CryptoException ex)
		{
			logger.warn("Cannot create Profile Manager", ex);
			JOptionPane.showMessageDialog(frame, "Cannot create Profile Manager",
				"XnatUi Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		profileDialog.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosed(WindowEvent we)
			{
				profileManagerClosed();
			}
		});
		profileDialog.pack();
		profileDialog.setLocationRelativeTo(frame);
		profileDialog.setVisible(true);
	}

	private void profileManagerClosed()
	{
		storeProfiles(password);
		updateProfilePopupMenu();
		setProfile(profileManager.getCurrentProfileName());
	}

	private void projectSelectionChanged(PropertyChangeEvent event)
	{
		logger.info("Project selection changed");
		String[] project = (String[]) event.getNewValue();
		if (!ignoreProjectUpdate && (project.length > 0))
		{
			onProjectSelectionChanged(event);
		}
	}

	private int readProfiles() throws IOException
	{
		String appPath = getAppPath();
		File path = new File(appPath);
		logger.info("Reading profiles");

		// Does a store exist? If not, ask for password and create an empty one.
		// Throws if store creation fails.
		int exists = profileManager.storeExists(path);
		if ((exists == ProfileManager.NoDirectory) ||
			 (exists == ProfileManager.NoStore))
		{
			logger.info("No keystore found");
			String result = PasswordDialog.showPasswordDialog(frame,
				"Creating new Keystore. New Password", productName);
			if (result != null)
			{
				appState.changeState(ApplicationState.NoKeystore);
				profileManager.createStore(path, result, true);
				password = result;
				appState.changeState(ApplicationState.Disconnected);
				return PASSWORD_OK;
			}
			else
			{
				appState.changeState(ApplicationState.NoKeystore);
				return PASSWORD_CANCEL;
			}
		}

		// Existing store, ask for password until user gets it right or gives up.
		boolean passOk = false;
		while (!passOk)
		{
			String result = PasswordDialog.showPasswordDialog(frame,
				"Keystore Password", productName);
			if (result == null)
			{
				appState.changeState(ApplicationState.NoKeystore);
				return PASSWORD_CANCEL;
			}
			try
			{
				readProfiles(result);
				password = result;
				passOk = true;
			}
			catch (CryptoException ex)
			{
				ExceptionCode code = ex.getCode();
				if (code == null)
				{
					showError(ex);
					return PASSWORD_ERROR;
				}
				if (code.equals(CryptoCode.BadPassword))
				{
					logger.info("Keystore password incorrect");
				}
				else if (code.equals(CryptoCode.FormatError))
				{
					logger.info("KeyStore format error, recreating", ex);
					storeProfiles(result);
					password = result;
					passOk = true;
				}
				else
				{
					showError(ex);
					return PASSWORD_ERROR;
				}
			}
		}
		return PASSWORD_OK;
	}

	private void readProfiles(String password) throws CryptoException
	{
		String path = getAppPath();
		try
		{
			profileManager.load(new File(path), password);
			logger.info("Profiles loaded from "+path);
			appState.changeState(ApplicationState.Disconnected);
		}
		catch (FileNotFoundException ex)
		{
			logger.info("No profiles found");
			storeProfiles(password);
		}
		catch (IOException ex)
		{
			logger.warn("Cannot read profiles", ex);
			JOptionPane.showMessageDialog(frame, 
				"Cannot read profiles\n\n"+ex.getMessage(),
				getClass().getName()+" Error",
				JOptionPane.ERROR_MESSAGE);
		}
	}

	private void resetPassword()
	{
		String newPassword = PasswordDialog.showResetPasswordDialog(frame);
		if (newPassword == null)
		{
			return;
		}
		disconnect();
		profileManager.clear();
		password = newPassword;
		storeProfiles(password);
		logger.info("Keystore password reset");
	}

	private void setProfile(String name)
	{
		logger.info("Using profile: "+name);
		profileManager.setCurrentProfileName(name);
		Profile profile = profileManager.getProfile(name);
		if (profile == null)
		{
			logger.warn("Profile not found: "+name);
			return;
		}
		disconnect();
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		SupplierConsumerCallbackTask<Boolean> task =
			new SupplierConsumerCallbackTask<>(
				this::connectToServer,
				this::connectToServerDone);
		netIoExec.submit(task);
	}

	private void showError(CryptoException ex)
	{
		JOptionPane.showMessageDialog(frame, "Unknown crypto error, see log",
			getProductName()+" - Fatal Error", JOptionPane.ERROR_MESSAGE);
	}

	private void stateChanged(PropertyChangeEvent evt)
	{
		String newState = (String) evt.getNewValue();
		switch (newState)
		{
			case ApplicationState.NoKeystore:
				profileItem.setEnabled(false);
				changePassItem.setEnabled(false);
				profileButton.setEnabled(false);
				projectSelector.setEnabled(false);
				break;

			case ApplicationState.Disconnected:
				profileItem.setEnabled(true);
				changePassItem.setEnabled(true);
				profileButton.setEnabled(true);
				projectSelector.clear();
				updateTitle();
				break;

			case ApplicationState.Authenticated:
				profileItem.setEnabled(true);
				changePassItem.setEnabled(true);
				profileButton.setEnabled(true);
				projectSelector.setEnabled(false);
				updateTitle();
				break;

			case ApplicationState.AuthWithProjects:
				profileItem.setEnabled(true);
				changePassItem.setEnabled(true);
				profileButton.setEnabled(true);
				projectSelector.setEnabled(true);
				break;

			default:
				throw new IllegalStateException("Illegal state: <"+newState+">");
		}
		onStateChanged(evt);
	}

	private void storeProfiles(String password)
	{
		String path = getAppPath();
		try
		{
			profileManager.store(new File(path), password);
			logger.info("Profiles stored to "+path);
		}
		catch (IOException ex)
		{
			logger.warn("Cannot store profiles", ex);
		}
	}

	private void updateProfilePopupMenu()
	{
		profilePopupMenu.removeAll();
		for (String name : profileManager.getProfileNames())
		{
			JMenuItem item = new JMenuItem(name);
			item.addActionListener((ActionEvent e) -> setProfile(name));
			profilePopupMenu.add(item);
		}
	}

	private void updateTitle()
	{
		String title = productName;
		String profileName = profileManager.getCurrentProfileName();
		if (!profileName.isEmpty())
		{
			title = title+" - "+profileName;
		}
		frame.setTitle(title);
	}

	/*
	 * This should only ever be called after successful loading of profiles so
	 * exceptions shouldn't happen but will be logged and a failure returned.
	 */
	private boolean validatePassword(String password)
	{
		boolean ok = false;
		String path = getAppPath();
		try
		{
			// Use new ProfileManager to avoid messing with member variable
			ProfileManager pm = new ProfileManager();
			pm.load(new File(path), password);
			ok = true;
		}
		catch (FileNotFoundException ex)
		{
			logger.info("No profiles found");
		}
		catch (IOException ex)
		{
			logger.warn("Cannot read profiles", ex);
		}
		catch (CryptoException ex)
		{
			if (ex.getCode().equals(CryptoCode.BadPassword))
			{
				logger.info("Bad password");
			}
			else
			{
				logger.warn("Cannot validate password", ex);
			}
		}
		return ok;
	}
	
	private void windowClosingActions(WindowEvent event)
	{
		onWindowClosing(event);
		onWriteConfig();
		netIoExec.shutdownNow();
		disconnect();
		logger.info(productName+" exiting");
		System.exit(0);
	}

}
