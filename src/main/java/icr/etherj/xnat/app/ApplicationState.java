/** *******************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package icr.etherj.xnat.app;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class ApplicationState
{
	public static final String Initialising = "initialising";
	public static final String NoKeystore = "nokeystore";
	public static final String Disconnected = "disconnected";
	public static final String Authenticated = "authenticated";
	public static final String AuthWithProjects = "authwithprojects";

	public static final String State = "state";

	private static final Logger logger = LoggerFactory.getLogger(
		ApplicationState.class);

	private final PropertyChangeSupport pcs;
	private String state = Initialising;

	/**
	 *
	 */
	public ApplicationState()
	{
		pcs = new PropertyChangeSupport(this);
	}

	/**
	 *
	 * @param pcl
	 */
	public void addPropertyChangeListener(PropertyChangeListener pcl)
	{
		pcs.addPropertyChangeListener(pcl);
	}

	/**
	 *
	 * @param newState
	 * @return
	 * @throws IllegalArgumentException
	 */
	public boolean changeState(String newState) throws IllegalArgumentException
	{
		if (newState == null)
		{
			throw new IllegalArgumentException("State must not be null");
		}
		if (state.equals(newState))
		{
			return false;
		}
		String oldState = state;
		boolean accept = false;
		switch (state)
		{
			case Initialising:
				accept = fromInitialising(newState);
				break;

			case NoKeystore:
				accept = fromNoKeystore(newState);
				break;

			case Disconnected:
				accept = fromDisconnected(newState);
				break;

			case Authenticated:
				accept = fromAuthenticated(newState);
				break;

			case AuthWithProjects:
				accept = fromAuthWithProjects(newState);
				break;

			default:
				throw new IllegalStateException(
					"Internal error, unknown state: "+state);
		}
		if (accept)
		{
			state = newState;
			logger.debug("State change: <"+oldState+"> to <"+newState+">");
			pcs.firePropertyChange(State, oldState, newState);
		}
		return accept;
	}

	/**
	 *
	 * @return
	 */
	public String getState()
	{
		return state;
	}

	/**
	 *
	 * @param pcl
	 */
	public void removePropertyChangeListener(PropertyChangeListener pcl)
	{
		pcs.removePropertyChangeListener(pcl);
	}

	private boolean fromAuthenticated(String newState)
	{
		boolean accept = false;
		switch (newState)
		{
			case Disconnected:
			case AuthWithProjects:
				accept = true;
				break;

			case NoKeystore:
			case Initialising:
				logger.warn("Attempted illegal state change from <"+state+"> to <"+
					newState+">");
				break;

			default:
				throw new IllegalArgumentException(
					"Unknown target state: <"+state+">");
		}
		return accept;
	}

	private boolean fromAuthWithProjects(String newState)
	{
		boolean accept = false;
		switch (newState)
		{
			case Disconnected:
				accept = true;
				break;

			case NoKeystore:
			case Authenticated:
			case Initialising:
				logger.warn("Attempted illegal state change from <"+state+"> to <"+
					newState+">");
				break;

			default:
				throw new IllegalArgumentException(
					"Unknown target state: <"+state+">");
		}
		return accept;
	}

	private boolean fromDisconnected(String newState)
	{
		boolean accept = false;
		switch (newState)
		{
			case NoKeystore:
			case Authenticated:
				accept = true;
				break;

			case AuthWithProjects:
			case Initialising:
				logger.warn("Attempted illegal state change from <"+state+"> to <"+
					newState+">");
				break;

			default:
				throw new IllegalArgumentException(
					"Unknown target state: <"+state+">");
		}
		return accept;
	}

	private boolean fromInitialising(String newState)
	{
		boolean accept = false;
		switch (newState)
		{
			case NoKeystore:
				accept = true;
				break;

			case Disconnected:
			case Authenticated:
			case AuthWithProjects:
				logger.warn("Attempted illegal state change from <"+state+"> to <"+
					newState+">");
				break;

			default:
				throw new IllegalArgumentException(
					"Unknown target state: <"+state+">");
		}
		return accept;
	}

	private boolean fromNoKeystore(String newState)
	{
		boolean accept = false;
		switch (newState)
		{
			case Disconnected:
				accept = true;
				break;

			case Authenticated:
			case AuthWithProjects:
			case Initialising:
				logger.warn("Attempted illegal state change from <"+state+"> to <"+
					newState+">");
				break;

			default:
				throw new IllegalArgumentException(
					"Unknown target state: <"+state+">");
		}
		return accept;
	}
	
}
