/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class XnatScan extends AbstractDisplayable
	implements XnatEntity, Comparable<XnatScan>
{
	private final String description;
	private final String id;
	private final String modality;
	private final int numericId;
	private String startTime = "00:00:00";
	private final XnatId xnatId;

	public XnatScan(XnatId xnatId, String description, String modality)
	{
		this.xnatId = xnatId;
		this.id = xnatId.getScanId();
		this.description = description;
		this.modality = modality;
		numericId = Integer.parseInt(id);
	}

	@Override
	public int compareTo(XnatScan other)
	{
		if (numericId > other.numericId)
		{
			return 1;
		}
		return (numericId < other.numericId) ? -1 : 0;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ID: "+id);
		ps.println(pad+"Modality: "+modality);
		ps.println(pad+"NumericId: "+numericId);
		ps.println(pad+"Description: "+description);
		ps.println(pad+"StartTime: "+startTime);
	}

	public String getDescription()
	{
		return description;
	}

	@Override
	public String getId()
	{
		return id;
	}

	/**
	 * @return the modality
	 */
	public String getModality()
	{
		return modality;
	}

	/**
	 * @return the numericId
	 */
	public int getNumericId()
	{
		return numericId;
	}
	
	/**
	 * @return the startTime
	 */
	public String getStartTime()
	{
		return startTime;
	}

	/**
	 * @return the startTime
	 */
	public double getFloatStartTime()
	{
		double floatTime = 0.0;
		try
		{
			floatTime = XnatUtils.timeToFloat(startTime);
		}
		catch (NumberFormatException ex)
		{ /* Deliberate no-op */ }
		return floatTime;
	}

	@Override
	public String getType()
	{
		return XnatEntity.Scan;
	}

	@Override
	public XnatId getXnatId()
	{
		return xnatId;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

}
