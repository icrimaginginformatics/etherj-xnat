/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.Objects;

/**
 *
 * @author jamesd
 */
public class XnatId extends AbstractDisplayable
{
	private final String projectId;
	private final String subjectId;
	private final String sessionId;
	private final String scanId;

	public XnatId(String projectId)
	{
		this(projectId, "", "", "");
	}

	public XnatId(String projectId, String subjectId)
	{
		this(projectId, subjectId, "", "");
	}

	public XnatId(String projectId, String subjectId, String sessionId)
	{
		this(projectId, subjectId, sessionId, "");
	}

	public XnatId(String projectId, String subjectId, String sessionId,
		String scanId)
	{
		if ((projectId == null) || (subjectId == null) || (sessionId == null) ||
			 (scanId == null))
		{
			throw new IllegalArgumentException("IDs may not be null");
		}
		this.projectId = projectId;
		this.subjectId = subjectId;
		this.sessionId = sessionId;
		this.scanId = scanId;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ProjectId: "+projectId);
		if (!subjectId.isEmpty())
		{
			ps.println(pad+"SubjectId: "+subjectId);
			if (!sessionId.isEmpty())
			{
				ps.println(pad+"SessionId: "+sessionId);
				if (!scanId.isEmpty())
				{
					ps.println(pad+"ScanId: "+scanId);
				}
			}
		}
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId()
	{
		return projectId;
	}

	/**
	 * @return the scanId
	 */
	public String getScanId()
	{
		return scanId;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId()
	{
		return sessionId;
	}

	/**
	 * @return the subjectId
	 */
	public String getSubjectId()
	{
		return subjectId;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final XnatId other = (XnatId) obj;
		return Objects.equals(this.projectId, other.projectId) &&
			Objects.equals(this.subjectId, other.subjectId) &&
			Objects.equals(this.sessionId, other.sessionId) &&
			Objects.equals(this.scanId, other.scanId);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(projectId, subjectId, sessionId, scanId);
	}

	public boolean isProjectId()
	{
		return !projectId.isEmpty() && subjectId.isEmpty() &&
			sessionId.isEmpty() && scanId.isEmpty();
	}

	public boolean isSubjectId()
	{
		return !projectId.isEmpty() && !subjectId.isEmpty() &&
			sessionId.isEmpty() && scanId.isEmpty();
	}

	public boolean isSessionId()
	{
		return !projectId.isEmpty() && !subjectId.isEmpty() &&
			!sessionId.isEmpty() && scanId.isEmpty();
	}

	public boolean isScanId()
	{
		return !projectId.isEmpty() && !subjectId.isEmpty() &&
			!sessionId.isEmpty() && !scanId.isEmpty();
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder(projectId);
		if (!subjectId.isEmpty())
		{
			sb.append("/").append(subjectId);
			if (!sessionId.isEmpty())
			{
				sb.append("/").append(sessionId);
				if (!scanId.isEmpty())
				{
					sb.append("/").append(scanId);
				}
			}
		}
		return sb.toString();
	}
}
