/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import icr.etherj.xnat.metadata.MetadataComplexType;
import icr.etherj.xnat.metadata.RoiDataMCT;
import icr.etherj.xnat.metadata.RtStructRoiCollectionDataMCT;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class RtStructUploadHelper implements XnatUploader.Helper<RtStruct>
{

	@Override
	public List<MetadataComplexType> createAuxiliaryMctList(
		UploadMetadata<RtStruct> metadata) throws IllegalArgumentException
	{
		List<MetadataComplexType> mctList = new ArrayList<>();
		RtStruct rtStruct = metadata.getUploadItem();
		String sopInstUid = rtStruct.getSopInstanceUid();
		StructureSetModule ssModule = rtStruct.getStructureSetModule();
		RoiContourModule rcModule = rtStruct.getRoiContourModule();
		for (StructureSetRoi ssRoi : ssModule.getStructureSetRoiList())
		{
			RoiContour rc = rcModule.getRoiContour(ssRoi.getRoiNumber());
			XnatRoi roi = new XnatRoi("Roi_"+Uids.createShortUnique());
			roi.setCollectionId(metadata.getAccessionId());
			roi.setName(ssRoi.getRoiName());
			roi.setUid(sopInstUid+"."+ssRoi.getRoiNumber());
			roi.setGeometricType(getGeometricType(rc));
			mctList.add(new RoiDataMCT(roi));
		}
	
		return mctList;
	}

	@Override
	public MetadataComplexType createComplexType(UploadMetadata<RtStruct> metadata)
	{
		return new RtStructRoiCollectionDataMCT(metadata);
	}

	@Override
	public boolean exists(UploadMetadata<RtStruct> metadata,
		XnatDataSource dataSource) throws XnatException
	{
		StudyUidContainer container = metadata.getPreferredContainer();
		XnatToolkit toolkit = XnatToolkit.getToolkit();
		SearchSpecification spec = toolkit.createSearchSpecification();
		SearchCriterion projCrit = toolkit.createSearchCriterion(XnatTag.ProjectID,
			SearchCriterion.Equal, container.getProjectId());
		spec.addCriterion(projCrit);
		SearchCriterion subjCrit = toolkit.createSearchCriterion(XnatTag.SubjectID,
			SearchCriterion.Equal, container.getSubjectId());
		spec.addCriterion(subjCrit);
		SearchCriterion sessionCrit = toolkit.createSearchCriterion(XnatTag.SessionID,
			SearchCriterion.Equal, container.getSessionId());
		spec.addCriterion(sessionCrit);
		SearchCriterion labelCrit = toolkit.createSearchCriterion(XnatTag.Label,
			SearchCriterion.Equal, metadata.getLabel());
		spec.addCriterion(labelCrit);
		return dataSource.exists(RtStruct.class, spec);
	}

	@Override
	public boolean existsInProject(UploadMetadata<RtStruct> metadata,
		XnatDataSource dataSource) throws XnatException
	{
		StudyUidContainer container = metadata.getPreferredContainer();
		XnatToolkit toolkit = XnatToolkit.getToolkit();
		SearchSpecification spec = toolkit.createSearchSpecification();
		SearchCriterion projCrit = toolkit.createSearchCriterion(XnatTag.ProjectID,
			SearchCriterion.Equal, container.getProjectId());
		spec.addCriterion(projCrit);
		SearchCriterion labelCrit = toolkit.createSearchCriterion(XnatTag.Label,
			SearchCriterion.Equal, metadata.getLabel());
		spec.addCriterion(labelCrit);
		return dataSource.exists(RtStruct.class, spec);
	}

	@Override
	public boolean populate(UploadMetadata<RtStruct> metadata)
	{
		return true;
	}

	private String getGeometricType(RoiContour rc) throws IllegalArgumentException
	{
		List<Contour> contourList = rc.getContourList();
		if (contourList.isEmpty())
		{
			throw new IllegalArgumentException("Contour list must not be empty");
		}
		Contour contour = contourList.get(0);
		switch (contour.getContourGeometricType())
		{
			case Contour.ClosedPlanar:
			case Contour.OpenPlanar:
				return XnatRoi.TwoDContourStack;
			case Contour.Point:
				return XnatRoi.Point;
			case Contour.OpenNonPlanar:
				throw new IllegalArgumentException("Unsupported contour type: "+
					Contour.OpenNonPlanar);
			default:
				throw new IllegalArgumentException("Unknown contour type: "+
					contour.getContourGeometricType());
		}
	}
	
}
