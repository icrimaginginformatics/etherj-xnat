/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;
import icr.etherj.xnat.schema.Provenance;
import icr.etherj.xnat.schema.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author jamesd
 * @param <T>
 */
public abstract class AbstractUploadMetadata<T> implements UploadMetadata<T>
{
	private final String accessionId;
	private int copyNumber = 0;
	private final Map<StudyUidContainer,XnatDependencyChecker.DependencyMaps>
		depMaps = new HashMap<>();
	private final File file;
	private String label;
	protected final Lock lock = new ReentrantLock();
	private StudyUidContainer prefContainer = null;
	private final XnatUploadableResource primaryRes;
	private Provenance provenance = null;
	private final Set<String> seriesUids = new HashSet<>();
	private final Set<String> sopInstUids = new HashSet<>();
	private SetMultimap<String,StudyUidContainer> studyUidContainers = 
		LinkedHashMultimap.create();
	private final Set<String> studyUids = new HashSet<>();
	public final String type;
	private final T uploadItem;
	private String version = "1";

	private final Map<Class<?>,List<AuxMetadata>> auxMap = new HashMap<>();
	
	private final List<Resource> inList = new ArrayList<>();
	private final List<Resource> outList = new ArrayList<>();

	/**
	 *
	 * @param uploadItem
	 * @param file
	 * @param accessionId
	 * @param primaryRes
	 * @throws IllegalArgumentException
	 */
	public AbstractUploadMetadata(T uploadItem, String type, File file, String accessionId,
		XnatUploadableResource primaryRes) throws IllegalArgumentException
	{
		if ((type == null) || type.isEmpty())
		{
			throw new IllegalArgumentException("Type must not be null or empty");
		}
		if (file == null)
		{
			throw new IllegalArgumentException("File must not be null");
		}
		this.uploadItem = uploadItem;
		this.type = type;
		this.file = file;
		this.accessionId = accessionId;
		this.primaryRes = primaryRes;
	}

	@Override
	public boolean addSeriesUid(String uid)
	{
		return seriesUids.add(uid);
	}

	@Override
	public boolean addSopInstanceUid(String uid)
	{
		return sopInstUids.add(uid);
	}

	@Override
	public boolean addStudyUid(String uid)
	{
		return studyUids.add(uid);
	}

	@Override
	public final String getAccessionId()
	{
		return accessionId;
	}

	@Override
	public List<XnatUploadableResource> getAuxiliaryResources()
	{
		return new ArrayList<>();
	}

	public Set<Class<?>> getAuxClasses()
	{
		return auxMap.keySet();
	}

	public List<AuxMetadata> getAuxMetadata(Class<?> klass)
	{
		return auxMap.get(klass);
	}

	@Override
	public int getCopyNumber()
	{
		return copyNumber;
	}

	@Override
	public XnatDependencyChecker.DependencyMaps getDependencyMaps(
		StudyUidContainer container)
	{
		return depMaps.get(container);
	}

	@Override
	public File getFile()
	{
		return file;
	}

	@Override
	public List<Resource> getInList()
	{
		return ImmutableList.copyOf(inList);
	}

	@Override
	public final String getLabel()
	{
		if (copyNumber == 0)
		{
			return label;
		}
		return String.format("%s_%d", label, copyNumber);
	}

	@Override
	public List<Resource> getOutList()
	{
		return ImmutableList.copyOf(outList);
	}

	@Override
	public UploadMetadata getParent()
	{
		return null;
	}

	@Override
	public StudyUidContainer getPreferredContainer()
	{
		lock.lock();
		try
		{
			return prefContainer;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public XnatUploadableResource getPrimaryResource()
	{
		return primaryRes;		
	}

	@Override
	public final Provenance getProvenance()
	{
		return provenance;
	}

	@Override
	public Set<String> getSeriesUids()
	{
		return ImmutableSet.copyOf(seriesUids);
	}

	@Override
	public Set<String> getSopInstanceUids()
	{
		return ImmutableSet.copyOf(sopInstUids);
	}

	@Override
	public SetMultimap<String,StudyUidContainer> getStudyUidContainers()
	{
		return ImmutableSetMultimap.copyOf(studyUidContainers);
	}

	@Override
	public Set<String> getStudyUids()
	{
		return ImmutableSet.copyOf(studyUids);
	}

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public T getUploadItem()
	{
		return uploadItem;
	}

	/**
	 * @return the version
	 */
	@Override
	public String getVersion()
	{
		return version;
	}

	@Override
	public int incrementCopyNumber()
	{
		lock.lock();
		try
		{
			copyNumber++;
		}
		finally
		{
			lock.unlock();
		}
		return copyNumber;
	}

	@Override
	public final void setLabel(String label)
	{
		lock.lock();
		try
		{
			this.label = label;
			// Label has changed, reset the copy number.
			copyNumber = 0;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public final void setProvenance(Provenance provenance)
	{
		this.provenance = provenance;
	}

	@Override
	public void setPreferredContainer(StudyUidContainer container)
		throws IllegalArgumentException
	{
		// This update must be atomic for thread safety
		lock.lock();
		try
		{
			if ((container != null) && !studyUidContainers.containsValue(container))
			{
				throw new IllegalArgumentException("Unknown StudyUidContainer");
			}
			this.prefContainer = container;
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * @param studyUidContainers the studyUidContainers to set
	 */
	@Override
	public void setStudyUidContainers(
		SetMultimap<String,StudyUidContainer> studyUidContainers)
	{
		// This update must be atomic for thread safety
		lock.lock();
		try
		{
			this.studyUidContainers = LinkedHashMultimap.create(studyUidContainers);
			this.prefContainer = null;
			setPreferredIfUnique();
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public final void setVersion(String version) throws IllegalArgumentException
	{
		lock.lock();
		try
		{
			Integer.parseInt(version);
			this.version = version;
		}
		catch (NumberFormatException ex)
		{
			throw new IllegalArgumentException("Version not an integer: "+version);
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setDependencyMaps(StudyUidContainer container,
		XnatDependencyChecker.DependencyMaps dependencyMaps)
	{
		lock.lock();
		try
		{
			depMaps.put(container, dependencyMaps);
		}
		finally
		{
			lock.unlock();
		}
	}

	protected final Map<Class<?>,List<AuxMetadata>> getAuxMap()
	{
		return auxMap;
	}

	private void setPreferredIfUnique()
	{
		// Any metadata with a unique container, set it as preferred
		Set<String> containerMxKeys = studyUidContainers.keySet();
		if (containerMxKeys.size() == 1)
		{
			Set<StudyUidContainer> containers = 
				studyUidContainers.get(containerMxKeys.iterator().next());
			if (containers.size() == 1)
			{
				setPreferredContainer(containers.iterator().next());
			}
		}
	}

}
