/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import icr.etherj.Xml;
import icr.etherj.XmlException;
import icr.etherj.concurrent.Concurrent;
import icr.etherj.concurrent.TaskMonitor;
import icr.etherj.xnat.impl.DefaultXnatDependencyChecker;
import icr.etherj.xnat.impl.DefaultXnatUploadResult;
import icr.etherj.xnat.metadata.MetadataComplexType;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 *
 * @author jamesd
 * @param <T>
 */
public class DefaultXnatUploader<T> implements XnatUploader<T>
{
	private static final Logger logger =
		LoggerFactory.getLogger(DefaultXnatUploader.class);

	private static final String None = "";
	private static final String Cancel = "Cancel";
	private static final String Overwrite = "Overwrite";
	private static final String Select = "Select";
	private static final String Skip = "Skip";
	
	private boolean ambiSkipAll = false;
	private final XnatServerConnection conn;
	private final XnatDataSource dataSource;
	private final XnatDependencyChecker depChecker;
	private boolean existSkipAll = false;
	private boolean existOverwriteAll = false;
	private final Lock lock = new ReentrantLock();
	// LinkedHashSet to give insertion order iteration
	private final HashSet<StudyUidContainer> prefContainers =
		new LinkedHashSet<>();
	private final Deque<UploadMetadata<T>> queue = new ArrayDeque<>();
	private boolean selectAll = false;
	private final TaskMonitor taskMonitor;
	private final XnatToolkit xnatToolkit;

	/**
	 *
	 * @param conn
	 * @throws XnatException
	 */
	public DefaultXnatUploader(XnatServerConnection conn) throws XnatException
	{
		this(conn, null, null, null);
	}

	/**
	 *
	 * @param conn
	 * @param depChecker
	 * @throws XnatException
	 */
	public DefaultXnatUploader(XnatServerConnection conn,
		XnatDependencyChecker depChecker) throws XnatException
	{
		this(conn, depChecker, null, null);
	}

	/**
	 *
	 * @param conn
	 * @param xnatToolkit
	 * @throws XnatException
	 */
	public DefaultXnatUploader(XnatServerConnection conn, XnatToolkit xnatToolkit)
		throws XnatException
	{
		this(conn, null, xnatToolkit, null);
	}

	/**
	 *
	 * @param conn
	 * @param taskMonitor
	 * @throws XnatException
	 */
	public DefaultXnatUploader(XnatServerConnection conn, TaskMonitor taskMonitor)
		throws XnatException
	{
		this(conn, null, null, taskMonitor);
	}

	/**
	 *
	 * @param conn
	 * @param depChecker
	 * @param xnatToolkit
	 * @param taskMonitor
	 * @throws XnatException
	 */
	public DefaultXnatUploader(XnatServerConnection conn,
		XnatDependencyChecker depChecker, XnatToolkit xnatToolkit,
		TaskMonitor taskMonitor)
		throws XnatException
	{
		this.conn = conn;
		this.xnatToolkit = (xnatToolkit != null)
			? xnatToolkit
			: XnatToolkit.getToolkit();
		dataSource = this.xnatToolkit.createDataSource(conn);
		this.depChecker = (depChecker != null)
			? depChecker
			: new DefaultXnatDependencyChecker(dataSource);
		this.taskMonitor = (taskMonitor != null)
			? taskMonitor
			: Concurrent.getTaskMonitor(true);
	}

	@Override
	public List<Result<T>> upload(String projectId,
		List<UploadMetadata<T>> metadataList, Helper<T> helper,
		EventHandler<T> handler)
		throws XnatException
	{
		// Almost inevitably used in multithreaded environment so protect internal
		// state.
		lock.lock();
		List<Result<T>> resultList;
		try
		{
			initState();
			resultList = uploadImpl(projectId, metadataList, helper, handler);
		}
		finally
		{
			clearState();
			lock.unlock();
		}
		return resultList;
	}

	private boolean checkContainers(String projectId, UploadMetadata<T> metadata)
		throws XnatException
	{
		SetMultimap<String,StudyUidContainer> containerMxMap = 
			depChecker.findStudyUidContainers(projectId, metadata.getStudyUids());
		normalise(containerMxMap);
		metadata.setStudyUidContainers(containerMxMap);

		// Flag metadata with no containers as orphaned
		Set<String> containerMxKeys = containerMxMap.keySet();
		if (containerMxKeys.isEmpty())
		{
			logger.info("Item "+metadata.getAccessionId()+" ("+metadata.getLabel()+
				") has no container in project: "+projectId);
			return false;
		}

		setPreferredIfUnique(metadata, containerMxMap);
		return true;
	}

	private boolean checkScanDependencies(UploadMetadata<T> metadata)
		throws XnatException
	{
		StudyUidContainer prefContainer = metadata.getPreferredContainer();
		SetMultimap <String,StudyUidContainer> containerMxMap =
			HashMultimap.create(metadata.getStudyUidContainers());
		// Each unique container needs dependency maps
		Set<StudyUidContainer> containerSet = new HashSet<>();
		containerSet.addAll(containerMxMap.values());
		Set<StudyUidContainer> missingSet = new HashSet<>();
		fetchContainerDependencies(containerSet, metadata, missingSet);
		if (missingSet.isEmpty())
		{
			logger.debug("All containers have required dependencies");
			return true;
		}
		// A container with missing dependencies cannot be the preferred one
		if (missingSet.contains(prefContainer))
		{
			logger.debug("Unsetting preferred container due to missing dependencies");
			metadata.setPreferredContainer(null);
		}
		// Remove all containers with missing dependencies and fail if that leaves
		// the multimap empty
		Iterator<String> mxIter = containerMxMap.keySet().iterator();
		while (mxIter.hasNext())
		{
			String mxKey = mxIter.next();
			for (StudyUidContainer container : missingSet)
			{
				containerMxMap.remove(mxKey, container);
			}
		}
		if (containerMxMap.isEmpty())
		{
			return false;
		}
		// Normalise and store
		normalise(containerMxMap);
		metadata.setStudyUidContainers(containerMxMap);
		setPreferredIfUnique(metadata, containerMxMap);
		return true;
	}

	private void clearQueue(List<Result<T>> resultList)
	{
		while (queue.peekFirst() != null)
		{
			resultList.add(
				new DefaultXnatUploadResult<>(queue.pollFirst(), Result.Cancelled));
		}
	}

	private void clearState()
	{
		queue.clear();
		prefContainers.clear();
	}

	private String createDataCreateCommand(UploadMetadata<T> metadata)
	{
		String rootPath = metadata.getRootServerPath();
		XnatUploadableResource xur = metadata.getPrimaryResource();
		StringBuilder sb = new StringBuilder();
		sb.append(rootPath).append(metadata.getTypePath())
			.append(metadata.getAccessionId())
			.append("/resources/").append(xur.getResourceType())
			.append("?")
			.append("content=").append(xur.getContentType())
			.append("&")
			.append("format=").append(xur.getFormat())
			.append("&")
			.append("description=").append(plusify(xur.getDescription()))
			.append("&")
			.append("name=").append(xur.getResourceType()); // <- why duplicate data?
		return sb.toString();
	}

	private String createDataUploadCommand(UploadMetadata<T> metadata)
	{
		String rootPath = metadata.getRootServerPath();
		XnatUploadableResource xur = metadata.getPrimaryResource();
		StringBuilder sb = new StringBuilder();
		sb.append(rootPath).append(metadata.getTypePath())
			.append(metadata.getAccessionId())
			.append("/resources/").append(xur.getResourceType())
			.append("/files/").append(xur.getServerFilename())
			.append("?inbody=true")
			.append("&")
			.append("content=").append(xur.getContentType())
			.append("&")
			.append("format=").append(xur.getFormat());
		return sb.toString();
	}

	private void deleteExisting(UploadMetadata<T> metadata) throws XnatException
	{
		String label = metadata.getLabel();
		logger.info("Deleting: label='"+label+"'");
		StudyUidContainer container = metadata.getPreferredContainer();
		StringBuilder sb = new StringBuilder();
		// Need to delete by ID not label
		sb.append("/data/archive/projects/").append(container.getProjectId())
			.append("/subjects/").append(container.getSubjectId())
			.append("/experiments/").append(container.getSessionId())
			.append(metadata.getTypePath()).append(label)
			.append("?removeFiles=true");
		conn.delete(sb.toString());
	}

	private void fetchContainerDependencies(Set<StudyUidContainer> containerSet,
		UploadMetadata<T> metadata, Set<StudyUidContainer> missingSet)
		throws XnatException
	{
		Iterator<StudyUidContainer> iter = containerSet.iterator();
		while (iter.hasNext())
		{
			StudyUidContainer container = iter.next();
			XnatDependencyChecker.DependencyMaps depMaps =
				depChecker.findDependencyMappings(container,
					metadata.getSeriesUids(), metadata.getSopInstanceUids());
			if (depMaps == null)
			{
				missingSet.add(container);
				iter.remove();
				continue;
			}
			metadata.setDependencyMaps(container, depMaps);
		}
	}

	private boolean handleAmbiguousItem(UploadMetadata<T> metadata,
		EventHandler handler, List<Result<T>> resultList)
	{
		logger.info("Item label '"+metadata.getLabel()+
			"' has ambiguous target StudyUidContainers");
		if (ambiSkipAll)
		{
			resultList.add(new DefaultXnatUploadResult<>(metadata, Result.Skipped));
			logger.debug("Skipping ambiguous item: label='"+metadata.getLabel()+"'");
			return false;
		}
		if (selectAll && selectAmbiguous(metadata))
		{
			return (metadata.getPreferredContainer() != null);
		}
		// EventHandler may execute on different thread e.g. EDT if user response
		// is needed, so use latch for synchronisation
		CountDownLatch latch = new CountDownLatch(1);
		ItemAmbiguousEventImpl event = new ItemAmbiguousEventImpl(metadata, latch);
		handler.itemAmbiguous(event);
		try
		{
			latch.await();
		}
		catch (InterruptedException ex)
		{
			logger.warn("Interrupted!", ex);
			// Clear the queue as if cancelled
			resultList.add(
				new DefaultXnatUploadResult<>(metadata, Result.Cancelled));
			clearQueue(resultList);
			return false;
		}
		logger.info("Ambiguous item result: "+event.getResult()+
			(event.getAll() ? "All" : ""));
		switch (event.getResult())
		{
			case Cancel:
				resultList.add(
					new DefaultXnatUploadResult<>(metadata, Result.Cancelled));
				clearQueue(resultList);
				return false;

			case Select:
				selectContainer(metadata, event);
				break;

			case Skip:
				ambiSkipAll |= event.getAll();
				resultList.add(
					new DefaultXnatUploadResult<>(metadata, Result.Skipped));
				logger.debug("Skipping: label='"+metadata.getLabel()+"'");
				return false;

			default:
				throw new IllegalStateException(
					"THIS SHOUND NEVER HAPPEN! Unknown event result: "+
						event.getResult());
		}
		return (metadata.getPreferredContainer() != null);
	}

	private boolean handleExistingItem(UploadMetadata<T> metadata,
		EventHandler handler, List<Result<T>> resultList)
		throws XnatException
	{
		if (existOverwriteAll)
		{
			deleteExisting(metadata);
			return true;
		}
		if (existSkipAll)
		{
			resultList.add(
				new DefaultXnatUploadResult<>(metadata, Result.Skipped));
			logger.debug("Skipping: label='"+metadata.getLabel()+"'");
			return false;
		}
		// EventHandler may execute on different thread e.g. EDT if user response
		// is needed, so use latch for synchronisation
		CountDownLatch latch = new CountDownLatch(1);
		ItemExistsEventImpl event = new ItemExistsEventImpl(metadata, latch);
		handler.itemExists(event);
		try
		{
			latch.await();
		}
		catch (InterruptedException ex)
		{
			logger.warn("Interrupted!", ex);
			// Clear the queue as if cancelled
			resultList.add(
				new DefaultXnatUploadResult<>(metadata, Result.Cancelled));
			clearQueue(resultList);
			return false;
		}
		logger.info("Existing item result: "+event.getResult()+
			(event.getAll() ? "All" : ""));
		switch (event.getResult())
		{
			case Cancel:
				resultList.add(
					new DefaultXnatUploadResult<>(metadata, Result.Cancelled));
				clearQueue(resultList);
				return false;

			case Overwrite:
				existOverwriteAll |= event.getAll();
				deleteExisting(metadata);
				return true;

			case Skip:
				existSkipAll |= event.getAll();
				resultList.add(
					new DefaultXnatUploadResult<>(metadata, Result.Skipped));
				logger.debug("Skipping: label='"+metadata.getLabel()+"'");
				return false;

			default:
				throw new IllegalStateException(
					"THIS SHOUND NEVER HAPPEN! Unknown event result: "+
						event.getResult());
		}
	}

	private void initState()
	{
		queue.clear();
		prefContainers.clear();
		existSkipAll = false;
		existOverwriteAll = false;
		selectAll = false;
	}

	/*
	 * Normalise such that each key has the same set of containers. Any
	 * containers not in all sets must be removed.
	 */
	private void normalise(SetMultimap<String,StudyUidContainer> containerMxMap)
	{
		// Zero or one keys are by definition normalised already. keySet() not
		// keys() as duplicate keys will return the same collection.
		if (containerMxMap.keySet().size() < 2)
		{
			logger.debug("Container multimap already normalised");
			return;
		}
		// Find the containers to remove
		Set<StudyUidContainer> removalSet = new HashSet<>();
		Set<StudyUidContainer> retainSet = new HashSet<>();
		Iterator<String> mxIter = containerMxMap.keySet().iterator();
		retainSet.addAll(containerMxMap.get(mxIter.next()));
		while (mxIter.hasNext())
		{
			Set<StudyUidContainer> containers = containerMxMap.get(mxIter.next());
			for (StudyUidContainer container : containers)
			{
				if (!retainSet.contains(container))
				{
					removalSet.add(container);
				}
				if (removalSet.contains(container))
				{
					retainSet.remove(container);
				}
			}
		}
		// Remove them
		mxIter = containerMxMap.keySet().iterator();
		while (mxIter.hasNext())
		{
			String mxKey = mxIter.next();
			for (StudyUidContainer container : removalSet)
			{
				containerMxMap.remove(mxKey, container);
			}
		}
		logger.debug("Container multimap normalised. {} containers removed",
			removalSet.size());
	}

	private String plusify(String description)
	{
		return description.replace(" ", "+");
	}

	// Return value of true indicates go ahead and upload, false indicates stop
	// processing.
	private boolean resolveExisting(UploadMetadata<T> metadata,
		EventHandler<T> handler, Helper<T> helper, List<Result<T>> resultList)
		throws XnatException
	{
		while (helper.existsInProject(metadata, dataSource))
		{
			logger.info("Item label='"+metadata.getLabel()+"' exists in project: "+
				metadata.getPreferredContainer().getProjectId());
			if (helper.exists(metadata, dataSource))
			{
				StudyUidContainer container = metadata.getPreferredContainer();
				logger.info("Item label='"+metadata.getLabel()+
					"' exists in target: "+container.getProjectId()+"/"+
					container.getSubjectLabel()+"/"+container.getSessionLabel());
				return handleExistingItem(metadata, handler, resultList);
			}
			else
			{
				logger.info("Item label='"+metadata.getLabel()+
					"' incrementing copy number");
				metadata.incrementCopyNumber();
			}
		}
		return true;
	}

	private boolean selectAmbiguous(UploadMetadata<T> metadata)
	{
		SetMultimap<String,StudyUidContainer> containerMxMap =
			metadata.getStudyUidContainers();
		Iterator<StudyUidContainer> iter = prefContainers.iterator();
		boolean found = false;
		while (iter.hasNext())
		{
			StudyUidContainer container = iter.next();
			if (containerMxMap.containsValue(container))
			{
				metadata.setPreferredContainer(container);
				logger.debug("Selecting item '"+metadata.getLabel()+
					"' to StudyUidContainer "+
					container.getProjectId()+"::"+
					container.getSubjectLabel()+"::"+
					container.getSessionLabel());
				found = true;
				break;
			}
		}
		return found;
	}

	private void selectContainer(UploadMetadata<T> metadata,
		ItemAmbiguousEventImpl event)
	{
		selectAll |= event.getAll();
		StudyUidContainer container = event.getContainer();
		prefContainers.add(container);
		metadata.setPreferredContainer(container);
	}

	private void setPreferredIfUnique(UploadMetadata<T> metadata,
		SetMultimap<String,StudyUidContainer> containerMxMap)
	{
		// Any metadata with a unique container, set it as preferred
		Set<String> containerMxKeys = containerMxMap.keySet();
		if (containerMxKeys.size() == 1)
		{
			Set<StudyUidContainer> containers = 
				containerMxMap.get(containerMxKeys.iterator().next());
			if (containers.size() == 1)
			{
				metadata.setPreferredContainer(containers.iterator().next());
			}
		}
	}

	private boolean uploadAuxiliaryDocs(UploadMetadata<T> metadata,
		Helper<T> helper) throws XnatException
	{
		System.out.println("AuxMctDocs upload");
		Set<Class<?>> auxClasses = metadata.getAuxClasses();
		for (Class<?> klass : auxClasses)
		{
			List<AuxMetadata> auxMetaList = metadata.getAuxMetadata(klass);
			for (AuxMetadata auxM : auxMetaList)
			{
				MetadataComplexType mct = auxM.getMetadataComplexType();
				String serverPath = metadata.getRootServerPath()+"/assessors/"+auxM.getAccessionId();
				if (!uploadMetadataDoc(serverPath, mct, auxM.getLabel(), true))
				{
					return false;
				}
			}
		}
		return true;
	}

	private List<Result<T>> uploadImpl(String projectId,
		List<UploadMetadata<T>> metadataList,
		Helper<T> helper, EventHandler<T> handler) throws XnatException
	{
		queue.addAll(metadataList);
		List<Result<T>> resultList = new ArrayList<>();
		taskMonitor.setMinimum(0);
		taskMonitor.setValue(0);
		int nStages = 5;
		taskMonitor.setMaximum(metadataList.size()*nStages);
		int ctr = 0;
		while (!queue.isEmpty())
		{
			UploadMetadata<T> metadata = queue.pollFirst();
			String stub = "Processing "+metadata.getLabel();
			logger.info("Processing "+metadata.getAccessionId()+
				" ("+metadata.getLabel()+")");
			try
			{
				taskMonitor.setDescription(stub+" - Finding upload targets");
				taskMonitor.setValue(ctr*nStages+1);
				if (!checkContainers(projectId, metadata))
				{
					resultList.add(new DefaultXnatUploadResult<>(metadata,
						Result.Orphan));
					ctr++;
					taskMonitor.setValue(ctr*nStages);
					continue;
				}
				taskMonitor.setDescription(stub+" - Checking dependencies present");
				taskMonitor.setValue(ctr*nStages+2);
				if (!checkScanDependencies(metadata))
				{
					resultList.add(new DefaultXnatUploadResult<>(metadata,
						Result.MissingDependencies));
					ctr++;
					taskMonitor.setValue(ctr*nStages);
					continue;
				}
				taskMonitor.setDescription(stub);

				// Resolve upload target ambiguity
				if (metadata.getPreferredContainer() == null)
				{
					if (!handleAmbiguousItem(metadata, handler, resultList))
					{
						ctr++;
						taskMonitor.setValue(ctr*nStages);
						continue;
					}
				}
				helper.populate(metadata);

				// Resolve pre-existing item
				if (!resolveExisting(metadata, handler, helper, resultList))
				{
					ctr++;
					taskMonitor.setValue(ctr*nStages);
					continue;
				
				}

				taskMonitor.setDescription(stub+" - Uploading metadata");
				taskMonitor.setValue(ctr*nStages+3);
				if (!uploadMetadataDoc(metadata, helper))
				{
					ctr++;
					taskMonitor.setValue(ctr*nStages);
					continue;
				}
				taskMonitor.setDescription(stub+" - Uploading");
				taskMonitor.setValue(ctr*nStages+4);
				uploadPrimaryResource(metadata);
//				uploadAuxiliaryDocs(metadata, helper);
				resultList.add(new DefaultXnatUploadResult<>(metadata,
					Result.Success));
				ctr++;
				taskMonitor.setValue(ctr*nStages);
			}
			catch (XnatException | IllegalArgumentException ex)
			{
				// Only break the loop if the connection has actually failed rather
				// than a recoverable error e.g. HTTP bad request
				if (!conn.isOpen())
				{
					throw ex;
				}
				logger.warn("Non-fatal error: "+ex.getMessage(), ex);
				resultList.add(new DefaultXnatUploadResult<>(metadata,
					Result.Exception, ex));
				ctr++;
				taskMonitor.setValue(ctr*nStages);
			}
		}
		taskMonitor.setValue(taskMonitor.getMaximum());
		return resultList;
	}

	private boolean uploadMetadataDoc(UploadMetadata<T> metadata,
		Helper<T> helper) throws XnatException
	{
		MetadataComplexType mct = helper.createComplexType(metadata);
		String serverPath = metadata.getRootServerPath()+
			metadata.getTypePath()+metadata.getAccessionId();
		return uploadMetadataDoc(serverPath, mct, metadata.getLabel());
	}

	private boolean uploadMetadataDoc(String serverPath, MetadataComplexType mct,
		String label) throws XnatException
	{
		return uploadMetadataDoc(serverPath, mct, label, true);
	}

	private boolean uploadMetadataDoc(String serverPath, MetadataComplexType mct,
		String label, boolean doUpload) throws XnatException
	{
		if (mct == null)
		{
			return false;
		}
		Document metadataDoc = null;
		try
		{
			metadataDoc = mct.createDocument();
			logger.debug(Xml.dump(metadataDoc));
		}
		catch (XmlException ex)
		{
			logger.warn("Document creation error:", ex);
			throw new XnatException(XnatCode.XML, ex);
		}
		String metadataUploadCmd = serverPath+"?inbody=true";
		logger.debug("Upload command: {}", metadataUploadCmd);
		if (doUpload)
		{
			conn.put(metadataUploadCmd, metadataDoc);
			logger.debug("Uploaded: label='{}'", label);
		}
		else
		{
			logger.debug("Not actually uploaded: label='{}'", label);
		}
		return true;
	}

	private void uploadPrimaryResource(UploadMetadata<T> metadata)
		throws XnatException
	{
		XnatUploadableResource primary = metadata.getPrimaryResource();
		if (primary != null)
		{
			String resDataCreateCmd = createDataCreateCommand(metadata);
			String resDataUploadCmd = createDataUploadCommand(metadata);
			logger.debug("Create command: "+resDataCreateCmd);
			conn.put(resDataCreateCmd);
			logger.debug("Primary resource created ok");
			logger.debug("Upload command: "+resDataUploadCmd);
			conn.put(resDataUploadCmd, primary.getStream());
			logger.debug("Primary resource uploaded ok");
		}
	}

	private abstract class EventImpl implements Event
	{
		protected final CountDownLatch latch;
		protected final UploadMetadata<T> metadata;
		protected String nextAction = None;
		protected boolean used = false;
		
		public EventImpl(UploadMetadata<T> metadata, CountDownLatch latch)
		{
			this.metadata = metadata;
			this.latch = latch;
		}

		@Override
		public void cancel() throws IllegalStateException
		{
			if (used)
			{
				throw new IllegalStateException("Action has already been triggered");
			}
			used = true;
			nextAction = Cancel;
			latch.countDown();
		}

		@Override
		public UploadMetadata<T> getUploadMetadata()
		{
			return metadata;
		}

		public String getResult()
		{
			return nextAction;
		}

	}

	private final class ItemAmbiguousEventImpl extends EventImpl
		implements ItemAmbiguousEvent
	{
		private boolean all = false;
		private StudyUidContainer container = null;

		public ItemAmbiguousEventImpl(UploadMetadata<T> metadata,
			CountDownLatch latch)
		{
			super(metadata, latch);
		}

		public boolean getAll()
		{
			return all;
		}

		public StudyUidContainer getContainer()
		{
			return container;
		}

		@Override
		public void select(StudyUidContainer container)
			throws IllegalStateException
		{
			select(container, false);
		}

		@Override
		public synchronized void select(StudyUidContainer container, boolean all)
			throws IllegalStateException
		{
			if (used)
			{
				throw new IllegalStateException("Action has already been triggered");
			}
			used = true;
			nextAction = Select;
			this.container = container;
			this.all = all;
			latch.countDown();
		}

		@Override
		public void skip() throws IllegalStateException
		{
			skip(false);
		}

		@Override
		public synchronized void skip(boolean all) throws IllegalStateException
		{
			if (used)
			{
				throw new IllegalStateException("Action has already been triggered");
			}
			used = true;
			nextAction = Skip;
			this.all = all;
			latch.countDown();
		}
	}

	private final class ItemExistsEventImpl extends EventImpl
		implements ItemExistsEvent
	{
		private boolean all = false;

		ItemExistsEventImpl(UploadMetadata<T> metadata, CountDownLatch latch)
		{
			super(metadata, latch);
		}

		public boolean getAll()
		{
			return all;
		}

		@Override
		public void overwrite() throws IllegalStateException
		{
			overwrite(false);
		}

		@Override
		public synchronized void overwrite(boolean all)
		{
			if (used)
			{
				throw new IllegalStateException("Action has already been triggered");
			}
			used = true;
			nextAction = Overwrite;
			this.all = all;
			latch.countDown();
		}

		@Override
		public void skip() throws IllegalStateException
		{
			skip(false);
		}

		@Override
		public synchronized void skip(boolean all)
		{
			if (used)
			{
				throw new IllegalStateException("Action has already been triggered");
			}
			used = true;
			nextAction = Skip;
			this.all = all;
			latch.countDown();
		}

	}

}
