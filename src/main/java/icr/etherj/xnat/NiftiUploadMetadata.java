/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.StringUtils;
import icr.etherj.Uids;
import icr.etherj.nifti.Nifti;
import java.io.File;

/**
 *
 * @author jamesd
 */
public class NiftiUploadMetadata
	extends AbstractUploadMetadata<Nifti>
	implements UploadMetadata<Nifti>
{
	public NiftiUploadMetadata(Nifti nifti, File file, String studyUid,
		String seriesUid)
	{
		super(nifti, "NIFTI", file, "RoiCollection_"+Uids.createShortUnique(),
			new XnatUploadableFile(file, "out", "NIFTI", "NIFTI", "EXTERNAL", "NIfTI file"));
		setLabel(getNameFromFile(file)+"_"+Uids.createShortUnique());
		addStudyUid(studyUid);
		addSeriesUid(seriesUid);
	}

	@Override
	public String getName()
	{
		String name = getUploadItem().getHeader().getDescription();
		if (StringUtils.isNullOrEmpty(name))
		{
			name = getNameFromFile(getFile());
		}
		return name;
	}

	@Override
	public String getRootServerPath()
	{
		StudyUidContainer container = getPreferredContainer();
		return "/data/archive/projects/"+container.getProjectId()+
			"/subjects/"+container.getSubjectId()+
			"/experiments/"+container.getSessionId();
	}

	@Override
	public String getTypePath()
	{
		return "/assessors/";
	}

	@Override
	public String getUid()
	{
		return "";
	}

	private String getNameFromFile(File file)
	{
		String filename = file.getName();
		if (filename.endsWith(".gz"))
		{
			filename = filename.substring(0, filename.length()-3);
		}
		if (filename.endsWith(".nii"))
		{
			filename = filename.substring(0, filename.length()-4);
		}
		return filename;
	}
}
