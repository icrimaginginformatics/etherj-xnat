/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.Displayable;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface XnatResultSet extends Displayable
{
	public static final String IntegerType = "integer";
	public static final String StringType = "string";

	/**
	 *
	 * @param row
	 * @param column
	 * @return
	 */
	public String get(int row, int column);

	/**
	 *
	 * @param row
	 * @param columnName
	 * @return
	 */
	public String get(int row, String columnName);

	/**
	 *
	 * @param index
	 * @return
	 */
	public XnatResultSet.Column getColumn(int index);

	/**
	 *
	 * @return
	 */
	public int getColumnCount();

	/**
	 *
	 * @param name
	 * @return
	 */
	public int getColumnIndex(String name);

	/**
	 *
	 * @param elementName
	 * @return
	 */
	public int getColumnIndexByElementName(String elementName);

	/**
	 *
	 * @param header
	 * @return
	 */
	public int getColumnIndexByHeader(String header);

	/**
	 *
	 * @param id
	 * @return
	 */
	public int getColumnIndexById(String id);

	/**
	 *
	 * @param xPath
	 * @return
	 */
	public int getColumnIndexByXPath(String xPath);

	/**
	 *
	 * @param index
	 * @return
	 */
	public String getColumnName(int index);

	/**
	 *
	 * @return
	 */
	public List<XnatResultSet.Column> getColumns();

	/**
	 *
	 * @param row
	 * @param column
	 * @return
	 */
	public int getInt(int row, int column);

	/**
	 *
	 * @param row
	 * @param columnName
	 * @return
	 */
	public int getInt(int row, String columnName);

	/**
	 *
	 * @return
	 */
	public int getRowCount();

	/**
	 *
	 * @return
	 */
	public String getTitle();

	/**
	 *
	 */
	public interface Column extends Displayable
	{
		public String getElementName();
		public String getId();
		public String getHeader();
		public String getName();
		public String getType();
		public String getXPath();
	}

	/**
	 *
	 */
	public interface Row extends List<String>, Displayable
	{
	}
}
