/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.Displayable;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.search.SearchSpecification;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.Study;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public interface XnatDataSource extends Displayable
{

	/**
	 *
	 * @param <T>
	 * @param klass
	 * @param plugin
	 * @return
	 */
	<T> XnatSearchPlugin<T> addSearchPlugin(Class<T> klass,
		XnatSearchPlugin<T> plugin);

	/**
	 *
	 * @param xnatEntity
	 * @throws icr.etherj.xnat.XnatException
	 */
	public void deleteEntity(XnatEntity xnatEntity) throws XnatException;

	/**
	 *
	 * @param <T>
	 * @param klass
	 * @param spec
	 * @return
	 * @throws icr.etherj.xnat.XnatException
	 */
	<T> boolean exists(Class<T> klass, SearchSpecification spec)
		throws XnatException;

	/**
	 *
	 * @param projectId
	 * @return
	 * @throws XnatException
	 */
	public List<Investigator> getInvestigators(String projectId)
		throws XnatException;

	/**
	 *
	 * @param projectId
	 * @param markupUid
	 * @return
	 */
	public RtStruct getRtStructForMarkup(String projectId, String markupUid);

	/**
	 *
	 * @param projectId
	 * @param uid
	 * @return
	 */
	public Map<XnatId,Series> getSeries(String projectId, String uid);

	/**
	 * @param projectId
	 * @param uid
	 * @param modality
	 * @return
	 */
	public Map<XnatId,Series> getSeries(String projectId, String uid, String modality);

	/**
	 *
	 * @param id
	 * @param uid
	 * @return
	 */
	public Series getSeries(XnatId id, String uid);

	/**
	 *
	 * @param projectId
	 * @param uid
	 * @return
	 */
	public Map<XnatId,Study> getStudies(String projectId, String uid);

	/**
	 *
	 * @return
	 */
	public XnatServerConnection getXnatServerConnection();

	/**
	 *
	 * @param <T>
	 * @param klass
	 * @param spec
	 * @return
	 * @throws icr.etherj.xnat.XnatException
	 */
	public <T> List<T> search(Class<T> klass, SearchSpecification spec)
		throws XnatException;

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<XnatAssessor> searchAssessor(SearchSpecification spec);

	/**
	 *
	 * @param project
	 * @param name
	 * @return
	 */
	public List<ImageAnnotationCollection> searchIac(String project, String name);

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<ImageAnnotationCollection> searchIac(SearchSpecification spec);

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<XnatProject> searchProject(SearchSpecification spec);

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<XnatScan> searchScan(SearchSpecification spec);

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<XnatSession> searchSession(SearchSpecification spec);

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<XnatSubject> searchSubject(SearchSpecification spec);

	/**
	 *
	 * @param <T>
	 * @param klass
	 * @return
	 */
	<T> boolean supportsSearch(Class<T> klass);
}
