/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.Displayable;
import icr.etherj.xnat.UploadMetadata;
import icr.etherj.xnat.XnatUploader.Result;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 * @param <T>
 */
public class DefaultXnatUploadResult<T> extends AbstractDisplayable
	implements Result<T>
{
	private final Exception ex;
	private final UploadMetadata<T> metadata;
	private final int status;

	/**
	 *
	 * @param metadata
	 * @param status
	 */
	public DefaultXnatUploadResult(UploadMetadata<T> metadata, int status)
		throws IllegalArgumentException
	{
		this(metadata, status, null);
	}

	/**
	 *
	 * @param metadata
	 * @param status
	 * @param ex
	 */
	public DefaultXnatUploadResult(UploadMetadata<T> metadata, int status,
		Exception ex) throws IllegalArgumentException
	{
		if ((status & Result.Success) != 0)
		{
			if ((status-Result.Success) != 0)
			{
				throw new IllegalArgumentException(
					"Invalid flag combination in status: "+
						String.format("0x%08x", status));
			}
		}
		this.metadata = metadata;
		this.status = status;
		this.ex = ex;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(indent+"  "+
			metadata.getUploadItem().getClass().getName());
		String recursePad = indent+"    * ";
		ps.println(recursePad+"Name: "+metadata.getName());
		ps.println(recursePad+"Label: "+metadata.getLabel());
		ps.println(recursePad+"AccessionId: "+metadata.getAccessionId());
		ps.println(pad+"Status: "+getStatusText(status));
		if (ex != null)
		{
			if (ex instanceof Displayable)
			{
				Displayable disp = (Displayable) ex;
				disp.display(ps, indent+"  ", recurse);
			}
			else
			{
				ps.println(pad+"Exception: "+ex.getClass().getName());
				ps.println(recursePad+"Message: "+ex.getMessage());
			}
		}
	}

	@Override
	public Exception getException()
	{
		return ex;
	}

	@Override
	public UploadMetadata<T> getMetadata()
	{
		return metadata;
	}

	@Override
	public int getStatus()
	{
		return status;
	}

	private String getStatusText(int status)
	{
		StringBuilder sb = new StringBuilder();
		if ((status & Result.Success) != 0)
		{
			sb.append("Success");
		}
		if ((status & Result.Orphan) != 0)
		{
			if (sb.length() > 0)
			{
				sb.append("|");
			}
			sb.append("Orphan");
		}
		if ((status & Result.MissingDependencies) != 0)
		{
			if (sb.length() > 0)
			{
				sb.append("|");
			}
			sb.append("MissingDependencies");
		}
		if ((status & Result.Skipped) != 0)
		{
			if (sb.length() > 0)
			{
				sb.append("|");
			}
			sb.append("Skipped");
		}
		if ((status & Result.Cancelled) != 0)
		{
			if (sb.length() > 0)
			{
				sb.append("|");
			}
			sb.append("Cancelled");
		}
		if ((status & Result.Exception) != 0)
		{
			if (sb.length() > 0)
			{
				sb.append("|");
			}
			sb.append("Exception");
		}
		return sb.toString();
	}

}
