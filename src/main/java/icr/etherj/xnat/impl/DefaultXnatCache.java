/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import icr.etherj.Ether;
import icr.etherj.IoUtils;
import icr.etherj.PathScan;
import icr.etherj.dicom.DicomReceiver;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.Patient;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.Study;
import icr.etherj.xnat.XnatCache;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatFileUriMap;
import icr.etherj.xnat.XnatFileUriMap.Entry;
import icr.etherj.xnat.XnatId;
import icr.etherj.xnat.XnatCode;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.dcm4che2.data.DicomObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class DefaultXnatCache implements XnatCache
{
	private static final Logger logger = LoggerFactory.getLogger(
		DefaultXnatCache.class);
	private final String cachePath;
	private final DicomToolkit dcmToolkit = DicomToolkit.getToolkit();

	public DefaultXnatCache() throws XnatException
	{
		this(new StringBuilder(Ether.getEtherDir()).append("xnat")
			.append(File.separator).append("cache").append(File.separator)
			.toString());
	}

	public DefaultXnatCache(String cachePath) throws XnatException
	{
		if ((cachePath == null) || cachePath.isEmpty())
		{
			throw new IllegalArgumentException("Path must not be null");
		}
		File file = new File(cachePath);
		ensureDirExists(file);
		this.cachePath = cachePath;
	}

	@Override
	public void clear() throws XnatException
	{
		logger.info("Clearing cache: "+cachePath);
		try
		{
			DirectoryStream<Path> dirStream =
				Files.newDirectoryStream(Paths.get(cachePath));
			Iterator<Path> iter = dirStream.iterator();
			while(iter.hasNext())
			{
				Files.walkFileTree(iter.next(), new DeleteVisitor());
			}
		}
		catch (IOException ex)
		{
			throw new XnatException("Cache clear error: "+ex.getMessage(),
				XnatCode.CacheDelete, ex);
		}
	}

	@Override
	public void delete(XnatId xnatId) throws XnatException
	{
		logger.info("Deleting from cache: "+xnatId.toString());
		Path idPath = Paths.get(id2path(xnatId));
		try
		{
			Files.walkFileTree(idPath, new DeleteVisitor());
		}
		catch (IOException ex)
		{
			throw new XnatException("Cache deletion failed: "+ex.getMessage(), 
				XnatCode.CacheDelete, ex);
		}
	}

	@Override
	public Series getSeries(String uid, XnatId xnatId) throws XnatException
	{
		Series series = null;
		if (!xnatId.isScanId())
		{
			throw new IllegalArgumentException("xnatId must specify a scan");
		}
		logger.info("Searching cache for series: "+xnatId.toString()+", UID: "+uid);
		String seriesPath = id2path(xnatId);
		DicomReceiver rx = new DicomReceiver();
		PathScan<DicomObject> scanner = dcmToolkit.createPathScan();
		scanner.addContext(rx);
		try
		{
			scanner.scan(seriesPath, true);
		}
		catch (IOException ex)
		{
			throw new XnatException("Error loading series :"+ex.getMessage(),
				XnatCode.CacheRead, ex);
		}
		// Return first instance of the desired series, should only be one but
		// not checking
		List<Patient> patients = rx.getPatientRoot().getPatientList();
		for (Patient patient : patients)
		{
			List<Study> studies = patient.getStudyList();
			for (Study study : studies)
			{
				series = study.getSeries(uid);
				if (series != null)
				{
					return series;
				}
			}
		}
		return series;
	}
	
	@Override
	public Study getStudy(String uid, XnatId xnatId) throws XnatException
	{
		Study study = null;
		if (!xnatId.isSessionId())
		{
			throw new IllegalArgumentException("xnatId must specify a session");
		}
		logger.info("Searching cache for study: "+xnatId.toString()+", UID: "+uid);
		String studyPath = id2path(xnatId);
		DicomReceiver rx = new DicomReceiver();
		PathScan<DicomObject> scanner = dcmToolkit.createPathScan();
		scanner.addContext(rx);
		try
		{
			scanner.scan(studyPath, true);
		}
		catch (IOException ex)
		{
			throw new XnatException("Error loading study: "+ex.getMessage(),
				XnatCode.CacheRead, ex);
		}
		// Return first instance of the desired study, should only be one but
		// not checking
		List<Patient> patients = rx.getPatientRoot().getPatientList();
		for (Patient patient : patients)
		{
			study = patient.getStudy(uid);
			if (study != null)
			{
				return study;
			}
		}
		return study;
	}

	@Override
	public void insert(InputStream is, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		ZipInputStream zis = null;
		XnatId id = fileUriMap.getXnatId();
		logger.info("Inserting to cache: "+id.toString());
		String idPath = id2path(id);
		Set<String> usedKeys = new HashSet<>();
		try
		{
			File idFile = new File(idPath);
			ensureDirExists(idFile);
			zis = new ZipInputStream(is);
			ZipEntry zipEntry = zis.getNextEntry();
			while (zipEntry != null)
			{
				// Strip zip internal path to get filename
				String entryName = (new File(zipEntry.getName()).getName());
				if (!fileUriMap.contains(entryName))
				{
					throw new XnatException("No entry found for :"+entryName,
						XnatCode.FileNotFound);
				}
				Entry fileUriEntry = fileUriMap.get(entryName);
				String targetPath = cachePath+fileUriEntry.getPath();
				if (!zipEntry.isDirectory())
				{
					File targetFile = new File(targetPath);
					extract(zis, targetFile);
					long actualSize = targetFile.length();
					if (actualSize != fileUriEntry.getSize())
					{
						targetFile.delete();
						throw new XnatException("File length mismatch for :"+
							targetPath+" Expected: "+fileUriEntry.getSize()+" Actual: "+
							actualSize,
							XnatCode.FileSize);
					}
				}
				zis.closeEntry();
				usedKeys.add(entryName);
				zipEntry = zis.getNextEntry();
			}
			if (!usedKeys.containsAll(fileUriMap.keySet()))
			{
				throw new XnatException("Not all files found in stream",
					XnatCode.FileNotFound);
			}
		}
		catch (IOException ex)
		{
			throw new XnatException("", XnatCode.CacheRead, ex);
		}
		finally
		{
			IoUtils.safeClose(zis);
		}
	}

	private void ensureDirExists(File file) throws XnatException
	{
		if (file.exists())
		{
			if (!file.isDirectory() || !file.canRead() || !file.canWrite())
			{
				throw new XnatException("Existing cache path ("+cachePath+
					") must be a directory and be readable and writable",
					XnatCode.CacheCreate);
			}
		}
		else
		{
			if (!file.mkdirs())
			{
				throw new XnatException("Cache path ("+cachePath+
					") could not be created",
					XnatCode.CacheCreate);
			}
		}
	}

	private void extract(ZipInputStream zis, File entryFile)
		throws IOException, XnatException
	{
		BufferedOutputStream bos = null;
		try
		{
			entryFile.getParentFile().mkdirs();
			bos = new BufferedOutputStream(new FileOutputStream(entryFile));
			byte[] buffer = new byte[4096];
			int nRead;
			while ((nRead = zis.read(buffer)) != -1)
			{
				bos.write(buffer, 0, nRead);
			}
		}
		finally
		{
			IoUtils.safeClose(bos);
		}
	}

	private String id2path(XnatId xnatId)
	{
		StringBuilder sb = new StringBuilder(cachePath);
		String currId = xnatId.getProjectId();
		if (!currId.isEmpty())
		{
			sb.append(currId).append(File.separator);
			currId = xnatId.getSubjectId();
			if (!currId.isEmpty())
			{
				sb.append(currId).append(File.separator);
				currId = xnatId.getSessionId();
				if (!currId.isEmpty())
				{
					sb.append(currId).append(File.separator);
					currId = xnatId.getScanId();
					if (!currId.isEmpty())
					{
						sb.append(currId).append(File.separator);
					}
				}
			}
		}
		return sb.toString();
	}

	private static class DeleteVisitor extends SimpleFileVisitor<Path>
	{
		public DeleteVisitor()
		{
			super();
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException ex)
			throws IOException
		{
			if (ex != null)
			{
				throw ex;
			}
			dir.toFile().delete();
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			throws IOException
		{
			file.toFile().delete();
			return FileVisitResult.CONTINUE;
		}
	}

}
