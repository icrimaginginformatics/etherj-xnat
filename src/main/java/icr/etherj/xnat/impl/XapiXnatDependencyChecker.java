/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.BiMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;
import icr.etherj.dicom.Series;
import icr.etherj.xnat.StudyUidContainer;
import icr.etherj.xnat.XnatDependencyChecker;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatId;
import icr.etherj.xnat.XnatServerConnection;
import icr.etherj.xnat.XnatUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class XapiXnatDependencyChecker implements XnatDependencyChecker
{
	private static final Logger logger =
		LoggerFactory.getLogger(XapiXnatDependencyChecker.class);

	private final XnatServerConnection conn;
	private String lastProjectId = "";
	private final Lock lock = new ReentrantLock();
	private final boolean noCache;
	private final Map<String,Map<XnatId,Series>> sessionSeriesCache = 
		new HashMap<>();
	private final Map<String,Map<String,String>> sessionSeriesUidIdCache = 
		new HashMap<>();
	private final Map<String,Map<String,BiMap<String,String>>> sessionSeriesSopInstFileCache = 
		new HashMap<>();
	private final SetMultimap<String,StudyUidContainer> studyUidContainerCache =
		LinkedHashMultimap.create();
	private final Set<String> studyUidQueryCache = new HashSet<>();

	/**
	 *
	 * @param conn
	 */
	public XapiXnatDependencyChecker(XnatServerConnection conn)
	{
		this(conn, false);
	}

	/**
	 *
	 * @param conn
	 * @param noCache
	 */
	public XapiXnatDependencyChecker(XnatServerConnection conn, boolean noCache)
	{
		this.conn = conn;
		this.noCache = noCache;
	}

	@Override
	public SetMultimap<String,StudyUidContainer> findStudyUidContainers(
		String projectId, Set<String> studyUids) throws XnatException
	{
		// Only one project can be cached at once so lock this method for thread
		// safety
		lock.lock();
		try
		{
			SetMultimap<String, StudyUidContainer> map = LinkedHashMultimap.create();
			if (studyUids.isEmpty())
			{
				logger.warn("No study UIDs supplied");
				return map;
			}
			logger.info("Finding containers for required study UIDs");
			if (!lastProjectId.equals(projectId))
			{
				clearCaches();
				if (!noCache)
				{
					updateContainerCache(projectId, studyUids);
				}
			}
			logger.debug("Checking study UIDs");
			for (String uid : studyUids)
			{
				Set<StudyUidContainer> containerSet = getContainerSet(projectId, uid);
				if (containerSet.isEmpty())
				{
					logger.info("No containers found for study UID: {}", uid);
					map.clear();
					return map;
				}
				map.putAll(uid, containerSet);
			}
			logger.debug("Study UIDs ok");
			return map;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public XnatDependencyChecker.DependencyMaps findDependencyMappings(
		StudyUidContainer container, Set<String> seriesUids,
		Set<String> sopInstUids) throws XnatException
	{
		// Only one project can be cached at once so lock this method for thread
		// safety
		lock.lock();
		try
		{
			logger.info("Finding dependency mappings for series/SOP instance UIDs");
			if (!lastProjectId.equals(container.getProjectId()))
			{
				clearCaches();
			}
			Map<String,Series> seriesMap = new HashMap<>();
			Map<String,String> sopInstSeriesMap = new HashMap<>();
			Set<String> seriesIdSet = new HashSet<>();
			if (!createSeriesMappings(container, seriesUids, sopInstUids,
				sopInstSeriesMap, seriesIdSet))
			{
				return null;
			}

			return new XnatDependencyChecker.DependencyMaps(seriesMap,
				sopInstSeriesMap, seriesIdSet);
		}
		catch (XnatException ex)
		{
			logger.info("Error checking dependencies", ex);
			return null;
		}
		finally
		{
			lock.unlock();
		}
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private void clearCaches()
	{
		studyUidContainerCache.clear();
		sessionSeriesUidIdCache.clear();
		sessionSeriesCache.clear();
		sessionSeriesSopInstFileCache.clear();
		studyUidQueryCache.clear();
		lastProjectId = "";
		logger.debug("Caches cleared");
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private boolean createSeriesMappings(StudyUidContainer container,
		Set<String> seriesUids, Set<String> sopInstUids,
		Map<String,String> sopInstSeriesMap, Set<String> seriesIdSet)
		throws XnatException
	{
		Map<String,String> seriesUidIdMap = getSeriesUidIdMap(container);
		logger.debug("Checking series UIDs");
		Set<String> foundUids = seriesUidIdMap.keySet();
		if (!foundUids.containsAll(seriesUids))
		{
			logger.info("Container is missing required series UIDs");
			return false;
		}
		logger.debug("Checking SOP instance UIDs");
		for (String seriesUid : seriesUids)
		{
			String seriesId = seriesUidIdMap.get(seriesUid);
			if (seriesId == null)
			{
				logger.info("Container is missing required Series UID: "+
					seriesUid);
				return false;
			}
			seriesIdSet.add(seriesId);
			XnatId seriesXnatId = new XnatId(container.getProjectId(),
				container.getSubjectId(), container.getSessionId(),
				seriesId);
			populateSopInstSeriesMap(seriesXnatId, seriesUid, sopInstSeriesMap);
		}
		if (!sopInstSeriesMap.keySet().containsAll(sopInstUids))
		{
			logger.info("Container is missing required SOP instance UIDs");
			return false;
		}
		logger.debug("Series/SOP instance UIDs ok");
		return true;
	}

	private Set<StudyUidContainer> fetchContainerSet(String projectId, String uid)
		throws XnatException
	{
		logger.debug("Fetching containers for study UID: {}", uid);
		String command = "/xapi/roi/projects/"+projectId+"/containers/"+uid;
		LinkedHashSet<StudyUidContainer> set = XnatUtils.readJson(
			new TypeReference<LinkedHashSet<StudyUidContainer>>(){}, conn.get(command));
		return set;
	}

	private Map<String,String> fetchSeriesUidIdMap(StudyUidContainer container)
		throws XnatException
	{
		String sessionId = container.getSessionId();
		// Fetch UID/ID for all scans in session
		logger.debug("Fetching series UID/ID mapping for session: {}", sessionId);
		String command = "/xapi/roi/projects/"+container.getProjectId()+
			"/sessions/"+container.getSessionId()+"/uididmap";
		LinkedHashMap<String,String> uidIdMap = XnatUtils.readJson(
			new TypeReference<LinkedHashMap<String,String>>(){}, conn.get(command));
		return uidIdMap;
	}

	private Set<String> fetchSopInstUids(XnatId seriesXnatId, String seriesUid)
		throws XnatException
	{
		logger.debug("Fetching SOP instance UIDs for series UID: {}", seriesUid);
		String command = "/xapi/roi/projects/"+seriesXnatId.getProjectId()+
			"/sessions/"+seriesXnatId.getSessionId()+
			"/scans/"+seriesXnatId.getScanId()+"/uids";
		HashSet<String> set = XnatUtils.readJson(
			new TypeReference<HashSet<String>>(){}, conn.get(command));
		return set;
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private Set<StudyUidContainer> getContainerSet(String projectId, String uid)
		throws XnatException
	{
		if (noCache)
		{
			return fetchContainerSet(projectId, uid);
		}
		updateContainerCache(projectId, uid);
		return studyUidContainerCache.get(uid);
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private Map<String,String> getSeriesUidIdMap(StudyUidContainer container)
		throws XnatException
	{
		if (noCache)
		{
			return fetchSeriesUidIdMap(container);
		}
		String sessionId = container.getSessionId();
		if (!sessionSeriesUidIdCache.containsKey(sessionId))
		{
			Map<String,String> uidIdMap = fetchSeriesUidIdMap(container);
			sessionSeriesUidIdCache.put(sessionId, uidIdMap);
		}
		return sessionSeriesUidIdCache.get(sessionId);
	}

	private void populateSopInstSeriesMap(XnatId seriesXnatId, String seriesUid,
		Map<String, String> sopInstSeriesMap) throws XnatException
	{
		Set<String> sopInstUids = fetchSopInstUids(seriesXnatId, seriesUid);
		for (String uid : sopInstUids)
		{
			sopInstSeriesMap.put(uid, seriesUid);
		}
	}

	/*
	 * Only call under the Lock! lastProjectId and studyUidContainers must not
	 * change independently
	 */
	private void updateContainerCache(String projectId, Set<String> studyUids)
		throws XnatException
	{
		logger.debug("Updating UID container mapping for project: {}", projectId);
		for (String studyUid : studyUids)
		{
			updateContainerCache(projectId, studyUid);
		}
		// Completed ok, set lastProjectId
		lastProjectId = projectId;
	}

	private void updateContainerCache(String projectId, String studyUid)
		throws XnatException
	{
		// Check this UID as having been queried, multimap returns an empty set
		// so can't use as a check
		if (studyUidQueryCache.contains(studyUid))
		{
			return;
		}
		// Perform the query and update the caches
		studyUidQueryCache.add(studyUid);
		studyUidContainerCache.putAll(studyUid,
			fetchContainerSet(projectId, studyUid));
	}

}
