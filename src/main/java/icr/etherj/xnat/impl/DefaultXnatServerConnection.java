/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.google.common.io.BaseEncoding;
import com.google.common.io.ByteStreams;
import icr.etherj.AbstractDisplayable;
import icr.etherj.IoUtils;
import icr.etherj.Xml;
import icr.etherj.XmlException;
import icr.etherj.xnat.XnatCode;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatResultSet;
import icr.etherj.xnat.XnatServerConnection;
import icr.etherj.xnat.XnatToolkit;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author jamesd
 */
public class DefaultXnatServerConnection extends AbstractDisplayable
	implements XnatServerConnection
{
	private static final Logger logger =
		LoggerFactory.getLogger(DefaultXnatServerConnection.class);

	private static final String BUILD_INFO = "/xapi/siteConfig/buildInfo";
	private static final String DELETE = "DELETE";
	private static final String GET = "GET";
	private static final String POST = "POST";
	private static final String PUT = "PUT";
	private static final String REST_JSESSION = "/REST/JSESSION";
	private static final String VERSION_1_6 = "/data/version";

	private final BaseEncoding base64 = BaseEncoding.base64();
	private boolean isOpen = false;
	private ScheduledThreadPoolExecutor keepAliveExec;
	private ScheduledFuture<Boolean> keepAliveFuture;
	private final Lock lock = new ReentrantLock();
	private final String password;
	private String sessionId = null;
	private final XnatToolkit toolkit;
	private final URL url;
	private final String userId;
	// Hack to allow self-signed certs. Needs proper handling
	private final TrustManager[] trustAllCerts = new TrustManager[]
	{
		new X509TrustManager()
		{
			@Override
			public void checkClientTrusted(X509Certificate[] xcs, String string)
				throws CertificateException
			{}

			@Override
			public void checkServerTrusted(X509Certificate[] xcs, String string)
				throws CertificateException
			{}

			@Override
			public X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}

		}
	};

	DefaultXnatServerConnection(URL serverUrl, String userId, String password)
		throws XnatException
	{
		this(serverUrl, userId, password, XnatToolkit.getToolkit());
	}

	DefaultXnatServerConnection(URL serverUrl, String userId, String password,
		XnatToolkit toolkit) throws XnatException
	{
		this.url = serverUrl;
		this.userId = userId;
		this.password = password;
		this.toolkit = toolkit;
		try
		{
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		}
		catch (NoSuchAlgorithmException ex)
		{
			throw new XnatException(XnatCode.NoSuchAlgorithm, ex);
		}
		catch (KeyManagementException ex)
		{
			throw new XnatException(XnatCode.KeyManagement, ex);
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Url: "+url.toString());
		ps.println(pad+"UserId: "+userId);
		ps.println(pad+"Password: !");
		ps.println(pad+"SessionId: "+(sessionId));
		ps.println(pad+"IsOpen: "+(isOpen ? "true" : "false"));
	}

	@Override
	public void close()
	{
		if (!isOpen)
		{
			return;
		}
		try
		{
			logger.debug("Deleting session: "+sessionId);
			doCommand(REST_JSESSION, DELETE, false);
			logger.debug("Connection closed");
		}
		catch (XnatException ex)
		{
			logger.warn("Error closing connection", ex);
		}
		finally
		{
			resetState();
		}
	}

	@Override
	public InputStream delete(String command) throws XnatException
	{
		return doCommand(command, DELETE, true);
	}

	@Override
	public InputStream get(String command) throws XnatException
	{
		return doCommand(command, GET, true);
	}

	@Override
	public Document getDocument(String command) throws XnatException
	{
		return streamToDoc(
			doCommand(command, GET, true));
	}

	@Override
	public Document getDocument(String command, String xml) throws XnatException
	{
		return streamToDoc(
			doCommand(command, POST, true, stringToStream(xml), false));
	}

	@Override
	public XnatResultSet getResultSet(String command) throws XnatException
	{
		return toolkit.createResultSet(
			streamToDoc(
				doCommand(command, GET, true)));
	}

	@Override
	public XnatResultSet getResultSet(String command, String xml) throws
		XnatException
	{
		return toolkit.createResultSet(
			streamToDoc(
				doCommand(command, POST, true, stringToStream(xml), false)));
	}

	@Override
	public URL getServerUrl()
	{
		return url;
	}

	@Override
	public String getServerVersion() throws XnatException
	{
		String version = null;
		// 1.7 uses xapi
		JsonFactory jsonFactory = new JsonFactory();
		try (InputStream is = doCommand(BUILD_INFO, GET, true))
		{
			try (JsonParser parser = jsonFactory.createParser(is))
			{
				JsonToken token;
				while (!parser.isClosed() && ((token = parser.nextToken()) != null))
				{
					if (JsonToken.FIELD_NAME.equals(token) &&
						"version".equals(parser.getCurrentName()))
					{
						parser.nextToken();
						version = parser.getText();
						break;
					}
				}
			}
			if (version != null)
			{
				return version;
			}
		}
		catch (IOException ex)
		{
			throw new XnatException(XnatCode.IO, ex);
		}
		catch (XnatException ex)
		{
			if (!ex.getCode().equals(XnatCode.HttpNotFound))
			{
				throw ex;
			}
		}

		// Support legacy 1.6 servers
		try
		{
			return IoUtils.toString(doCommand(VERSION_1_6, GET, true));
		}
		catch (XnatException ex)
		{
			if (!ex.getCode().equals(XnatCode.HttpNotFound))
			{
				throw ex;
			}
		}
		return version;
	}

	@Override
	public boolean isOpen()
	{
		return isOpen;
	}

	@Override
	public void open() throws XnatException
	{
		logger.debug("Opening XnatServerConnection");
		if (isOpen)
		{
			logger.debug("XnatServerConnection already open");
			return;
		}
		keepAliveExec = new ScheduledThreadPoolExecutor(1);
		try
		{
			sessionId = IoUtils.toString(
				doCommand(REST_JSESSION, POST, false, null, true));
			logger.debug("XnatServerConnection open ok");
			isOpen = true;
			authPing();
			resetKeepAlive();
		}
		catch (XnatException ex)
		{
			resetState();
			throw ex;
		}
	}

	@Override
	public InputStream post(String command) throws XnatException
	{
		return doCommand(command, POST, true, null, false);
	}

	@Override
	public InputStream post(String command, Document doc) throws XnatException
	{
		return doCommand(command, POST, true, docToStream(doc), false);
	}

	@Override
	public InputStream post(String command, InputStream is) throws XnatException
	{
		return doCommand(command, POST, true, is, false);
	}

	@Override
	public InputStream post(String command, String xml) throws XnatException
	{
		return doCommand(command, POST, true, stringToStream(xml), false);
	}

	@Override
	public InputStream put(String command) throws XnatException
	{
		return doCommand(command, PUT, true, null, false);
	}

	@Override
	public InputStream put(String command, Document doc) throws XnatException
	{
		return doCommand(command, PUT, true, docToStream(doc), false);
	}

	@Override
	public InputStream put(String command, InputStream is) throws XnatException
	{
		return doCommand(command, PUT, true, is, false);
	}

	private boolean authPing() throws XnatException
	{
		InputStream is = null;
		String response;
		boolean value = false;
		try
		{
			is = get("/data/auth");
			response = IoUtils.toString(is);
			value = (response != null) && !response.isEmpty();
			logger.info("KeepAlive: "+(value ? "OK" : "Failed"));
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return value;
	}

	private InputStream doCommand(String command, String commandType,
		boolean keepAlive) throws XnatException
	{
		return doCommand(command, commandType, keepAlive, null, false);
	}

	private InputStream doCommand(String command, String commandType,
		boolean keepAlive, InputStream commandStream, boolean allowOpen)
		throws XnatException
	{
		HttpURLConnection conn = null;
		String rest = url.toString()+command;
		InputStream is;
		try
		{
			conn = getHttpConnection(new URL(rest), commandType, allowOpen);
			conn.setReadTimeout(900000);
			if ((commandType.equals(POST) || commandType.equals(PUT))
				&& (commandStream != null))
			{
				conn.setDoOutput(true);
//				conn.setRequestProperty("Cache-Control", "no-cache");
//				conn.setRequestProperty("Content-Type", "application/octet-stream");
				try (OutputStream os = new BufferedOutputStream(conn.getOutputStream()))
				{
					ByteStreams.copy(commandStream, os);
				}
				commandStream.close();
			}
			if (commandType.equals(POST) && commandStream == null)
			{
				// Hack to pander to proxies that don't accept POST with no body
				conn.setDoOutput(true);
				conn.setRequestProperty("Content-Length", "0");
				try (OutputStream os = new BufferedOutputStream(conn.getOutputStream()))
				{
					os.write(new byte[0]);
				}
			}
			conn.connect();
			is = new BufferedInputStream(conn.getInputStream());
			if (keepAlive)
			{
				resetKeepAlive();
			}
			logger.debug(commandType+" - Code: {}, Message: {}",
				conn.getResponseCode(), conn.getResponseMessage());
		}
		catch (MalformedURLException ex)
		{
			throw new XnatException(XnatCode.MalformedUrl, ex);
		}
		catch (UnknownHostException ex)
		{
			resetState();
			throw new XnatException(XnatCode.UnknownHost, ex);
		}
		catch (ConnectException ex)
		{
			resetState();
			throw new XnatException(XnatCode.Connect, ex);
		}
		catch (SocketTimeoutException ex)
		{
			throw new XnatException(XnatCode.SocketTimeout, ex);
		}
		catch (UnknownServiceException ex)
		{
			throw new XnatException(XnatCode.UnknownService, ex);
		}
		catch (SSLException ex)
		{
			throw new XnatException(XnatCode.SSL, ex);
		}
		catch (IOException ex)
		{
			throw handleIoException(conn, ex);
		}

		return is;
	}

	private InputStream docToStream(Document doc) throws XnatException
	{
		try
		{
			return stringToStream(Xml.toString(doc));
		}
		catch (XmlException ex)
		{
			throw new XnatException(XnatCode.XML, ex);
		}
	}

	private String getAuthorization()
	{
		return "Basic "+base64.encode((userId+":"+password).getBytes()).trim();
	}

	private HttpURLConnection getHttpConnection(URL restUrl, String type,
		boolean allowOpen) throws XnatException
	{
		if (!isOpen && !allowOpen)
		{
			throw new XnatException("Connection is not open",
				XnatCode.IllegalState);
		}
		HttpURLConnection conn = null;
		try
		{
			conn = (HttpURLConnection) restUrl.openConnection();
			conn.setRequestMethod(type);
			if (!isOpen)
			{
				conn.setRequestProperty("Authorization", getAuthorization());
			}
			else
			{
				conn.setRequestProperty("Cookie", "JSESSIONID=" + sessionId);
			}
			conn.setConnectTimeout(5000);
		}
		catch (ProtocolException ex)
		{
			throw new XnatException(XnatCode.Protocol, ex);
		}
		catch (IllegalArgumentException ex)
		{
			throw new XnatException(XnatCode.IllegalArgument, ex);
		}
		catch (IOException ex)
		{
			throw new XnatException(XnatCode.IO, ex);
		}
		return conn;
	}

	private XnatException handleIoException(HttpURLConnection conn, IOException ex)
	{
		int code;
		String message;
		try
		{
			code = conn.getResponseCode();
			message = conn.getResponseMessage();
			logger.warn("HTTP Response - Code: {}, Message: {}", code, message);
		}
		catch (IOException exNew)
		{
			return new XnatException(
				"Could not determine HTTP response code for original exception",
				XnatCode.IO, ex);
		}
		// Handle the commonly useful cases or record the code in the message
		switch (code)
		{
			case HttpURLConnection.HTTP_BAD_REQUEST:
				return new XnatException(
					XnatCode.HttpBadRequest.getMessage()+" - Message: "+message,
					XnatCode.HttpBadRequest, ex);

			case HttpURLConnection.HTTP_UNAUTHORIZED:
				return new XnatException(
					XnatCode.HttpUnauthorised.getMessage()+" - Message: "+message,
					XnatCode.HttpUnauthorised, ex);

			case HttpURLConnection.HTTP_FORBIDDEN:
				return new XnatException(
					XnatCode.HttpForbidden.getMessage()+" - Message: "+message,
					XnatCode.HttpForbidden, ex);

			case HttpURLConnection.HTTP_NOT_FOUND:
				return new XnatException(
					XnatCode.HttpNotFound.getMessage()+" - Message: "+message,
					XnatCode.HttpNotFound, ex);

			case HttpURLConnection.HTTP_CONFLICT:
				return new XnatException(
					XnatCode.HttpConflict.getMessage()+" - Message: "+message,
					XnatCode.HttpConflict, ex);

			case 422:
				return new XnatException(
					XnatCode.HttpUnprocessableEntity.getMessage()+" - Message: "+message,
					XnatCode.HttpUnprocessableEntity, ex);

			case HttpURLConnection.HTTP_INTERNAL_ERROR:
				return new XnatException(
					XnatCode.HttpInternalError.getMessage()+" - Message: "+message,
					XnatCode.HttpInternalError, ex);

			case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
				return new XnatException(
					XnatCode.HttpGatewayTimeout.getMessage()+" - Message: "+message,
					XnatCode.HttpGatewayTimeout, ex);

			// Error return where conn cannot determine the response code
			case -1:
				return new XnatException(XnatCode.HttpInvalid, ex);

			default:
				return new XnatException(
					"Response not implemented yet: HTTP code "+code,
					XnatCode.HttpNotImplementedYet, ex);
		}
	}

	private void resetState()
	{
		lock.lock();
		logger.debug("XnatServerConnection resetting state");
		try
		{
			isOpen = false;
			sessionId = null;
			if (keepAliveFuture != null)
			{
				keepAliveFuture.cancel(true);
			}
			if (keepAliveExec != null)
			{
				keepAliveExec.shutdownNow();
			}
			keepAliveFuture = null;
			keepAliveExec = null;
		}
		finally
		{
			lock.unlock();
		}
	}

	private void resetKeepAlive()
	{
		if (keepAliveFuture != null)
		{
			keepAliveFuture.cancel(false);
		}
		keepAliveFuture = keepAliveExec.schedule(new KeepAliveCallable(), 600,
			TimeUnit.SECONDS);
	}

	private Document streamToDoc(InputStream is) throws XnatException
	{
		Document doc = null;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.parse(is);
		}
		catch (ParserConfigurationException ex)
		{
			throw new XnatException(XnatCode.ParserConfiguration, ex);
		}
		catch (SAXException ex)
		{
			throw new XnatException(XnatCode.SAX, ex);
		}
		catch (IllegalArgumentException ex)
		{
			throw new XnatException(XnatCode.IllegalArgument, ex);
		}
		catch (IOException ex)
		{
			throw new XnatException(XnatCode.IO, ex);
		}
		return doc;
	}

	private InputStream stringToStream(String string) throws XnatException
	{
		ByteArrayInputStream bais = null;
		if (string != null)
		{
			try
			{
				bais = new ByteArrayInputStream(string.getBytes("UTF-8"));
			}
			catch (UnsupportedEncodingException ex)
			{
				throw new XnatException(XnatCode.UnknownEncoding, ex);
			}
		}
		return bais;
	}

	private class KeepAliveCallable implements Callable<Boolean>
	{
		@Override
		public Boolean call() throws Exception
		{
			boolean value = authPing();
			resetKeepAlive();
			return value;
		}
	}
}
