/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.IoUtils;
import icr.etherj.Xml;
import icr.etherj.XmlException;
import icr.etherj.aim.AimToolkit;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.XmlParser;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.SopInstance;
import icr.etherj.dicom.Study;
import icr.etherj.xnat.Investigator;
import icr.etherj.xnat.XnatAssessor;
import icr.etherj.xnat.XnatCache;
import icr.etherj.xnat.XnatDataSource;
import icr.etherj.xnat.XnatAnnotationItem;
import icr.etherj.xnat.XnatEntity;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatFileUriMap;
import icr.etherj.xnat.XnatFileUriMap.Entry;
import icr.etherj.xnat.XnatId;
import icr.etherj.xnat.XnatImageAnnotation;
import icr.etherj.xnat.XnatImageAnnotationCollection;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatRegion;
import icr.etherj.xnat.XnatRegionSet;
import icr.etherj.xnat.XnatResultSet;
import icr.etherj.xnat.XnatScan;
import icr.etherj.xnat.XnatServerConnection;
import icr.etherj.xnat.XnatSession;
import icr.etherj.xnat.XnatCode;
import icr.etherj.xnat.XnatSearchPlugin;
import icr.etherj.xnat.XnatSubject;
import icr.etherj.xnat.XnatTag;
import icr.etherj.xnat.XnatToolkit;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.dcm4che2.data.DicomObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author jamesd
 */
public class DefaultXnatDataSource extends AbstractDisplayable implements XnatDataSource
{
	private static final Logger logger =
		LoggerFactory.getLogger(DefaultXnatDataSource.class);

	private static final String Equals = "=";
	private static final String LessThan = "&lt;";
	private static final String Like = "LIKE";

	private static final String AimImageAnnotationCollectionIdHeader =
		"AimImageAnnotationCollectionId";
	private static final String AimImageAnnotationCollectionUidHeader =
		"AimImageAnnotationCollectionUid";
	private static final String AimImageAnnotationIdHeader =
		"AimImageAnnotationId";
	private static final String AimImageAnnotationUidHeader =
		"AimImageAnnotationUid";
	private static final String AimSubclassType = "subclassType";
	private static final String AimSubclassTypeHeader = "SubclassType";
	private static final String AimUid = "aimId";
	private static final String AnnotationRoleCount = "nAnnotationRoleEntity";
	private static final String AnnotationRoleCountHeader = "AnnotationRoleCount";
	private static final String AssessorDateHeader = "AssessorDate";
	private static final String AssessorIdHeader = "AssessorId";
	private static final String AssessorLabelHeader = "AssessorLabel";
	private static final String AssessorSessionId = "imageSession_ID";
	private static final String AssessorTimeHeader = "AssessorTime";
	private static final String AssociatedRegionSetId =
		"associatedRegionSetId";
	private static final String AssociatedRegionSetIdHeader =
		"AssociatedRegionSetId";
	private static final String CalculationCount = "nCalculationEntity";
	private static final String CalculationCountHeader = "CalculationCount";
	private static final String Date = "date";
	private static final String DicomSubjectName = "dicomSubjName";
	private static final String ID = "ID";
	private static final String ImagingObservationCount = "nImagingObservationEntity";
	private static final String ImagingObservationCountHeader = "ImagingObservationCount";
	private static final String ImagingPhysicalCount = "nImagingPhysicalEntity";
	private static final String ImagingPhysicalCountHeader = "ImagingPhysicalCount";
	private static final String InferenceCount = "nInferenceEntity";
	private static final String InferenceCountHeader = "InferenceCount";
	private static final String IntegerType = "integer";
	private static final String Keywords = "keywords";
	private static final String Label = "Label";
	private static final String MarkupCount = "nMarkupEntity";
	private static final String MarkupCountHeader = "MarkupCount";
	private static final String MarkupEntity = "MarkupEntity";
	private static final String Name = "Name";
	private static final String OriginalTypeHeader = "OriginalType";
	private static final String ParentIaIdHeader = "ParentIaId";
	private static final String ParentIacIdHeader = "ParentIacId";
	private static final String Project = "project";
	private static final String ProjectIdHeader = "ProjectId";
	private static final String ProjectKeywordsHeader = "ProjectKeywords";
	private static final String ProjectLabelHeader = "ProjectLabel";
	private static final String RegionCount = "nRegions";
	private static final String RegionCountHeader = "RegionCount";
	private static final String RegionSetOriginalType =
		"originatingRegionSetSource/originalDataType";
	private static final String ScanIdHeader = "ScanId";
	private static final String ScanProjectId = "project";
	private static final String ScanProjectIdHeader = "ScanProjectId";
	private static final String ScanSessionId = "image_session_ID";
	private static final String ScanSessionIdHeader = "ScanSessionId";
	private static final String ScanStartTime = "startTime";
	private static final String ScanStartTimeHeader = "ScanStartTime";
	private static final String ScanSubjectId = "subject_ID";
	private static final String ScanSubjectIdHeader = "ScanSubjectId";
	private static final String ScanTypeHeader = "ScanType";
	private static final String ScanDescription = "series_description";
	private static final String ScanDescHeader = "ScanDescription";
	private static final String SegmentationCount = "nSegmentationEntity";
	private static final String SegmentationCountHeader = "SegmentationCount";
	private static final String SessionDateHeader = "SessionDate";
	private static final String SessionIdHeader = "SessionId";
	private static final String SessionLabelHeader = "SessionLabel";
	private static final String SessionProjectId = "project";
	private static final String SessionProjectIdHeader = "SessionProjectId";
	private static final String SessionSubjectId = "subject_ID";
	private static final String SessionSubjectIdHeader = "SessionSubjectId";
	private static final String Size = "Size";
	private static final String StringType = "string";
	private static final String SubjectId = "subject_ID";
	private static final String SubjectIdHeader = "SubjectId";
	private static final String SubjectNameHeader = "SubjectName";
	private static final String TaskContextCount = "nTaskContextEntity";
	private static final String TaskContextCountHeader = "TaskContextCount";
	private static final String Time = "time";
	private static final String Uid = "UID";
	private static final String UidHeader = "Uid";
	private static final String URI = "URI";
	private static final String Type = "type";
	private static final String Unknown = "<<UNKNOWN>>";
	private static final String XnatSubjectId = "xnatSubjID";
	private static final String XsiType = "xsiType";
	private static final String XsiTypeHeader = "XsiType";

	// Use a LinkedHashMap to ensure iteraration is in insertion order
	private static final Map<String,String> modalityMap = new LinkedHashMap<>();

	private final XnatServerConnection xsc;
	private final XnatCache cache;
	private final AimToolkit aimToolkit;

	static
	{
		// Build modalityMap, most common modalities first
		modalityMap.put("MR", "mr");
		modalityMap.put("CT", "ct");
		modalityMap.put("PT", "pet");
	}

	DefaultXnatDataSource(XnatServerConnection xsc) throws XnatException
	{
		this.xsc = xsc;
		this.cache = XnatToolkit.getToolkit().createCache();
		this.aimToolkit = AimToolkit.getToolkit();
	}

	DefaultXnatDataSource(XnatServerConnection xsc, XnatCache cache)
	{
		this.xsc = xsc;
		this.cache = cache;
		this.aimToolkit = AimToolkit.getToolkit();
	}

	@Override
	public <T> XnatSearchPlugin<T> addSearchPlugin(Class<T> klass, XnatSearchPlugin<T> plugin)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void deleteEntity(XnatEntity xnatEntity) throws XnatException
	{
		Map<XnatId,String> xnatIds = new LinkedHashMap<>();
		findIdsToDelete(xnatEntity, xnatIds);
		for (XnatId id : xnatIds.keySet())
		{
			StringBuilder sb = new StringBuilder("/data/archive/projects/");
			String projectId = id.getProjectId();
			if (projectId.isEmpty())
			{
				throw new XnatException("Invalid XnatID",
					XnatCode.InvalidXnatId);
			}
			sb.append(projectId);
			String subjectId = id.getSubjectId();
			if (!subjectId.isEmpty())
			{
				sb.append("/subjects/").append(subjectId);
				String sessionId = id.getSessionId();
				if (!sessionId.isEmpty())
				{
					sb.append("/experiments/").append(sessionId);
					String scanId = id.getScanId();
					if (!scanId.isEmpty())
					{
						switch (xnatIds.get(id))
						{
							case XnatEntity.Assessor:
								sb.append("/assessors/");
								break;
							case XnatEntity.Scan:
								sb.append("/scans/");
								break;
							default:
								throw new XnatException(
									"Unknown type for ID: "+xnatIds.get(id),
									XnatCode.InvalidXnatId);
						}
						sb.append(scanId);
					}
				}
			}
			sb.append("?removeFiles=true");
			logger.info("Entity delete command: {}", sb.toString());
			xsc.delete(sb.toString());
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		xsc.display(ps, indent+"  ");
	}

	@Override
	public <T> boolean exists(Class<T> klass, SearchSpecification spec)
	{
		switch (klass.getCanonicalName())
		{
			case "etherj.aim.ImageAnnotationCollection":
				return !searchIac(spec).isEmpty();
			default:
				throw new IllegalArgumentException(
					"Class "+klass.getCanonicalName()+" not supported");
		}
	}

	@Override
	public List<Investigator> getInvestigators(String projectId)
		throws XnatException
	{
		logger.info("Fetching investigators: {}", projectId);
		List<Investigator> investigators = new ArrayList<>();
		String command = "/data/archive/projects/"+projectId+
			"?xsiType=xnat:projectData"+
			"&format=xml";
		Document doc = xsc.getDocument(command);
		Node piNode = Xml.getFirstMatch(doc.getDocumentElement(), "xnat:PI");
		if (piNode == null)
		{
			return investigators;
		}
		Investigator investigator = parseInvestigator(piNode);
		if (investigator != null)
		{
			investigators.add(investigator);
		}
		Node invNode = Xml.getFirstMatch(doc.getDocumentElement(),
			"xnat:investigators");
		if (invNode == null)
		{
			return investigators;
		}
		NodeList children = invNode.getChildNodes();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			if (child.getNodeName().equals("xnat:investigator"))
			{
				investigator = parseInvestigator(child);
				if (investigator != null)
				{
					investigators.add(investigator);
				}
			}
		}
		return investigators;
	}

	@Override
	public RtStruct getRtStructForMarkup(String project, String markupUid)
	{
		RtStruct rtStruct = null;
		try
		{
			XnatResultSet markupRs = fetchMarkupRs(project, markupUid);
			int regionSetIdIdx = markupRs.getColumnIndexByHeader(
				AssociatedRegionSetIdHeader);
			XnatId xnatId = xnatIdFromResultSet(markupRs);
			rtStruct = getRtStruct(xnatId, markupRs.get(0, regionSetIdIdx));
		}
		catch (XnatException ex)
		{
			logger.warn("Error fetching RtStruct for Markup UID: "+markupUid, ex);
		}
		return rtStruct;
	}

	@Override
	public Map<XnatId,Series> getSeries(String projectId, String uid)
	{
		logger.info("Searching for series. UID: {}", uid);
		Map<XnatId,Series> emptyMap = new HashMap<>();
		for (String key : modalityMap.keySet())
		{
			Map<XnatId,Series> modalitySeriesMap = getSeries(projectId, uid, key);
			if (!modalitySeriesMap.isEmpty())
			{
				return modalitySeriesMap;
			}
		}
		return emptyMap;
	}

	@Override
	public Map<XnatId,Series> getSeries(String projectId, String uid, String modality)
	{
		Map<XnatId,Series> seriesMap = new HashMap<>();
		if (!modalityMap.containsKey(modality))
		{
			logger.error("Unsupported modality: {}", modality);
			return seriesMap;
		}
		logger.info("Searching for {} series. UID: {}", modality, uid);
		Set<XnatId> idSet = getXnatSeriesIds(projectId, uid, modality);
		if (idSet.isEmpty())
		{
			return seriesMap;
		}
		for (XnatId id : idSet)
		{
			try
			{
				Series series = fetchSeries(id, uid);
				seriesMap.put(id, series);
			}
			catch (XnatException ex)
			{
				logger.error("Scan search error: ", ex);
			}
		}
		logger.info("Series search complete: {}", !seriesMap.isEmpty() ? "OK" : "Failed");
		return seriesMap;
	}

	@Override
	public Series getSeries(XnatId id, String uid)
	{
		Series series = null;
		try
		{
			series = fetchSeries(id, uid);
		}
		catch (XnatException ex)
		{
			logger.warn("Series fetch failed for XnatId: "+id.toString(), ex);
		}
		return series;
	}

	@Override
	public Map<XnatId,Study> getStudies(String projectId, String uid)
	{
		logger.info("Searching for study. UID: {}", uid);
		Map<XnatId,Study> studyMap = new HashMap<>();
		for (String key : modalityMap.keySet())
		{
			Map<XnatId,Study> modalityStudyMap = getStudies(projectId, uid, key);
			studyMap.putAll(modalityStudyMap);
		}
		return studyMap;
	}

	@Override
	public XnatServerConnection getXnatServerConnection()
	{
		return xsc;
	}

	@Override
	public <T> List<T> search(Class<T> klass, SearchSpecification spec)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<XnatAssessor> searchAssessor(SearchSpecification spec)
	{
		List<XnatAssessor> assessors = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return assessors;
		}

		List<XnatImageAnnotationCollection> iacs = null;
		List<XnatImageAnnotation> ias = null;
		List<XnatAnnotationItem> items = null;
		List<XnatRegionSet> regionSets = null;
		List<XnatRegion> regions = null;
		try
		{
			String iacQuery = createSearchIacAssessorQuery(spec);
			iacs = queryIacAssessors(iacQuery);
			String iaQuery = createSearchIaAssessorQuery(spec);
			ias = queryIaAssessors(iaQuery);
			String itemQuery = createSearchAnnotationItemAssessorQuery(spec);
			items = queryAnnotationItemAssessors(itemQuery);
			String regionSetQuery = createSearchRegionSetAssessorQuery(spec);
			regionSets = queryRegionSetAssessors(regionSetQuery);
			String regionQuery = createSearchRegionAssessorQuery(spec);
			regions = queryRegionAssessors(regionQuery);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			assessors.clear();
		}
		nestAssessors(assessors, iacs, ias, items, regionSets, regions);
		return assessors;
	}

	@Override
	public List<ImageAnnotationCollection> searchIac(SearchSpecification spec)
	{
		List<ImageAnnotationCollection> iacList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return iacList;
		}

		try
		{
			String query = createSearchIacQuery(spec);
			queryIacs(iacList, query);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			iacList.clear();
		}
		return iacList;
	}

	@Override
	public List<ImageAnnotationCollection> searchIac(String project, String name)
	{
		XnatToolkit xnatToolkit = XnatToolkit.getToolkit();
		SearchSpecification spec = xnatToolkit.createSearchSpecification();
		SearchCriterion projSc = xnatToolkit.createSearchCriterion(
			XnatTag.ProjectID, SearchCriterion.Like, project);
		SearchCriterion subjSc = xnatToolkit.createSearchCriterion(
			XnatTag.SubjectName, SearchCriterion.Like, name);
		spec.addCriterion(projSc);
		spec.addCriterion(subjSc);
		return searchIac(spec);
	}

	@Override
	public List<XnatProject> searchProject(SearchSpecification spec)
	{
		List<XnatProject> projectList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return projectList;
		}

		try
		{
			String query = createSearchProjectQuery(spec);
			queryProjects(projectList, query);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			projectList.clear();
		}
		return projectList;
	}

	@Override
	public List<XnatScan> searchScan(SearchSpecification spec)
	{
		List<XnatScan> scanList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return scanList;
		}

		for (String key : modalityMap.keySet())
		{
			searchScan(spec, key, scanList);
		}
		Collections.sort(scanList);
		return scanList;
	}

	@Override
	public List<XnatSession> searchSession(SearchSpecification spec)
	{
		List<XnatSession> sessionList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return sessionList;
		}

		for (String key : modalityMap.keySet())
		{
			searchSession(spec, key, sessionList);
		}
		Collections.sort(sessionList);
		return sessionList;
	}

	@Override
	public List<XnatSubject> searchSubject(SearchSpecification spec)
	{
		List<XnatSubject> subjectList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return subjectList;
		}

		try
		{
			String query = createSearchSubjectQuery(spec);
			querySubjects(subjectList, query);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			subjectList.clear();
		}
		return subjectList;
	}

	@Override
	public <T> boolean supportsSearch(Class<T> klass)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	private int addAssessorSearchFields(StringBuilder sb, String searchData)
	{
		addSearchField(sb, searchData, Project, 0, StringType,
			ProjectIdHeader);
		addSearchField(sb, searchData, XnatSubjectId, 1, StringType,
			SubjectIdHeader);
		addSearchField(sb, searchData, AssessorSessionId, 2, StringType,
			SessionIdHeader);
		addSearchField(sb, searchData, ID, 3, StringType,
			AssessorIdHeader);
		addSearchField(sb, searchData, Label, 4, StringType,
			AssessorLabelHeader);
		addSearchField(sb, searchData, Date, 5, StringType,
			AssessorDateHeader);
		addSearchField(sb, searchData, Time, 6, StringType,
			AssessorTimeHeader);

		// Return the number of fields added so callers can avoid reusing indices
		return 7;
	}

	private StringBuilder addRootElement(StringBuilder sb, String name)
	{
		sb.append("	<xdat:root_element_name>").append(name)
			.append("</xdat:root_element_name>\n");
		return sb;
	}

	private StringBuilder addSearchField(StringBuilder sb, String name,
		String fieldId, int seq, String type, String header)
	{
		sb.append("	<xdat:search_field>\n");
		sb.append("		<xdat:element_name>").append(name)
			.append("</xdat:element_name>\n");
		sb.append("		<xdat:field_ID>").append(fieldId).append("</xdat:field_ID>\n");
		sb.append("		<xdat:sequence>").append(seq).append("</xdat:sequence>\n");
		sb.append("		<xdat:type>").append(type).append("</xdat:type>\n");
		sb.append("		<xdat:header>").append(header).append("</xdat:header>\n");
		sb.append("	</xdat:search_field>\n");
		return sb;
	}

	private StringBuilder addCriterion(StringBuilder sb, String schemaField,
		String comparator, String value)
	{
		sb.append("		<xdat:criteria override_value_formatting=\"0\">\n");
		sb.append("			<xdat:schema_field>").append(schemaField)
			.append("</xdat:schema_field>\n");
		sb.append("			<xdat:comparison_type>").append(comparator)
			.append("</xdat:comparison_type>\n");
		sb.append("			<xdat:value>").append(value).append("</xdat:value>\n");
		sb.append("		</xdat:criteria>\n");
		return sb;
	}

	private StringBuilder addCriterion(StringBuilder sb, String schemaField,
		SearchCriterion crit)
	{
		sb.append("		<xdat:criteria override_value_formatting=\"0\">\n");
		sb.append("			<xdat:schema_field>").append(schemaField)
			.append("</xdat:schema_field>\n");
		sb.append("			<xdat:comparison_type>")
			.append(crit.createComparatorSql())
			.append("</xdat:comparison_type>\n");
		sb.append("			<xdat:value>");
		if (crit.getComparator() == SearchCriterion.Like)
		{
			sb.append("%").append(crit.getValue()).append("%");
		}
		else
		{
			sb.append(crit.getValue());
		}
		sb.append("</xdat:value>\n");
		sb.append("		</xdat:criteria>\n");
		return sb;
	}

	private StringBuilder addCriterion(StringBuilder sb, String schemaField,
		SearchCriterion crit, int value)
	{
		sb.append("		<xdat:criteria override_value_formatting=\"0\">\n");
		sb.append("			<xdat:schema_field>").append(schemaField)
			.append("</xdat:schema_field>\n");
		sb.append("			<xdat:comparison_type>")
			.append(crit.createComparatorSql())
			.append("</xdat:comparison_type>\n");
		sb.append("			<xdat:value>");
		if (crit.getComparator() == SearchCriterion.Like)
		{
			sb.append("%").append(value).append("%");
		}
		else
		{
			sb.append(value);
		}
		sb.append("</xdat:value>\n");
		sb.append("		</xdat:criteria>\n");
		return sb;
	}

	private StringBuilder createInitialXml(String desc)
	{
		StringBuilder sb = new StringBuilder(
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		sb.append("<xdat:search ID=\"\"")
			.append(" allow-diff-columns=\"0\"")
			.append(" secure=\"false\"")
			.append(" brief-description=\"").append(desc).append("\"")
			.append(" xmlns:xdat=\"http://nrg.wustl.edu/security\"")
			.append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
		return sb;
	}

	private String createRtStructForMarkupQuery(String project, String markupUid)
	{
		StringBuilder sb = createInitialXml("RtStructForMarkup");

		String markupData = "icr:aimEntitySubclassData";
		addRootElement(sb, markupData);
		addSearchField(sb, markupData, Project, 0, StringType,
			ProjectIdHeader);
		addSearchField(sb, markupData, XnatSubjectId, 1, StringType,
			SubjectIdHeader);
		addSearchField(sb, markupData, AssessorSessionId, 2, StringType,
			SessionIdHeader);
		addSearchField(sb, markupData, AssociatedRegionSetId, 3, StringType,
			AssociatedRegionSetIdHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		addCriterion(sb, markupData+"/"+Project, Equals, project);
		addCriterion(sb, markupData+"/"+AimSubclassType, Equals, MarkupEntity);
		addCriterion(sb, markupData+"/"+AimUid, Equals, markupUid);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchAnnotationItemAssessorQuery(
		SearchSpecification spec) throws XnatException
	{
		StringBuilder sb = createInitialXml("EntitySubclass");

		String subclassData = "icr:aimEntitySubclassData";
		addRootElement(sb, subclassData);
		int nFields = addAssessorSearchFields(sb, subclassData);

		addSearchField(sb, subclassData, AimUid, nFields++, StringType,
			AimImageAnnotationUidHeader);
		addSearchField(sb, subclassData, AimSubclassType, nFields++, StringType,
			AimSubclassTypeHeader);
		addSearchField(sb, subclassData, "ParentAnnotationId", nFields++,
			StringType, ParentIaIdHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processAssessorCriteria(spec, subclassData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchIaAssessorQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("ImageAnnotation");

		String annoData = "icr:aimImageAnnotationData";
		addRootElement(sb, annoData);
		int nFields = addAssessorSearchFields(sb, annoData);

		addSearchField(sb, annoData, AimUid, nFields++, StringType,
			AimImageAnnotationUidHeader);
		addSearchField(sb, annoData, "ParentIacId", nFields++, StringType,
			ParentIacIdHeader);
		addSearchField(sb, annoData, AssociatedRegionSetId, nFields++,
			StringType, AssociatedRegionSetIdHeader);
		addSearchField(sb, annoData, AnnotationRoleCount, nFields++,
			IntegerType, AnnotationRoleCountHeader);
		addSearchField(sb, annoData, CalculationCount, nFields++,
			IntegerType, CalculationCountHeader);
		addSearchField(sb, annoData, ImagingObservationCount, nFields++,
			IntegerType, ImagingObservationCountHeader);
		addSearchField(sb, annoData, ImagingPhysicalCount, nFields++,
			IntegerType, ImagingPhysicalCountHeader);
		addSearchField(sb, annoData, InferenceCount, nFields++,
			IntegerType, InferenceCountHeader);
		addSearchField(sb, annoData, MarkupCount, nFields++,
			IntegerType, MarkupCountHeader);
		addSearchField(sb, annoData, SegmentationCount, nFields++,
			IntegerType, SegmentationCountHeader);
		addSearchField(sb, annoData, TaskContextCount, nFields++,
			IntegerType, TaskContextCountHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processAssessorCriteria(spec, annoData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchIacAssessorQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("ImageAnnotationCollection");

		String annoColData = "icr:aimImageAnnCollData";
		addRootElement(sb, annoColData);
		int nFields = addAssessorSearchFields(sb, annoColData);

		addSearchField(sb, annoColData, AimUid, nFields++, StringType,
			AimImageAnnotationCollectionUidHeader);
		addSearchField(sb, annoColData, AssociatedRegionSetId, nFields++,
			StringType, AssociatedRegionSetIdHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processAssessorCriteria(spec, annoColData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchIacQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("ImageAnnotationCollection");

		String annoColData = "icr:aimImageAnnCollData";
		addRootElement(sb, annoColData);
		addSearchField(sb, annoColData, Project, 0, StringType,
			ProjectIdHeader);
		addSearchField(sb, annoColData, XnatSubjectId, 1, StringType,
			SubjectIdHeader);
		addSearchField(sb, annoColData, AssessorSessionId, 2, StringType,
			SessionIdHeader);
		addSearchField(sb, annoColData, ID, 3, StringType,
			AimImageAnnotationCollectionIdHeader);
		addSearchField(sb, annoColData, AimUid, 4, StringType,
			AimImageAnnotationCollectionUidHeader);
		addSearchField(sb, annoColData, AssociatedRegionSetId, 5, StringType,
			AssociatedRegionSetIdHeader);
		addSearchField(sb, annoColData, Label, 6, StringType, Label);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSearchCriteria(spec, annoColData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchProjectQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("Project");

		String projectData = "xnat:projectData";
		addRootElement(sb, projectData);
		addSearchField(sb, projectData, ID, 0, StringType, ProjectIdHeader);
		addSearchField(sb, projectData, Name, 1, StringType, ProjectLabelHeader);
		addSearchField(sb, projectData, Keywords, 2, StringType,
			ProjectKeywordsHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSearchCriteria(spec, projectData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchRegionAssessorQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("Region");

		String regionData = "icr:regionData";
		addRootElement(sb, regionData);
		int nFields = addAssessorSearchFields(sb, regionData);

		addSearchField(sb, regionData, AssociatedRegionSetId, nFields++,
			StringType, AssociatedRegionSetIdHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processAssessorCriteria(spec, regionData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchRegionSetAssessorQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("RegionSet");

		String rsData = "icr:regionSetData";
		addRootElement(sb, rsData);
		int nFields = addAssessorSearchFields(sb, rsData);

		addSearchField(sb, rsData, rsData+"/"+RegionSetOriginalType, nFields++,
			StringType, OriginalTypeHeader);
		addSearchField(sb, rsData, RegionCount, nFields++, IntegerType,
			RegionCountHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processAssessorCriteria(spec, rsData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchScanQuery(SearchSpecification spec,
		String modality) throws XnatException
	{
		StringBuilder sb = createInitialXml("Scan");

		String prefix = modalityMap.get(modality);
//		String scanData = scanData(prefix);
		String scanData = "xnat:imageScanData";
		System.out.println("scanData: "+scanData);
		String sessionData = sessionData(prefix);
		addRootElement(sb, scanData);
		addSearchField(sb, scanData, ID, 0, StringType, ScanIdHeader);
		addSearchField(sb, scanData, ScanSessionId, 1, StringType,
			ScanSessionIdHeader);
		addSearchField(sb, scanData, ScanDescription, 2, StringType,
			ScanDescHeader);
		addSearchField(sb, scanData, ScanStartTime, 3, StringType,
			ScanStartTimeHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processScanCriteria(spec, scanData, prefix, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchSessionQuery(SearchSpecification spec,
		String modality) throws XnatException
	{
		StringBuilder sb = createInitialXml("Session");

		String sessionData = sessionData(modalityMap.get(modality));
		addRootElement(sb, sessionData);
		addSearchField(sb, sessionData, ID, 0, StringType, SessionIdHeader);
		addSearchField(sb, sessionData, SessionSubjectId, 0, StringType,
			SessionSubjectIdHeader);
		addSearchField(sb, sessionData, SessionProjectId, 0, StringType,
			SessionProjectIdHeader);
		addSearchField(sb, sessionData, Label, 1, StringType, SessionLabelHeader);
		addSearchField(sb, sessionData, Date, 2, StringType, SessionDateHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSessionCriteria(spec, sessionData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchSubjectQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = createInitialXml("Subject");

		String subjectData = "xnat:subjectData";
		addRootElement(sb, subjectData);
		addSearchField(sb, subjectData, ID, 0, StringType, SubjectIdHeader);
		addSearchField(sb, subjectData, Name, 1, StringType, SubjectNameHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSearchCriteria(spec, subjectData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");
		return sb.toString();
	}

	private XnatFileUriMap createSeriesFileUriMap(XnatId id)
		throws XnatException
	{
		String query = new StringBuilder("/data/archive/projects/")
			.append(id.getProjectId())
			.append("/subjects/").append(id.getSubjectId())
			.append("/experiments/").append(id.getSessionId())
			.append("/scans/").append(id.getScanId())
			.append("/resources/DICOM/files").toString();
		// Get list of files to compare to cache
		XnatResultSet fileRs = xsc.getResultSet(query+"?format=xml");
			
		XnatFileUriMap map = new DefaultXnatFileUriMap(id);
		int fileIdx = fileRs.getColumnIndex(Name);
		int uriIdx = fileRs.getColumnIndex(URI);
		int sizeIdx = fileRs.getColumnIndex(Size);
		int nFiles = fileRs.getRowCount();
		String projectStr = "/projects/"+id.getProjectId()+"/";
		String subjectStr = "/subjects/"+id.getSubjectId()+"/";
		String sessionStr = "/experiments/"+id.getSessionId()+"/";
		String scanStr = "/scans/"+id.getScanId()+"/";
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<nFiles; i++)
		{
			String filename = fileRs.get(i, fileIdx);
			String uri = fileRs.get(i, uriIdx);
			int size = fileRs.getInt(i, sizeIdx);
			if (uri.contains(projectStr) &&
				 uri.contains(subjectStr) &&
				 uri.contains(sessionStr) &&
				 uri.contains(scanStr) &&
				 uri.endsWith(filename) &&
				 size > 0)
			{
				sb.setLength(0);
				sb.append(id.getProjectId()).append(File.separator)
					.append(id.getSubjectId()).append(File.separator)
					.append(id.getSessionId()).append(File.separator)
					.append(id.getScanId()).append(File.separator)
					.append(filename);
				map.add(filename, sb.toString(), size);
			}
			else
			{
				throw new XnatException("File ResultSet contains invalid entry",
					XnatCode.InvalidFileUri);
			}
		}

		return map;
	}

	private String createSeriesSearchXml(String projectId, String uid,
		String modality)
	{
		StringBuilder sb = createInitialXml("Series By UID");

		String sessionData = sessionData(modality);
		String scanData = scanData(modality);
		addRootElement(sb, scanData);
		addSearchField(sb, scanData, "ID", 0, StringType, ScanIdHeader);
		addSearchField(sb, scanData, "TYPE", 1, StringType, ScanTypeHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		addCriterion(sb, sessionData+"/"+Project, Equals, projectId);
		addCriterion(sb, scanData+"/UID", Equals, uid);
		sb.append("	</xdat:search_where>\n");

		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSessionIdSearchXml(String sessionId, String modality)
	{
		StringBuilder sb = createInitialXml("SessionID");

		String sessionData = sessionData(modality);
		addRootElement(sb, sessionData);
		addSearchField(sb, sessionData, Project, 0, StringType,
			ProjectIdHeader);
		addSearchField(sb, sessionData, SubjectId, 1, StringType,
			SubjectIdHeader);
		addSearchField(sb, sessionData, ID, 1, StringType,
			SessionIdHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		addCriterion(sb, sessionData+"/"+ID, Equals, sessionId);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createStudyBySeriesUidSearchXml(String projectId, String uid,
		String modality)
	{
		StringBuilder sb = createInitialXml("Study By SeriesUID");

		String sessionData = sessionData(modality);
		String scanData = scanData(modality);
		addRootElement(sb, sessionData);
		addSearchField(sb, sessionData, "PROJECT", 0, StringType,
			ProjectIdHeader);
		addSearchField(sb, sessionData, "subject_ID", 1, StringType,
			SubjectIdHeader);
		addSearchField(sb, sessionData, "ID", 2, StringType, SessionIdHeader);
		addSearchField(sb, sessionData, "LABEL", 3, StringType, SessionLabelHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		addCriterion(sb, sessionData+"/PROJECT", Equals, projectId);
		addCriterion(sb, scanData+"/UID", Equals, uid);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private XnatFileUriMap createStudyFileUriMap(XnatId studyId)
		throws XnatException
	{
		String seriesQuery = new StringBuilder("/data/archive/projects/")
			.append(studyId.getProjectId())
			.append("/subjects/").append(studyId.getSubjectId())
			.append("/experiments/").append(studyId.getSessionId())
			.append("/scans").toString();
		// Get list of files to compare to cache
		XnatResultSet seriesRs = xsc.getResultSet(seriesQuery+"?format=xml");
		int nSeries = seriesRs.getRowCount();
		XnatFileUriMap fileUriMap = new DefaultXnatFileUriMap(studyId);

		for (int i=0; i<nSeries; i++)
		{
			XnatId seriesId = new XnatId(studyId.getProjectId(),
				studyId.getSubjectId(), studyId.getSessionId(),
				Integer.toString(seriesRs.getInt(i, ID)));
			XnatFileUriMap seriesFUMap = createSeriesFileUriMap(seriesId);
			fileUriMap.addAll(seriesFUMap);
		}
		return fileUriMap;
	}

	private String createStudySearchXml(String projectId, String uid, String modality)
	{
		StringBuilder sb = createInitialXml("Study By UID");

		String sessionData = sessionData(modality);
		addRootElement(sb, sessionData);
		addSearchField(sb, sessionData, "PROJECT", 0, StringType,
			ProjectIdHeader);
		addSearchField(sb, sessionData, "subject_ID", 1, StringType,
			SubjectIdHeader);
		addSearchField(sb, sessionData, "ID", 2, StringType, SessionIdHeader);
		addSearchField(sb, sessionData, "LABEL", 3, StringType, SessionLabelHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		addCriterion(sb, sessionData+"/PROJECT", Equals, projectId);
		addCriterion(sb, sessionData+"/UID", Equals, uid);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	// TODO: Fix for multiple results
	private XnatResultSet fetchMarkupRs(String projectId, String markupUid)
		throws XnatException
	{
		String query = createRtStructForMarkupQuery(projectId, markupUid);
		XnatResultSet markupRs = xsc.getResultSet("/data/search?format=xml",
			query);
		if ((markupRs == null) || (markupRs.getRowCount() != 1))
		{
			throw new XnatException("No unique result for Markup UID: "+markupUid,
				XnatCode.NoUniqueResult);
		}
		return markupRs;
	}

	private Series fetchSeries(XnatId seriesId, String uid) throws XnatException
	{
		Series series = null;
		InputStream is = null;
		try
		{
			logger.info("Checking cache");
			XnatFileUriMap fileUriMap = createSeriesFileUriMap(seriesId);
			series = cache.getSeries(uid, seriesId);
			// Not found in cache, pull data from server and cache it
			if (series == null)
			{
				// Not found in cache, pull data from server and cache it
				logger.info("Cache miss, fetching from server");
				fetchSeriesToCache(seriesId, fileUriMap);
				series = cache.getSeries(uid, seriesId);
			}
			else
			{
				// Check the cache matches the server's version
				try
				{
					validateSeries(series, fileUriMap);
				}
				catch (XnatException ex)
				{
					// Cached series may have been modified server side.
					if (!ex.getCode().equals(XnatCode.FileNotFound))
					{
						throw ex;
					}
					logger.info("Cache invalid, refetching from server");
					cache.delete(seriesId);
					fetchSeriesToCache(seriesId, fileUriMap);
					series = cache.getSeries(uid, seriesId);
				}
			}
			validateSeries(series, fileUriMap);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return series;
	}

	private void fetchSeriesToCache(XnatId seriesId, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		logger.info("Retrieving series from server");
		String query = new StringBuilder("/data/archive/projects/")
			.append(seriesId.getProjectId())
			.append("/subjects/").append(seriesId.getSubjectId())
			.append("/experiments/").append(seriesId.getSessionId())
			.append("/scans/").append(seriesId.getScanId())
			.append("/resources/DICOM/files").toString();
		InputStream is = xsc.get(query+"?format=zip");
		cache.insert(is, fileUriMap);
	}

	private Study fetchStudy(XnatId studyId, String uid) throws XnatException
	{
		Study study = null;
		InputStream is = null;
		try
		{
			logger.info("Fetching studyId: <"+studyId.getProjectId()+","+
				studyId.getSubjectId()+","+studyId.getSessionId()+">");
			XnatFileUriMap fileUriMap = createStudyFileUriMap(studyId);
			logger.info("Checking cache");
			study = cache.getStudy(uid, studyId);
			if (study == null)
			{
				// Not found in cache, pull data from server and cache it
				logger.info("Cache miss, fetching from server");
				fetchStudyToCache(studyId, fileUriMap);
				study = cache.getStudy(uid, studyId);
			}
			else
			{
				// Check the cache matches the server's version
				try
				{
					validateStudy(study, fileUriMap);
				}
				catch (XnatException ex)
				{
					// Cached study could be only a partial version e.g. if a series
					// in the study has been fetched previously or the study has been
					// modified server side.
					if (!ex.getCode().equals(XnatCode.FileNotFound))
					{
						throw ex;
					}
					logger.info("Cache invalid, refetching from server");
					cache.delete(studyId);
					fetchStudyToCache(studyId, fileUriMap);
					study = cache.getStudy(uid, studyId);
				}
			}
			validateStudy(study, fileUriMap);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return study;
	}

	private void fetchStudyToCache(XnatId studyId, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		logger.info("Retrieving study from server");
		String query = new StringBuilder("/data/archive/projects/")
			.append(studyId.getProjectId())
			.append("/subjects/").append(studyId.getSubjectId())
			.append("/experiments/").append(studyId.getSessionId())
			.append("/scans/ALL/resources/DICOM/files").toString();
		InputStream is = xsc.get(query+"?format=zip");
		cache.insert(is, fileUriMap);
	}

	private void findIdsToDelete(XnatEntity entity, Map<XnatId,String> ids)
	{
		if (entity == null)
		{
			return;
		}
		switch (entity.getType())
		{
			case XnatEntity.ImageAnnotationCollection:
				XnatImageAnnotationCollection iac =
					(XnatImageAnnotationCollection) entity;
				for (XnatImageAnnotation ia : iac.getImageAnnotations())
				{
					findIdsToDelete(ia, ids);
				}
				findIdsToDelete(iac.getRegionSet(), ids);
				logger.debug("IAC to delete: "+iac.getXnatId());
				ids.put(iac.getXnatId(), XnatEntity.Assessor);
				break;

			case XnatEntity.ImageAnnotation:
				XnatImageAnnotation ia = (XnatImageAnnotation) entity;
				for (XnatAnnotationItem item : ia.getAnnotationItems())
				{
					logger.debug(item.getType()+" to delete: "+item.getXnatId());
					ids.put(item.getXnatId(), XnatEntity.Assessor);
				}
				logger.debug("IA to delete: "+ia.getXnatId());
				ids.put(ia.getXnatId(), XnatEntity.Assessor);
				break;

			case XnatEntity.AnnotationRole:
			case XnatEntity.Calculation:
			case XnatEntity.ImageObservation:
			case XnatEntity.ImagePhysical:
			case XnatEntity.Inference:
			case XnatEntity.Markup:
			case XnatEntity.Segmentation:
			case XnatEntity.TaskContext:
			case XnatEntity.Region:
				logger.debug(entity.getType()+" to delete: "+entity.getXnatId());
				ids.put(entity.getXnatId(), XnatEntity.Assessor);
				break;

			case XnatEntity.RegionSet:
				XnatRegionSet rs = (XnatRegionSet) entity;
				for (XnatRegion region : rs.getRegions())
				{
					logger.debug(region.getType()+" to delete: "+region.getXnatId());
					ids.put(region.getXnatId(), XnatEntity.Assessor);
				}
				logger.debug("RegionSet to delete: "+rs.getXnatId());
				ids.put(rs.getXnatId(), XnatEntity.Assessor);
				break;

			default:
				logger.warn("Unknown XnatEntity type: "+entity.getType());
		}
	}

	private ImageAnnotationCollection getIac(XnatId xnatId, String iacId,
		String iacUid)
	{
		ImageAnnotationCollection iac = null;
		InputStream is = null;
		try
		{
			// Wasteful extra query just to get file name
			String baseQuery = new StringBuilder("/data/archive/projects/")
				.append(xnatId.getProjectId())
				.append("/subjects/").append(xnatId.getSubjectId())
				.append("/experiments/").append(xnatId.getSessionId())
				.append("/assessors/").append(iacId)
				.append("/resources/AIM-INSTANCE/files").toString();
			XnatResultSet nameRs = xsc.getResultSet(baseQuery+"?format=xml");
			if ((nameRs == null) || (nameRs.getRowCount() != 1))
			{
				logger.warn("No unique result for IAC ID: "+iacId);
				return null;
//				throw new XnatException("No unique result for IAC ID: "+iacId,
//					XnatCode.NoUniqueResult);
			}
			logger.debug("IAC XNAT ID: <"+xnatId.getProjectId()+","+
				xnatId.getSubjectId()+","+xnatId.getSessionId()+
				"> IAC ID: "+iacId);
			String name = nameRs.get(0, Name);
			// Retrieve the file
			String query = new StringBuilder(baseQuery).append("/").append(name)
				.append("?format=xml").toString();
			is = xsc.get(query);
			XmlParser parser = aimToolkit.createXmlParser();
			iac = parser.parse(is);
			if (!iac.getUid().equals(iacUid))
			{
				throw new XnatException("Returned IAC doesn't match UID: "+iacUid,
					XnatCode.UidMismatch);
			}
		}
		catch (IOException | XmlException | XnatException ex)
		{
			logger.warn("Error fetching IAC for XnatId: <"+
				xnatId.getProjectId()+","+xnatId.getSubjectId()+","+
				xnatId.getSessionId()+"> and IAC ID: "+iacId, ex);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return iac;
	}

	private String getOriginalType(String value)
	{
		if (value.startsWith("AIM"))
		{
			return XnatEntity.ImageAnnotationCollection;
		}
		if (value.startsWith("RT-STRUCT"))
		{
			return XnatEntity.RtStruct;
		}
		return Unknown;
	}

	private RtStruct getRtStruct(XnatId xnatId, String regionSetId)
	{
		RtStruct rtStruct = null;
		InputStream is = null;
		try
		{
			String query = new StringBuilder("/data/archive/projects/")
				.append(xnatId.getProjectId())
				.append("/subjects/").append(xnatId.getSubjectId())
				.append("/experiments/").append(xnatId.getSessionId())
				.append("/assessors/").append(regionSetId)
				.append("/resources/RT-STRUCT/files/?format=xml").toString();
			XnatResultSet rs = xsc.getResultSet(query);
			if ((rs == null) || (rs.getRowCount() != 1))
			{
				throw new XnatException("No unique RtStruct for RegionSetId: "+
					regionSetId);
			}
			is = xsc.get(rs.get(0, "URI"));
			DicomObject dcm = DicomUtils.readDicomObject(is);
			rtStruct = DicomToolkit.getToolkit().createRtStruct(dcm);
			logger.debug("RT-STRUCT XNAT ID: <"+xnatId.getProjectId()+","+
				xnatId.getSubjectId()+","+xnatId.getSessionId()+
				"> Region Set ID: "+regionSetId);
		}
		catch (IOException | XnatException ex)
		{
			logger.warn("Error fetching RtStruct for RegionSetId: "+regionSetId,
				ex);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return rtStruct;
	}

	private Map<XnatId,Study> getStudies(String projectId, String uid,
		String modality)
	{
		Map<XnatId,Study> studyMap = new HashMap<>();
		if (!modalityMap.containsKey(modality))
		{
			logger.error("Unsupported modality: {}", modality);
			return studyMap;
		}
		logger.info("Searching for {} study. UID: {}", modality, uid);
		Set<XnatId> studyIdSet = getXnatStudyIds(projectId, uid, modality);
		if (studyIdSet.isEmpty())
		{
			return studyMap;
		}
		for (XnatId id : studyIdSet)
		{
			try
			{
				Study study = fetchStudy(id, uid);
				studyMap.put(id, study);
			}
			catch (XnatException ex)
			{
				logger.error("Study search error: ", ex);
			}
		}
		logger.info("Study search complete: {}", !studyMap.isEmpty() ? "OK" : "Failed");
		return studyMap;
	}

	private Set<XnatId> getXnatSeriesIds(String projectId, String uid,
		String modality)
	{
		Set<XnatId> idSet = new HashSet<>();
		try
		{
			String search = "/data/search?format=xml";
			// XNAT won't return all info in one query
			// Get ID down to study level
			String studySearchXml = createStudyBySeriesUidSearchXml(projectId, uid,
				modalityMap.get(modality));
			XnatResultSet studySearchRs = xsc.getResultSet(search, studySearchXml);
			if (studySearchRs.getRowCount() == 0)
			{
				logger.info(String.format(
					"No Study found for project %s, modality %s and UID %s",
					projectId, modality, uid));
				return idSet;
			}
			for (int i=0; i<studySearchRs.getRowCount(); i++)
			{
				XnatId xnatSessionId = xnatIdFromResultSet(studySearchRs, i);
				// Search for series ID
				String searchXml = createSeriesSearchXml(projectId, uid,
					modalityMap.get(modality));
				XnatResultSet searchRs = xsc.getResultSet(search, searchXml);
				if (searchRs.getRowCount() == 0)
				{
					logger.info(String.format(
						"No Series found for project %s, modality %s and UID %s",
						projectId, modality, uid));
					continue;
				}
				for (int j=0; j<searchRs.getRowCount(); j++)
				{
					String scanId = searchRs.get(j, searchRs.getColumnIndexByHeader(
						ScanIdHeader));
					XnatId id = new XnatId(projectId, xnatSessionId.getSubjectId(),
						xnatSessionId.getSessionId(), scanId);
					logger.debug("Series XNAT ID: <"+id.getProjectId()+","+
						id.getSubjectId()+","+id.getSessionId()+","+id.getScanId()+">");
					idSet.add(id);
				}
			}
		}
		catch (XnatException ex)
		{
			logger.error("Scan search error", ex);
		}
		return idSet;
	}

	private XnatId getXnatSessionId(String sessionId, String modality)
	{
		XnatId id = null;
		try
		{
			String search = "/data/search?format=xml";
			// XNAT won't return all info in one query
			// Get ID down to study level
			String xml = createSessionIdSearchXml(sessionId,
				modalityMap.get(modality));
			XnatResultSet rs = xsc.getResultSet(search, xml);
			if (rs.getRowCount() != 1)
			{
				logger.info(String.format(
					"No unique Session found for ID %s, modality %s",
					sessionId, modality));
				return id;
			}
			id = xnatIdFromResultSet(rs);
		}
		catch (XnatException ex)
		{
			logger.error("Scan search error", ex);
		}
		return id;
	}

	private Set<XnatId> getXnatStudyIds(String projectId, String uid, String modality)
	{
		Set<XnatId> idSet = new HashSet<>();
		try
		{
			String search = "/data/search?format=xml";
			// Get ID down to study level
			String studySearchXml = createStudySearchXml(projectId, uid,
				modalityMap.get(modality));
			XnatResultSet studySearchRs = xsc.getResultSet(search, studySearchXml);
			if (studySearchRs.getRowCount() == 0)
			{
				logger.info("No unique result found for modality {} and UID {}",
					modality, uid);
				return idSet;
			}
			for (int i=0; i<studySearchRs.getRowCount(); i++)
			{
//				String projectId = studySearchRs.get(i, 
//					studySearchRs.getColumnIndexByHeader(ProjectIdHeader));
				String subjectId = studySearchRs.get(i,
					studySearchRs.getColumnIndexByHeader(SubjectIdHeader));
				String sessionId = studySearchRs.get(i,
					studySearchRs.getColumnIndexByHeader(SessionIdHeader));
				XnatId id = new XnatId(projectId, subjectId, sessionId);
				logger.debug("Study XNAT ID: <"+id.getProjectId()+","+
					id.getSubjectId()+","+id.getSessionId()+">");
				idSet.add(id);
			}
		}
		catch (XnatException ex)
		{
			logger.error("Scan search error", ex);
		}
		return idSet;
	}

	private void nestAssessors(List<XnatAssessor> assessors,
		List<XnatImageAnnotationCollection> iacs,
		List<XnatImageAnnotation> ias,
		List<XnatAnnotationItem> annoItems,
		List<XnatRegionSet> regionSets,
		List<XnatRegion> regions)
	{
		Map<String,XnatRegionSet> regionSetMap = new HashMap<>();
		for (XnatRegionSet regionSet : regionSets)
		{
			regionSetMap.put(regionSet.getId(), regionSet);
		}
		// RegionSets may contain multiple Regions
		Map<String,Map<String,XnatRegion>> regionRsMap = new HashMap<>();
		for (XnatRegion region : regions)
		{
			Map<String,XnatRegion> itemMap =
				regionRsMap.get(region.getRegionSetId());
			if (itemMap == null)
			{
				itemMap = new HashMap<>();
				regionRsMap.put(region.getRegionSetId(), itemMap);
			}
			itemMap.put(region.getId(), region);
		}
		Map<String,XnatImageAnnotation> iaMap = new HashMap<>();
		for (XnatImageAnnotation ia : ias)
		{
			iaMap.put(ia.getRegionSetId(), ia);
		}
		// ImageAnnotations may contain multiple items
		Map<String,Map<String,XnatAnnotationItem>> itemIaMap = new HashMap<>();
		for (XnatAnnotationItem item : annoItems)
		{
			Map<String,XnatAnnotationItem> itemMap = itemIaMap.get(item.getIaId());
			if (itemMap == null)
			{
				itemMap = new HashMap<>();
				itemIaMap.put(item.getIaId(), itemMap);
			}
			itemMap.put(item.getId(), item);
		}
		// IACs are top level assessors
		assessors.addAll(iacs);
		for (XnatImageAnnotationCollection iac : iacs)
		{
			String regionSetId = iac.getRegionSetId();
			XnatRegionSet regionSet = regionSetMap.get(regionSetId);
			if (regionSet != null)
			{
				iac.setRegionSet(regionSet);
				regionSetMap.remove(regionSetId);
				nestRegionSet(regionSet, regionRsMap);
			}
			XnatImageAnnotation ia = iaMap.get(regionSetId);
			if (ia != null)
			{
				iac.addImageAnnotation(ia);
				iaMap.remove(regionSetId);
				nestImageAnnotations(ia, itemIaMap);
			}
		}
		// Anything left in iaMap, itemIaMap is orphaned, add at top level
		assessors.addAll(iaMap.values());
		for (Map<String,XnatAnnotationItem> itemMap : itemIaMap.values())
		{
			assessors.addAll(itemMap.values());
		}
		// Anything left in regionSetMap is orphaned or independent of AIM objects
		// so add at top level
		assessors.addAll(regionSetMap.values());
		// Anything left in regionRsMap is orphaned, add at top level
		for (Map<String,XnatRegion> regionMap : regionRsMap.values())
		{
			assessors.addAll(regionMap.values());
		}
	}

	private void nestImageAnnotations(XnatImageAnnotation ia,
		Map<String,Map<String,XnatAnnotationItem>> itemIaMap)
	{
		Map<String,XnatAnnotationItem> itemMap = itemIaMap.get(ia.getId());
		if ((itemMap == null) || itemMap.isEmpty())
		{
			return;
		}
		Iterator<String> iter = itemMap.keySet().iterator();
		while (iter.hasNext())
		{
			String key = iter.next();
			XnatAnnotationItem item = itemMap.get(key);
			ia.addAnnotationItem(item);
			iter.remove();
		}
	}

	private void nestRegionSet(XnatRegionSet regionSet,
		Map<String,Map<String,XnatRegion>> regionRsMap)
	{
		Map<String,XnatRegion> regionMap = regionRsMap.get(regionSet.getId());
		if ((regionMap == null) || regionMap.isEmpty())
		{
			return;
		}
		Iterator<String> iter = regionMap.keySet().iterator();
		while (iter.hasNext())
		{
			String key = iter.next();
			XnatRegion region = regionMap.get(key);
			regionSet.addRegion(region);
			iter.remove();
		}
	}

	private Investigator parseInvestigator(Node node)
	{
		String firstName = null;
		String lastName = null;
		String title = null;
		String institution = null;
		String dept = null;
		String email = null;
		String phone = null;
		NodeList children = node.getChildNodes();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			switch (child.getNodeName())
			{
				case "text#":
					continue;
				case "xnat:firstname":
					firstName = child.getTextContent().trim();
					break;
				case "xnat:lastname":
					lastName = child.getTextContent().trim();
					break;
				case "xnat:title":
					title = child.getTextContent().trim();
					break;
				case "xnat:institution":
					institution = child.getTextContent().trim();
					break;
				case "xnat:department":
					dept = child.getTextContent().trim();
					break;
				case "xnat:email":
					email = child.getTextContent().trim();
					break;
				case "xnat:phone":
					phone = child.getTextContent().trim();
					break;
				default:
			}
		}
		Investigator investigator = null;
		if ((firstName != null) && !firstName.isEmpty() &&
			 (lastName != null) && !lastName.isEmpty())
		{
			investigator = new Investigator(firstName, lastName);
			investigator.setTitle(title);
			investigator.setInstitution(institution);
			investigator.setDepartment(dept);
			investigator.setEmail(email);
			investigator.setPhoneNumber(phone);			
		}
		return investigator;
	}

	private StringBuilder processAssessorCriteria(SearchSpecification spec,
		String searchData, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.SessionID:
					if (crit.getComparator() == SearchCriterion.Like)
					{
						addCriterion(sb, searchData+"/"+AssessorSessionId,
							crit.createComparatorSql(), "%"+crit.getValue()+"%");
					}
					else
					{
						addCriterion(sb, searchData+"/"+AssessorSessionId,
							crit.createComparatorSql(), crit.getValue());						
					}
					break;

//				case XnatTag.SessionDate:
//					addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
//						"%"+crit.getValue()+"%");
//					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private StringBuilder processScanCriteria(SearchSpecification spec,
		String searchData, String modality, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.SessionID:
					String sessionData = sessionData(modality);
					addCriterion(sb, sessionData+"/"+ID, crit);
//					if (crit.getComparator() == SearchCriterion.Like)
//					{
//						addCriterion(sb, sessionData+"/"+ID,
//							crit.createComparatorSql(), "%"+crit.getValue()+"%");
//					}
//					else
//					{
//						addCriterion(sb, sessionData+"/"+ID,
//							crit.createComparatorSql(), crit.getValue());						
//					}
					break;

				case XnatTag.ProjectID:
					addCriterion(sb, searchData+"/"+ScanProjectId, crit);
					break;

				case XnatTag.Uid:
					addCriterion(sb, searchData+"/"+Uid, crit);
					break;

//				case XnatTag.SeriesDescription:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.Scanner:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.ScannerManufacturer:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.ScannerModel:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.SessionDate:
//					addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
//						"%"+crit.getValue()+"%");
//					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private StringBuilder processSearchCriteria(SearchSpecification spec,
		String searchData, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.ProjectID:
					addCriterion(sb, searchData+"/"+Project, Equals,
						crit.getValue());
					break;

				case XnatTag.SubjectID:
					addCriterion(sb, searchData+"/"+XnatSubjectId, Like,
						"%"+crit.getValue()+"%");
					break;

				case XnatTag.SubjectName:
					addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
						"%"+crit.getValue()+"%");
					break;

				case XnatTag.SessionID:
					addCriterion(sb, searchData+"/"+AssessorSessionId, Equals,
						crit.getValue());
					break;

				case XnatTag.Label:
					addCriterion(sb, searchData+"/"+Label, Equals,
						crit.getValue());
					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private StringBuilder processSessionCriteria(SearchSpecification spec,
		String searchData, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.SubjectID:
					if (crit.getComparator() == SearchCriterion.Like)
					{
						addCriterion(sb, searchData+"/"+SessionSubjectId,
							crit.createComparatorSql(), "%"+crit.getValue()+"%");
					}
					else
					{
						addCriterion(sb, searchData+"/"+SessionSubjectId,
							crit.createComparatorSql(), crit.getValue());						
					}
					break;

//				case XnatTag.SessionDate:
//					addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
//						"%"+crit.getValue()+"%");
//					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private List<XnatAnnotationItem> queryAnnotationItemAssessors(String query)
		throws XnatException
	{
		List<XnatAnnotationItem> items = new ArrayList<>();
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		// Generic assessor stuff
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int idIdx = rs.getColumnIndexByHeader(AssessorIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(AssessorLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(AssessorDateHeader);
		int timeIdx = rs.getColumnIndexByHeader(AssessorTimeHeader);

		// Subclass specific stuff
		int iaUidIdx = rs.getColumnIndexByHeader(
			AimImageAnnotationUidHeader);
		int typeIdx = rs.getColumnIndexByHeader(
			AimSubclassTypeHeader);
		int parentIdIdx = rs.getColumnIndexByHeader(
			ParentIaIdHeader);

		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(rs.get(i, projIdx),
				rs.get(i, subjIdIdx),
				rs.get(i, sessionIdx),
				rs.get(i, idIdx));
			String parentId = Unknown;
			if (parentIdIdx >= 0)
			{
				parentId = rs.get(i, parentIdIdx);
			}
			XnatAnnotationItem item = new XnatAnnotationItem(
				xnatId,
				rs.get(i, labelIdx),
				rs.get(i, iaUidIdx),
				parentId,
				rs.get(i, typeIdx));
			item.setDate(rs.get(i, dateIdx));
			item.setTime(rs.get(i, timeIdx));
			items.add(item);
		}
		return items;
	}

	private List<XnatImageAnnotation> queryIaAssessors(String query)
		throws XnatException
	{
		List<XnatImageAnnotation> ias = new ArrayList<>();
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		// Generic assessor stuff
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int idIdx = rs.getColumnIndexByHeader(AssessorIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(AssessorLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(AssessorDateHeader);
		int timeIdx = rs.getColumnIndexByHeader(AssessorTimeHeader);

		// IA specific stuff
		int iaUidIdx = rs.getColumnIndexByHeader(
			AimImageAnnotationUidHeader);
		int regionSetIdx = rs.getColumnIndexByHeader(
			AssociatedRegionSetIdHeader);
		int parentIdIdx = rs.getColumnIndexByHeader(
			ParentIacIdHeader);
		int annoRoleCountIdx = rs.getColumnIndexByHeader(
			AnnotationRoleCountHeader);
		int calcCountIdx = rs.getColumnIndexByHeader(
			CalculationCountHeader);
		int imageObsCountIdx = rs.getColumnIndexByHeader(
			ImagingObservationCountHeader);
		int imagePhysCountIdx = rs.getColumnIndexByHeader(
			ImagingPhysicalCountHeader);
		int infCountIdx = rs.getColumnIndexByHeader(
			InferenceCountHeader);
		int markupCountIdx = rs.getColumnIndexByHeader(
			MarkupCountHeader);
		int segCountIdx = rs.getColumnIndexByHeader(
			SegmentationCountHeader);
		int taskCtxCountIdx = rs.getColumnIndexByHeader(
			TaskContextCountHeader);

		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(rs.get(i, projIdx),
				rs.get(i, subjIdIdx),
				rs.get(i, sessionIdx),
				rs.get(i, idIdx));
			String parentId = Unknown;
			if (parentIdIdx >= 0)
			{
				parentId = rs.get(i, parentIdIdx);
			}
			XnatImageAnnotation xia =
				new XnatImageAnnotation(xnatId, rs.get(i, labelIdx),
					rs.get(i, iaUidIdx), parentId);
			xia.setDate(rs.get(i, dateIdx));
			xia.setTime(rs.get(i, timeIdx));
			xia.setRegionSetId(rs.get(i, regionSetIdx));
			try
			{
				xia.setSchemaAnnotationRoleCount(rs.getInt(i, annoRoleCountIdx));
				xia.setSchemaCalculationCount(rs.getInt(i, calcCountIdx));
				xia.setSchemaImagingObservationCount(rs.getInt(i, imageObsCountIdx));
				xia.setSchemaImagingPhysicalCount(rs.getInt(i, imagePhysCountIdx));
				xia.setSchemaInferenceCount(rs.getInt(i, infCountIdx));
				xia.setSchemaMarkupCount(rs.getInt(i, markupCountIdx));
				xia.setSchemaSegmentationCount(rs.getInt(i, segCountIdx));
				xia.setSchemaTaskContextCount(rs.getInt(i, taskCtxCountIdx));
			}
			catch (NumberFormatException ex)
			{
				logger.warn(
					"Child count error for XnatImageAnnotation ID: "+xia.getId());
			}
			ias.add(xia);
		}
		return ias;
	}

	private List<XnatImageAnnotationCollection> queryIacAssessors(String query)
		throws XnatException
	{
		List<XnatImageAnnotationCollection> iacs = new ArrayList<>();
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		// Generic assessor stuff
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int idIdx = rs.getColumnIndexByHeader(AssessorIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(AssessorLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(AssessorDateHeader);
		int timeIdx = rs.getColumnIndexByHeader(AssessorTimeHeader);

		// IAC specific stuff
		int iacUidIdx = rs.getColumnIndexByHeader(
			AimImageAnnotationCollectionUidHeader);
		int regionSetIdx = rs.getColumnIndexByHeader(
			AssociatedRegionSetIdHeader);

		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(rs.get(i, projIdx),
				rs.get(i, subjIdIdx),
				rs.get(i, sessionIdx),
				rs.get(i, idIdx));
			XnatImageAnnotationCollection xiac =
				new XnatImageAnnotationCollection(xnatId, rs.get(i, labelIdx),
					rs.get(i, iacUidIdx), rs.get(i, regionSetIdx));
			xiac.setDate(rs.get(i, dateIdx));
			xiac.setTime(rs.get(i, timeIdx));
			iacs.add(xiac);
		}
		return iacs;
	}

	private void queryIacs(List<ImageAnnotationCollection> iacList, String query)
		throws XnatException
	{
		XnatResultSet iacRs = xsc.getResultSet("/data/search?format=xml",
			query);
		int iacIdIdx = iacRs.getColumnIndexByHeader(
			AimImageAnnotationCollectionIdHeader);
		int iacUidIdx = iacRs.getColumnIndexByHeader(
			AimImageAnnotationCollectionUidHeader);
		int nRows = iacRs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = xnatIdFromResultSet(iacRs, i);
			ImageAnnotationCollection iac = getIac(xnatId,
				iacRs.get(i, iacIdIdx), iacRs.get(i, iacUidIdx));
			if (iac != null)
			{
				iacList.add(iac);
			}
		}
	}

	private void queryProjects(List<XnatProject> projectList, String query)
		throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int idIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(ProjectLabelHeader);
		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatProject project = new XnatProject(
				rs.get(i, idIdx), rs.get(i, labelIdx));
				projectList.add(project);
		}
	}

	private List<XnatRegion> queryRegionAssessors(String query)
		throws XnatException
	{
		List<XnatRegion> regions = new ArrayList<>();
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		// Generic assessor stuff
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int idIdx = rs.getColumnIndexByHeader(AssessorIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(AssessorLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(AssessorDateHeader);
		int timeIdx = rs.getColumnIndexByHeader(AssessorTimeHeader);

		// Region specific stuff
		int regionSetIdx = rs.getColumnIndexByHeader(AssociatedRegionSetIdHeader);

		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(rs.get(i, projIdx),
				rs.get(i, subjIdIdx),
				rs.get(i, sessionIdx),
				rs.get(i, idIdx));
			String regionSetId = Unknown;
			if (regionSetIdx >= 0)
			{
				regionSetId = rs.get(i, regionSetIdx);
			}
			XnatRegion region = new XnatRegion(xnatId, 
				rs.get(i, labelIdx),
				regionSetId);
			region.setDate(rs.get(i, dateIdx));
			region.setTime(rs.get(i, timeIdx));
			regions.add(region);
		}
		return regions;
	}

	private List<XnatRegionSet> queryRegionSetAssessors(String query)
		throws XnatException
	{
		List<XnatRegionSet> regionSets = new ArrayList<>();
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		// Generic assessor stuff
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int idIdx = rs.getColumnIndexByHeader(AssessorIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(AssessorLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(AssessorDateHeader);
		int timeIdx = rs.getColumnIndexByHeader(AssessorTimeHeader);

		// RegionSet specific stuff
		int originalTypeIdx = rs.getColumnIndexByHeader(OriginalTypeHeader);
		int regionCountIdx = rs.getColumnIndexByHeader(RegionCountHeader);

		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(rs.get(i, projIdx),
				rs.get(i, subjIdIdx),
				rs.get(i, sessionIdx),
				rs.get(i, idIdx));
			String originalType = Unknown;
			if (originalTypeIdx >= 0)
			{
				originalType = getOriginalType(rs.get(i, originalTypeIdx));
			}
			XnatRegionSet xrs =
				new XnatRegionSet(xnatId,
					rs.get(i, labelIdx),
					originalType);
			xrs.setDate(rs.get(i, dateIdx));
			xrs.setTime(rs.get(i, timeIdx));
			try
			{
				xrs.setSchemaRegionCount(rs.getInt(i, regionCountIdx));
			}
			catch (NumberFormatException ex)
			{
				logger.warn("Child count error for RegionSet ID: "+xrs.getId());
			}
			regionSets.add(xrs);
		}
		return regionSets;
	}

	private void queryScans(List<XnatScan> scanList, String query,
		String modality) throws XnatException
	{
		System.out.println(query);
		XnatResultSet scanRs = xsc.getResultSet("/data/search?format=xml", query);
		int idIdx = scanRs.getColumnIndexByHeader(ScanIdHeader);
		int sessionIdIdx = scanRs.getColumnIndexByHeader(ScanSessionIdHeader);
		int descIdx = scanRs.getColumnIndexByHeader(ScanDescHeader);
		int timeIdx = scanRs.getColumnIndexByHeader(ScanStartTimeHeader);
		int nRows = scanRs.getRowCount();
		Map<String,XnatId> sessionXnatIds = new HashMap<>();
		for (int i=0; i<nRows; i++)
		{
			String sessionId = scanRs.get(i, sessionIdIdx);
			XnatId sessionXnatId = sessionXnatIds.get(sessionId);
			if (sessionXnatId == null)
			{
				sessionXnatId = getXnatSessionId(sessionId, modality);
				sessionXnatIds.put(sessionId, sessionXnatId);
			}
			XnatId xnatId = new XnatId(sessionXnatId.getProjectId(),
				sessionXnatId.getSubjectId(),
				sessionId,
				scanRs.get(i, idIdx));
			XnatScan scan = new XnatScan(xnatId,
				scanRs.get(i, descIdx),
				modality);
			scan.setStartTime(scanRs.get(i, timeIdx));
			scanList.add(scan);
		}
	}

	private void querySessions(List<XnatSession> sessionList, String query,
		String modality) throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int idIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int subjectIdIdx = rs.getColumnIndexByHeader(SessionSubjectIdHeader);
		int projectIdIdx = rs.getColumnIndexByHeader(SessionProjectIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(SessionLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(SessionDateHeader);
		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(
				rs.get(i, projectIdIdx),
				rs.get(i, subjectIdIdx),
				rs.get(i, idIdx));
			XnatSession session = new XnatSession(
				xnatId,
				rs.get(i, labelIdx),
				rs.get(i, dateIdx),
				modality);
			sessionList.add(session);
		}
	}

	private void querySubjects(List<XnatSubject> subjectList, String query)
		throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int projectIdIdx = rs.getColumnIndexByHeader(
			ProjectIdHeader);
		int idIdx = rs.getColumnIndexByHeader(
			SubjectIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(
			SubjectNameHeader);
		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatSubject subject = new XnatSubject(
				rs.get(i, projectIdIdx), 
				rs.get(i, idIdx),
				rs.get(i, labelIdx));
				subjectList.add(subject);
		}
	}

	private String scanData(String modality)
	{
		return new StringBuilder("xnat:").append(modality).append("ScanData")
			.toString();
	}

	private void searchScan(SearchSpecification spec, String modality,
		List<XnatScan> scanList)
	{
		try
		{
			String query = createSearchScanQuery(spec, modality);
			queryScans(scanList, query, modality);
		}
		catch (XnatException ex)
		{
			logger.warn(modality+" scan search failed", ex);
		}
	}

	private void searchSession(SearchSpecification spec, String modality,
		List<XnatSession> sessionList)
	{
		try
		{
			String query = createSearchSessionQuery(spec, modality);
			querySessions(sessionList, query, modality);
		}
		catch (XnatException ex)
		{
			logger.warn(modality+" session search failed", ex);
		}
	}

	private String sessionData(String modality)
	{
		return new StringBuilder("xnat:").append(modality).append("SessionData")
			.toString();
	}

	private void validateSeries(Series series, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		List<SopInstance> sopInstList = series.getSopInstanceList();
		Set<String> usedKeys = validateSopInstances(sopInstList, fileUriMap);
		if (!usedKeys.containsAll(fileUriMap.keySet()))
		{
			throw new XnatException(
				"Required files missing from series: "+series.getUid(),
				XnatCode.FileNotFound);
		}
	}

	private Set<String> validateSopInstances(List<SopInstance> sopInstList,
		XnatFileUriMap fileUriMap) throws XnatException
	{
		Set<String> usedKeys = new HashSet<>();
		for (SopInstance sopInst : sopInstList)
		{
			File file = sopInst.getFile();
			Entry entry = fileUriMap.get(file.getName());
			if (entry == null)
			{
				throw new XnatException(
					"File doesn't exist: "+file.getAbsolutePath(),
					XnatCode.FileNotFound);
			}
			if (file.length() != entry.getSize())
			{
				throw new XnatException(
					"File size mismatch: "+file.getAbsolutePath(),
					XnatCode.FileSize);
			}
			usedKeys.add(file.getName());
		}
		return usedKeys;
	}

	private void validateStudy(Study study, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		List<Series> seriesList = study.getSeriesList();
		List<SopInstance> sopInstList = new ArrayList<>();
		for (Series series : seriesList)
		{
			sopInstList.addAll(series.getSopInstanceList());
		}
		Set<String> usedKeys = validateSopInstances(sopInstList, fileUriMap);
		if (!usedKeys.containsAll(fileUriMap.keySet()))
		{
			throw new XnatException(
				"Required files missing from study: "+study.getUid(),
				XnatCode.FileNotFound);
		}
	}

	private XnatId xnatIdFromResultSet(XnatResultSet rs)
	{
		return xnatIdFromResultSet(rs, 0);
	}

	private XnatId xnatIdFromResultSet(XnatResultSet rs, int idx)
	{
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		return new XnatId(rs.get(idx, projIdx), rs.get(idx, subjIdIdx),
			rs.get(idx, sessionIdx));
	}

}
