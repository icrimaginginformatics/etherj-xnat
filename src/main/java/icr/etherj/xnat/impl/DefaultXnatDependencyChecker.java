/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import com.google.common.collect.BiMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.SopInstance;
import icr.etherj.xnat.StudyUidContainer;
import icr.etherj.xnat.XnatDataSource;
import icr.etherj.xnat.XnatDependencyChecker;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatId;
import icr.etherj.xnat.XnatResultSet;
import icr.etherj.xnat.XnatServerConnection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultXnatDependencyChecker implements XnatDependencyChecker
{
	private static final Logger logger =
		LoggerFactory.getLogger(DefaultXnatDependencyChecker.class);

	private final XnatServerConnection conn;
	private String lastProjectId = "";
	private final Lock lock = new ReentrantLock();
	private final boolean noCache;
	private final Map<String,Map<XnatId,Series>> sessionSeriesCache = 
		new HashMap<>();
	private final Map<String,Map<String,String>> sessionSeriesUidIdCache = 
		new HashMap<>();
	private final Map<String,Map<String,BiMap<String,String>>> sessionSeriesSopInstFileCache = 
		new HashMap<>();
	private final SetMultimap<String,StudyUidContainer> studyUidContainerCache =
		LinkedHashMultimap.create();
	private final XnatDataSource xds;

	public DefaultXnatDependencyChecker(XnatDataSource xds)
	{
		this(xds, false);
	}

	public DefaultXnatDependencyChecker(XnatDataSource xds, boolean noCache)
	{
		this.xds = xds;
		this.conn = xds.getXnatServerConnection();
		this.noCache = noCache;
	}

	@Override
	public SetMultimap<String,StudyUidContainer> findStudyUidContainers(
		String projectId, Set<String> studyUids) throws XnatException
	{
		// Only one project can be cached at once so lock this method for thread
		// safety
		lock.lock();
		try
		{
			SetMultimap<String, StudyUidContainer> map = LinkedHashMultimap.create();
			if (studyUids.isEmpty())
			{
				logger.warn("No study UIDs supplied");
				return map;
			}
			logger.info("Finding containers for required study UIDs");
			if (!lastProjectId.equals(projectId) || noCache)
			{
				buildContainerMap(projectId);
			}
			logger.debug("Checking study UIDs");
			for (String uid : studyUids)
			{
				Set<StudyUidContainer> containerSet = getContainerSet(projectId, uid);
				if (containerSet.isEmpty())
				{
					logger.info("No containers found for study UID: {}", uid);
					map.clear();
					return map;
				}
				map.putAll(uid, containerSet);
			}
			logger.debug("Study UIDs ok");
			return map;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public DependencyMaps findDependencyMappings(StudyUidContainer container,
		Set<String> seriesUids, Set<String> sopInstUids) throws XnatException
	{
		// Only one project can be cached at once so lock this method for thread
		// safety
		lock.lock();
		try
		{
			logger.info("Finding dependency mappings for series/SOP instance UIDs");
			if (!lastProjectId.equals(container.getProjectId()))
			{
				clearCaches();
			}
			Map<String,Series> seriesMap = new HashMap<>();
			Map<String,String> sopInstSeriesMap = new HashMap<>();
			Set<String> seriesIdSet = new HashSet<>();
			if (!createSeriesMappings(container, seriesUids, sopInstUids,
				seriesMap, sopInstSeriesMap, seriesIdSet))
			{
				return null;
			}

			return new DependencyMaps(seriesMap, sopInstSeriesMap, seriesIdSet);
		}
		catch (XnatException ex)
		{
			logger.info("Error checking dependencies", ex);
			return null;
		}
		finally
		{
			lock.unlock();
		}
	}

	/*
	 * Only call under the Lock! lastProjectId and studyUidContainers must not
	 * change independently
	 */
	private void buildContainerMap(String projectId) throws XnatException
	{
		clearCaches();

		logger.debug("Building UID container mapping for project: {}", projectId);
		// Fetch subject label as not part of xnat:imageSessionData
		String command = "/data/archive/projects/"+projectId+"/subjects"+
			"?xsiType=xnat:subjectData"+
			"&columns=xnat:subjectData/label,xnat:subjectData/id"+
			"&format=xml";
		XnatResultSet labelRs = conn.getResultSet(command);
		int subjectIdIdx = labelRs.getColumnIndex("ID");
		int subjectLabelIdx = labelRs.getColumnIndex("label");
		// Link subject IDs and labels for stage two
		Map<String,String> subjIdLabelMap = new HashMap<>();
		for (int i=0; i<labelRs.getRowCount(); i++)
		{
			subjIdLabelMap.put(labelRs.get(i, subjectIdIdx),
				labelRs.get(i, subjectLabelIdx));
		}

		// Fetch info for all sessions in project
		command = "/data/archive/projects/"+projectId+"/experiments"+
			"?xsiType=xnat:imageSessionData"+
			"&columns=xnat:imageSessionData/UID,xnat:imageSessionData/label,xnat:imageSessionData/subject_ID"+
			"&format=xml";
		XnatResultSet sessionRs = conn.getResultSet(command);
		// Brittle but cannot specify column headers or element names AFAICT
		int sessionIdIdx = sessionRs.getColumnIndex("session_ID");
		int sessionLabelIdx = sessionRs.getColumnIndex("session_label");
		int uidIdx = sessionRs.getColumnIndex("UID");
		subjectIdIdx = sessionRs.getColumnIndex("xnat:imagesessiondata/subject_id");
		for (int i=0; i<sessionRs.getRowCount(); i++)
		{
			String subjectId = sessionRs.get(i, subjectIdIdx);
			String subjectLabel = subjIdLabelMap.get(subjectId);
			String sessionId = sessionRs.get(i, sessionIdIdx);
			String sessionLabel = sessionRs.get(i, sessionLabelIdx);
			String studyUid = sessionRs.get(i, uidIdx);
			Set<StudyUidContainer> containerSet = studyUidContainerCache.get(studyUid);
			containerSet.add(
				new StudyUidContainer(projectId, subjectId, subjectLabel, sessionId,
					sessionLabel));
		}
		// Completed ok, set lastProjectId
		lastProjectId = projectId;
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private void clearCaches()
	{
		studyUidContainerCache.clear();
		sessionSeriesUidIdCache.clear();
		sessionSeriesCache.clear();
		sessionSeriesSopInstFileCache.clear();
		lastProjectId = "";
		logger.debug("Caches cleared");
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private boolean createSeriesMappings(StudyUidContainer container,
		Set<String> seriesUids, Set<String> sopInstUids,
		Map<String,Series> seriesMap, Map<String,String> sopInstSeriesMap,
		Set<String> seriesIdSet) throws XnatException
	{
		Map<String,String> seriesUidIdMap = getSeriesUidIdMap(container);
		logger.debug("Checking series UIDs");
		Set<String> foundUids = seriesUidIdMap.keySet();
		if (!foundUids.containsAll(seriesUids))
		{
			logger.info("Container is missing required series UIDs");
			return false;
		}
		logger.debug("Checking SOP instance UIDs");
		for (String seriesUid : seriesUids)
		{
			String seriesId = seriesUidIdMap.get(seriesUid);
			if (seriesId == null)
			{
				logger.info("Container is missing required Series UID: "+
					seriesUid);
				return false;
			}
			seriesIdSet.add(seriesId);
			XnatId seriesXnatId = new XnatId(container.getProjectId(),
				container.getSubjectId(), container.getSessionId(),
				seriesId);
			Series series = fetchSeries(seriesXnatId, seriesUid);
			seriesMap.put(seriesUid, series);
			List<SopInstance> sopInstList = series.getSopInstanceList();
			for (SopInstance sopInst : sopInstList)
			{
				sopInstSeriesMap.put(sopInst.getUid(), seriesUid);
			}
		}
		if (!sopInstSeriesMap.keySet().containsAll(sopInstUids))
		{
			logger.info("Container is missing required SOP instance UIDs");
			return false;
		}
		logger.debug("Series/SOP instance UIDs ok");
		return true;
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private Series fetchSeries(XnatId seriesId, String seriesUid)
	{
		Series series = null;
		if (noCache)
		{
			series = xds.getSeries(seriesId, seriesUid);
			series.compact();
			return series;
		}
		String sessionId = seriesId.getSessionId();
		Map<XnatId,Series> seriesMap = sessionSeriesCache.get(sessionId);
		if (seriesMap == null)
		{
			seriesMap = new HashMap<>();
			sessionSeriesCache.put(sessionId, seriesMap);
		}
		series = seriesMap.get(seriesId);
		if (series == null)
		{
			series = xds.getSeries(seriesId, seriesUid);
			series.compact();
			seriesMap.put(seriesId, series);
			logger.debug("Caching series for seriesId: {}", seriesId.toString());
		}
		return series;
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private Set<StudyUidContainer> getContainerSet(String projectId, String uid)
		throws XnatException
	{
		if (noCache)
		{
			buildContainerMap(projectId);
		}
		return studyUidContainerCache.get(uid);
		
	}

	/*
	 * Affects the caching, only call under the Lock!
	 */
	private Map<String,String> getSeriesUidIdMap(StudyUidContainer container)
		throws XnatException
	{
		String sessionId = container.getSessionId();
		Map<String,String> uidIdMap = sessionSeriesUidIdCache.get(sessionId);
		if (uidIdMap != null)
		{
			return uidIdMap;
		}
		// Fetch UID/ID for all scans in session
		logger.debug("Building series UID/ID mapping for session: {}", sessionId);
		String command = "/data/archive/projects/"+container.getProjectId()+
			"/subjects/"+container.getSubjectId()+
			"/experiments/"+container.getSessionId()+"/scans"+
			"?xsiType=xnat:imageScanData"+
			"&columns=xnat:imageScanData/UID,xnat:imageScanData/ID"+
			"&format=xml";
		XnatResultSet scanRs = conn.getResultSet(command);
		int uidIdx = scanRs.getColumnIndex("UID");
		int idIdx = scanRs.getColumnIndex("ID");
		uidIdMap = new LinkedHashMap<>();
		for (int i=0; i<scanRs.getRowCount(); i++)
		{
			uidIdMap.put(scanRs.get(i, uidIdx), scanRs.get(i, idIdx));
		}
		if (!noCache)
		{
			sessionSeriesUidIdCache.put(sessionId, uidIdMap);
		}
		return uidIdMap;
	}

}
