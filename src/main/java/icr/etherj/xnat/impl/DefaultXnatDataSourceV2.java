/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.IoUtils;
import icr.etherj.Xml;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.xnat.RtStructXnatSearchPlugin;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.SopInstance;
import icr.etherj.dicom.Study;
import icr.etherj.xnat.IacXnatSearchPlugin;
import icr.etherj.xnat.Investigator;
import icr.etherj.xnat.XnatAnnotationItem;
import icr.etherj.xnat.XnatAssessor;
import icr.etherj.xnat.XnatCache;
import icr.etherj.xnat.XnatCode;
import icr.etherj.xnat.XnatDataSource;
import icr.etherj.xnat.XnatEntity;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatFileUriMap;
import icr.etherj.xnat.XnatId;
import icr.etherj.xnat.XnatImageAnnotation;
import icr.etherj.xnat.XnatImageAnnotationCollection;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatRegion;
import icr.etherj.xnat.XnatRegionSet;
import icr.etherj.xnat.XnatResultSet;
import icr.etherj.xnat.XnatScan;
import icr.etherj.xnat.XnatSearchPlugin;
import icr.etherj.xnat.XnatServerConnection;
import icr.etherj.xnat.XnatSession;
import icr.etherj.xnat.XnatSubject;
import icr.etherj.xnat.XnatTag;
import icr.etherj.xnat.XnatToolkit;
import java.io.File;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author jamesd
 */
public final class DefaultXnatDataSourceV2 extends AbstractDisplayable
	implements XnatDataSource
{
	private static final Logger logger =
		LoggerFactory.getLogger(DefaultXnatDataSourceV2.class);

	private static final String Equals = "=";
	private static final String LessThan = "&lt;";
	private static final String Like = "LIKE";

	private static final String AssessorSessionId = "imageSession_ID";
	private static final String Date = "date";
	private static final String ID = "ID";
	private static final String Keywords = "keywords";
	private static final String Label = "Label";
	private static final String Name = "Name";
	private static final String Project = "project";
	private static final String ProjectIdHeader = "ProjectId";
	private static final String ProjectKeywordsHeader = "ProjectKeywords";
	private static final String ProjectLabelHeader = "ProjectLabel";
	private static final String ScanIdHeader = "ScanId";
	private static final String ScanProjectId = "project";
	private static final String ScanProjectIdHeader = "ScanProjectId";
	private static final String ScanSessionId = "image_session_ID";
	private static final String ScanSessionIdHeader = "ScanSessionId";
	private static final String ScanStartTime = "startTime";
	private static final String ScanStartTimeHeader = "ScanStartTime";
	private static final String ScanSubjectId = "subject_ID";
	private static final String ScanSubjectIdHeader = "ScanSubjectId";
	private static final String ScanTypeHeader = "ScanType";
	private static final String ScanDescription = "series_description";
	private static final String ScanDescHeader = "ScanDescription";
	private static final String SessionDateHeader = "SessionDate";
	private static final String SessionIdHeader = "SessionId";
	private static final String SessionLabelHeader = "SessionLabel";
	private static final String SessionProjectId = "project";
	private static final String SessionProjectIdHeader = "SessionProjectId";
	private static final String SessionSubjectId = "subject_ID";
	private static final String SessionSubjectIdHeader = "SessionSubjectId";
	private static final String Size = "Size";
	private static final String StringType = "string";
	private static final String SubjectId = "subject_ID";
	private static final String SubjectIdHeader = "SubjectId";
	private static final String SubjectNameHeader = "SubjectName";
	private static final String Uid = "UID";
	private static final String UidHeader = "Uid";
	private static final String URI = "URI";
	private static final String Type = "type";
	private static final String Unknown = "<<UNKNOWN>>";

	// Use a LinkedHashMap to ensure iteraration is in insertion order
	private static final Map<String,String> modalityMap = new LinkedHashMap<>();

	private final XnatServerConnection xsc;
	private final XnatCache cache;
	private final Map<Class<?>,XnatSearchPlugin> plugins = new HashMap<>();
	private final XnatSearchPlugin.Helper helper = new XnatSearchPlugin.Helper();

	static
	{
		// Build modalityMap, most common modalities first
		modalityMap.put("MR", "mr");
		modalityMap.put("CT", "ct");
		modalityMap.put("PT", "pet");
	}

	DefaultXnatDataSourceV2(XnatServerConnection xsc) throws XnatException
	{
		this(xsc, XnatToolkit.getToolkit().createCache());
	}

	DefaultXnatDataSourceV2(XnatServerConnection xsc, XnatCache cache)
	{
		this.xsc = xsc;
		this.cache = cache;
		addSearchPlugin(ImageAnnotationCollection.class, new IacXnatSearchPlugin());
		addSearchPlugin(RtStruct.class, new RtStructXnatSearchPlugin());
	}

	@Override
	public <T> XnatSearchPlugin<T> addSearchPlugin(Class<T> klass,
		XnatSearchPlugin<T> plugin)
	{
		XnatSearchPlugin<T> result = plugins.put(klass, plugin);
		logger.info("XnatSearchPlugin<{}>, installation {}", 
			klass.getCanonicalName(),
			(plugins.containsKey(klass) ? "OK" : "FAILED"));
		return result;
	}

	@Override
	public void deleteEntity(XnatEntity xnatEntity) throws XnatException
	{
		Map<XnatId,String> xnatIds = new LinkedHashMap<>();
		findIdsToDelete(xnatEntity, xnatIds);
		for (XnatId id : xnatIds.keySet())
		{
			StringBuilder sb = new StringBuilder("/data/archive/projects/");
			String projectId = id.getProjectId();
			if (projectId.isEmpty())
			{
				throw new XnatException("Invalid XnatID",
					XnatCode.InvalidXnatId);
			}
			sb.append(projectId);
			String subjectId = id.getSubjectId();
			if (!subjectId.isEmpty())
			{
				sb.append("/subjects/").append(subjectId);
				String sessionId = id.getSessionId();
				if (!sessionId.isEmpty())
				{
					sb.append("/experiments/").append(sessionId);
					String scanId = id.getScanId();
					if (!scanId.isEmpty())
					{
						switch (xnatIds.get(id))
						{
							case XnatEntity.Assessor:
								sb.append("/assessors/");
								break;
							case XnatEntity.Scan:
								sb.append("/scans/");
								break;
							default:
								throw new XnatException(
									"Unknown type for ID: "+xnatIds.get(id),
									XnatCode.InvalidXnatId);
						}
						sb.append(scanId);
					}
				}
			}
			sb.append("?removeFiles=true");
			logger.info("Entity delete command: {}", sb.toString());
			xsc.delete(sb.toString());
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		xsc.display(ps, indent+"  ");
	}

	@Override
	public <T> boolean exists(Class<T> klass, SearchSpecification spec)
		throws XnatException
	{
		if (supportsSearch(klass))
		{
			XnatSearchPlugin<T> plugin = plugins.get(klass);
			return plugin.exists(xsc, spec);
		}
		return false;
	}

	@Override
	public List<Investigator> getInvestigators(String projectId)
		throws XnatException
	{
		logger.info("Fetching investigators: {}", projectId);
		List<Investigator> investigators = new ArrayList<>();
		String command = "/data/archive/projects/"+projectId+
			"?xsiType=xnat:projectData"+
			"&format=xml";
		Document doc = xsc.getDocument(command);
		Node piNode = Xml.getFirstMatch(doc.getDocumentElement(), "xnat:PI");
		if (piNode == null)
		{
			return investigators;
		}
		Investigator investigator = parseInvestigator(piNode);
		if (investigator != null)
		{
			investigators.add(investigator);
		}
		Node invNode = Xml.getFirstMatch(doc.getDocumentElement(),
			"xnat:investigators");
		if (invNode == null)
		{
			return investigators;
		}
		NodeList children = invNode.getChildNodes();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			if (child.getNodeName().equals("xnat:investigator"))
			{
				investigator = parseInvestigator(child);
				if (investigator != null)
				{
					investigators.add(investigator);
				}
			}
		}
		return investigators;
	}

	@Override
	public RtStruct getRtStructForMarkup(String project, String markupUid)
	{
		throw new UnsupportedOperationException("getRtStructForMarkup() is deprecated!");
	}

	@Override
	public Map<XnatId,Series> getSeries(String projectId, String uid)
	{
		logger.info("Searching for series. UID: {}", uid);
		Map<XnatId,Series> emptyMap = new HashMap<>();
		for (String key : modalityMap.keySet())
		{
			Map<XnatId,Series> modalitySeriesMap = getSeries(projectId, uid, key);
			if (!modalitySeriesMap.isEmpty())
			{
				return modalitySeriesMap;
			}
		}
		return emptyMap;
	}

	@Override
	public Map<XnatId,Series> getSeries(String projectId, String uid, String modality)
	{
		Map<XnatId,Series> seriesMap = new HashMap<>();
		if (!modalityMap.containsKey(modality))
		{
			logger.error("Unsupported modality: {}", modality);
			return seriesMap;
		}
		logger.info("Searching for {} series. UID: {}", modality, uid);
		Set<XnatId> idSet = getXnatSeriesIds(projectId, uid, modality);
		if (idSet.isEmpty())
		{
			return seriesMap;
		}
		for (XnatId id : idSet)
		{
			try
			{
				Series series = fetchSeries(id, uid);
				seriesMap.put(id, series);
			}
			catch (XnatException ex)
			{
				logger.error("Scan search error: ", ex);
			}
		}
		logger.info("Series search complete: {}", !seriesMap.isEmpty() ? "OK" : "Failed");
		return seriesMap;
	}

	@Override
	public Series getSeries(XnatId id, String uid)
	{
		Series series = null;
		try
		{
			series = fetchSeries(id, uid);
		}
		catch (XnatException ex)
		{
			logger.warn("Series fetch failed for XnatId: "+id.toString(), ex);
		}
		return series;
	}

	@Override
	public Map<XnatId,Study> getStudies(String projectId, String uid)
	{
		logger.info("Searching for study. UID: {}", uid);
		Map<XnatId,Study> studyMap = new HashMap<>();
		for (String key : modalityMap.keySet())
		{
			Map<XnatId,Study> modalityStudyMap = getStudies(projectId, uid, key);
			studyMap.putAll(modalityStudyMap);
		}
		return studyMap;
	}

	@Override
	public XnatServerConnection getXnatServerConnection()
	{
		return xsc;
	}

	@Override
	public <T> List<T> search(Class<T> klass, SearchSpecification spec)
		throws XnatException
	{
		if (supportsSearch(klass))
		{
			XnatSearchPlugin<T> plugin = plugins.get(klass);
			return plugin.search(xsc, spec);
		}
		return new ArrayList<>();
	}

	@Override
	public List<XnatAssessor> searchAssessor(SearchSpecification spec)
	{
		throw new UnsupportedOperationException("searchAssessor() is deprecated!");
	}

	@Override
	public List<ImageAnnotationCollection> searchIac(SearchSpecification spec)
	{
		throw new UnsupportedOperationException("searchIac() is deprecated!");
	}

	@Override
	public List<ImageAnnotationCollection> searchIac(String project, String name)
	{
		throw new UnsupportedOperationException("searchIac() is deprecated!");
	}

	@Override
	public List<XnatProject> searchProject(SearchSpecification spec)
	{
		List<XnatProject> projectList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return projectList;
		}

		try
		{
			String query = createSearchProjectQuery(spec);
			queryProjects(projectList, query);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			projectList.clear();
		}
		return projectList;
	}

	@Override
	public List<XnatScan> searchScan(SearchSpecification spec)
	{
		List<XnatScan> scanList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return scanList;
		}

		for (String key : modalityMap.keySet())
		{
			searchScan(spec, key, scanList);
		}
		Collections.sort(scanList);
		return scanList;
	}

	@Override
	public List<XnatSession> searchSession(SearchSpecification spec)
	{
		List<XnatSession> sessionList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return sessionList;
		}

		for (String key : modalityMap.keySet())
		{
			searchSession(spec, key, sessionList);
		}
		Collections.sort(sessionList);
		return sessionList;
	}

	@Override
	public List<XnatSubject> searchSubject(SearchSpecification spec)
	{
		List<XnatSubject> subjectList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return subjectList;
		}

		try
		{
			String query = createSearchSubjectQuery(spec);
			querySubjects(subjectList, query);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			subjectList.clear();
		}
		return subjectList;
	}

	@Override
	public <T> boolean supportsSearch(Class<T> klass)
	{
		return plugins.containsKey(klass);
	}

	private String createSearchProjectQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = helper.createInitialXml("Project");

		String projectData = "xnat:projectData";
		helper.addRootElement(sb, projectData);
		helper.addSearchField(sb, projectData, ID, 0, StringType,
			ProjectIdHeader);
		helper.addSearchField(sb, projectData, Name, 1, StringType,
			ProjectLabelHeader);
		helper.addSearchField(sb, projectData, Keywords, 2, StringType,
			ProjectKeywordsHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSearchCriteria(spec, projectData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchScanQuery(SearchSpecification spec,
		String modality) throws XnatException
	{
		StringBuilder sb = helper.createInitialXml("Scan");

		String prefix = modalityMap.get(modality);
//		String scanData = scanData(prefix);
		String scanData = "xnat:imageScanData";
		System.out.println("scanData: "+scanData);
		String sessionData = sessionData(prefix);
		helper.addRootElement(sb, scanData);
		helper.addSearchField(sb, scanData, ID, 0, StringType, ScanIdHeader);
		helper.addSearchField(sb, scanData, ScanSessionId, 1, StringType,
			ScanSessionIdHeader);
		helper.addSearchField(sb, scanData, ScanDescription, 2, StringType,
			ScanDescHeader);
		helper.addSearchField(sb, scanData, ScanStartTime, 3, StringType,
			ScanStartTimeHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processScanCriteria(spec, scanData, prefix, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchSessionQuery(SearchSpecification spec,
		String modality) throws XnatException
	{
		StringBuilder sb = helper.createInitialXml("Session");

		String sessionData = sessionData(modalityMap.get(modality));
		helper.addRootElement(sb, sessionData);
		helper.addSearchField(sb, sessionData, ID, 0, StringType, SessionIdHeader);
		helper.addSearchField(sb, sessionData, SessionSubjectId, 0, StringType,
			SessionSubjectIdHeader);
		helper.addSearchField(sb, sessionData, SessionProjectId, 0, StringType,
			SessionProjectIdHeader);
		helper.addSearchField(sb, sessionData, Label, 1, StringType,
			SessionLabelHeader);
		helper.addSearchField(sb, sessionData, Date, 2, StringType,
			SessionDateHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSessionCriteria(spec, sessionData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSearchSubjectQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = helper.createInitialXml("Subject");

		String subjectData = "xnat:subjectData";
		helper.addRootElement(sb, subjectData);
		helper.addSearchField(sb, subjectData, Project, 0, StringType,
			ProjectIdHeader);
		helper.addSearchField(sb, subjectData, ID, 1, StringType,
			SubjectIdHeader);
		helper.addSearchField(sb, subjectData, Label, 2, StringType,
			SubjectNameHeader);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSearchCriteria(spec, subjectData, sb);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");
		return sb.toString();
	}

	private XnatFileUriMap createSeriesFileUriMap(XnatId id)
		throws XnatException
	{
		String query = new StringBuilder("/data/archive/projects/")
			.append(id.getProjectId())
			.append("/subjects/").append(id.getSubjectId())
			.append("/experiments/").append(id.getSessionId())
			.append("/scans/").append(id.getScanId())
			.append("/resources/DICOM/files").toString();
		// Get list of files to compare to cache
		XnatResultSet fileRs = xsc.getResultSet(query+"?format=xml");
			
		XnatFileUriMap map = new DefaultXnatFileUriMap(id);
		int fileIdx = fileRs.getColumnIndex(Name);
		int uriIdx = fileRs.getColumnIndex(URI);
		int sizeIdx = fileRs.getColumnIndex(Size);
		int nFiles = fileRs.getRowCount();
		String projectStr = "/projects/"+id.getProjectId()+"/";
		String subjectStr = "/subjects/"+id.getSubjectId()+"/";
		String sessionStr = "/experiments/"+id.getSessionId()+"/";
		String scanStr = "/scans/"+id.getScanId()+"/";
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<nFiles; i++)
		{
			String filename = fileRs.get(i, fileIdx);
			String uri = fileRs.get(i, uriIdx);
			int size = fileRs.getInt(i, sizeIdx);
			if (uri.contains(projectStr) &&
				 uri.contains(subjectStr) &&
				 uri.contains(sessionStr) &&
				 uri.contains(scanStr) &&
				 uri.endsWith(filename) &&
				 size > 0)
			{
				sb.setLength(0);
				sb.append(id.getProjectId()).append(File.separator)
					.append(id.getSubjectId()).append(File.separator)
					.append(id.getSessionId()).append(File.separator)
					.append(id.getScanId()).append(File.separator)
					.append(filename);
				map.add(filename, sb.toString(), size);
			}
			else
			{
				throw new XnatException("File ResultSet contains invalid entry",
					XnatCode.InvalidFileUri);
			}
		}

		return map;
	}

	private String createSeriesSearchXml(String projectId, String uid,
		String modality)
	{
		StringBuilder sb = helper.createInitialXml("Series By UID");

		String sessionData = sessionData(modality);
		String scanData = scanData(modality);
		helper.addRootElement(sb, scanData);
		helper.addSearchField(sb, scanData, "ID", 0, StringType, ScanIdHeader);
		helper.addSearchField(sb, scanData, "TYPE", 1, StringType, ScanTypeHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		helper.addCriterion(sb, sessionData+"/"+Project, Equals, projectId);
		helper.addCriterion(sb, scanData+"/UID", Equals, uid);
		sb.append("	</xdat:search_where>\n");

		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createSessionIdSearchXml(String sessionId, String modality)
	{
		StringBuilder sb = helper.createInitialXml("SessionID");

		String sessionData = sessionData(modality);
		helper.addRootElement(sb, sessionData);
		helper.addSearchField(sb, sessionData, Project, 0, StringType,
			ProjectIdHeader);
		helper.addSearchField(sb, sessionData, SubjectId, 1, StringType,
			SubjectIdHeader);
		helper.addSearchField(sb, sessionData, ID, 1, StringType,
			SessionIdHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		helper.addCriterion(sb, sessionData+"/"+ID, Equals, sessionId);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private String createStudyBySeriesUidSearchXml(String projectId, String uid,
		String modality)
	{
		StringBuilder sb = helper.createInitialXml("Study By SeriesUID");

		String sessionData = sessionData(modality);
		String scanData = scanData(modality);
		helper.addRootElement(sb, sessionData);
		helper.addSearchField(sb, sessionData, "PROJECT", 0, StringType,
			ProjectIdHeader);
		helper.addSearchField(sb, sessionData, "subject_ID", 1, StringType,
			SubjectIdHeader);
		helper.addSearchField(sb, sessionData, "ID", 2, StringType, SessionIdHeader);
		helper.addSearchField(sb, sessionData, "LABEL", 3, StringType, SessionLabelHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		helper.addCriterion(sb, sessionData+"/PROJECT", Equals, projectId);
		helper.addCriterion(sb, scanData+"/UID", Equals, uid);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private XnatFileUriMap createStudyFileUriMap(XnatId studyId)
		throws XnatException
	{
		String seriesQuery = new StringBuilder("/data/archive/projects/")
			.append(studyId.getProjectId())
			.append("/subjects/").append(studyId.getSubjectId())
			.append("/experiments/").append(studyId.getSessionId())
			.append("/scans").toString();
		// Get list of files to compare to cache
		XnatResultSet seriesRs = xsc.getResultSet(seriesQuery+"?format=xml");
		int nSeries = seriesRs.getRowCount();
		XnatFileUriMap fileUriMap = new DefaultXnatFileUriMap(studyId);

		for (int i=0; i<nSeries; i++)
		{
			XnatId seriesId = new XnatId(studyId.getProjectId(),
				studyId.getSubjectId(), studyId.getSessionId(),
				Integer.toString(seriesRs.getInt(i, ID)));
			XnatFileUriMap seriesFUMap = createSeriesFileUriMap(seriesId);
			fileUriMap.addAll(seriesFUMap);
		}
		return fileUriMap;
	}

	private String createStudySearchXml(String projectId, String uid, String modality)
	{
		StringBuilder sb = helper.createInitialXml("Study By UID");

		String sessionData = sessionData(modality);
		helper.addRootElement(sb, sessionData);
		helper.addSearchField(sb, sessionData, "PROJECT", 0, StringType,
			ProjectIdHeader);
		helper.addSearchField(sb, sessionData, "subject_ID", 1, StringType,
			SubjectIdHeader);
		helper.addSearchField(sb, sessionData, "ID", 2, StringType, SessionIdHeader);
		helper.addSearchField(sb, sessionData, "LABEL", 3, StringType, SessionLabelHeader);
		sb.append("	<xdat:search_where method=\"AND\">\n");
		helper.addCriterion(sb, sessionData+"/PROJECT", Equals, projectId);
		helper.addCriterion(sb, sessionData+"/UID", Equals, uid);
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private Series fetchSeries(XnatId seriesId, String uid) throws XnatException
	{
		Series series = null;
		InputStream is = null;
		try
		{
			logger.info("Checking cache");
			XnatFileUriMap fileUriMap = createSeriesFileUriMap(seriesId);
			series = cache.getSeries(uid, seriesId);
			// Not found in cache, pull data from server and cache it
			if (series == null)
			{
				// Not found in cache, pull data from server and cache it
				logger.info("Cache miss, fetching from server");
				fetchSeriesToCache(seriesId, fileUriMap);
				series = cache.getSeries(uid, seriesId);
			}
			else
			{
				// Check the cache matches the server's version
				try
				{
					validateSeries(series, fileUriMap);
				}
				catch (XnatException ex)
				{
					// Cached series may have been modified server side.
					if (!ex.getCode().equals(XnatCode.FileNotFound))
					{
						throw ex;
					}
					logger.info("Cache invalid, refetching from server");
					cache.delete(seriesId);
					fetchSeriesToCache(seriesId, fileUriMap);
					series = cache.getSeries(uid, seriesId);
				}
			}
			validateSeries(series, fileUriMap);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return series;
	}

	private void fetchSeriesToCache(XnatId seriesId, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		logger.info("Retrieving series from server");
		String query = new StringBuilder("/data/archive/projects/")
			.append(seriesId.getProjectId())
			.append("/subjects/").append(seriesId.getSubjectId())
			.append("/experiments/").append(seriesId.getSessionId())
			.append("/scans/").append(seriesId.getScanId())
			.append("/resources/DICOM/files").toString();
		InputStream is = xsc.get(query+"?format=zip");
		cache.insert(is, fileUriMap);
	}

	private Study fetchStudy(XnatId studyId, String uid) throws XnatException
	{
		Study study = null;
		InputStream is = null;
		try
		{
			logger.info("Fetching studyId: <"+studyId.getProjectId()+","+
				studyId.getSubjectId()+","+studyId.getSessionId()+">");
			XnatFileUriMap fileUriMap = createStudyFileUriMap(studyId);
			logger.info("Checking cache");
			study = cache.getStudy(uid, studyId);
			if (study == null)
			{
				// Not found in cache, pull data from server and cache it
				logger.info("Cache miss, fetching from server");
				fetchStudyToCache(studyId, fileUriMap);
				study = cache.getStudy(uid, studyId);
			}
			else
			{
				// Check the cache matches the server's version
				try
				{
					validateStudy(study, fileUriMap);
				}
				catch (XnatException ex)
				{
					// Cached study could be only a partial version e.g. if a series
					// in the study has been fetched previously or the study has been
					// modified server side.
					if (!ex.getCode().equals(XnatCode.FileNotFound))
					{
						throw ex;
					}
					logger.info("Cache invalid, refetching from server");
					cache.delete(studyId);
					fetchStudyToCache(studyId, fileUriMap);
					study = cache.getStudy(uid, studyId);
				}
			}
			validateStudy(study, fileUriMap);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return study;
	}

	private void fetchStudyToCache(XnatId studyId, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		logger.info("Retrieving study from server");
		String query = new StringBuilder("/data/archive/projects/")
			.append(studyId.getProjectId())
			.append("/subjects/").append(studyId.getSubjectId())
			.append("/experiments/").append(studyId.getSessionId())
			.append("/scans/ALL/resources/DICOM/files").toString();
		InputStream is = xsc.get(query+"?format=zip");
		cache.insert(is, fileUriMap);
	}

	private void findIdsToDelete(XnatEntity entity, Map<XnatId,String> ids)
	{
		if (entity == null)
		{
			return;
		}
		switch (entity.getType())
		{
			case XnatEntity.ImageAnnotationCollection:
				XnatImageAnnotationCollection iac =
					(XnatImageAnnotationCollection) entity;
				for (XnatImageAnnotation ia : iac.getImageAnnotations())
				{
					findIdsToDelete(ia, ids);
				}
				findIdsToDelete(iac.getRegionSet(), ids);
				logger.debug("IAC to delete: "+iac.getXnatId());
				ids.put(iac.getXnatId(), XnatEntity.Assessor);
				break;

			case XnatEntity.ImageAnnotation:
				XnatImageAnnotation ia = (XnatImageAnnotation) entity;
				for (XnatAnnotationItem item : ia.getAnnotationItems())
				{
					logger.debug(item.getType()+" to delete: "+item.getXnatId());
					ids.put(item.getXnatId(), XnatEntity.Assessor);
				}
				logger.debug("IA to delete: "+ia.getXnatId());
				ids.put(ia.getXnatId(), XnatEntity.Assessor);
				break;

			case XnatEntity.AnnotationRole:
			case XnatEntity.Calculation:
			case XnatEntity.ImageObservation:
			case XnatEntity.ImagePhysical:
			case XnatEntity.Inference:
			case XnatEntity.Markup:
			case XnatEntity.Segmentation:
			case XnatEntity.TaskContext:
			case XnatEntity.Region:
				logger.debug(entity.getType()+" to delete: "+entity.getXnatId());
				ids.put(entity.getXnatId(), XnatEntity.Assessor);
				break;

			case XnatEntity.RegionSet:
				XnatRegionSet rs = (XnatRegionSet) entity;
				for (XnatRegion region : rs.getRegions())
				{
					logger.debug(region.getType()+" to delete: "+region.getXnatId());
					ids.put(region.getXnatId(), XnatEntity.Assessor);
				}
				logger.debug("RegionSet to delete: "+rs.getXnatId());
				ids.put(rs.getXnatId(), XnatEntity.Assessor);
				break;

			default:
				logger.warn("Unknown XnatEntity type: "+entity.getType());
		}
	}

	private Map<XnatId,Study> getStudies(String projectId, String uid,
		String modality)
	{
		Map<XnatId,Study> studyMap = new HashMap<>();
		if (!modalityMap.containsKey(modality))
		{
			logger.error("Unsupported modality: {}", modality);
			return studyMap;
		}
		logger.info("Searching for {} study. UID: {}", modality, uid);
		Set<XnatId> studyIdSet = getXnatStudyIds(projectId, uid, modality);
		if (studyIdSet.isEmpty())
		{
			return studyMap;
		}
		for (XnatId id : studyIdSet)
		{
			try
			{
				Study study = fetchStudy(id, uid);
				studyMap.put(id, study);
			}
			catch (XnatException ex)
			{
				logger.error("Study search error: ", ex);
			}
		}
		logger.info("Study search complete: {}", !studyMap.isEmpty() ? "OK" : "Failed");
		return studyMap;
	}

	private Set<XnatId> getXnatSeriesIds(String projectId, String uid,
		String modality)
	{
		Set<XnatId> idSet = new HashSet<>();
		try
		{
			String search = "/data/search?format=xml";
			// XNAT won't return all info in one query
			// Get ID down to study level
			String studySearchXml = createStudyBySeriesUidSearchXml(projectId, uid,
				modalityMap.get(modality));
			XnatResultSet studySearchRs = xsc.getResultSet(search, studySearchXml);
			if (studySearchRs.getRowCount() == 0)
			{
				logger.info(String.format(
					"No Study found for project %s, modality %s and UID %s",
					projectId, modality, uid));
				return idSet;
			}
			for (int i=0; i<studySearchRs.getRowCount(); i++)
			{
				XnatId xnatSessionId = xnatIdFromResultSet(studySearchRs, i);
				// Search for series ID
				String searchXml = createSeriesSearchXml(projectId, uid,
					modalityMap.get(modality));
				XnatResultSet searchRs = xsc.getResultSet(search, searchXml);
				if (searchRs.getRowCount() == 0)
				{
					logger.info(String.format(
						"No Series found for project %s, modality %s and UID %s",
						projectId, modality, uid));
					continue;
				}
				for (int j=0; j<searchRs.getRowCount(); j++)
				{
					String scanId = searchRs.get(j, searchRs.getColumnIndexByHeader(
						ScanIdHeader));
					XnatId id = new XnatId(projectId, xnatSessionId.getSubjectId(),
						xnatSessionId.getSessionId(), scanId);
					logger.debug("Series XNAT ID: <"+id.getProjectId()+","+
						id.getSubjectId()+","+id.getSessionId()+","+id.getScanId()+">");
					idSet.add(id);
				}
			}
		}
		catch (XnatException ex)
		{
			logger.error("Scan search error", ex);
		}
		return idSet;
	}

	private XnatId getXnatSessionId(String sessionId, String modality)
	{
		XnatId id = null;
		try
		{
			String search = "/data/search?format=xml";
			// XNAT won't return all info in one query
			// Get ID down to study level
			String xml = createSessionIdSearchXml(sessionId,
				modalityMap.get(modality));
			XnatResultSet rs = xsc.getResultSet(search, xml);
			if (rs.getRowCount() != 1)
			{
				logger.info(String.format(
					"No unique Session found for ID %s, modality %s",
					sessionId, modality));
				return id;
			}
			id = xnatIdFromResultSet(rs);
		}
		catch (XnatException ex)
		{
			logger.error("Scan search error", ex);
		}
		return id;
	}

	private Set<XnatId> getXnatStudyIds(String projectId, String uid, String modality)
	{
		Set<XnatId> idSet = new HashSet<>();
		try
		{
			String search = "/data/search?format=xml";
			// Get ID down to study level
			String studySearchXml = createStudySearchXml(projectId, uid,
				modalityMap.get(modality));
			XnatResultSet studySearchRs = xsc.getResultSet(search, studySearchXml);
			if (studySearchRs.getRowCount() == 0)
			{
				logger.info("No unique result found for modality {} and UID {}",
					modality, uid);
				return idSet;
			}
			for (int i=0; i<studySearchRs.getRowCount(); i++)
			{
//				String projectId = studySearchRs.get(i, 
//					studySearchRs.getColumnIndexByHeader(ProjectIdHeader));
				String subjectId = studySearchRs.get(i,
					studySearchRs.getColumnIndexByHeader(SubjectIdHeader));
				String sessionId = studySearchRs.get(i,
					studySearchRs.getColumnIndexByHeader(SessionIdHeader));
				XnatId id = new XnatId(projectId, subjectId, sessionId);
				logger.debug("Study XNAT ID: <"+id.getProjectId()+","+
					id.getSubjectId()+","+id.getSessionId()+">");
				idSet.add(id);
			}
		}
		catch (XnatException ex)
		{
			logger.error("Scan search error", ex);
		}
		return idSet;
	}

	private Investigator parseInvestigator(Node node)
	{
		String firstName = null;
		String lastName = null;
		String title = null;
		String institution = null;
		String dept = null;
		String email = null;
		String phone = null;
		NodeList children = node.getChildNodes();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			switch (child.getNodeName())
			{
				case "text#":
					continue;
				case "xnat:firstname":
					firstName = child.getTextContent().trim();
					break;
				case "xnat:lastname":
					lastName = child.getTextContent().trim();
					break;
				case "xnat:title":
					title = child.getTextContent().trim();
					break;
				case "xnat:institution":
					institution = child.getTextContent().trim();
					break;
				case "xnat:department":
					dept = child.getTextContent().trim();
					break;
				case "xnat:email":
					email = child.getTextContent().trim();
					break;
				case "xnat:phone":
					phone = child.getTextContent().trim();
					break;
				default:
			}
		}
		Investigator investigator = null;
		if ((firstName != null) && !firstName.isEmpty() &&
			 (lastName != null) && !lastName.isEmpty())
		{
			investigator = new Investigator(firstName, lastName);
			investigator.setTitle(title);
			investigator.setInstitution(institution);
			investigator.setDepartment(dept);
			investigator.setEmail(email);
			investigator.setPhoneNumber(phone);			
		}
		return investigator;
	}

	private StringBuilder processScanCriteria(SearchSpecification spec,
		String searchData, String modality, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.SessionID:
					String sessionData = sessionData(modality);
					helper.addCriterion(sb, sessionData+"/"+ID, crit);
//					if (crit.getComparator() == SearchCriterion.Like)
//					{
//						addCriterion(sb, sessionData+"/"+ID,
//							crit.createComparatorSql(), "%"+crit.getValue()+"%");
//					}
//					else
//					{
//						addCriterion(sb, sessionData+"/"+ID,
//							crit.createComparatorSql(), crit.getValue());						
//					}
					break;

				case XnatTag.ProjectID:
					helper.addCriterion(sb, searchData+"/"+ScanProjectId, crit);
					break;

				case XnatTag.Uid:
					helper.addCriterion(sb, searchData+"/"+Uid, crit);
					break;

//				case XnatTag.SeriesDescription:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.Scanner:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.ScannerManufacturer:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.ScannerModel:
//					addCriterion(sb, searchData+"/"+Project, Equals,
//						crit.getValue());
//					break;

//				case XnatTag.SessionDate:
//					addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
//						"%"+crit.getValue()+"%");
//					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private StringBuilder processSearchCriteria(SearchSpecification spec,
		String searchData, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.ProjectID:
					helper.addCriterion(sb, searchData+"/"+Project, Equals,
						crit.getValue());
					break;

				case XnatTag.SubjectID:
					helper.addCriterion(sb, searchData+"/"+"subjectID", Like,
						"%"+crit.getValue()+"%");
					break;

				case XnatTag.SubjectName:
					helper.addCriterion(sb, searchData+"/"+Label, Like,
						"%"+crit.getValue()+"%");
					break;

				case XnatTag.SessionID:
					helper.addCriterion(sb, searchData+"/"+AssessorSessionId, Equals,
						crit.getValue());
					break;

				case XnatTag.Label:
					helper.addCriterion(sb, searchData+"/"+Label, Equals,
						crit.getValue());
					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private StringBuilder processSessionCriteria(SearchSpecification spec,
		String searchData, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.SubjectID:
					if (crit.getComparator() == SearchCriterion.Like)
					{
						helper.addCriterion(sb, searchData+"/"+SessionSubjectId,
							crit.createComparatorSql(), "%"+crit.getValue()+"%");
					}
					else
					{
						helper.addCriterion(sb, searchData+"/"+SessionSubjectId,
							crit.createComparatorSql(), crit.getValue());						
					}
					break;

//				case XnatTag.SessionDate:
//					addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
//						"%"+crit.getValue()+"%");
//					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private void queryProjects(List<XnatProject> projectList, String query)
		throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int idIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(ProjectLabelHeader);
		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatProject project = new XnatProject(
				rs.get(i, idIdx), rs.get(i, labelIdx));
				projectList.add(project);
		}
	}

	private void queryScans(List<XnatScan> scanList, String query,
		String modality) throws XnatException
	{
		System.out.println(query);
		XnatResultSet scanRs = xsc.getResultSet("/data/search?format=xml", query);
		int idIdx = scanRs.getColumnIndexByHeader(ScanIdHeader);
		int sessionIdIdx = scanRs.getColumnIndexByHeader(ScanSessionIdHeader);
		int descIdx = scanRs.getColumnIndexByHeader(ScanDescHeader);
		int timeIdx = scanRs.getColumnIndexByHeader(ScanStartTimeHeader);
		int nRows = scanRs.getRowCount();
		Map<String,XnatId> sessionXnatIds = new HashMap<>();
		for (int i=0; i<nRows; i++)
		{
			String sessionId = scanRs.get(i, sessionIdIdx);
			XnatId sessionXnatId = sessionXnatIds.get(sessionId);
			if (sessionXnatId == null)
			{
				sessionXnatId = getXnatSessionId(sessionId, modality);
				sessionXnatIds.put(sessionId, sessionXnatId);
			}
			XnatId xnatId = new XnatId(sessionXnatId.getProjectId(),
				sessionXnatId.getSubjectId(),
				sessionId,
				scanRs.get(i, idIdx));
			XnatScan scan = new XnatScan(xnatId,
				scanRs.get(i, descIdx),
				modality);
			scan.setStartTime(scanRs.get(i, timeIdx));
			scanList.add(scan);
		}
	}

	private void querySessions(List<XnatSession> sessionList, String query,
		String modality) throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int idIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		int subjectIdIdx = rs.getColumnIndexByHeader(SessionSubjectIdHeader);
		int projectIdIdx = rs.getColumnIndexByHeader(SessionProjectIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(SessionLabelHeader);
		int dateIdx = rs.getColumnIndexByHeader(SessionDateHeader);
		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = new XnatId(
				rs.get(i, projectIdIdx),
				rs.get(i, subjectIdIdx),
				rs.get(i, idIdx));
			XnatSession session = new XnatSession(
				xnatId,
				rs.get(i, labelIdx),
				rs.get(i, dateIdx),
				modality);
			sessionList.add(session);
		}
	}

	private void querySubjects(List<XnatSubject> subjectList, String query)
		throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int projectIdIdx = rs.getColumnIndexByHeader(
			ProjectIdHeader);
		int idIdx = rs.getColumnIndexByHeader(
			SubjectIdHeader);
		int labelIdx = rs.getColumnIndexByHeader(
			SubjectNameHeader);
		int nRows = rs.getRowCount();
		for (int i=0; i<nRows; i++)
		{
			XnatSubject subject = new XnatSubject(
				rs.get(i, projectIdIdx), 
				rs.get(i, idIdx),
				rs.get(i, labelIdx));
				subjectList.add(subject);
		}
	}

	private String scanData(String modality)
	{
		return new StringBuilder("xnat:").append(modality).append("ScanData")
			.toString();
	}

	private void searchScan(SearchSpecification spec, String modality,
		List<XnatScan> scanList)
	{
		try
		{
			String query = createSearchScanQuery(spec, modality);
			queryScans(scanList, query, modality);
		}
		catch (XnatException ex)
		{
			logger.warn(modality+" scan search failed", ex);
		}
	}

	private void searchSession(SearchSpecification spec, String modality,
		List<XnatSession> sessionList)
	{
		try
		{
			String query = createSearchSessionQuery(spec, modality);
			querySessions(sessionList, query, modality);
		}
		catch (XnatException ex)
		{
			logger.warn(modality+" session search failed", ex);
		}
	}

	private String sessionData(String modality)
	{
		return new StringBuilder("xnat:").append(modality).append("SessionData")
			.toString();
	}

	private void validateSeries(Series series, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		List<SopInstance> sopInstList = series.getSopInstanceList();
		Set<String> usedKeys = validateSopInstances(sopInstList, fileUriMap);
		if (!usedKeys.containsAll(fileUriMap.keySet()))
		{
			throw new XnatException(
				"Required files missing from series: "+series.getUid(),
				XnatCode.FileNotFound);
		}
	}

	private Set<String> validateSopInstances(List<SopInstance> sopInstList,
		XnatFileUriMap fileUriMap) throws XnatException
	{
		Set<String> usedKeys = new HashSet<>();
		for (SopInstance sopInst : sopInstList)
		{
			File file = sopInst.getFile();
			XnatFileUriMap.Entry entry = fileUriMap.get(file.getName());
			if (entry == null)
			{
				throw new XnatException(
					"File doesn't exist: "+file.getAbsolutePath(),
					XnatCode.FileNotFound);
			}
			if (file.length() != entry.getSize())
			{
				logger.warn("File size mismatch for: "+file.getName()+" Expected: {} Actual: {}",
					entry.getSize(), file.length());
//				throw new XnatException(
//					"File size mismatch: "+file.getAbsolutePath(),
//					XnatCode.FileSize);
			}
			usedKeys.add(file.getName());
		}
		return usedKeys;
	}

	private void validateStudy(Study study, XnatFileUriMap fileUriMap)
		throws XnatException
	{
		List<Series> seriesList = study.getSeriesList();
		List<SopInstance> sopInstList = new ArrayList<>();
		for (Series series : seriesList)
		{
			sopInstList.addAll(series.getSopInstanceList());
		}
		Set<String> usedKeys = validateSopInstances(sopInstList, fileUriMap);
		if (!usedKeys.containsAll(fileUriMap.keySet()))
		{
			throw new XnatException(
				"Required files missing from study: "+study.getUid(),
				XnatCode.FileNotFound);
		}
	}

	private XnatId xnatIdFromResultSet(XnatResultSet rs)
	{
		return xnatIdFromResultSet(rs, 0);
	}

	private XnatId xnatIdFromResultSet(XnatResultSet rs, int idx)
	{
		int projIdx = rs.getColumnIndexByHeader(ProjectIdHeader);
		int subjIdIdx = rs.getColumnIndexByHeader(SubjectIdHeader);
		int sessionIdx = rs.getColumnIndexByHeader(SessionIdHeader);
		return new XnatId(rs.get(idx, projIdx), rs.get(idx, subjIdIdx),
			rs.get(idx, sessionIdx));
	}

}
