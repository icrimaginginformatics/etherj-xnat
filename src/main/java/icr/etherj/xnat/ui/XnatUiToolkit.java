/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.Unstable;
import icr.etherj.xnat.XnatAssessor;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatScan;
import icr.etherj.xnat.XnatSession;
import icr.etherj.xnat.XnatSubject;
import icr.etherj.xnat.ui.impl.DefaultXnatUiFactory;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class XnatUiToolkit
{
	public static final String ErrorOverlay = "ErrorOverlay";
	public static final String WarningOverlay = "WarningOverlay";

	private static final String Default = "default";
	private static final Logger logger = LoggerFactory.getLogger(
		XnatUiToolkit.class);
	private static final Map<String,XnatUiToolkit> toolkitMap = new HashMap<>();

	private final XnatUiToolkit.XnatUiFactory factory =
		new DefaultXnatUiFactory();

	static
	{
		toolkitMap.put(Default, new XnatUiToolkit());
	}

	/**
	 *
	 * @return
	 */
	public static XnatUiToolkit getToolkit()
	{
		return getToolkit(Default);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public static XnatUiToolkit getToolkit(String key)
	{
		return toolkitMap.get(key);
	}

	/**
	 *
	 * @param key
	 * @param toolkit
	 * @return
	 */
	public static XnatUiToolkit setToolkit(String key, XnatUiToolkit toolkit)
	{
		XnatUiToolkit tk = toolkitMap.put(key, toolkit);
		logger.info(toolkit.getClass().getName()+" set with key '"+key+"'");
		return tk;
	}

	@Unstable
	public XnatTreeNode createNode(XnatAssessor assessor)
	{
		return factory.createNode(assessor);
	}

	@Unstable
	public XnatTreeNode createNode(XnatProject project)
	{
		return factory.createNode(project);
	}

	@Unstable
	public XnatTreeNode createNode(XnatScan scan)
	{
		return factory.createNode(scan);
	}

	@Unstable
	public XnatTreeNode createNode(XnatSession session)
	{
		return factory.createNode(session);
	}

	@Unstable
	public XnatTreeNode createNode(XnatSubject subject)
	{
		return factory.createNode(subject);
	}

	@Unstable
	public XnatTreeCellRenderer createTreeCellRenderer()
	{
		return factory.createTreeCellRenderer();
	}
	
	public Icon getIconOverlay(String key)
	{
		return factory.getIconOverlay(key);
	}

	/*
	 *	Private constructor to prevent direct instantiation
	 */
	private XnatUiToolkit()
	{}

	/**
	 *
	 */
	@Unstable
	public interface XnatUiFactory
	{

		/**
		 *
		 * @param assessor
		 * @return
		 */
		public XnatTreeNode createNode(XnatAssessor assessor);

		/**
		 *
		 * @param project
		 * @return
		 */
		public XnatTreeNode createNode(XnatProject project);

		/**
		 *
		 * @param scan
		 * @return
		 */
		public XnatTreeNode createNode(XnatScan scan);

		/**
		 *
		 * @param session
		 * @return
		 */
		public XnatTreeNode createNode(XnatSession session);

		/**
		 *
		 * @param subject
		 * @return
		 */
		public XnatTreeNode createNode(XnatSubject subject);

		/**
		 *
		 * @return
		 */
		public XnatTreeCellRenderer createTreeCellRenderer();

		/**
		 *
		 * @param key
		 * @return
		 */
		public Icon getIconOverlay(String key);

	}
}
