/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.xnat.XnatProject;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public final class ProjectSelector extends JPanel
{
	public static final String SELECTION = "Selection";

	private static final Logger logger = LoggerFactory.getLogger(
		ProjectSelector.class);
	private static final String NoProjects = "<No Projects>";

	private final JButton advButton;
	private final JComboBox<String> combo;
	private final Icon icon;
	private final JPopupMenu popup;
	private final List<XnatProject> projects = new ArrayList<>();
	private final List<String> selection = new ArrayList<>();

	/**
	 *
	 */
	public ProjectSelector()
	{
		this(false);
	}

	/**
	 *
	 */
	public ProjectSelector(boolean advanced)
	{
		super();
		combo = new JComboBox<>();
		combo.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				onItemStateChanged(e);
			}
		});
		icon = new ImageIcon(
			getClass().getResource("/icr/etherj/ui/resources/Gear24.png"));
		advButton = new JButton(icon);
		advButton.setBorderPainted(false);
		advButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onAdvButton(e);
			}
		});
		setComboEmpty();

		int height = icon.getIconHeight()+4;
		int width = icon.getIconWidth()+4+192;
		advButton.setPreferredSize(new Dimension(height, height));
		setPreferredSize(new Dimension(width, height));
		setMaximumSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		
		setLayout(new BorderLayout());
		add(combo, BorderLayout.CENTER);
		if (advanced)
		{
			add(advButton, BorderLayout.EAST);
		}

		popup = new JPopupMenu();
		popup.setLayout(new BorderLayout());
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setPreferredSize(new Dimension(300, 200));
		JLabel label = new JLabel("Clever Multi-Project UI here");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setEnabled(false);
		panel.add(label, BorderLayout.CENTER);
		popup.add(panel, BorderLayout.CENTER);
	}

	/**
	 *
	 */
	public void clear()
	{
		projects.clear();
		combo.removeAllItems();
		combo.addItem("<No Projects>");
		setEnabled(false);
	}

	/**
	 *
	 * @return
	 */
	public String[] getSelection()
	{
		String[] value = new String[selection.size()];
		value = selection.toArray(value);
		return value;
	}

	/**
	 *
	 * @return
	 */
	public boolean hasProjects()
	{
		return !projects.isEmpty();
	}

	@Override
	public void setEnabled(boolean enable)
	{
		super.setEnabled(enable);
		combo.setEnabled(enable && !projects.isEmpty());
		advButton.setEnabled(enable && !projects.isEmpty());
	}

	/**
	 *
	 * @param projects
	 */
	public void setProjects(List<XnatProject> projects)
	{
		this.projects.clear();
		if (projects.isEmpty())
		{
			setComboEmpty();
			return;
		}
		this.projects.addAll(projects);
		combo.removeAllItems();
//		if (projects.size() > 1)
//		{
//			combo.addItem("ALL");
//		}
		for (XnatProject project : projects)
		{
			combo.addItem(project.getId());
		}
	}

	/**
	 *
	 * @param value
	 */
	public void setSelection(String value)
	{
		int idx = 0;
		for (XnatProject project : projects)
		{
			if (project.getId().equals(value))
			{
				break;
			}
			idx++;
		}
		if (idx < projects.size())
		{
			combo.setSelectedIndex(idx);
		}
	}

	private void onAdvButton(ActionEvent e)
	{
		popup.show(this, 0, getHeight());
	}

	private void onItemStateChanged(ItemEvent e)
	{
		if (e.getStateChange() == ItemEvent.SELECTED)
		{
			String[] oldValue = new String[selection.size()];
			oldValue = selection.toArray(oldValue);
			selection.clear();
			selection.add((String) e.getItem());
			String[] newValue = new String[selection.size()];
			newValue = selection.toArray(newValue);
			if ((newValue.length > 0) && newValue[0].equals(NoProjects))
			{
				newValue = new String[0];
			}
			firePropertyChange(SELECTION, oldValue, newValue);
		}
	}

	private void setComboEmpty()
	{
		combo.removeAllItems();
		combo.addItem(NoProjects);
		combo.setEnabled(false);
		advButton.setEnabled(false);
	}
}
