/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.crypto.CryptoException;
import icr.etherj.xnat.ProfileManager;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author jamesd
 */
public final class ProfileManagerDialog extends JDialog
{
	private final ProfileManagerPanel managerPanel;
	private final PropertyChangeListener pcl = new PanelListener();
	private final JButton ok;

	public ProfileManagerDialog(JFrame parent, ProfileManager profileManager)
		throws CryptoException
	{
		super(parent, "Profile Manager", true);
		managerPanel = new ProfileManagerPanel();
		managerPanel.setBorder(new EtchedBorder());
		managerPanel.setManager(profileManager);
		Container contentPane = getContentPane();
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		contentPane.add(managerPanel, gbc);
		ok = new JButton(" OK ");
		ok.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				onOk();
			}
		});
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.NONE;
		add(ok, gbc);
	}

	@Override
	public void setVisible(boolean visible)
	{
		if (visible)
		{
			if (!isVisible())
			{
				managerPanel.addPropertyChangeListener(
					ProfileManagerPanel.IS_EDITING,
					pcl);
			}
		}
		else
		{
			if (isVisible())
			{
				managerPanel.removePropertyChangeListener(
					ProfileManagerPanel.IS_EDITING,
					pcl);
			}
		}
		super.setVisible(visible);
	}

	private void onOk()
	{
		dispose();
	}

	private class PanelListener implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			boolean isEditing = (boolean) evt.getNewValue();
			ok.setEnabled(!isEditing);
		}
	}
}
