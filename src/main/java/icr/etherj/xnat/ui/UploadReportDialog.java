/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.aim.AimUtils;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.Person;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.xnat.XnatUploader.Result;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author jamesd
 * @param <T>
 */
public class UploadReportDialog<T> extends JDialog
{

	private JScrollPane detailScroll;
	private JTable detailTable;
	private JButton details;
	private boolean detailsShown = false;
	private final ImageIcon down;
	private JLabel mainLabel;
	private JPanel mainPanel;
	private DetailTableModel model;
	private final List<Result<T>> resultList;
	private JTextArea summaryText;
	private final ImageIcon up;

	/**
	 *
	 * @param parent
	 * @param title
	 * @param resultList
	 */
	public UploadReportDialog(JFrame parent, String title,
		List<Result<T>> resultList)
	{
		super(parent, title, true);
		this.resultList = resultList;
		down = new ImageIcon(
			getClass().getResource("/icr/etherj/ui/resources/GreenDownTriangle16.png"));
		up = new ImageIcon(
			getClass().getResource("/icr/etherj/ui/resources/GreenUpTriangle16.png"));
		layoutUi();
	}

	private void createDetails()
	{
		model = new DetailTableModel();
		detailTable = new JTable(model);
		detailScroll = new JScrollPane(detailTable);
		List<DetailItem> items = new ArrayList<>();
		Class klass = resultList.get(0).getMetadata().getUploadItem().getClass();
		for (Result<T> result : resultList)
		{
			DetailItem item = createItem(klass, result);
			items.add(item);
		}
		model.addAll(items);
		detailTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnModel colModel = detailTable.getColumnModel();
		colModel.getColumn(0).setPreferredWidth(150);
		colModel.getColumn(1).setPreferredWidth(250);
		colModel.getColumn(2).setPreferredWidth(100);
		colModel.getColumn(3).setPreferredWidth(100);
		colModel.getColumn(4).setPreferredWidth(400);
		detailScroll.setPreferredSize(new Dimension(1002, 400));
		colModel.getColumn(3).setCellRenderer(new StatusCellRenderer());
	}

	private DetailItem createItem(Class<T> klass, Result<T> result)
	{
		Object item = result.getMetadata().getUploadItem();
		if (icr.etherj.aim.ImageAnnotationCollection.class.isInstance(item))
		{
			return new IacDetailItem((Result<ImageAnnotationCollection>) result);
		}
		if (icr.etherj.dicom.iod.RtStruct.class.isInstance(item))
		{
			return new RtStructDetailItem((Result<RtStruct>) result);
		}
		if (icr.etherj.dicom.iod.Segmentation.class.isInstance(item))
		{
			return new SegmentationDetailItem((Result<Segmentation>) result);
		}
		throw new IllegalArgumentException(
			"Unupported class: "+klass.getCanonicalName());
	}

	private void createSummary(Font font)
	{
		int nSuccess = 0;
		int nSkipped = 0;
		int nOrphan = 0;
		int nMissing = 0;
		int nCancelled = 0;
		int nExcept = 0;
		for (Result<T> result : resultList)
		{
			switch (result.getStatus())
			{
				case Result.Success:
					nSuccess++;
					break;
				case Result.Skipped:
					nSkipped++;
					break;
				case Result.Orphan:
					nOrphan++;
					break;
				case Result.MissingDependencies:
					nMissing++;
					break;
				case Result.Cancelled:
					nCancelled++;
					break;
				case Result.Exception:
					nExcept++;
					break;
				default:
					throw new IllegalArgumentException("Unknown result status");
			}
		}
		int nRows = 2;
		StringBuilder sb = new StringBuilder(
			"One or more uploads did not complete.\n");
		if (nSuccess > 0)
		{
			sb.append("\nSuccessful: ").append(nSuccess);
			nRows++;
		}
		if (nSkipped > 0)
		{
			sb.append("\nSkipped: ").append(nSkipped);
			nRows++;
		}
		if (nOrphan > 0)
		{
			sb.append("\nOrphans: ").append(nOrphan);
			nRows++;
		}
		if (nMissing > 0)
		{
			sb.append("\nMissing Dependencies: ").append(nMissing);
			nRows++;
		}
		if (nCancelled > 0)
		{
			sb.append("\nCancelled: ").append(nCancelled);
			nRows++;
		}
		if (nExcept > 0)
		{
			sb.append("\nException: ").append(nExcept);
			nRows++;
		}
		summaryText = new JTextArea();
		summaryText.setEditable(false);
		summaryText.setLineWrap(true);
		summaryText.setWrapStyleWord(true);
		summaryText.setBackground(mainLabel.getBackground());
		summaryText.setText(sb.toString());
		summaryText.setRows(nRows);
		summaryText.setFont(font);
	}

	private void layoutUi()
	{
		Container contentPane = getContentPane();
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 4;
		mainLabel = new JLabel("Upload Report");
		contentPane.add(mainLabel, gbc);
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.BOTH;
		mainPanel = new JPanel(new BorderLayout());
		contentPane.add(mainPanel, gbc);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.NONE;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		details = new JButton("Details", down);
		details.setPreferredSize(new Dimension(100, 30));
		details.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onDetails();
			}
		});
		contentPane.add(details, gbc);
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 1;
		gbc.gridwidth = 2;
		contentPane.add(Box.createHorizontalGlue(), gbc);
		gbc.anchor = GridBagConstraints.LINE_END;
		gbc.fill = GridBagConstraints.NONE;
		gbc.gridx = 3;
		gbc.gridwidth = 1;
		JButton close = new JButton("Close");
		close.setPreferredSize(new Dimension(100, 30));
		close.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
			}
		});
		contentPane.add(close, gbc);

		Font font = mainLabel.getFont();
		float size = font.getSize2D();
		mainLabel.setFont(font.deriveFont(Font.BOLD, size*1.25f));

		createSummary(font);
		createDetails();
		mainPanel.add(summaryText, BorderLayout.CENTER);

		pack();
		setLocationRelativeTo(getParent());
	}
	
	private void onDetails()
	{
		setVisible(false);
		if (detailsShown)
		{
			mainPanel.remove(detailScroll);
			mainPanel.add(summaryText, BorderLayout.CENTER);
			details.setIcon(down);
		}
		else
		{
			mainPanel.remove(summaryText);
			mainPanel.add(detailScroll, BorderLayout.CENTER);
			details.setIcon(up);
		}
		detailsShown = !detailsShown;
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}

	private class StatusCellRenderer extends JLabel implements TableCellRenderer
	{
		private final Color blue;
		private final Color green;
		private final Color orange;
		private final Color red;

		public StatusCellRenderer()
		{
			green = Color.GREEN.darker();
			blue = Color.BLUE.darker();
			orange = Color.ORANGE.darker();
			red = Color.RED.darker();
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column)
		{
			int status = model.getItem(row).getStatus();
			switch (status)
			{
				case Result.Success:
					setForeground(green);
					break;
				case Result.Cancelled:
				case Result.Skipped:
					setForeground(blue);
					break;
				case Result.Orphan:
				case Result.MissingDependencies:
					setForeground(orange);
					break;
				case Result.Exception:
					setForeground(red);
					break;
				default:
			}
			if (isSelected)
			{
				setBackground(table.getSelectionBackground());
			}
			else
			{
				setBackground(table.getBackground());
			}
			setText((String) value);
			return this;
		}
	}
	
	private class DetailTableModel extends AbstractTableModel
	{
		private final String[] colNames = new String[] {
			"Patient Name",
			"ROI Name",
			"Date",
			"Status",
			"Details"
		};
		private final List<DetailItem> items = new ArrayList<>();

		public boolean addAll(List<DetailItem> list)
		{
			int first = items.size();
			boolean b = items.addAll(list);
			fireTableRowsInserted(first, items.size()-1);
			return b;
		}

		public void clear()
		{
			items.clear();
			fireTableDataChanged();
		}

		@Override
		public Class<?> getColumnClass(int idx)
		{
			return ((idx < 0) || (idx >= colNames.length)) ? Object.class : String.class;
		}

		@Override
		public int getColumnCount()
		{
			return colNames.length;
		}

		@Override
		public String getColumnName(int idx)
		{
			return ((idx < 0) || (idx >= colNames.length)) ? "" : colNames[idx];
		}

		public DetailItem getItem(int idx)
		{
			return items.get(idx);
		}

		@Override
		public int getRowCount()
		{
			return items.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex)
		{
			if ((rowIndex < 0) || (rowIndex >= items.size()))
			{
				return null;
			}
			DetailItem item = items.get(rowIndex);
			switch (columnIndex)
			{
				case 0:
					return item.getPatientName();
				case 1:
					return item.getRoiName();
				case 2:
					return item.getDate();
				case 3:
					return item.getStatusString();
				case 4:
					return item.getDetails();
				default:
					return null;
			}
		}
	}

	private abstract class DetailItem<T>
	{
		protected final Result<T> result;

		public DetailItem(Result<T> result)
		{
			this.result = result;
		}

		public abstract String getDate();

		public String getDetails()
		{
			Exception ex = getException();
			return (ex != null) ? ex.getMessage() : "";
		}

		public Exception getException()
		{
			return result.getException();
		}

		public abstract String getPatientName();

		public abstract String getRoiName();

		public int getStatus()
		{
			return result.getStatus();
		}

		public String getStatusString()
		{
			switch (result.getStatus())
			{
				case Result.Success:
					return "Successful";
				case Result.Skipped:
					return "Skipped";
				case Result.Orphan:
					return "Orphan";
				case Result.MissingDependencies:
					return "Missing Dependencies";
				case Result.Cancelled:
					return "Cancelled";
				case Result.Exception:
					return "Error";
				default:
					return "UNKNOWN";
			}
		}
		
	}

	private class IacDetailItem extends DetailItem<ImageAnnotationCollection>
	{
		public IacDetailItem(Result<ImageAnnotationCollection> result)
		{
			super(result);
		}

		@Override
		public String getDate()
		{
			return AimUtils.getDate(
				result.getMetadata().getUploadItem().getDateTime());
		}

		@Override
		public String getPatientName()
		{
			Person person = result.getMetadata().getUploadItem().getPerson();
			return (person != null) ? person.getName() : "Unknown";
		}

		@Override
		public String getRoiName()
		{
			List<ImageAnnotation> list = result.getMetadata().getUploadItem()
				.getAnnotationList();
			return list.isEmpty() ? null : list.get(0).getName();
		}
	}

	private class RtStructDetailItem extends DetailItem<RtStruct>
	{
		public RtStructDetailItem(Result<RtStruct> result)
		{
			super(result);
		}

		@Override
		public String getDate()
		{
			return result.getMetadata().getUploadItem().getStructureSetModule()
				.getStructureSetDate();
		}

		@Override
		public String getPatientName()
		{
			String name = result.getMetadata().getUploadItem().getPatientModule()
				.getPatientName();
			return (name != null) ? name : "Unknown";
		}

		@Override
		public String getRoiName()
		{
			String name = result.getMetadata().getUploadItem()
				.getStructureSetModule().getStructureSetLabel();
			return (name != null) ? name : "Unknown";
		}
	}

	private class SegmentationDetailItem extends DetailItem<Segmentation>
	{
		public SegmentationDetailItem(Result<Segmentation> result)
		{
			super(result);
		}

		@Override
		public String getDate()
		{
			return IodUtils.getDate(result.getMetadata().getUploadItem());
		}

		@Override
		public String getPatientName()
		{
			String name = result.getMetadata().getUploadItem().getPatientModule()
				.getPatientName();
			return (name != null) ? name : "Unknown";
		}

		@Override
		public String getRoiName()
		{
			return IodUtils.getName(result.getMetadata().getUploadItem());
		}
	}

}
