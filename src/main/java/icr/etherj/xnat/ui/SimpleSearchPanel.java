/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.xnat.XnatTag;
import icr.etherj.xnat.XnatToolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public final class SimpleSearchPanel extends JPanel
{
	public static final String DO_SEARCH = "doSearch";

	private static final Logger logger = LoggerFactory.getLogger(
		ProjectSelector.class);

	private final Set<ActionListener> actionListeners = new HashSet<>();
	private final JButton searchButton;
	private final JTextField searchText;

	public SimpleSearchPanel()
	{
		super();
		setBorder(new TitledBorder("Search"));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		searchText = new JTextField(24);
		searchButton = new JButton("Search");
		searchButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				fireActionPerformed();
			}

		});
		add(searchText);
		add(searchButton);
		add(Box.createHorizontalGlue());
	}

	public boolean addActionListener(ActionListener l)
	{
		return actionListeners.add(l);
	}

	public SearchSpecification getSearchSpecification(String projectId)
	{
		SearchSpecification spec = new SearchSpecification();
		XnatToolkit tk = XnatToolkit.getToolkit();
		SearchCriterion procCrit = tk.createSearchCriterion(XnatTag.ProjectID,
			SearchCriterion.Equal, projectId);
		SearchCriterion patCrit = tk.createSearchCriterion(XnatTag.SubjectName,
			SearchCriterion.Like, searchText.getText());
		spec.addCriterion(procCrit);
		spec.addCriterion(patCrit);

		return spec;
	}

	public boolean removeActionListener(ActionListener l)
	{
		return actionListeners.remove(l);
	}

	@Override
	public void setEnabled(boolean enable)
	{
		super.setEnabled(enable);
		searchButton.setEnabled(enable);
		searchText.setEnabled(enable);
	}

	private void fireActionPerformed()
	{
		ActionEvent ev = new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
			DO_SEARCH);
		for (ActionListener l : actionListeners)
		{
			l.actionPerformed(ev);
		}
	}
}
