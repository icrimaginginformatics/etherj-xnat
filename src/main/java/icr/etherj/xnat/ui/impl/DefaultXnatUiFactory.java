/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui.impl;

import icr.etherj.xnat.XnatAnnotationItem;
import icr.etherj.xnat.XnatAssessor;
import icr.etherj.xnat.XnatEntity;
import icr.etherj.xnat.XnatImageAnnotation;
import icr.etherj.xnat.XnatImageAnnotationCollection;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatRegion;
import icr.etherj.xnat.XnatRegionSet;
import icr.etherj.xnat.XnatScan;
import icr.etherj.xnat.XnatSession;
import icr.etherj.xnat.XnatSubject;
import icr.etherj.xnat.ui.XnatTreeCellRenderer;
import icr.etherj.xnat.ui.XnatTreeNode;
import icr.etherj.xnat.ui.XnatUiToolkit;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author jamesd
 */
public class DefaultXnatUiFactory implements XnatUiToolkit.XnatUiFactory
{
	private final static Icon defaultIcon;
	private final static Map<String,Icon> icons = new HashMap<>();
	private final static Map<String,Icon> iconOverlays = new HashMap<>();
	private final static String Project = "Project";
	private final static String Subject = "Subject";
	private final static String Assessor = "Assessor";

	static
	{
		// Icons
		defaultIcon = 
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Question16.png"));
		icons.put(Project,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Project16.png")));
		icons.put(Subject,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Person16.png")));
		icons.put(Assessor,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Examine16.png")));
		icons.put("MRScan",
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Spin16.png")));
		icons.put("MRSession",
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Spin16.png")));
		icons.put("CTScan",
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Skull16.png")));
		icons.put("CTSession",
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Skull16.png")));
		icons.put("PTSession",
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/OrangeDiamond16.png")));
		icons.put(XnatEntity.ImageAnnotationCollection,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/CrosshairGroup16.png")));
		icons.put(XnatEntity.ImageAnnotation,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Crosshair16.png")));
		icons.put(XnatEntity.Markup,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/BlueRoi16.png")));
		icons.put(XnatEntity.RegionSet,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/GreenStack16.png")));
		icons.put(XnatEntity.Region,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/GreenRoi16.png")));

		// Icon overlay
		iconOverlays.put(XnatUiToolkit.ErrorOverlay,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/ErrorOverlay16.png")));
		iconOverlays.put(XnatUiToolkit.WarningOverlay,
			new ImageIcon(DefaultXnatUiFactory.class.getResource(
				"/icr/etherj/ui/resources/WarningOverlay16.png")));
	}

	@Override
	public XnatTreeNode createNode(XnatAssessor assessor)
	{
		XnatTreeNode node;
		String type = assessor.getType();
		switch (type)
		{
			case XnatEntity.ImageAnnotationCollection:
				XnatImageAnnotationCollection iac =
					(XnatImageAnnotationCollection) assessor;
				node = new IacNode(iac);
//				iac.addXnatAssessorContainerListener(node);
				break;

			case XnatEntity.ImageAnnotation:
				node = new IaNode((XnatImageAnnotation) assessor);
				break;

			case XnatEntity.Markup:
				node = new AnnotationItemNode((XnatAnnotationItem) assessor);
				break;

			case XnatEntity.RegionSet:
				node = new RegionSetNode((XnatRegionSet) assessor);
				break;

			case XnatEntity.Region:
				node = new RegionNode((XnatRegion) assessor);
				break;

			default:
				node = new AssessorNode(assessor);
		}
		Icon icon = getIcon(type);
		node.setIcon(icon);
		node.setOpenIcon(icon);
		return node;
	}

	@Override
	public XnatTreeNode createNode(XnatProject project)
	{
		ProjectNode node = new ProjectNode(project);
		Icon icon = getIcon(Project);
		node.setIcon(icon);
		node.setOpenIcon(icon);
		project.addXnatProjectListener(node);
		return node;
	}

	@Override
	public XnatTreeNode createNode(XnatScan scan)
	{
		ScanNode node = new ScanNode(scan);
		Icon icon = getIcon(scan.getModality()+"Scan");
		node.setIcon(icon);
		node.setOpenIcon(icon);
		return node;
	}

	@Override
	public XnatTreeNode createNode(XnatSession session)
	{
		SessionNode node = new SessionNode(session);
		Icon icon = getIcon(session.getModality()+"Session");
		node.setIcon(icon);
		node.setOpenIcon(icon);
		session.addXnatScanContainerListener(node);
		session.addXnatAssessorContainerListener(node);
		return node;
	}

	@Override
	public XnatTreeNode createNode(XnatSubject subject)
	{
		SubjectNode node = new SubjectNode(subject);
		Icon icon = getIcon(Subject);
		node.setIcon(icon);
		node.setOpenIcon(icon);
		subject.addXnatSubjectListener(node);
		return node;
	}

	@Override
	public XnatTreeCellRenderer createTreeCellRenderer()
	{
		XnatTreeCellRenderer xtcr = new DefaultXnatTreeCellRenderer();
		return xtcr;
	}

	@Override
	public Icon getIconOverlay(String key)
	{
		return iconOverlays.getOrDefault(key, null);
	}

	private Icon getIcon(String key)
	{
		return icons.getOrDefault(key, defaultIcon);
	}

}
