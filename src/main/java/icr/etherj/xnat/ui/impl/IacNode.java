/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui.impl;

import icr.etherj.xnat.XnatAssessorContainer;
import icr.etherj.xnat.XnatEntity;
import icr.etherj.xnat.XnatImageAnnotation;
import icr.etherj.xnat.XnatImageAnnotationCollection;
import icr.etherj.xnat.XnatRegionSet;
import icr.etherj.xnat.XnatUtils;
import icr.etherj.xnat.ui.AbstractXnatTreeNode;
import icr.etherj.xnat.ui.XnatTreeNode;
import icr.etherj.xnat.ui.XnatUiToolkit;
import java.util.List;

/**
 *
 * @author jamesd
 */
public final class IacNode extends AbstractXnatTreeNode
	implements XnatAssessorContainer.Listener
{
	private final XnatImageAnnotationCollection iac;

	public IacNode(XnatImageAnnotationCollection iac)
	{
		super(true);
		XnatUiToolkit uiTk = XnatUiToolkit.getToolkit();
		this.iac = iac;
		setDisplayName(iac.getLabel());
		XnatRegionSet regionSet = iac.getRegionSet();
		if (regionSet != null)
		{
			XnatTreeNode node = uiTk.createNode(regionSet);
			addChild(node);
		}
		List<XnatImageAnnotation> annotations = iac.getImageAnnotations();
		for (XnatImageAnnotation annotation : annotations)
		{
			XnatTreeNode node = uiTk.createNode(annotation);
			addChild(node);			
		}
	}

	@Override
	public void assessorAdded(XnatAssessorContainer.Event event)
	{
		if (event.getSource() != iac)
		{
			return;
		}
		XnatTreeNode node = XnatUiToolkit.getToolkit()
			.createNode(event.getAssessor());
		addChild(node);
	}

	@Override
	public void assessorRemoved(XnatAssessorContainer.Event event)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public String getKey()
	{
		return "00_"+iac.getDate()+"_"+
			String.format("%05.0f", XnatUtils.timeToFloat(iac.getTime()))+
			iac.getId();
	}

	@Override
	public XnatEntity getXnatEntity()
	{
		return iac;
	}
	
}
