/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.AbstractDisplayable;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.search.SearchSpecification;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.Study;
import icr.etherj.xnat.Investigator;
import icr.etherj.xnat.XnatAssessor;
import icr.etherj.xnat.XnatDataSource;
import icr.etherj.xnat.XnatEntity;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatId;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatScan;
import icr.etherj.xnat.XnatSearchPlugin;
import icr.etherj.xnat.XnatServerConnection;
import icr.etherj.xnat.XnatSession;
import icr.etherj.xnat.XnatSubject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *	Package-private class used by XnatTreeController
 * @author jamesd
 */
class NullDataSource extends AbstractDisplayable implements XnatDataSource
{

	@Override
	public <T> XnatSearchPlugin<T> addSearchPlugin(Class<T> klass, XnatSearchPlugin<T> plugin)
	{
		return null;
	}

	@Override
	public void deleteEntity(XnatEntity xnatEntity)
	{}

	@Override
	public <T> boolean exists(Class<T> klass, SearchSpecification spec) throws XnatException
	{
		return false;
	}

	@Override
	public List<Investigator> getInvestigators(String projectId) throws XnatException
	{
		return new ArrayList<>();
	}

	@Override
	public RtStruct getRtStructForMarkup(String project, String markupUid)
	{
		return null;
	}

	@Override
	public Map<XnatId, Series> getSeries(String projectId, String uid)
	{
		return new HashMap<>();
	}

	@Override
	public Map<XnatId, Series> getSeries(String projectId, String uid, String modality)
	{
		return new HashMap<>();
	}

	@Override
	public Series getSeries(XnatId id, String uid)
	{
		return null;
	}

	@Override
	public Map<XnatId, Study> getStudies(String projectId, String uid)
	{
		return new HashMap<>();
	}

	@Override
	public XnatServerConnection getXnatServerConnection()
	{
		return null;
	}

	@Override
	public <T> List<T> search(Class<T> klass, SearchSpecification spec) 
		throws XnatException
	{
		return new ArrayList<>();
	}

	@Override
	public List<XnatAssessor> searchAssessor(SearchSpecification spec)
	{
		return new ArrayList<>();
	}

	@Override
	public List<ImageAnnotationCollection> searchIac(String project, String name)
	{
		return new ArrayList<>();
	}

	@Override
	public List<ImageAnnotationCollection> searchIac(SearchSpecification spec)
	{
		return new ArrayList<>();
	}

	@Override
	public List<XnatProject> searchProject(SearchSpecification spec)
	{
		return new ArrayList<>();
	}

	@Override
	public List<XnatScan> searchScan(SearchSpecification spec)
	{
		return new ArrayList<>();
	}

	@Override
	public List<XnatSession> searchSession(SearchSpecification spec)
	{
		return new ArrayList<>();
	}

	@Override
	public List<XnatSubject> searchSubject(SearchSpecification spec)
	{
		return new ArrayList<>();
	}

	@Override
	public <T> boolean supportsSearch(Class<T> klass)
	{
		return false;
	}

}
