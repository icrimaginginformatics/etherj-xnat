/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import com.google.common.collect.SetMultimap;
import icr.etherj.concurrent.Concurrent;
import icr.etherj.concurrent.TaskMonitor;
import icr.etherj.ui.IndeterminateTask;
import icr.etherj.xnat.StudyUidContainer;
import icr.etherj.xnat.UploadMetadata;
import icr.etherj.xnat.XnatUploader;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author jamesd
 * @param <T>
 */
public class UploadMonitorDialog<T> extends JDialog
	implements XnatUploader.EventHandler<T>
{

	private JTextArea ambiText;
	private JList ambiList;
	private DefaultListModel ambiListModel;
	private JCheckBox ambiAll;
	private JPanel ambiPanel;
	private Action cancelAction;
	private JPanel choiceContainer;
	private final List<StudyUidContainer> containerList = new ArrayList<>();
	private XnatUploader.Event<T> event;
	private JCheckBox eventAll;
	private JCheckBox existsAll;
	private JTextArea existsText;
	private JPanel existsPanel;
	private JLabel progressLabel;
	private Action overwriteAction;
	private JProgressBar progressBar;
	private Action selectAction;
	private Action skipAction;
	private IndeterminateTask task;
	private TaskMonitor taskMonitor;

	/**
	 *
	 * @param parent
	 * @param title
	 */
	public UploadMonitorDialog(JFrame parent, String title)
	{
		this(parent, title, null);
	}
	
	/**
	 *
	 * @param parent
	 * @param title
	 * @param taskMonitor
	 */
	public UploadMonitorDialog(JFrame parent, String title,
		TaskMonitor taskMonitor)
	{
		super(parent, title, true);
		createActions();
		layoutUi();
		configTaskMonitor(taskMonitor);
	}

	public TaskMonitor getTaskMonitor()
	{
		return taskMonitor;
	}

	@Override
	public void itemAmbiguous(XnatUploader.ItemAmbiguousEvent<T> event)
	{
		this.event = event;
		choiceContainer.removeAll();
		selectAction.setEnabled(false);
		choiceContainer.add(ambiPanel, BorderLayout.CENTER);
		eventAll = ambiAll;
		UploadMetadata<T> metadata = event.getUploadMetadata();
		ambiText.setText("Upload item '"+metadata.getName()+"' with label '"+
			metadata.getLabel()+
			"' has multiple possible targets. Choose destination.");
		SetMultimap<String,StudyUidContainer> containerMxMap =
			metadata.getStudyUidContainers();
		containerList.clear();
		Set<String> keys = containerMxMap.keySet();
		// Should only be one key, the study UID of the referenced image
		for (String key : keys)
		{
			Set<StudyUidContainer> containers = containerMxMap.get(key);
			containerList.addAll(containers);
		}
		containerList.sort(null);
		ambiListModel.clear();
		for (StudyUidContainer container : containerList)
		{
			ambiListModel.addElement("Subject: "+container.getSubjectLabel()+
				", Session: "+container.getSessionLabel());
		}
		pack();
		setLocationRelativeTo(getParent());
	}

	@Override
	public void itemExists(XnatUploader.ItemExistsEvent<T> event)
	{
		this.event = event;
		choiceContainer.removeAll();
		choiceContainer.add(existsPanel, BorderLayout.CENTER);
		eventAll = existsAll;
		UploadMetadata<T> metadata = event.getUploadMetadata();
		existsText.setText("Upload item '"+metadata.getName()+"' with label '"+
			metadata.getLabel()+"' already exists.");
		pack();
		setLocationRelativeTo(getParent());
	}

	private void configTaskMonitor(TaskMonitor taskMonitor)
	{
		if (taskMonitor != null)
		{
			this.taskMonitor = taskMonitor;
			return;
		}
		this.taskMonitor = Concurrent.getTaskMonitor(true);
	}

	private void createActions()
	{
		cancelAction = new CancelAction();
		skipAction = new SkipAction();
		selectAction = new SelectAction();
		overwriteAction = new OverwriteAction();
	}

	private void createAmbiguousPanel()
	{
		ambiPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 4;
		gbc.ipady = 5;
		ambiText = new JTextArea("Item has multiple possible upload targets");
		ambiText.setEditable(false);
		ambiText.setRows(2);
		ambiText.setLineWrap(true);
		ambiText.setWrapStyleWord(true);
		ambiText.setFont(progressLabel.getFont());
		ambiText.setBackground(progressLabel.getBackground());
		ambiPanel.add(ambiText, gbc);
		gbc.gridy = 1;
		gbc.ipady = 0;
		ambiPanel.add(new JLabel("Destinations"), gbc);
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.BOTH;
		ambiListModel = new DefaultListModel();
		ambiList = new JList(ambiListModel);
		ambiList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ambiList.setLayoutOrientation(JList.VERTICAL);
		ambiList.setVisibleRowCount(3);
		ambiList.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		ambiList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				selectAction.setEnabled(ambiList.getSelectedIndex() != -1);
			}
			
		});
		ambiPanel.add(ambiList, gbc);
		gbc.gridy = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
		ambiAll = new JCheckBox("Do this for all future items");
		ambiPanel.add(ambiAll, gbc);
		gbc.gridx = 1;
		ambiPanel.add(new JButton(selectAction), gbc);
		gbc.gridx = 2;
		ambiPanel.add(new JButton(skipAction), gbc);
		gbc.gridx = 3;
		ambiPanel.add(new JButton(cancelAction), gbc);
	}

	private void createExistsPanel()
	{
		existsPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 4;
		gbc.ipady = 5;
		existsText = new JTextArea("Item already exists");
		existsText.setEditable(false);
		existsText.setRows(2);
		existsText.setLineWrap(true);
		existsText.setWrapStyleWord(true);
		existsText.setFont(progressLabel.getFont());
		existsText.setBackground(progressLabel.getBackground());
		existsPanel.add(existsText, gbc);
		gbc.gridy = 1;
		gbc.ipady = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
		existsAll = new JCheckBox("Do this for all future items");
		existsPanel.add(existsAll, gbc);
		gbc.gridx = 1;
		existsPanel.add(new JButton(overwriteAction), gbc);
		gbc.gridx = 2;
		existsPanel.add(new JButton(skipAction), gbc);
		gbc.gridx = 3;
		existsPanel.add(new JButton(cancelAction), gbc);
	}

	private void layoutUi()
	{
		Container contentPane = getContentPane();
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		progressLabel = new JLabel("Uploading...");
		contentPane.add(progressLabel, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		progressBar = new JProgressBar();
		progressBar.setPreferredSize(new Dimension(400, 24));
		contentPane.add(progressBar, gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.BOTH;
		choiceContainer = new JPanel();
		choiceContainer.setLayout(new BorderLayout());
		contentPane.add(choiceContainer, gbc);
	
		createAmbiguousPanel();
		createExistsPanel();
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosed(WindowEvent event)
			{
				onWindowClosed(event);
			}

			@Override
			public void windowOpened(WindowEvent event)
			{
				onWindowOpened(event);
			}
		});
		pack();
		setLocationRelativeTo(getParent());
	}

	private void monitorPropertyChange(PropertyChangeEvent event)
	{
		Integer intValue;
		switch (event.getPropertyName())
		{
			case TaskMonitor.DESCRIPTION:
				String text = (String) event.getNewValue();
				progressLabel.setText((text != null) ? text : "");
				break;

			case TaskMonitor.VALUE:
				intValue = (Integer) event.getNewValue();
				progressBar.setValue(intValue);
				break;

			case TaskMonitor.INDETERMINATE:
				Boolean flag = (Boolean) event.getNewValue();
				if (flag == null)
				{
					break;
				}
				progressBar.setIndeterminate(flag);
				if (flag)
				{
					task = new IndeterminateTask(progressBar);
					task.start();
				}
				else
				{
					task.cancel();
					task = null;
				}
				break;
				
			case TaskMonitor.MINIMUM:
				intValue = (Integer) event.getNewValue();
				if (intValue != null)
				{
					progressBar.setMinimum(intValue);
				}
				break;

			case TaskMonitor.MAXIMUM:
				intValue = (Integer) event.getNewValue();
				if (intValue != null)
				{
					progressBar.setMaximum(intValue);
				}
				break;

			default:
				StringBuilder sb = new StringBuilder("PropertyChange '");
				String name = event.getPropertyName();
				sb.append((name != null) ? name : "Multiple Properties");
				sb.append("' - Old: ");
				Object value = event.getOldValue();
				sb.append((value != null) ? value : "Null");
				sb.append(", New: ");
				value = event.getNewValue();
				sb.append((value != null) ? value : "Null");
				System.out.println(sb.toString());
		}
	}

	private void onCancel()
	{
		event.cancel();
		event = null;
		choiceContainer.removeAll();
		pack();
		setLocationRelativeTo(getParent());
	}

	private void onOverwrite(boolean all)
	{
		((XnatUploader.ItemExistsEvent) event).overwrite(all);
		event = null;
		choiceContainer.removeAll();
		pack();
		setLocationRelativeTo(getParent());
	}

	private void onSkip(boolean all)
	{
		event.skip(all);
		event = null;
		choiceContainer.removeAll();
		pack();
		setLocationRelativeTo(getParent());
	}

	private void onSelect(boolean all)
	{
		StudyUidContainer selected = containerList.get(
			ambiList.getSelectedIndex());
		choiceContainer.removeAll();
		pack();
		setLocationRelativeTo(getParent());
		((XnatUploader.ItemAmbiguousEvent) event).select(selected, all);
		event = null;
	}

	private void onWindowClosed(WindowEvent event)
	{
		if (task != null)
		{
			task.cancel();
		}
	}

	private void onWindowOpened(WindowEvent event)
	{
		taskMonitor.addPropertyChangeListener(new TaskMonitorListener());
		progressBar.setMinimum(taskMonitor.getMinimum());
		progressBar.setMaximum(taskMonitor.getMaximum());
		progressBar.setValue(taskMonitor.getValue());
		setLocationRelativeTo(getParent());
		if (task != null)
		{
			task.start();
		}
	}

	private final class CancelAction extends AbstractAction
	{
		CancelAction()
		{
			super("Cancel");
			putValue(Action.SHORT_DESCRIPTION, "Cancel uploading");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onCancel();
		}
	}

	private final class OverwriteAction extends AbstractAction
	{
		OverwriteAction()
		{
			super("Overwrite");
			putValue(Action.SHORT_DESCRIPTION, "Overwrite existing item");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onOverwrite(eventAll.isSelected());
		}
	}

	private final class SelectAction extends AbstractAction
	{
		SelectAction()
		{
			super("Select");
			putValue(Action.SHORT_DESCRIPTION, "Select this container");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onSelect(eventAll.isSelected());
		}
	}

	private final class SkipAction extends AbstractAction
	{
		SkipAction()
		{
			super("Skip");
			putValue(Action.SHORT_DESCRIPTION, "Skip this item");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onSkip(eventAll.isSelected());
		}
	}

	private final class TaskMonitorListener implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent event)
		{
			monitorPropertyChange(event);
		}		
	}

}
