/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.crypto.CryptoException;
import icr.etherj.ui.SimpleDocumentListener;
import icr.etherj.ui.TableCellListener;
import icr.etherj.ui.TableCellListener.Change;
import icr.etherj.xnat.Profile;
import icr.etherj.xnat.ProfileManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RootPaneContainer;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author jamesd
 */
public final class ProfileManagerPanel extends JPanel
{
	public static final String IS_EDITING = "isEditing";

	private String currProfileName = "";
	private JButton deleteButton;
	private JButton dupeButton;
	private JTextField hostText;
	private JTextField instanceText;
	private boolean isEditing = false;
	private MouseClickListener mouseClickListener;
	private JButton newButton;
	private JPasswordField passText;
	private JTextField portText;
	private JTable profileTable;
	private final ProfileTableModel profileTableModel;
	private ProfileManager manager;
	private JButton renameButton;
	private JCheckBox sslCheck;
	private JTextField urlText;
	private JTextField userText;

	public ProfileManagerPanel() throws CryptoException
	{
		super(new BorderLayout());
		profileTableModel = new ProfileTableModel();
		manager = new ProfileManager();
		layoutUi();
		updateProfileTable();
		updateUrl();
	}

	/**
	 * @return the manager
	 */
	public ProfileManager getManager()
	{
		return manager;
	}

	/**
	 *
	 * @return
	 */
	public boolean isEditing()
	{
		return isEditing;
	}

	/**
	 * @param manager the manager to set
	 */
	public void setManager(ProfileManager manager)
	{
		if (manager == null)
		{
			throw new IllegalArgumentException();
		}
		this.manager = manager;
		updateProfileTable();
		updateCurrentProfile();
	}

	private void addMouseTrap()
	{
		addAncestorListener(new AncestorListener()
		{
			@Override
			public void ancestorAdded(AncestorEvent event)
			{
				Window window = SwingUtilities.getWindowAncestor(
					ProfileManagerPanel.this);
				if (!(window instanceof RootPaneContainer))
				{
					return;
				}
				RootPaneContainer rpc = (RootPaneContainer) window;
				Component glassPane = rpc.getGlassPane();
				mouseClickListener = new MouseClickListener(glassPane,
					rpc.getContentPane());
				glassPane.addMouseListener(mouseClickListener);
				glassPane.addMouseMotionListener(mouseClickListener);
				glassPane.setVisible(true);
			}

			@Override
			public void ancestorMoved(AncestorEvent event){}

			@Override
			public void ancestorRemoved(AncestorEvent event)
			{
				Window window = SwingUtilities.getWindowAncestor(
					ProfileManagerPanel.this);
				if (!(window instanceof RootPaneContainer))
				{
					return;
				}
				RootPaneContainer rpc = (RootPaneContainer) window;
				Component glassPane = rpc.getGlassPane();
				glassPane.removeMouseListener(mouseClickListener);
				glassPane.removeMouseMotionListener(mouseClickListener);
				mouseClickListener = null;
				saveCurrentProfile();
			}
		});
	}

	private JPanel createButtonPanel()
	{
		JPanel buttonPanel = new JPanel(new GridLayout(2, 2));
		newButton = new JButton("New");
		newButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				onNewPressed();
			}
		});
		buttonPanel.add(newButton);
		renameButton = new JButton("Rename");
		renameButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				onRenamePressed();
			}
		});
		buttonPanel.add(renameButton);
		deleteButton = new JButton("Delete");
		deleteButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				onDeletePressed();
			}
		});
		buttonPanel.add(deleteButton);
		dupeButton = new JButton("Duplicate");
		dupeButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				onDuplicatePressed();
			}
		});
		buttonPanel.add(dupeButton);

		return buttonPanel;
	}

	private Component createLeftPanel()
	{
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.insets = new Insets(1, 1, 1, 1);
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(new JLabel("Profiles"), gbc);
		createTable();
		JScrollPane tableScroll = new JScrollPane(profileTable);
		tableScroll.setMinimumSize(new Dimension(128, 96));
		tableScroll.setPreferredSize(new Dimension(128, 192));
		Color bg = profileTable.getBackground();
		tableScroll.setBackground(bg);
		tableScroll.getViewport().setBackground(bg);
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		panel.add(tableScroll, gbc);
		JPanel buttonPanel = createButtonPanel();
		gbc.gridheight = 1;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		panel.add(buttonPanel, gbc);

		return panel;
	}

	private Component createRightPanel()
	{
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc.insets = new Insets(1, 1, 1, 1);
		JLabel hostLabel = new JLabel("Host");
		hostLabel.setHorizontalAlignment(SwingConstants.LEFT);
		gbc.gridy = 0;
		gbc.gridx = 0;
		panel.add(hostLabel, gbc);
		hostText = new JTextField(24);
		hostText.getDocument().addDocumentListener(new SimpleDocumentListener()
		{
			@Override
			public void textChanged(DocumentEvent de)
			{
				updateUrl();
			}
		});
		gbc.gridy = 1;
		panel.add(hostText, gbc);
		JLabel portLabel = new JLabel("Port");
		gbc.gridy = 0;
		gbc.gridx = 1;
		panel.add(portLabel, gbc);
		portText = new JTextField(5);
		portText.getDocument().addDocumentListener(new SimpleDocumentListener()
		{
			@Override
			public void textChanged(DocumentEvent de)
			{
				portTextChanged(de);
			}
		});
		gbc.gridy = 1;
		panel.add(portText, gbc);
		JLabel instanceLabel = new JLabel("Instance");
		gbc.gridy = 2;
		gbc.gridx = 0;
		panel.add(instanceLabel, gbc);
		instanceText = new JTextField(24);
		instanceText.getDocument().addDocumentListener(new SimpleDocumentListener()
		{
			@Override
			public void textChanged(DocumentEvent de)
			{
				updateUrl();
			}
		});
		gbc.gridy = 3;
		panel.add(instanceText, gbc);
		sslCheck = new JCheckBox("Use SSL");
		sslCheck.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				updateUrl();
			}	
		});
		gbc.gridy = 4;
		panel.add(sslCheck, gbc);
		JLabel urlLabel = new JLabel("URL");
		gbc.gridy = 5;
		panel.add(urlLabel, gbc);
		urlText = new JTextField();
		urlText.setEnabled(false);
		gbc.gridy = 6;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		panel.add(urlText, gbc);
		Component padding = Box.createVerticalStrut(2);
		gbc.gridy = 7;
		panel.add(padding, gbc);
		gbc.gridy = 8;
		panel.add(new JSeparator(SwingConstants.HORIZONTAL), gbc);
		padding = Box.createVerticalStrut(2);
		gbc.gridy = 9;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		panel.add(padding, gbc);
		JLabel userLabel = new JLabel("Username");
		gbc.gridy = 10;
		panel.add(userLabel, gbc);
		userText = new JTextField(24);
		gbc.gridy = 11;
		panel.add(userText, gbc);
		JLabel passLabel = new JLabel("Password");
		gbc.gridy = 12;
		panel.add(passLabel, gbc);
		passText = new JPasswordField(24);
		gbc.gridy = 13;
		panel.add(passText, gbc);

		gbc.gridy = 100;
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.weighty = 1.0;
		panel.add(Box.createVerticalGlue(), gbc);

		return panel;
	}

	private void createTable()
	{
		profileTable = new JTable(profileTableModel);
		Action start = new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setEditing(true);
			}
		};
		Action stop = new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent e)
			{
				endEdit(e);
			}
		};
		TableCellListener.newInstance(profileTable, start, stop);
		profileTable.setTableHeader(null);
		profileTable.setRowHeight(20);
		profileTable.setShowGrid(false);
		profileTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		profileTable.getSelectionModel()
			.addListSelectionListener(new ListSelectionListener()
			{
				@Override
				public void valueChanged(ListSelectionEvent lse)
				{
					if (!lse.getValueIsAdjusting())
					{
						onProfileSelect(lse);
					}
				}
			});
		addMouseTrap();
	}

	private void editNewProfile()
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				int idx = profileTableModel.indexOf(currProfileName);
				idx = profileTable.convertRowIndexToView(idx);
				profileTable.setRowSelectionInterval(idx, idx);
				updateCurrentProfile();
				startEdit(idx);
			}
		});
	}

	private void enableButtons(boolean enable)
	{
		deleteButton.setEnabled(enable);
		dupeButton.setEnabled(enable);
		newButton.setEnabled(enable);
		renameButton.setEnabled(enable);
	}

	private void endEdit(ActionEvent e)
	{
		final Change change = (Change) e.getSource();
		String newValue = (String) change.getNewValue();
		final int idx = change.getRow();
		if (!newValue.equals((String) change.getOldValue()) &&
			 manager.containsProfile(newValue))
		{
			// Reject edit as a different profile with that name already exists.
			java.awt.Toolkit.getDefaultToolkit().beep();
			setEditing(false);
			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run()
				{
					profileTableModel.setValueAt(change.getOldValue(), idx, 0);
					startEdit(idx);
				}			
			});
			return;
		}
		Profile oldProfile = manager.getProfile(currProfileName);
		manager.removeProfile(oldProfile);
		Profile newProfile = new Profile(newValue);
		saveUiValuesToProfile(newProfile);
		manager.addProfile(newProfile);
		profileTableModel.setValueAt(newValue, idx, 0);
		currProfileName = newValue;
		manager.setCurrentProfileName(currProfileName);
		setEditing(false);
	}

	private String findNewName(String baseName)
	{
		Set<String> names = manager.getProfileNames();
		String name = baseName;
		if (!names.contains(name))
		{
			return name;
		}
		int idx = 1;
		do
		{
			name = baseName+" ("+idx+")";
			idx++;
		} while (names.contains(name));
		return name;
	}

	private void layoutUi()
	{
		setPreferredSize(new Dimension(512, 320));
		add(createLeftPanel(), BorderLayout.CENTER);
		add(createRightPanel(), BorderLayout.EAST);
	}

	private void onDeletePressed()
	{
		int idx = profileTable.getSelectedRow();
		manager.removeProfile(manager.getProfile(currProfileName));
		profileTableModel.remove(currProfileName);
		idx = (idx > 0) ? idx-1 : 0;
		if (profileTableModel.getRowCount() > 0)
		{
			currProfileName = (String) profileTable.getValueAt(idx, 0);
			profileTable.setRowSelectionInterval(idx, idx);
		}
		else
		{
			currProfileName = "";
			updateCurrentProfile();
		}
	}

	private void onDuplicatePressed()
	{
		Profile old = manager.getProfile(currProfileName);
		saveUiValuesToProfile(old);
		String newName = findNewName(currProfileName);
		Profile dupe = new Profile(newName);
		dupe.setServer(old.getServer());
		dupe.setPort(old.getPort());
		dupe.setInstance(old.getInstance());
		dupe.setUsingSsl(old.isUsingSsl());
		dupe.setUserName(old.getUserName());
		dupe.setPassword(old.getPassword());
		manager.addProfile(dupe);
		profileTableModel.add(newName);
		currProfileName = newName;
		manager.setCurrentProfileName(currProfileName);
		editNewProfile();
	}

	private void onNewPressed()
	{
		saveCurrentProfile();
		String newName = findNewName("New Profile");
		Profile profile = new Profile(newName);
		manager.addProfile(profile);
		profileTableModel.add(newName);
		currProfileName = newName;
		manager.setCurrentProfileName(currProfileName);
		editNewProfile();
	}

	private void onProfileSelect(ListSelectionEvent lse)
	{
		int idx = profileTable.getSelectedRow();
		if (idx < 0)
		{
			updateCurrentProfile();
			return;
		}
		String name = (String) profileTableModel.getValueAt(idx, 0);
		if (!name.equals(currProfileName))
		{
			saveCurrentProfile();
		}
		currProfileName = name;
		manager.setCurrentProfileName(currProfileName);
		updateCurrentProfile();
	}

	private void onRenamePressed()
	{
		int idx = profileTableModel.indexOf(currProfileName);
		startEdit(idx);
	}

	private void portTextChanged(DocumentEvent de)
	{
		String port = portText.getText();
		int intPort = 0;
		try
		{
			intPort = Integer.parseInt(port);
		}
		catch (NumberFormatException ex)
		{
			if (de.getType() != DocumentEvent.EventType.REMOVE)
			{
				Toolkit.getDefaultToolkit().beep();
			}
		}
		updateUrl((intPort < 1) || (intPort > 65535));
	}

	private void saveCurrentProfile()
	{
		Profile profile = manager.getProfile(currProfileName);
		saveUiValuesToProfile(profile);
	}

	private void saveUiValuesToProfile(Profile profile)
	{
		profile.setServer(hostText.getText());
		int port = 0;
		try
		{
			port = Integer.parseInt(portText.getText());
		}
		catch (NumberFormatException ex)
		{ /* Deliberate no-op */ }
		profile.setPort(port);
		profile.setInstance(instanceText.getText());
		profile.setUsingSsl(sslCheck.isSelected());
		profile.setUserName(userText.getText());
		char[] userPass = passText.getPassword();
		profile.setPassword(new String(userPass));
	}

	private void setEditing(boolean editing)
	{
		boolean oldValue = isEditing;
		isEditing = editing;
		enableButtons(!editing);
		firePropertyChange(IS_EDITING, oldValue, editing);
	}

	private boolean startEdit(int idx)
	{
		boolean edit = profileTable.editCellAt(idx, 0);
		if (!edit)
		{
			return edit;
		}
		Component editor = profileTable.getEditorComponent();
		if ((editor == null) || !(editor instanceof JTextComponent))
		{
			return edit;
		}
		setEditing(true);
		((JTextComponent) editor).selectAll();
		((JTextComponent) editor).requestFocus();
		return edit;
	}

	private void updateCurrentProfile()
	{
		Profile profile = manager.getProfile(currProfileName);
		if (profile == null)
		{
			hostText.setText("");
			portText.setText("");
			instanceText.setText("");
			sslCheck.setSelected(true);
			userText.setText("");
			passText.setText("");
			updateUrl();
			return;
		}
		hostText.setText(profile.getServer());
		int port = profile.getPort();
		portText.setText((port > 0) ? String.valueOf(port) : "");
		instanceText.setText(profile.getInstance());
		sslCheck.setSelected(profile.isUsingSsl());
		userText.setText(profile.getUserName());
		passText.setText(profile.getPassword());
		updateUrl();
	}

	private void updateProfileTable()
	{
		profileTableModel.clear();
		Set<String> keys = manager.getProfileNames();
		if (keys.isEmpty())
		{
			Profile newProfile = new Profile("New Profile");
			manager.addProfile(newProfile);
			keys = manager.getProfileNames();
		}
		for (String key : keys)
		{
			profileTableModel.add(key);
		}
		currProfileName = manager.getCurrentProfileName();
		int idx = profileTableModel.indexOf(currProfileName);
		if (idx >= 0)
		{
			profileTable.setRowSelectionInterval(idx, idx);
		}
	}

	private void updateUrl()
	{
		updateUrl(false);
	}

	private void updateUrl(boolean ignorePort)
	{
		StringBuilder sb = new StringBuilder("http");
		sb.append(sslCheck.isSelected() ? "s" : "").append("://")
			.append(hostText.getText());
		String port = portText.getText();
		if (!ignorePort && !port.isEmpty())
		{
			sb.append(":").append(port);
		}
		sb.append("/").append(instanceText.getText());
		urlText.setText(sb.toString());
		urlText.setToolTipText(urlText.getText());
	}

	private class MouseClickListener extends MouseInputAdapter
	{

		private final Container contentPane;
		private final Component glassPane;

		public MouseClickListener(Component glassPane, Container contentPane)
		{
			this.glassPane = glassPane;
			this.contentPane = contentPane;
		}

		@Override
		public void mouseClicked(MouseEvent e)
		{
			if (profileTable.isEditing() && (e.getClickCount() == 1))
			{
				profileTable.getCellEditor().stopCellEditing();
			}
			redispatchMouseEvent(e);
		}

		@Override
		public void mouseDragged(MouseEvent e)
		{
			redispatchMouseEvent(e);
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			redispatchMouseEvent(e);
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			redispatchMouseEvent(e);
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			redispatchMouseEvent(e);
		}

		@Override
		public void mouseMoved(MouseEvent e)
		{
			redispatchMouseEvent(e);
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			redispatchMouseEvent(e);
		}

		private void redispatchMouseEvent(MouseEvent e)
		{
			Point glassPanePoint = e.getPoint();
			Point containerPoint = SwingUtilities.convertPoint(glassPane,
				glassPanePoint, contentPane);
			if (containerPoint.y >= 0)
			{
				//The mouse event is probably over the content pane. Find out 
				// exactly which component it's over.  
				Component component = SwingUtilities.getDeepestComponentAt(contentPane,
					containerPoint.x,containerPoint.y);
				if (component != null)
				{
					Point componentPoint = SwingUtilities.convertPoint(glassPane,
						glassPanePoint, component);
					component.dispatchEvent(new MouseEvent(
						component,e.getID(), e.getWhen(), e.getModifiers(),
						componentPoint.x, componentPoint.y, e.getClickCount(),
						e.isPopupTrigger()));
				}
			}
		}
	}

	private class ProfileTableModel extends AbstractTableModel
	{
		private final List<String> profiles = new ArrayList<>();
		private final Comparator<String> comparator = new Comparator<String>()
		{
			@Override
			public int compare(String a, String b)
			{
				return a.compareTo(b);
			}
		};

		public boolean add(String name)
		{
			if (profiles.contains(name))
			{
				return false;
			}
			boolean added = profiles.add(name);
			profiles.sort(comparator);
			fireTableDataChanged();
			return added;
		}

		public void clear()
		{
			profiles.clear();
			fireTableDataChanged();
		}

		@Override
		public int getRowCount()
		{
			return profiles.size();
		}

		@Override
		public int getColumnCount()
		{
			return 1;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex)
		{
			return (rowIndex < profiles.size())
				? profiles.get(rowIndex)
				: null;
		}

		public int indexOf(String name)
		{
			return profiles.indexOf(name);
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex)
		{
			return true;
		}

		public boolean remove(String name)
		{
			boolean removed = profiles.remove(name);
			if (removed)
			{
				profiles.sort(comparator);
				fireTableDataChanged();
			}
			return removed;
		}

		@Override
		public void setValueAt(Object value, int rowIndex, int colIndex)
		{
			profiles.set(rowIndex, (String) value);
			fireTableRowsUpdated(rowIndex, rowIndex);
		}
	}

}
