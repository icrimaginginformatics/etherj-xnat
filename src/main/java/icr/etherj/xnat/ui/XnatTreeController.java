/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.ui;

import icr.etherj.ExceptionCode;
import icr.etherj.Unstable;
import icr.etherj.concurrent.CallbackTaskResult;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.xnat.XnatAssessor;
import icr.etherj.xnat.XnatDataSource;
import icr.etherj.xnat.XnatEntity;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatImageAnnotation;
import icr.etherj.xnat.XnatProject;
import icr.etherj.xnat.XnatRegionSet;
import icr.etherj.xnat.XnatScan;
import icr.etherj.xnat.XnatSession;
import icr.etherj.xnat.XnatCode;
import icr.etherj.xnat.XnatSubject;
import icr.etherj.xnat.XnatTag;
import icr.etherj.xnat.XnatToolkit;
import icr.etherj.xnat.ui.impl.SessionNode;
import icr.etherj.xnat.ui.impl.SubjectNode;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
@Unstable
public class XnatTreeController
{
	private static final Logger logger =
		LoggerFactory.getLogger(XnatTreeController.class);

	private Action deleteAction;
	private final ExecutorService netIoExec = Executors.newSingleThreadExecutor();
	private JPopupMenu popupMenu;
	private XnatTreeNode root;
	private final XnatToolkit toolkit;
	private final JTree tree;
	private final XnatUiToolkit uiToolkit;
	private XnatDataSource xds;

	/**
	 *
	 */
	public XnatTreeController()
	{
		this(XnatUiToolkit.getToolkit(), XnatToolkit.getToolkit());
	}

	/**
	 *
	 * @param uiToolkit
	 * @param toolkit
	 */
	public XnatTreeController(XnatUiToolkit uiToolkit, XnatToolkit toolkit)
	{
		this.uiToolkit = uiToolkit;
		this.toolkit = toolkit;
		tree = new JTree();
		root = uiToolkit.createNode(new XnatProject("VOID"));
		tree.setModel(new DefaultTreeModel(root));
		tree.setRootVisible(false);
		tree.setShowsRootHandles(true);
		tree.setCellRenderer(uiToolkit.createTreeCellRenderer());
		tree.addTreeSelectionListener(new TreeSelectionListener()
		{
			@Override
			public void valueChanged(TreeSelectionEvent e)
			{
				treeSelectionChanged(e);
			}
		});
		tree.addTreeWillExpandListener(new TreeWillExpandListener()
		{
			@Override
			public void treeWillCollapse(TreeExpansionEvent e) throws ExpandVetoException
			{
				XnatTreeController.this.treeWillCollapse(e);
			}

			@Override
			public void treeWillExpand(TreeExpansionEvent e) throws ExpandVetoException
			{
				XnatTreeController.this.treeWillExpand(e);
			}
		});
		tree.addMouseListener(new TreeMouseListener());
		xds = new NullDataSource();

		popupMenu = new JPopupMenu();
		deleteAction = new DeleteAction();
		popupMenu.add(deleteAction);
	}

	public void expandPath(TreePath path)
	{
		tree.expandPath(path);
	}

	/**
	 *
	 * @return
	 */
	public JComponent getComponent()
	{
		return tree;
	}

	/**
	 *
	 * @return
	 */
	public XnatTreeNode getRootNode()
	{
		return root;
	}

	/**
	 *
	 * @param xds
	 */
	public void setDataSource(XnatDataSource xds)
	{
		if (xds != null)
		{
			this.xds = xds;
		}
		else
		{
			this.xds = new NullDataSource();
		}
		setRootNode(uiToolkit.createNode(new XnatProject("VOID")));
	}

	/**
	 *
	 * @param root
	 */
	public void setRootNode(XnatTreeNode root)
	{
		this.root = root;
		tree.setModel(new DefaultTreeModel(root));
	}

	private void assessorIntegrityCheck(SessionNode sessionNode)
	{
		XnatTreeNode apn = sessionNode.getAssessorParentNode();
		sessionNode.setIconOverlay(null);
		if (apn == null)
		{
			return;
		}
		apn.setIconOverlay(null);
		boolean fail = false;
		int nNodes = apn.getChildCount();
		for (int i=0; i<nNodes; i++)
		{
			XnatTreeNode node = (XnatTreeNode) apn.getChildAt(i);
			XnatEntity entity = node.getXnatEntity();
			String entityType = entity.getType();
			// Only allowable top level annotations are IACs and RegionSets which
			// do not have originalType of IAC.
			switch (entityType)
			{
				case XnatEntity.ImageAnnotationCollection:
					fail |= iacIntegrityCheck(node);
					break;

				case XnatEntity.RegionSet:
					XnatRegionSet rs = (XnatRegionSet) entity;
					if (rs.getOriginalType().equals(XnatEntity.ImageAnnotationCollection))
					{
						fail = true;
						node.setIconOverlay(
							uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay));
						break;
					}
					fail |= regionSetIntegrityCheck(node);
					break;

				default:
					fail = true;
					node.setIconOverlay(
						uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay));
			}
		}
		if (fail)
		{
			apn.setIconOverlay(
				uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay));
			sessionNode.setIconOverlay(
				uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay));
		}
	}

	private void assessorIntegrityCheckDone(
		CallbackTaskResult<Void> result, SessionNode sessionNode)
	{
		tree.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		logger.info("Assessor integrity check done for session node: "+
			sessionNode.getDisplayName());
	}

	private void assessorListFetch(SessionNode node)
	{
		XnatSession session = (XnatSession) node.getXnatEntity();
		SearchSpecification spec = new SearchSpecification();
		SearchCriterion sc = toolkit.createSearchCriterion(XnatTag.SessionID,
			SearchCriterion.Equal, session.getId());
		spec.addCriterion(sc);
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		AssessorListFetchWorker alfw = new AssessorListFetchWorker(spec, node);
		netIoExec.submit(alfw);
	}

	private void assessorListFetchDone(
		CallbackTaskResult<List<XnatAssessor>> result, SessionNode sessionNode)
	{
		tree.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<XnatAssessor> assessors = result.get();
		XnatSession session = (XnatSession) sessionNode.getXnatEntity();
		for (XnatAssessor assessor : assessors)
		{
			session.add(assessor);
		}
		DefaultTreeModel dtm = (DefaultTreeModel)(tree.getModel());
		dtm.nodeStructureChanged(sessionNode);
		AssessorIntegrityWorker aiw = new AssessorIntegrityWorker(sessionNode);
		netIoExec.submit(aiw);
	}

	private List<XnatTreeNode> deleteNodes(List<XnatTreeNode> nodes)
		throws XnatException
	{
		// TODO: Make this operation cancellable with a gui progress dialog
		List<XnatTreeNode> deletedNodes = new ArrayList<>();

		logger.info("Building list of entities for deletion");
		for (XnatTreeNode node : nodes)
		{
			XnatTreeNode nodeToDelete = node;
			while ((nodeToDelete != null) && 
				!isTopLevel(nodeToDelete))
			{
				XnatTreeNode parent = (XnatTreeNode) nodeToDelete.getParent();
				if (parent.getXnatEntity().getId().equals(XnatEntity.AssessorContainer))
				{
					// Parent is a SessionNode's AssessorContainerNode and this node
					// should be deleted
					XnatEntity entity = nodeToDelete.getXnatEntity();
					logger.info("Possible orphaned entity "+entity.getType()+" ID: "+
						entity.getId());
					break;
				}
				nodeToDelete = parent;
			}
			deletedNodes.add(nodeToDelete);
		}
		logger.info("Deleting "+deletedNodes.size()+" entities from XNAT");
		for (XnatTreeNode node : deletedNodes)
		{
			xds.deleteEntity(node.getXnatEntity());
		}
		logger.info("Entity deletion complete");
		return deletedNodes;
	}

	private void deleteNodesDone(CallbackTaskResult<List<XnatTreeNode>> result)
	{
		if (!result.success())
		{
			tree.setCursor(Cursor.getDefaultCursor());
			handleNetIoException(result.getException());
			return;
		}
		logger.debug("Deleting UI nodes");
		TreePath ancestorPath = null;
		List<XnatTreeNode> deletedNodes = result.get();
		// Find common ancestor
		for (XnatTreeNode node : deletedNodes)
		{
			XnatTreeNode parentNode = (XnatTreeNode) node.getParent();
			ancestorPath = findAncestorPath(ancestorPath, parentNode);
		}
		// Delete nodes
		for (XnatTreeNode node : deletedNodes)
		{
			XnatTreeNode parentNode = (XnatTreeNode) node.getParent();
			XnatEntity parentEntity = parentNode.getXnatEntity();
			XnatEntity entity = node.getXnatEntity();
			logger.debug("Removing "+entity.getType()+" ID: "+entity.getId()+
				" from "+parentEntity.getType()+" ID: "+parentEntity.getId());
			removeEntityFromParent(entity, parentEntity, parentNode);
		}
		logger.debug(deletedNodes.size()+" nodes deleted");
		DefaultTreeModel dtm = (DefaultTreeModel)(tree.getModel());
		// If ancestorPath isn't null, iteratively shorten until last component's
		// parent isn't null e.g. assessorContainerNode hasn't been deleted
		XnatTreeNode ancestorNode = null;
		if (ancestorPath == null)
		{
			ancestorNode = root;
		}
		else
		{
			ancestorNode = (XnatTreeNode) ancestorPath.getLastPathComponent();
			while ((ancestorNode.getParent() == null) &&
				(ancestorPath.getPathCount() > 1))
			{
				ancestorPath = ancestorPath.getParentPath();
				ancestorNode = (XnatTreeNode) ancestorPath.getLastPathComponent();
			}
		}
		dtm.nodeStructureChanged(ancestorNode);
		runIntegrityChecks(ancestorNode);
		tree.scrollRowToVisible(tree.getRowForPath(ancestorPath));
		logger.debug("Tree UI update complete");
		tree.setCursor(Cursor.getDefaultCursor());
	}

	private TreePath findAncestorPath(TreePath ancestorPath,
		XnatTreeNode parentNode)
	{
		TreePath path = nodeToTreePath(parentNode);
		if (ancestorPath == null)
		{
			return path;
		}
		// Path equal to or further down tree than current ancestorPath
		if (ancestorPath.equals(path) || ancestorPath.isDescendant(path))
		{
			return ancestorPath;
		}
		// Iterate up tree until ancestor found
		while (!path.isDescendant(ancestorPath))
		{
			ancestorPath = path.getParentPath();
		}
		return ancestorPath;
	}

	private void handleNetIoException(Exception ex)
	{
		if (ex instanceof InterruptedException)
		{
			logger.info("Operation cancelled");
			return;
		}
		Throwable cause = ex.getCause();
		// If it isn't an XnatException something worse has happened
		if (!(cause instanceof XnatException))
		{
			logger.error("Error connecting to server", cause);
			return;
		}
		XnatException exXnat = (XnatException) cause;
		ExceptionCode code = exXnat.getCode();
		if (code.equals(XnatCode.UnknownHost))
		{
			logger.error("Unknown host", exXnat);
		}
		else if (code.equals(XnatCode.Connect))
		{
			logger.error("Error connecting to server", exXnat);
		}
		else if (code.equals(XnatCode.HttpUnauthorised))
		{
			logger.error("Login credentials invalid", exXnat);
		}
		else
		{
			logger.error("Error creating server connection", exXnat);
		}
	}

	private boolean iaIntegrityCheck(XnatTreeNode iaNode)
	{
		XnatImageAnnotation ia = (XnatImageAnnotation) iaNode.getXnatEntity();
		boolean fail =
			(ia.getAnnotationRoleCount() != ia.getSchemaAnnotationRoleCount()) ||
			(ia.getCalculationCount() != ia.getSchemaCalculationCount()) ||
			(ia.getImagingObservationCount() != ia.getSchemaImagingObservationCount()) ||
			(ia.getImagingPhysicalCount() != ia.getSchemaImagingPhysicalCount()) ||
			(ia.getInferenceCount() != ia.getSchemaInferenceCount()) ||
			(ia.getMarkupCount() != ia.getSchemaMarkupCount()) ||
			(ia.getSegmentationCount() != ia.getSchemaSegmentationCount()) ||
			(ia.getTaskContextCount() != ia.getSchemaTaskContextCount());
		iaNode.setIconOverlay(fail
			? uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay)
			: null);
		return fail;
	}

	private boolean iacIntegrityCheck(XnatTreeNode iacNode)
	{
		boolean fail = false;
		int nNodes = iacNode.getChildCount();
		for (int i=0; i<nNodes; i++)
		{
			XnatTreeNode node = (XnatTreeNode) iacNode.getChildAt(i);
			XnatEntity entity = node.getXnatEntity();
			String entityType = entity.getType();
			// Only allowable children are IAs and RegionSets which
			// do not have originalType of IAC.
			switch (entityType)
			{
				case XnatEntity.ImageAnnotation:
					fail |= iaIntegrityCheck(node);
					break;

				case XnatEntity.RegionSet:
					fail |= regionSetIntegrityCheck(node);
					break;

				default:
					fail = true;
					logger.warn("Unknown IAC child type: "+entityType);
					node.setIconOverlay(
						uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay));
			}
		}
		iacNode.setIconOverlay(fail
			? uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay)
			: null);
		return fail;
	}

	private boolean isTopLevel(XnatTreeNode node)
	{
		boolean isTop = false;
		XnatEntity entity = node.getXnatEntity();
		String type = entity.getType();
		switch (type)
		{
			case XnatEntity.ImageAnnotation:
			case XnatEntity.AnnotationRole:
			case XnatEntity.Calculation:
			case XnatEntity.ImageObservation:
			case XnatEntity.ImagePhysical:
			case XnatEntity.Inference:
			case XnatEntity.Markup:
			case XnatEntity.Segmentation:
			case XnatEntity.TaskContext:
				break;

			case XnatEntity.ImageAnnotationCollection:
				isTop = true;
				break;

			case XnatEntity.RegionSet:
				XnatRegionSet rs = (XnatRegionSet) entity;
				isTop = !rs.getOriginalType().equals(XnatEntity.AimInstance);
				break;

			default:
		}
		return isTop;
	}

	private TreePath nodeToTreePath(XnatTreeNode node)
	{
		List<Object> nodes = new ArrayList<>();
		if (node != null)
		{
			nodes.add(node);
			node = (XnatTreeNode) node.getParent();
			while (node != null)
			{
				nodes.add(0, node);
				node = (XnatTreeNode) node.getParent();
			}
		}
		return nodes.isEmpty() ? null : new TreePath(nodes.toArray());
	}

	private void performDelete(ActionEvent e)
	{
		logger.debug("performDelete()");
		TreePath[] paths = tree.getSelectionPaths();
		List<XnatTreeNode> nodes = new ArrayList<>();
		for (TreePath path : paths)
		{
			XnatTreeNode node = (XnatTreeNode) path.getLastPathComponent();
			String type = node.getXnatEntity().getType();
			switch (type)
			{
				// Ignore all but assessors for now
				case XnatEntity.Project:
				case XnatEntity.Subject:
				case XnatEntity.Session:
				case XnatEntity.Scan:
					break;

				default:
					nodes.add(node);
			}
		}
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		DeleteWorker dw = new DeleteWorker(nodes);
		netIoExec.submit(dw);
	}

	private boolean regionSetIntegrityCheck(XnatTreeNode rsNode)
	{
		XnatRegionSet rs = (XnatRegionSet) rsNode.getXnatEntity();
		boolean fail = (rs.getRegionCount() != rs.getSchemaRegionCount());
		rsNode.setIconOverlay(fail
			? uiToolkit.getIconOverlay(XnatUiToolkit.ErrorOverlay)
			: null);
		return fail;
	}

	private void removeEntityFromParent(XnatEntity entity,
		XnatEntity parentEntity, XnatTreeNode parentNode)
	{
		String parentType = parentEntity.getType();
		// Handle existence of container nodes
		if (parentType.equals(XnatEntity.Container))
		{
			parentNode = (XnatTreeNode) parentNode.getParent();
			parentEntity = parentNode.getXnatEntity();
			parentType = parentEntity.getType();
		}
		switch (parentType)
		{
			case XnatEntity.Session:
				XnatSession session = (XnatSession) parentEntity;
				if (entity instanceof XnatAssessor)
				{
					session.remove((XnatAssessor) entity);
					break;
				}
				if (entity instanceof XnatScan)
				{
					session.remove((XnatScan) entity);
					break;
				}
				logger.warn("Unknown child type of Session ID: "+session.getId()+
					" -> Type: "+entity.getType()+", ID: "+entity.getId());
				break;

			default:
				logger.warn("Unsupported parent type: "+parentType);
		}
	}

	private void runIntegrityChecks(XnatTreeNode node)
	{
		String type = node.getXnatEntity().getType();
		switch (type)
		{
			case XnatEntity.Session:
				sessionIntegrityCheck((SessionNode) node);
				break;

			case XnatEntity.Subject:
				for (int i=0; i<node.getChildCount(); i++)
				{
					sessionIntegrityCheck((SessionNode) node.getChildAt(i));
				}
				break;

			default:
				logger.warn("Ignoring integrity checks on node type: "+type);
		}
	}

	private void scanListFetchDone(
		CallbackTaskResult<List<XnatScan>> result, SessionNode sessionNode)
	{
		tree.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<XnatScan> scans = result.get();
		XnatSession session = (XnatSession) sessionNode.getXnatEntity();
		for (XnatScan scan : scans)
		{
			session.add(scan);
		}
		DefaultTreeModel dtm = (DefaultTreeModel)(tree.getModel());
		dtm.nodeStructureChanged(sessionNode);
		assessorListFetch(sessionNode);
	}

	private void sessionIntegrityCheck(SessionNode sessionNode)
	{
		if (!sessionNode.isInitialised())
		{
			return;
		}
		AssessorIntegrityWorker aiw = new AssessorIntegrityWorker(sessionNode);
		netIoExec.submit(aiw);
	}

	private void sessionListFetchDone(
		CallbackTaskResult<List<XnatSession>> result, SubjectNode subjectNode)
	{
		tree.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<XnatSession> sessions = result.get();
		XnatSubject subject = (XnatSubject) subjectNode.getXnatEntity();
		for (XnatSession session : sessions)
		{
			subject.add(session);
		}
		DefaultTreeModel dtm = (DefaultTreeModel)(tree.getModel());
		dtm.nodeStructureChanged(subjectNode);
	}

	private void treeMouseClicked(MouseEvent e)
	{
		logger.debug("treeMouseClicked()");
		if (!SwingUtilities.isRightMouseButton(e))
		{
			return;
		}
		TreePath clickPath = tree.getPathForLocation(e.getX(), e.getY());
		TreePath[] selectionPaths = tree.getSelectionPaths();
		if ((selectionPaths == null) || (selectionPaths.length == 0))
		{
			tree.setSelectionPath(clickPath);
		}
		else
		{
			// Did the user click within the existing selection? If not, update
			// selection to where they did click
			boolean found = false;
			for (TreePath path : selectionPaths)
			{
				if (path.equals(clickPath))
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				tree.setSelectionPath(clickPath);
			}
		}
		// ToDo: Configure popupMenu based on selection
		popupMenu.show(e.getComponent(), e.getX(), e.getY());
	}

	private void treeSelectionChanged(TreeSelectionEvent e)
	{
		logger.debug("TreeSelectionChanged");
	}

	private void treeWillCollapse(TreeExpansionEvent e)
		throws ExpandVetoException
	{
		logger.debug("TreeWillCollapse");
	}

	private void treeWillExpand(TreeExpansionEvent e) throws ExpandVetoException
	{
		logger.debug("TreeWillExpand");
		TreePath path = e.getPath();
		XnatTreeNode node = (XnatTreeNode) path.getLastPathComponent();
		if (node.isInitialised())
		{
			return;
		}
		logger.debug("Node not initialised: "+node.getDisplayName());
		XnatEntity entity = node.getXnatEntity();
		switch (entity.getType())
		{
			case XnatEntity.Project:
				willExpandProject((XnatProject) entity);
				break;

			case XnatEntity.Subject:
				willExpandSubject((SubjectNode) node, (XnatSubject) entity);
				break;

			case XnatEntity.Session:
				willExpandSession((SessionNode) node, (XnatSession) entity);
				break;

			case XnatEntity.ImageAnnotation:
			case XnatEntity.RegionSet:
				// No action needed
				break;

			default:
				logger.warn("Unsupported XnatEntity type: "+entity.getType());
		}
	}

	private void willExpandProject(XnatProject project)
	{
		logger.warn("Uninitialised project: "+project.getId());
	}

	private void willExpandSession(SessionNode node, XnatSession session)
	{
		logger.debug("Expanding session: "+session.getLabel());
		SearchSpecification spec = new SearchSpecification();
		SearchCriterion sc = toolkit.createSearchCriterion(XnatTag.SessionID,
			SearchCriterion.Equal, session.getId());
		spec.addCriterion(sc);
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		ScanListFetchWorker slfw = new ScanListFetchWorker(spec, node);
		netIoExec.submit(slfw);
	}

	private void willExpandSubject(SubjectNode node, XnatSubject subject)
	{
		logger.debug("Expanding subject: "+subject.getLabel());
		SearchSpecification spec = new SearchSpecification();
		SearchCriterion sc = toolkit.createSearchCriterion(XnatTag.SubjectID,
			SearchCriterion.Equal, subject.getId());
		spec.addCriterion(sc);
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		SessionListFetchWorker slfw = new SessionListFetchWorker(spec, node);
		netIoExec.submit(slfw);
	}

	private class AssessorIntegrityWorker
		extends SwingWorker<Void,Void>
	{
		private final SessionNode node;

		public AssessorIntegrityWorker(SessionNode node)
		{
			super();
			this.node = node;
		}

		@Override
		protected Void doInBackground() throws Exception
		{
			assessorIntegrityCheck(node);
			return null;
		}

		@Override
		protected void done()
		{
			CallbackTaskResult<Void> result;
			try
			{
				Void r = get();
				result = new CallbackTaskResult<>(r);
			}
			catch (InterruptedException | ExecutionException ex)
			{
				result = new CallbackTaskResult<>(ex);
			}
			assessorIntegrityCheckDone(result, node);
		}
	}

	private class AssessorListFetchWorker
		extends SwingWorker<List<XnatAssessor>,Void>
	{
		private final SessionNode node;
		private final SearchSpecification spec;

		public AssessorListFetchWorker(SearchSpecification spec, SessionNode node)
		{
			super();
			this.spec = spec;
			this.node = node;
		}

		@Override
		protected List<XnatAssessor> doInBackground() throws Exception
		{
			return xds.searchAssessor(spec);
		}

		@Override
		protected void done()
		{
			CallbackTaskResult<List<XnatAssessor>> result;
			try
			{
				List<XnatAssessor> r = get();
				result = new CallbackTaskResult<>(r);
			}
			catch (InterruptedException | ExecutionException ex)
			{
				result = new CallbackTaskResult<>(ex);
			}
			assessorListFetchDone(result, node);
		}
	}

	private class DeleteAction extends AbstractAction
	{
		public DeleteAction()
		{
			super("Delete", null);
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			performDelete(e);
		}
	}

	private class DeleteWorker
		extends SwingWorker<List<XnatTreeNode>,Void>
	{
		private final List<XnatTreeNode> nodes = new ArrayList<>();

		public DeleteWorker(List<XnatTreeNode> nodes)
		{
			super();
			this.nodes.addAll(nodes);
		}

		@Override
		protected List<XnatTreeNode> doInBackground() throws Exception
		{
			return deleteNodes(nodes);
		}

		@Override
		protected void done()
		{
			CallbackTaskResult<List<XnatTreeNode>> result;
			try
			{
				List<XnatTreeNode> r = get();
				result = new CallbackTaskResult<>(r);
			}
			catch (InterruptedException | ExecutionException ex)
			{
				result = new CallbackTaskResult<>(ex);
			}
			deleteNodesDone(result);
		}
	}

	private class ScanListFetchWorker extends SwingWorker<List<XnatScan>,Void>
	{
		private final SessionNode node;
		private final SearchSpecification spec;

		public ScanListFetchWorker(SearchSpecification spec, SessionNode node)
		{
			super();
			this.spec = spec;
			this.node = node;
		}

		@Override
		protected List<XnatScan> doInBackground() throws Exception
		{
			return xds.searchScan(spec);
		}

		@Override
		protected void done()
		{
			CallbackTaskResult<List<XnatScan>> result;
			try
			{
				List<XnatScan> r = get();
				result = new CallbackTaskResult<>(r);
			}
			catch (InterruptedException | ExecutionException ex)
			{
				result = new CallbackTaskResult<>(ex);
			}
			scanListFetchDone(result, node);
		}
	}

	private class SessionListFetchWorker
		extends SwingWorker<List<XnatSession>,Void>
	{
		private final SubjectNode node;
		private final SearchSpecification spec;

		public SessionListFetchWorker(SearchSpecification spec, SubjectNode node)
		{
			super();
			this.spec = spec;
			this.node = node;
		}

		@Override
		protected List<XnatSession> doInBackground() throws Exception
		{
			return xds.searchSession(spec);
		}

		@Override
		protected void done()
		{
			CallbackTaskResult<List<XnatSession>> result;
			try
			{
				List<XnatSession> r = get();
				result = new CallbackTaskResult<>(r);
			}
			catch (InterruptedException | ExecutionException ex)
			{
				result = new CallbackTaskResult<>(ex);
			}
			sessionListFetchDone(result, node);
		}
	}

	private class TreeMouseListener implements MouseListener
	{
		@Override
		public void mouseClicked(MouseEvent e)
		{
			treeMouseClicked(e);
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{}

		@Override
		public void mouseExited(MouseEvent e)
		{}

		@Override
		public void mousePressed(MouseEvent e)
		{}

		@Override
		public void mouseReleased(MouseEvent e)
		{}
		
	}
}
