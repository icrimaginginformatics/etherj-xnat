/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.Uids;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public final class RtStructUploadMetadata extends AbstractUploadMetadata<RtStruct>
	implements UploadMetadata<RtStruct>
{

	/**
	 *
	 * @param rtStruct
	 * @param file
	 */
	public RtStructUploadMetadata(RtStruct rtStruct, File file)
	{
		super(rtStruct, "RTSTRUCT", file, "RoiCollection_"+Uids.createShortUnique(),
			new XnatUploadableFile(file, "out", "RTSTRUCT", "DICOM", "EXTERNAL", "RT Structure Set"));
		setLabel("RTSTRUCT_"+getDate(rtStruct)+"_"+getTime(rtStruct));
		buildUidSets(rtStruct);
		buildAux(rtStruct);
	}

	@Override
	public String getName()
	{
		return getUploadItem().getStructureSetModule().getStructureSetLabel();
	}

	@Override
	public String getRootServerPath()
	{
		StudyUidContainer container = getPreferredContainer();
		return "/data/archive/projects/"+container.getProjectId()+
			"/subjects/"+container.getSubjectId()+
			"/experiments/"+container.getSessionId();
	}

	@Override
	public String getTypePath()
	{
		return "/assessors/";
	}
	@Override
	public String getUid()
	{
		return getUploadItem().getSopInstanceUid();
	}

	private void buildAux(RtStruct rtStruct)
	{
		Map<Class<?>,List<AuxMetadata>> auxMap = getAuxMap();
		List<AuxMetadata> list = new ArrayList<>();
		auxMap.put(XnatRoi.class, list);
		
		String sopInstUid = rtStruct.getSopInstanceUid();
		StructureSetModule ssModule = rtStruct.getStructureSetModule();
		RoiContourModule rcModule = rtStruct.getRoiContourModule();
		for (StructureSetRoi ssRoi : ssModule.getStructureSetRoiList())
		{
			RoiContour rc = rcModule.getRoiContour(ssRoi.getRoiNumber());
			XnatRoi roi = new XnatRoi("Roi_"+Uids.createShortUnique());
			roi.setCollectionId(getAccessionId());
			roi.setName(ssRoi.getRoiName());
			roi.setUid(sopInstUid+"."+ssRoi.getRoiNumber());
			roi.setGeometricType(getGeometricType(rc));
			XnatRoiAuxMetadata auxMeta = new XnatRoiAuxMetadata(roi);
			list.add(auxMeta);
		}
	}

	private void buildUidSets(RtStruct rtStruct)
	{
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		for (ReferencedFrameOfReference refFoR :
			ssm.getReferencedFrameOfReferenceList())
		{
			for (RtReferencedStudy refStudy : refFoR.getRtReferencedStudyList())
			{
				addStudyUid(refStudy.getReferencedSopInstanceUid());
				for (RtReferencedSeries refSeries :
					refStudy.getRtReferencedSeriesList())
				{
					addSeriesUid(refSeries.getSeriesInstanceUid());
					for (ContourImage ci : refSeries.getContourImageList())
					{
						addSopInstanceUid(ci.getReferencedSopInstanceUid());
					}
				}
			}
		}
		// Theoretically not needed as all SOP instance UIDs should be in the
		// StructureSetModule
		RoiContourModule rcm = rtStruct.getRoiContourModule();
		for (RoiContour roi : rcm.getRoiContourList())
		{
			for (Contour contour : roi.getContourList())
			{
				for (ContourImage ci : contour.getContourImageList())
				{
					addSopInstanceUid(ci.getReferencedSopInstanceUid());
				}
			}
		}
	}

	private String getDate(RtStruct rtStruct)
	{
		// Find the date from the most specific but optional to the least specific
		// but mandatory
		String date = rtStruct.getStructureSetModule().getStructureSetDate();
		if (date.isEmpty())
		{
			date = rtStruct.getRtSeriesModule().getSeriesDate();
			if ((date == null) || date.isEmpty())
			{
				date = rtStruct.getStudyDate();
			}
		}
		Date dt = DicomUtils.parseDate(date);
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(dt);
	}

	private String getGeometricType(RoiContour rc) throws IllegalArgumentException
	{
		List<Contour> contourList = rc.getContourList();
		if (contourList.isEmpty())
		{
			throw new IllegalArgumentException("Contour list must not be empty");
		}
		Contour contour = contourList.get(0);
		switch (contour.getContourGeometricType())
		{
			case Contour.ClosedPlanar:
			case Contour.OpenPlanar:
				return XnatRoi.TwoDContourStack;
			case Contour.Point:
				return XnatRoi.Point;
			case Contour.OpenNonPlanar:
				throw new IllegalArgumentException("Unsupported contour type: "+
					Contour.OpenNonPlanar);
			default:
				throw new IllegalArgumentException("Unknown contour type: "+
					contour.getContourGeometricType());
		}
	}
	
	private String getTime(RtStruct rtStruct)
	{
		// Find the time from the most specific but optional to the least specific
		// but mandatory
		String date = rtStruct.getStructureSetModule().getStructureSetTime();
		if (date.isEmpty())
		{
			date = rtStruct.getRtSeriesModule().getSeriesTime();
			if ((date == null) || date.isEmpty())
			{
				date = rtStruct.getStudyTime();
			}
		}
		Date dt = DicomUtils.parseTime(date);
		DateFormat format = new SimpleDateFormat("HHmmss");
		return format.format(dt);
	}

}
