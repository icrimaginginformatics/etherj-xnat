/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class XnatImageAnnotation extends XnatAssessor
	implements XnatAssessorContainer
{
	private final Map<String,XnatAnnotationItem> annoItems = new HashMap<>();
	private final Map<String,XnatAnnotationItem> annoRoleItems = new HashMap<>();
	private final Map<String,XnatAnnotationItem> calcItems = new HashMap<>();
	private final String iacId;
	private final Map<String,XnatAnnotationItem> imageObsItems = new HashMap<>();
	private final Map<String,XnatAnnotationItem> imagePhysItems = new HashMap<>();
	private final Map<String,XnatAnnotationItem> inferenceItems = new HashMap<>();
	private final Map<String,XnatAnnotationItem> markupItems = new HashMap<>();
	private String regionSetId = "<<UNKNOWN>>";
	private final Map<String,XnatAnnotationItem> segmentItems = new HashMap<>();
	private final Map<String,XnatAnnotationItem> taskContextItems = new HashMap<>();
	private int schemaAnnotationRoleCount = 0;
	private int schemaCalculationCount = 0;
	private int schemaImagingObservationCount = 0;
	private int schemaImagingPhysicalCount = 0;
	private int schemaInferenceCount = 0;
	private int schemaMarkupCount = 0;
	private int schemaSegmentationCount = 0;
	private int schemaTaskContextCount = 0;
	private final String uid;

	public XnatImageAnnotation(XnatId xnatId, String label, String uid,
		String iacId)
	{
		super(xnatId, label);
		this.uid = uid;
		this.iacId = iacId;
	}

	public XnatAnnotationItem addAnnotationItem(XnatAnnotationItem xai)
	{
		String id = xai.getId();
		switch (xai.getType())
		{
			case XnatEntity.AnnotationRole:
				annoRoleItems.put(id, xai);
				break;
			case XnatEntity.Calculation:
				calcItems.put(id, xai);
				break;
			case XnatEntity.ImageObservation:
				imageObsItems.put(id, xai);
				break;
			case XnatEntity.ImagePhysical:
				imagePhysItems.put(id, xai);
				break;
			case XnatEntity.Inference:
				inferenceItems.put(id, xai);
				break;
			case XnatEntity.Markup:
				markupItems.put(id, xai);
				break;
			case XnatEntity.Segmentation:
				segmentItems.put(id, xai);
				break;
			case XnatEntity.TaskContext:
				taskContextItems.put(id, xai);
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown item type: "+xai.getType());
		}
		XnatAnnotationItem value = annoItems.put(id, xai);
		return value;
	}

	/**
	 * @return the annotationRoleCount
	 */
	public int getAnnotationRoleCount()
	{
		return annoRoleItems.size();
	}

	public XnatAnnotationItem getAnnotationItem(String id)
	{
		return annoItems.get(id);
	}

	public List<XnatAnnotationItem> getAnnotationItems()
	{
		List<XnatAnnotationItem> list = new ArrayList<>();
		list.addAll(annoItems.values());
		return list;
	}

	/**
	 * @return the calculationCount
	 */
	public int getCalculationCount()
	{
		return calcItems.size();
	}

	/**
	 * @return the iacId
	 */
	public String getIacId()
	{
		return iacId;
	}

	/**
	 * @return the imagingObservationCount
	 */
	public int getImagingObservationCount()
	{
		return imageObsItems.size();
	}

	/**
	 * @return the imagingPhysicalCount
	 */
	public int getImagingPhysicalCount()
	{
		return imagePhysItems.size();
	}

	/**
	 * @return the inferenceCount
	 */
	public int getInferenceCount()
	{
		return inferenceItems.size();
	}

	/**
	 * @return the markupCount
	 */
	public int getMarkupCount()
	{
		return markupItems.size();
	}

	/**
	 * @return the regionSetId
	 */
	public String getRegionSetId()
	{
		return regionSetId;
	}

	/**
	 * @return the schemaAnnotationRoleCount
	 */
	public int getSchemaAnnotationRoleCount()
	{
		return schemaAnnotationRoleCount;
	}

	/**
	 * @return the schemaCalculationCount
	 */
	public int getSchemaCalculationCount()
	{
		return schemaCalculationCount;
	}

	/**
	 * @return the schemaImagingObservationCount
	 */
	public int getSchemaImagingObservationCount()
	{
		return schemaImagingObservationCount;
	}

	/**
	 * @return the schemaImagingPhysicalCount
	 */
	public int getSchemaImagingPhysicalCount()
	{
		return schemaImagingPhysicalCount;
	}

	/**
	 * @return the schemaInferenceCount
	 */
	public int getSchemaInferenceCount()
	{
		return schemaInferenceCount;
	}

	/**
	 * @return the schemaMarkupCount
	 */
	public int getSchemaMarkupCount()
	{
		return schemaMarkupCount;
	}

	/**
	 * @return the schemaSegmentationCount
	 */
	public int getSchemaSegmentationCount()
	{
		return schemaSegmentationCount;
	}

	/**
	 * @return the schemaTaskContextCount
	 */
	public int getSchemaTaskContextCount()
	{
		return schemaTaskContextCount;
	}

	/**
	 * @return the segmentationCount
	 */
	public int getSegmentationCount()
	{
		return segmentItems.size();
	}

	/**
	 * @return the taskContextCount
	 */
	public int getTaskContextCount()
	{
		return taskContextItems.size();
	}

	@Override
	public String getType()
	{
		return XnatEntity.ImageAnnotation;
	}

	/**
	 * @return the uid
	 */
	public String getUid()
	{
		return uid;
	}

	public XnatAnnotationItem removeAnnotationItem(String id)
	{
		XnatAnnotationItem xai = annoItems.remove(id);
		switch (xai.getType())
		{
			case XnatEntity.AnnotationRole:
				annoRoleItems.put(id, xai);
				break;
			case XnatEntity.Calculation:
				calcItems.put(id, xai);
				break;
			case XnatEntity.ImageObservation:
				imageObsItems.put(id, xai);
				break;
			case XnatEntity.ImagePhysical:
				imagePhysItems.put(id, xai);
				break;
			case XnatEntity.Inference:
				inferenceItems.put(id, xai);
				break;
			case XnatEntity.Markup:
				markupItems.put(id, xai);
				break;
			case XnatEntity.Segmentation:
				segmentItems.put(id, xai);
				break;
			case XnatEntity.TaskContext:
				taskContextItems.put(id, xai);
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown item type: "+xai.getType());
		}
		
		return xai;
	}

	public XnatAnnotationItem removeAnnotationItem(XnatAnnotationItem xai)
	{
		return removeAnnotationItem(xai.getId());
	}

	/**
	 * @param schemaAnnotationRoleCount the schemaAnnotationRoleCount to set
	 */
	public void setSchemaAnnotationRoleCount(int schemaAnnotationRoleCount)
	{
		this.schemaAnnotationRoleCount = schemaAnnotationRoleCount;
	}

	/**
	 * @param schemaCalculationCount the schemaCalculationCount to set
	 */
	public void setSchemaCalculationCount(int schemaCalculationCount)
	{
		this.schemaCalculationCount = schemaCalculationCount;
	}

	/**
	 * @param schemaImagingObservationCount the schemaImagingObservationCount to set
	 */
	public void setSchemaImagingObservationCount(int schemaImagingObservationCount)
	{
		this.schemaImagingObservationCount = schemaImagingObservationCount;
	}

	/**
	 * @param schemaImagingPhysicalCount the schemaImagingPhysicalCount to set
	 */
	public void setSchemaImagingPhysicalCount(int schemaImagingPhysicalCount)
	{
		this.schemaImagingPhysicalCount = schemaImagingPhysicalCount;
	}

	/**
	 * @param schemaInferenceCount the schemaInferenceCount to set
	 */
	public void setSchemaInferenceCount(int schemaInferenceCount)
	{
		this.schemaInferenceCount = schemaInferenceCount;
	}

	/**
	 * @param schemaMarkupCount the schemaMarkupCount to set
	 */
	public void setSchemaMarkupCount(int schemaMarkupCount)
	{
		this.schemaMarkupCount = schemaMarkupCount;
	}

	/**
	 * @param schemaSegmentationCount the schemaSegmentationCount to set
	 */
	public void setSchemaSegmentationCount(int schemaSegmentationCount)
	{
		this.schemaSegmentationCount = schemaSegmentationCount;
	}

	/**
	 * @param schemaTaskContextCount the schemaTaskContextCount to set
	 */
	public void setSchemaTaskContextCount(int schemaTaskContextCount)
	{
		this.schemaTaskContextCount = schemaTaskContextCount;
	}

	/**
	 * @param regionSetId the regionSetId to set
	 */
	public void setRegionSetId(String regionSetId)
	{
		this.regionSetId = regionSetId;
	}
	
}
