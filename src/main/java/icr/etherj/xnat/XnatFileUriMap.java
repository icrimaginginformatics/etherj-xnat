/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import java.util.Set;

/**
 *
 * @author jamesd
 */
public interface XnatFileUriMap
{

	/**
	 *
	 * @param filename
	 * @param path
	 * @param size
	 * @return
	 */
	public Entry add(String filename, String path, int size);

	/**
	 *
	 * @param fileUriMap
	 */
	public void addAll(XnatFileUriMap fileUriMap);

	/**
	 *
	 * @param key
	 * @return
	 */
	public boolean contains(String key);

	/**
	 *
	 * @param key
	 * @return
	 */
	public Entry get(String key);

	/**
	 *
	 * @return
	 */
	public XnatId getXnatId();

	/**
	 *
	 * @return
	 */
	public Set<String> keySet();

	/**
	 *
	 */
	public class Entry
	{
		private final String filename;
		private final String path;
		private final long size;

		public Entry(String filename, String path, int size)
		{
			if ((filename == null) || (path == null) || (size < 1))
			{
				throw new IllegalArgumentException();
			}
			this.filename = filename;
			this.path = path;
			this.size = size;
		}

		/**
		 * @return the filename
		 */
		public String getFilename()
		{
			return filename;
		}

		/**
		 * @return the path
		 */
		public String getPath()
		{
			return path;
		}

		/**
		 * @return the size
		 */
		public long getSize()
		{
			return size;
		}
	}

}
