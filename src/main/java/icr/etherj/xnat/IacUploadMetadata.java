/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import com.google.common.collect.ImmutableList;
import icr.etherj.Uids;
import icr.etherj.aim.AimUtils;
import icr.etherj.aim.DicomImageReference;
import icr.etherj.aim.Image;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.ImageReference;
import icr.etherj.aim.ImageSeries;
import icr.etherj.aim.ImageStudy;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class IacUploadMetadata
	extends AbstractUploadMetadata<ImageAnnotationCollection>
	implements UploadMetadata<ImageAnnotationCollection>
{
	private final List<Investigator> investigators = new ArrayList<>();

	/**
	 *
	 * @param iac
	 * @param file
	 */
	public IacUploadMetadata(ImageAnnotationCollection iac, File file)
	{
		super(iac, "AIM", file, "AimImageAnnotationCollection_"+Uids.createShortUnique(),
			new XnatUploadableFile(file, "out", "AIM-INSTANCE", "XML", "EXTERNAL", "AIM instance file"));
		setLabel("AIM_"+getDate()+"_"+getTime());
		buildUidSets();
	}

	@Override
	public Set<Class<?>> getAuxClasses()
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/**
	 *
	 * @return
	 */
	public List<Investigator> getInvestigators()
	{
		return ImmutableList.copyOf(investigators);
	}

	@Override
	public String getName()
	{
		List<ImageAnnotation> list = getUploadItem().getAnnotationList();
		return list.isEmpty() ? null : list.get(0).getName();
	}

	@Override
	public String getRootServerPath()
	{
		StudyUidContainer container = getPreferredContainer();
		return "/data/archive/projects/"+container.getProjectId()+
			"/subjects/"+container.getSubjectId()+
			"/experiments/"+container.getSessionId();
	}

	@Override
	public String getTypePath()
	{
		return "/assessors/";
	}

	@Override
	public String getUid()
	{
		return getUploadItem().getUid();
	}

	public void setInvestigators(List<Investigator> investigators)
	{
		lock.lock();
		try
		{
			this.investigators.clear();
			this.investigators.addAll(investigators);
		}
		finally
		{
			lock.unlock();
		}
	}

	private void buildUidSets()
	{
		for (ImageAnnotation ia : getUploadItem().getAnnotationList())
		{
			for (ImageReference ir : ia.getReferenceList())
			{
				if (!(ir instanceof DicomImageReference))
				{
					continue;
				}
				DicomImageReference dir = (DicomImageReference) ir;
				ImageStudy study = dir.getStudy();
				addStudyUid(study.getInstanceUid());
				ImageSeries series = study.getSeries();
				addSeriesUid(series.getInstanceUid());
				for (Image im : series.getImageList())
				{
					addSopInstanceUid(im.getSopInstanceUid());
				}
			}
		}
	}

	private String getDate()
	{
		Date dt = AimUtils.parseDateTime(getUploadItem().getDateTime());
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(dt);
	}

	private String getTime()
	{
		Date dt = AimUtils.parseDateTime(getUploadItem().getDateTime());
		DateFormat format = new SimpleDateFormat("HHmmss");
		return format.format(dt);
	}

}
