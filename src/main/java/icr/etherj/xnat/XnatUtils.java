/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import icr.etherj.XmlException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSException;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

/**
 *
 * @author jamesd
 */
public class XnatUtils
{

	/**
	 *
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String contents(InputStream is) throws IOException
	{
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try
		{
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}

		}
		finally
		{
			if (br != null)
			{
				br.close();
			}
		}

		return sb.toString();
	}

	/**
	 *
	 * @param doc
	 * @return
	 * @throws XmlException
	 */
	public static String dumpDOMDocument(Document doc) throws XmlException
	{
		return dumpDOMDocument(doc, "UTF-8");
	}

	/**
	 *
	 * @param doc
	 * @param encoding
	 * @return
	 * @throws XmlException
	 */
	public static String dumpDOMDocument(Document doc, String encoding)
	 throws XmlException
	{
		String docString = null;

		try
		{
			DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();

			DOMImplementationLS impl = (DOMImplementationLS)registry.getDOMImplementation("LS");

			LSSerializer   lss = impl.createLSSerializer();
			LSOutput       lso = impl.createLSOutput();
			StringWriter   sw  = new StringWriter();
			lso.setEncoding(encoding);
			lso.setCharacterStream(sw);
			lss.write(doc, lso);
			docString = sw.toString();
		}
		catch (ClassNotFoundException | InstantiationException | 
			IllegalAccessException | ClassCastException | LSException ex)
		{
			throw new XmlException("String write failed", ex);
		}

		return docString;
	}

	/**
	 *
	 * @param <T>
	 * @param typeRef
	 * @param is
	 * @return
	 * @throws XnatException
	 */
	public static <T> T readJson(TypeReference<T> typeRef, InputStream is)
		throws XnatException
	{
		T target;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			target = mapper.readValue(is, typeRef);
		}
		catch (IOException ex)
		{
			throw new XnatException("JSON mapping error", XnatCode.IO, ex);
		}
		return target;
	}

	/**
	 *
	 * @param is
	 * @return
	 * @throws XnatException
	 */
	public static Document streamToDoc(InputStream is) throws XnatException
	{
		Document doc = null;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.parse(is);
		}
		catch (ParserConfigurationException ex)
		{
			throw new XnatException(XnatCode.ParserConfiguration, ex);
		}
		catch (SAXException ex)
		{
			throw new XnatException(XnatCode.SAX, ex);
		}
		catch (IllegalArgumentException ex)
		{
			throw new XnatException(XnatCode.IllegalArgument, ex);
		}
		catch (IOException ex)
		{
			throw new XnatException(XnatCode.IO, ex);
		}
		return doc;
	}

	/**
	 *
	 * @param tag
	 * @return
	 */
	public static String tagName(int tag)
	{
		for (Field field : XnatTag.class.getDeclaredFields())
		{
			try
			{
				if (field.getInt(null) == tag)
				{
					return field.getName();
				}
			}
			catch (IllegalArgumentException | IllegalAccessException ignore)
			{}
		}
		return null;
	}

	/**
	 *
	 * @param time
	 * @return
	 * @throws NumberFormatException
	 */
	public static double timeToFloat(String time) throws NumberFormatException
	{
		if ((time.length() != 8) ||
			 (time.charAt(2) != ':') ||
			 (time.charAt(5) != ':'))
		{
			throw new NumberFormatException();
		}
		return 3600.0*Integer.parseInt(time.substring(0, 2))+
			60.0*Integer.parseInt(time.substring(3, 5))+
			Integer.parseInt(time.substring(6, 8));
	}

}
