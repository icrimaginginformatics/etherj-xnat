/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.IoUtils;
import icr.etherj.XmlException;
import icr.etherj.aim.AimToolkit;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.XmlParser;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class IacXnatSearchPlugin
	implements XnatSearchPlugin<ImageAnnotationCollection>
{
	private static final Logger logger =
		LoggerFactory.getLogger(IacXnatSearchPlugin.class);

	private static final String AimImageAnnotationCollectionIdHeader =
		"AimImageAnnotationCollectionId";
	private static final String AimImageAnnotationCollectionUidHeader =
		"AimImageAnnotationCollectionUid";
	private static final String AssessorSubjectId = "subjectID";
	private static final String CollectionType = "collectionType";

	private final Helper helper = new Helper();

	@Override
	public boolean exists(XnatServerConnection xsc, SearchSpecification spec)
		throws XnatException
	{
		if (spec.getCriteriaCount() == 0)
		{
			return false;
		}
		String query = createSearchQuery(spec);
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml", query);

		return rs.getRowCount() > 0;
	}

	@Override
	public List<ImageAnnotationCollection> search(XnatServerConnection xsc,
		SearchSpecification spec) throws XnatException
	{
		List<ImageAnnotationCollection> iacList = new ArrayList<>();
		if (spec.getCriteriaCount() == 0)
		{
			return iacList;
		}

		String query = createSearchQuery(spec);
		runQuery(xsc, iacList, query);

		return iacList;
	}
	
	private String createSearchQuery(SearchSpecification spec)
		throws XnatException
	{
		StringBuilder sb = helper.createInitialXml("ImageAnnotationCollection");

		String type = "icr:roiCollectionData";
		helper.addRootElement(sb, type);
		helper.addSearchField(sb, type, Project, 0, StringType, ProjectIdHeader);
		helper.addSearchField(sb, type, AssessorSubjectId, 1, StringType,
			SubjectIdHeader);
		helper.addSearchField(sb, type, AssessorSessionId, 2, StringType,
			SessionIdHeader);
		helper.addSearchField(sb, type, ID, 3, StringType,
			AimImageAnnotationCollectionIdHeader);
		helper.addSearchField(sb, type, Uid, 4, StringType,
			AimImageAnnotationCollectionUidHeader);
		helper.addSearchField(sb, type, Label, 5, StringType, Label);

		sb.append("	<xdat:search_where method=\"AND\">\n");
		processSearchCriteria(spec, type, sb);
		helper.addCriterion(sb, type+"/"+CollectionType, Equals, "AIM");
		sb.append("	</xdat:search_where>\n");
		sb.append("</xdat:search>\n");

		return sb.toString();
	}

	private ImageAnnotationCollection get(XnatServerConnection xsc,
		XnatId xnatId, String iacId, String iacUid)
	{
		ImageAnnotationCollection iac = null;
		InputStream is = null;
		try
		{
			// Wasteful extra query just to get file name
			String baseQuery = new StringBuilder("/data/archive/projects/")
				.append(xnatId.getProjectId())
				.append("/subjects/").append(xnatId.getSubjectId())
				.append("/experiments/").append(xnatId.getSessionId())
				.append("/assessors/").append(iacId)
				.append("/resources/AIM").toString();
			XnatResultSet nameRs = xsc.getResultSet(baseQuery+"/files?format=xml");
			if ((nameRs == null) || (nameRs.getRowCount() != 1))
			{
				logger.warn("No unique result for IAC ID: "+iacId);
				return null;
			}
			logger.debug("IAC XNAT ID: <"+xnatId.getProjectId()+","+
				xnatId.getSubjectId()+","+xnatId.getSessionId()+
				"> IAC ID: "+iacId);
			String name = nameRs.get(0, Name);
			// Retrieve the file
			String query = new StringBuilder(baseQuery).append("/files/")
				.append(name).append("?format=xml").toString();
			is = xsc.get(query);
			XmlParser parser = AimToolkit.getToolkit().createXmlParser();
			iac = parser.parse(is);
			if (!iac.getUid().equals(iacUid))
			{
				throw new XnatException("Returned IAC doesn't match UID: "+iacUid,
					XnatCode.UidMismatch);
			}
		}
		catch (IOException | XmlException | XnatException ex)
		{
			logger.warn("Error fetching IAC for XnatId: <"+
				xnatId.getProjectId()+","+xnatId.getSubjectId()+","+
				xnatId.getSessionId()+"> and IAC ID: "+iacId, ex);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return iac;
	}

	private StringBuilder processSearchCriteria(SearchSpecification spec,
		String searchData, StringBuilder sb) throws XnatException
	{
		List<SearchCriterion> critList = spec.getCriteria();
		for (SearchCriterion crit : critList)
		{
			switch (crit.getTag())
			{
				case XnatTag.ProjectID:
					helper.addCriterion(sb, searchData+"/"+Project, crit);
					break;

				case XnatTag.SubjectID:
					helper.addCriterion(sb, searchData+"/"+AssessorSubjectId, crit);
					break;

//				case XnatTag.SubjectName:
//					XnatSearchPlugin.addCriterion(sb, searchData+"/"+DicomSubjectName, Like,
//						"%"+crit.getValue()+"%");
//					break;

				case XnatTag.SessionID:
					helper.addCriterion(sb, searchData+"/"+AssessorSessionId, crit);
					break;

				case XnatTag.Label:
					helper.addCriterion(sb, searchData+"/"+Label, crit);
					break;

				default:
					throw new XnatException("Unsupported tag: "+crit.getTagName(),
						XnatCode.UnknownTag);
			}
		}
		return sb;
	}

	private void runQuery(XnatServerConnection xsc,
		List<ImageAnnotationCollection> list, String query)
		throws XnatException
	{
		XnatResultSet rs = xsc.getResultSet("/data/search?format=xml",
			query);
		int iacIdIdx = rs.getColumnIndexByHeader(
			AimImageAnnotationCollectionIdHeader);
		int iacUidIdx = rs.getColumnIndexByHeader(
			AimImageAnnotationCollectionUidHeader);
		int nRows = rs.getRowCount();
		logger.debug("IAC count: {}", nRows);
		for (int i=0; i<nRows; i++)
		{
			XnatId xnatId = helper.xnatIdFromResultSet(rs, i);
			ImageAnnotationCollection iac = get(xsc, xnatId, rs.get(i, iacIdIdx),
				rs.get(i, iacUidIdx));
			if (iac != null)
			{
				list.add(iac);
			}
		}
	}

}
