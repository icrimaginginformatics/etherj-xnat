/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package icr.etherj.xnat.schema;

/**
 * AbstractResource represents the XNAT schema abstractResource element. Despite
 * the name, it is not an abstract type implementing part of a Java interface.
 * 
 * @author jamesd
 */
public class AbstractResource implements XnatSchemaElement
{
	private String label = "";
	private int fileCount = -1;
	private long fileSize = -1;
	private String note = "";
	// Note that, in a highly confusing lack of consistency, tag in xnat.xsd
	// is the extension of xs:string called metaField in catalog.xsd, while
	// tag in catalog.xsd is a simple xs:string.
//	public List<MetaField> tagList;

	public AbstractResource()
	{}

	public AbstractResource(String label, int fileCount, long fileSize,
		String note)
	{
		this.label = label;
		this.fileCount = fileCount;
		this.fileSize = fileSize;
		this.note = note;
	}

	/**
	 * @return the fileCount
	 */
	public int getFileCount()
	{
		return fileCount;
	}

	/**
	 * @return the fileSize
	 */
	public long getFileSize()
	{
		return fileSize;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @return the note
	 */
	public String getNote()
	{
		return note;
	}

	/**
	 * @param fileCount the fileCount to set
	 */
	public void setFileCount(int fileCount)
	{
		this.fileCount = fileCount;
	}

	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(long fileSize)
	{
		this.fileSize = fileSize;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label)
	{
		this.label = label;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note)
	{
		this.note = note;
	}
}
