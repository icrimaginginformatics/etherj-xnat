/********************************************************************
* Copyright (c) 2017, Institute of Cancer Research
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
* 
* (2) Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
* 
* (3) Neither the name of the Institute of Cancer Research nor the
*     names of its contributors may be used to endorse or promote
*     products derived from this software without specific prior
*     written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

package icr.etherj.xnat.schema;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class Provenance implements XnatSchemaElement
{
	public static final String SEPARATOR = " | ";
	public static final String UNKNOWN = "Unknown";

	private static final String PROV_STRING = "_!PS!_";
	private static final String LIB_STRING  = "_!LS!_";

	private final List<ProcessStep> stepList = new ArrayList<>();

	/**
	 *
	 */
	public Provenance()
	{}

	/**
	 *
	 * @param stepList
	 */
	public Provenance(List<ProcessStep> stepList)
	{
		this.stepList.addAll(stepList);
	}

	/**
	 *
	 * @param processStep
	 * @return
	 */
	public boolean addProcessStep(ProcessStep processStep)
	{
		return stepList.add(processStep);
	}

	/**
	 *
	 * @return
	 */
	public int getProcessStepCount()
	{
		return stepList.size();
	}

	/**
	 *
	 * @param index
	 * @return
	 */
	public ProcessStep getProcessStep(int index)
	{
		return ((index >= 0) && (index < stepList.size()))
			? stepList.get(index)
			: null;
	}

	/**
	 * @return the stepList
	 */
	public List<ProcessStep> getProcessSteps()
	{
		return ImmutableList.copyOf(stepList);
	}

	/**
	 *
	 * @param processStep
	 * @return
	 */
	public boolean removeProcessStep(ProcessStep processStep)
	{
		return stepList.remove(processStep);
	}

	/**
	 *
	 */
	public static class ProcessStep
	{
		private final Program program;
		private final String timestamp;
		private final String cvs;
		private final String user;
		private final String machine;
		private final Platform platform;
		private final Compiler compiler;
		private final List<Library> libraryList = new ArrayList<>();

		/**
		 *
		 * @param program
		 * @param timestamp
		 * @param cvs
		 * @param user
		 * @param machine
		 * @param platform
		 * @param compiler
		 * @param libraryList
		 */
		public ProcessStep(Program program, String timestamp, String cvs,
			String user, String machine, Platform platform, Compiler compiler,
			List<Library> libraryList)
		{
			this.program = (program != null)? program : new Program();
			this.timestamp = (timestamp != null)? timestamp : UNKNOWN;
			this.cvs = (cvs != null)? cvs : UNKNOWN;
			this.user = (user != null)? user : UNKNOWN;
			this.machine = (machine != null)? machine : UNKNOWN;
			this.platform = (platform != null)? platform : new Platform();
			this.compiler = (compiler != null)? compiler : new Compiler();
			this.libraryList.addAll(libraryList); 
		}

		/**
		 * @return the compiler
		 */
		public Compiler getCompiler()
		{
			return compiler;
		}

		/**
		 * @return the cvs
		 */
		public String getCvs()
		{
			return cvs;
		}

		/**
		 * @return the libraryList
		 */
		public List<Library> getLibraryList()
		{
			return libraryList;
		}

		/**
		 * @return the machine
		 */
		public String getMachine()
		{
			return machine;
		}

		/**
		 * @return the platform
		 */
		public Platform getPlatform()
		{
			return platform;
		}

		/**
		 * @return the program
		 */
		public Program getProgram()
		{
			return program;
		}

		/**
		 * @return the timestamp
		 */
		public String getTimestamp()
		{
			return timestamp;
		}

		/**
		 * @return the user
		 */
		public String getUser()
		{
			return user;
		}

		/**
		 *
		 */
		public static class Program
		{
			private final String name;
			private final String version;
			private final String arguments;

			/**
			 *
			 */
			public Program()
			{
				this(UNKNOWN, UNKNOWN, UNKNOWN);
			}

			/**
			 *
			 * @param name
			 * @param version
			 * @param arguments
			 */
			public Program(String name, String version, String arguments)
			{
				this.name = ((name != null) && !name.isEmpty())
					? name : UNKNOWN;
				this.version = ((version != null) && !version.isEmpty())
					? version : UNKNOWN;
				this.arguments = ((arguments != null) && !arguments.isEmpty())
					? arguments : UNKNOWN;
			}

			/**
			 * @return the arguments
			 */
			public String getArguments()
			{
				return arguments;
			}

			/**
			 * @return the name
			 */
			public String getName()
			{
				return name;
			}

			/**
			 * @return the version
			 */
			public String getVersion()
			{
				return version;
			}
		}

		/**
		 *
		 */
		public static class Platform
		{
			private final String name;
			private final String version;

			/**
			 *
			 */
			public Platform()
			{
				this(UNKNOWN, UNKNOWN);
			}

			/**
			 *
			 * @param name
			 * @param version
			 */
			public Platform(String name, String version)
			{
				this.name = ((name != null) && !name.isEmpty())
					? name : UNKNOWN;
				this.version = ((version != null) && !version.isEmpty())
					? version : UNKNOWN;
			}

			/**
			 * @return the name
			 */
			public String getName()
			{
				return name;
			}

			/**
			 * @return the version
			 */
			public String getVersion()
			{
				return version;
			}
		}

		public static class Compiler
		{
			private final String name;
			private final String version;

			/**
			 *
			 */
			public Compiler()
			{
				this(UNKNOWN, UNKNOWN);
			}

			/**
			 *
			 * @param name
			 * @param version
			 */
			public Compiler(String name, String version)
			{
				this.name = ((name != null) && !name.isEmpty())
					? name : UNKNOWN;
				this.version = ((version != null) && !version.isEmpty())
					? version : UNKNOWN;
			}

			/**
			 * @return the name
			 */
			public String getName()
			{
				return name;
			}

			/**
			 * @return the version
			 */
			public String getVersion()
			{
				return version;
			}
		}

		/**
		 *
		 */
		public static class Library
		{
			private final String name;
			private final String version;

			/**
			 *
			 */
			public Library()
			{
				this(UNKNOWN, UNKNOWN);
			}

			/**
			 *
			 * @param name
			 * @param version
			 */
			public Library(String name, String version)
			{
				this.name = ((name != null) && !name.isEmpty())
					? name : UNKNOWN;
				this.version = ((version != null) && !version.isEmpty())
					? version : UNKNOWN;
			}

			/**
			 * @return the name
			 */
			public String getName()
			{
				return name;
			}

			/**
			 * @return the version
			 */
			public String getVersion()
			{
				return version;
			}
		}

	}

}
