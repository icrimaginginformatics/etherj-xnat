/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.schema;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class CatalogEntry
{
	private String cachePath;
	private final String content;
	private String createdBy;
	private String createdEventId;
	private String createdTime;
	private String description;
	private String digest;
	private final String format;
	private final String id;
	private String modifiedBy;
	private String modifiedEventId;
	private String modifiedTime;
	private final String name;
//		private List<MetaField> metaFieldList = new ArrayList<>();
	private final List<String> tags = new ArrayList<>();	
	private String uri;

	/**
	 *
	 * @param id
	 * @param name
	 * @param format
	 * @param content
	 */
	public CatalogEntry(String id, String name, String format, String content)
	{
		this.id = id;
		this.name = name;
		this.format = format;
		this.content = content;
	}

	/**
	 * @return the cachePath
	 */
	public String getCachePath()
	{
		return cachePath;
	}

	/**
	 * @return the content
	 */
	public String getContent()
	{
		return content;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy()
	{
		return createdBy;
	}

	/**
	 * @return the createdEventId
	 */
	public String getCreatedEventId()
	{
		return createdEventId;
	}

	/**
	 * @return the createdTime
	 */
	public String getCreatedTime()
	{
		return createdTime;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the digest
	 */
	public String getDigest()
	{
		return digest;
	}

	/**
	 * @return the format
	 */
	public String getFormat()
	{
		return format;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy()
	{
		return modifiedBy;
	}

	/**
	 * @return the modifiedEventId
	 */
	public String getModifiedEventId()
	{
		return modifiedEventId;
	}

	/**
	 * @return the modifiedTime
	 */
	public String getModifiedTime()
	{
		return modifiedTime;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the tags
	 */
	public List<String> getTags()
	{
		return tags;
	}

	/**
	 * @return the uri
	 */
	public String getUri()
	{
		return uri;
	}

	/**
	 * @param cachePath the cachePath to set
	 */
	public void setCachePath(String cachePath)
	{
		this.cachePath = cachePath;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * @param createdEventId the createdEventId to set
	 */
	public void setCreatedEventId(String createdEventId)
	{
		this.createdEventId = createdEventId;
	}

	/**
	 * @param createdTime the createdTime to set
	 */
	public void setCreatedTime(String createdTime)
	{
		this.createdTime = createdTime;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param digest the digest to set
	 */
	public void setDigest(String digest)
	{
		this.digest = digest;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @param modifiedEventId the modifiedEventId to set
	 */
	public void setModifiedEventId(String modifiedEventId)
	{
		this.modifiedEventId = modifiedEventId;
	}

	/**
	 * @param modifiedTime the modifiedTime to set
	 */
	public void setModifiedTime(String modifiedTime)
	{
		this.modifiedTime = modifiedTime;
	}

	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri)
	{
		this.uri = uri;
	}

}
