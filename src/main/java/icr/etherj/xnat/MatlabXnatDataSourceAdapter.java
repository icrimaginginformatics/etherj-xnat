/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Adapter class for use in MATLAB which can't handle generics.
 * @author jamesd
 */
public class MatlabXnatDataSourceAdapter
{
	private static final Logger logger =
		LoggerFactory.getLogger(MatlabXnatDataSourceAdapter.class);

	private final XnatDataSource xds;

	/**
	 *
	 * @param xds
	 * @throws IllegalArgumentException
	 */
	public MatlabXnatDataSourceAdapter(XnatDataSource xds)
		throws IllegalArgumentException
	{
		if (xds == null)
		{
			throw new IllegalArgumentException("XnatDataSource must not be null");
		}
		this.xds = xds;
	}

	/**
	 *
	 * @param projectId
	 * @param subjectName
	 * @return
	 */
	public List<ImageAnnotationCollection> searchIac(String projectId,
		String subjectName)
	{
		XnatToolkit tk = XnatToolkit.getToolkit();
		SearchSpecification subjSpec = tk.createSearchSpecification();
		SearchCriterion projSc = tk.createSearchCriterion(
			XnatTag.ProjectID, SearchCriterion.Equal, projectId);
		SearchCriterion nameSc = tk.createSearchCriterion(
			XnatTag.SubjectName, SearchCriterion.Like, subjectName);
		subjSpec.addCriterion(projSc);
		subjSpec.addCriterion(nameSc);
		List<XnatSubject> subjects = xds.searchSubject(subjSpec);
		List<ImageAnnotationCollection> iacs = new ArrayList<>();
		try
		{
			for (XnatSubject subject : subjects)
			{
				SearchSpecification spec = tk.createSearchSpecification();
				spec.addCriterion(projSc);
				SearchCriterion idSc = tk.createSearchCriterion(
					XnatTag.SubjectID, SearchCriterion.Equal, subject.getId());
				spec.addCriterion(idSc);
				iacs.addAll(xds.search(ImageAnnotationCollection.class, spec));
			}
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			iacs.clear();
		}
		return iacs;
	}

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<ImageAnnotationCollection> searchIac(SearchSpecification spec)
	{
		try
		{
			return xds.search(ImageAnnotationCollection.class, spec);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
		}
		return new ArrayList<>();
	}

	/**
	 *
	 * @param projectId
	 * @param subjectName
	 * @return
	 */
	public List<RtStruct> searchRtStruct(String projectId,
		String subjectName)
	{
		XnatToolkit tk = XnatToolkit.getToolkit();
		SearchSpecification subjSpec = tk.createSearchSpecification();
		SearchCriterion projSc = tk.createSearchCriterion(
			XnatTag.ProjectID, SearchCriterion.Equal, projectId);
		SearchCriterion nameSc = tk.createSearchCriterion(
			XnatTag.SubjectName, SearchCriterion.Like, subjectName);
		subjSpec.addCriterion(projSc);
		subjSpec.addCriterion(nameSc);
		List<XnatSubject> subjects = xds.searchSubject(subjSpec);
		List<RtStruct> list = new ArrayList<>();
		try
		{
			for (XnatSubject subject : subjects)
			{
				SearchSpecification spec = tk.createSearchSpecification();
				spec.addCriterion(projSc);
				SearchCriterion idSc = tk.createSearchCriterion(
					XnatTag.SubjectID, SearchCriterion.Equal, subject.getId());
				spec.addCriterion(idSc);
				list.addAll(xds.search(RtStruct.class, spec));
			}
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			list.clear();
		}
		return list;
	}

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<RtStruct> searchRtStruct(SearchSpecification spec)
	{
		try
		{
			return xds.search(RtStruct.class, spec);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
		}
		return new ArrayList<>();
	}

	/**
	 *
	 * @param projectId
	 * @param subjectName
	 * @return
	 */
	public List<Segmentation> searchSegmentation(String projectId,
		String subjectName)
	{
		XnatToolkit tk = XnatToolkit.getToolkit();
		SearchSpecification subjSpec = tk.createSearchSpecification();
		SearchCriterion projSc = tk.createSearchCriterion(
			XnatTag.ProjectID, SearchCriterion.Equal, projectId);
		SearchCriterion nameSc = tk.createSearchCriterion(
			XnatTag.SubjectName, SearchCriterion.Like, subjectName);
		subjSpec.addCriterion(projSc);
		subjSpec.addCriterion(nameSc);
		List<XnatSubject> subjects = xds.searchSubject(subjSpec);
		List<Segmentation> result = new ArrayList<>();
		try
		{
			for (XnatSubject subject : subjects)
			{
				SearchSpecification spec = tk.createSearchSpecification();
				spec.addCriterion(projSc);
				SearchCriterion idSc = tk.createSearchCriterion(
					XnatTag.SubjectID, SearchCriterion.Equal, subject.getId());
				spec.addCriterion(idSc);
				result.addAll(xds.search(Segmentation.class, spec));
			}
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
			result.clear();
		}
		return result;
	}

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<Segmentation> searchSegmentation(SearchSpecification spec)
	{
		try
		{
			return xds.search(Segmentation.class, spec);
		}
		catch (XnatException ex)
		{
			logger.warn("Search failed", ex);
		}
		return new ArrayList<>();
	}

	/**
	 *
	 * @param spec
	 * @return
	 */
	public List<XnatSubject> searchSubject(SearchSpecification spec)
	{
		return xds.searchSubject(spec);
	}

}
