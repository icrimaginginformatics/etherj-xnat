/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.search.AbstractSearchCriterion;
import icr.etherj.search.SearchCriterion;
import java.io.PrintStream;
import java.util.List;

/**
 *
 * @author jamesd
 */
class XnatSearchCriterion extends AbstractSearchCriterion
{
	private int combinator;
	private final int comparator;
	private final int tag;
	private int type = Unspecified;
	private final String value;

	XnatSearchCriterion(int tag, int comparator, String value)
	{
		this(tag, comparator, value, And);
	}

	XnatSearchCriterion(int tag, int comparator, String value, int combinator)
	{
		this.tag = tag;
		this.value = value;
		this.comparator = comparator;
		this.combinator = combinator;
	}
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Combinator: "+createCombinatorSql());
		ps.println(pad+buildString(new StringBuilder(), this).toString());
	}

	/**
	 * @return the combinator
	 */
	@Override
	public int getCombinator()
	{
		return combinator;
	}

	/**
	 * @return the comparator
	 */
	@Override
	public int getComparator()
	{
		return comparator;
	}

	@Override
	public List<SearchCriterion> getCriteria()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * @return the tag
	 */
	@Override
	public int getTag()
	{
		return tag;
	}

	@Override
	public String getTagName()
	{
		return XnatUtils.tagName(tag);
	}

	/**
	 * @return the type
	 */
	@Override
	public int getType()
	{
		return type;
	}

	@Override
	public String getTypeString()
	{
		String typeStr = "";
//		switch (type)
//		{
//			case Person:
//				typeStr = "Instance";
//				break;
//
//			default:
//		}
		return typeStr;
	}

	/**
	 * @return the value
	 */
	@Override
	public String getValue()
	{
		return value;
	}

	@Override
	public boolean hasCriteria()
	{
		return false;
	}

	/**
	 * @param combinator the combinator to set
	 */
	@Override
	public void setCombinator(int combinator)
	{
		this.combinator = combinator;
	}

	/**
	 * @param type the type to set
	 */
	@Override
	public void setType(int type)
	{
		this.type = type;
	}

}
