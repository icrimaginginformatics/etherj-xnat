/** *******************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package icr.etherj.xnat;

/**
 *
 * @author jamesd
 */
public class XnatRoi
{
	public static final String Line = "Line";
	public static final String Point = "Point";
	public static final String Segmentation = "Segmentation";
	public static final String TwoDContourStack = "TwoDContourStack";

	private final String id;
	private String collectId = "";
	private String geomType = "";
	private String name = "";
	private String uid = "";

	/**
	 *
	 * @param id
	 * @throws IllegalArgumentException
	 */
	public XnatRoi(String id) throws IllegalArgumentException
	{
		if ((id == null) || id.isEmpty())
		{
			throw new IllegalArgumentException("ID must not be null or empty");
		}
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getCollectionId()
	{
		return collectId;
	}

	/**
	 *
	 * @return
	 */
	public String getGeometricType()
	{
		return geomType;
	}

	/**
	 *
	 * @return
	 */
	public String getId()
	{
		return id;
	}

	/**
	 *
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 *
	 * @return
	 */
	public String getUid()
	{
		return uid;
	}

	/**
	 *
	 * @param id
	 * @throws IllegalArgumentException
	 */
	public void setCollectionId(String id) throws IllegalArgumentException
	{
		if ((id == null) || id.isEmpty())
		{
			throw new IllegalArgumentException(
				"Collection ID must not be null or empty");
		}
		collectId = id;
	}

	/**
	 *
	 * @param type
	 * @throws IllegalArgumentException
	 */
	public void setGeometricType(String type) throws IllegalArgumentException
	{
		if (type == null)
		{
			throw new IllegalArgumentException("Geometric must not be null");
		}
		switch (type)
		{
			case Line:
			case Point:
			case Segmentation:
			case TwoDContourStack:
				geomType = type;
				break;
			default:
				throw new IllegalArgumentException("Unknown geometric type: '"+
					type+"'");
		}
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = (name != null) ? name : "";
	}

	/**
	 *
	 * @param uid
	 */
	public void setUid(String uid)
	{
		this.uid = (uid != null) ? uid : "";
	}

}
