/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.StringUtils;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class XnatSubject implements XnatEntity, Comparable<XnatSubject>
{

	private final String id;
	private final String label;
	private final Set<Listener> listeners = new HashSet<>();
	private final String projectId;
	private final Map<String,XnatSession> sessions = new TreeMap<>();
	private final XnatId xnatId;

	public XnatSubject(String projectId, String id, String label)
	{
		if (StringUtils.isNullOrEmpty(projectId) ||
			 StringUtils.isNullOrEmpty(id) ||
			 StringUtils.isNullOrEmpty(label))
		{
			throw new IllegalArgumentException(
				"ProjectID, ID and label must not be null or empty");
		}
		this.projectId = projectId;
		this.id = id;
		this.label = label;
		xnatId = new XnatId(projectId, id);
	}

	public XnatSession add(XnatSession session)
	{
//		if (!id.equals(session.getSubjectId()))
//		{
//			throw new IllegalArgumentException("ProjectIDs must match");
//		}
		XnatSession result = sessions.put(session.getId(), session);
		fireSessionAdded(session);
		return result;
	}

	public boolean addXnatSubjectListener(Listener listener)
	{
		return listeners.add(listener);
	}

	@Override
	public int compareTo(XnatSubject other)
	{
		int result = projectId.compareTo(other.projectId);
		if (result != 0)
		{
			return result;
		}
		result = id.compareTo(other.id);
		return (result != 0) ? result : label.compareTo(other.label);
	}

	/**
	 * @return the id
	 */
	@Override
	public String getId()
	{
		return id;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId()
	{
		return projectId;
	}

	@Override
	public String getType()
	{
		return XnatEntity.Subject;
	}

	@Override
	public XnatId getXnatId()
	{
		return xnatId;
	}

	public XnatSession remove(String sessionId)
	{
		XnatSession result = sessions.remove(sessionId);
		if (result != null)
		{
			fireSessionRemoved(result);
		}
		return result;
	}

	public XnatSession remove(XnatSession session)
	{
		XnatSession result = sessions.remove(session.getId());
		fireSessionRemoved(session);
		return result;
	}

	public boolean removeXnatSubjectListener(Listener listener)
	{
		return listeners.remove(listener);
	}

	private void fireSessionAdded(XnatSession session)
	{
		Event e = new Event(this, session);
		for (Listener l : listeners)
		{
			l.sessionAdded(e);
		}
	}

	private void fireSessionRemoved(XnatSession session)
	{
		Event e = new Event(this, session);
		for (Listener l : listeners)
		{
			l.sessionRemoved(e);
		}
	}

	public static class Event
	{
		private final XnatSubject source;
		private final XnatSession session;

		/**
		 *
		 * @param source
		 * @param session
		 */
		public Event(XnatSubject source, XnatSession session)
		{
			this.source = source;
			this.session = session;
		}

		/**
		 * @return the source
		 */
		public XnatSubject getSource()
		{
			return source;
		}

		/**
		 * @return the subject
		 */
		public XnatSession getSession()
		{
			return session;
		}

	}

	public static interface Listener
	{
		/**
		 *
		 * @param event
		 */
		public void sessionAdded(Event event);

		/**
		 *
		 * @param event
		 */
		public void sessionRemoved(Event event);
	}

}
