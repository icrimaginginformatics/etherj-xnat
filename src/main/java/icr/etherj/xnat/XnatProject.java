/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class XnatProject implements XnatEntity, Comparable<XnatProject>
{
	private final String id;
	private String label = "";
	private final Set<Listener> listeners = new HashSet<>();
	private final SortedMap<String,XnatSubject> subjects = new TreeMap<>();
	private final XnatId xnatId;

	public XnatProject(String id)
	{
		this.id = id;
		xnatId = new XnatId(id);
	}

	public XnatProject(String id, String name)
	{
		this.id = id;
		xnatId = new XnatId(id);
		this.label = name;
	}

	public XnatSubject add(XnatSubject subject)
	{
		if (!id.equals(subject.getProjectId()))
		{
			throw new IllegalArgumentException("ProjectIDs must match");
		}
		XnatSubject result = subjects.put(subject.getId(), subject);
		fireSubjectAdded(subject);
		return result;
	}

	public boolean addXnatProjectListener(Listener listener)
	{
		return listeners.add(listener);
	}

	@Override
	public int compareTo(XnatProject other)
	{
		int result = id.compareTo(other.id);
		return (result == 0) ? label.compareTo(other.label) : result;
	}

	/**
	 * @return the id
	 */
	@Override
	public String getId()
	{
		return id;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	@Override
	public String getType()
	{
		return XnatEntity.Project;
	}

	@Override
	public XnatId getXnatId()
	{
		return xnatId;
	}

	public XnatSubject remove(String subjectId)
	{
		XnatSubject result = subjects.remove(subjectId);
		if (result != null)
		{
			fireSubjectRemoved(result);
		}
		return result;
	}

	public XnatSubject remove(XnatSubject subject)
	{
		XnatSubject result = subjects.remove(subject.getId());
		fireSubjectRemoved(subject);
		return result;
	}

	public boolean removeXnatProjectListener(Listener listener)
	{
		return listeners.remove(listener);
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label)
	{
		this.label = label;
	}

	private void fireSubjectAdded(XnatSubject subject)
	{
		Event e = new Event(this, subject);
		for (Listener l : listeners)
		{
			l.subjectAdded(e);
		}
	}

	private void fireSubjectRemoved(XnatSubject subject)
	{
		Event e = new Event(this, subject);
		for (Listener l : listeners)
		{
			l.subjectRemoved(e);
		}
	}

	public static class Event
	{
		private final XnatProject source;
		private final XnatSubject subject;

		public Event(XnatProject source, XnatSubject subject)
		{
			this.source = source;
			this.subject = subject;
		}

		/**
		 * @return the source
		 */
		public XnatProject getSource()
		{
			return source;
		}

		/**
		 * @return the subject
		 */
		public XnatSubject getSubject()
		{
			return subject;
		}

	}

	public static interface Listener
	{
		/**
		 *
		 * @param event
		 */
		public void subjectAdded(Event event);

		/**
		 *
		 * @param event
		 */
		public void subjectRemoved(Event event);
	}

}
