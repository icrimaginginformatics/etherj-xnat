/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.search.CompoundSearchCriterion;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import icr.etherj.xnat.impl.BifrostXnatFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.w3c.dom.Document;

/**
 *
 * @author jamesd
 */
public class BifrostXnatToolkit extends XnatToolkit
{
	private final XnatToolkit.XnatFactory xnatFactory = new BifrostXnatFactory();

	/**
	 *
	 * @return
	 * @throws XnatException
	 */
	@Override
	public XnatCache createCache() throws XnatException
	{
		return xnatFactory.createCache();
	}

	/**
	 *
	 * @param cachePath
	 * @return
	 * @throws XnatException
	 */
	@Override
	public XnatCache createCache(String cachePath) throws XnatException
	{
		return xnatFactory.createCache(cachePath);
	}

	/**
	 *
	 * @param connection
	 * @return
	 * @throws XnatException
	 */
	@Override
	public XnatDataSource createDataSource(XnatServerConnection connection)
		throws XnatException
	{
		return xnatFactory.createDataSource(connection);
	}

	/**
	 *
	 * @param connection
	 * @param cache
	 * @return
	 * @throws XnatException
	 */
	@Override
	public XnatDataSource createDataSource(XnatServerConnection connection,
		XnatCache cache) throws XnatException
	{
		return xnatFactory.createDataSource(connection, cache);
	}

	/**
	 *
	 * @param doc
	 * @return
	 */
	@Override
	public XnatResultSet createResultSet(Document doc)
	{
		return xnatFactory.createResultSet(doc);
	}

	/**
	 *
	 * @param tag
	 * @param comparator
	 * @param value
	 * @return
	 */
	@Override
	public SearchCriterion createSearchCriterion(int tag, int comparator,
		String value)
	{
		return new XnatSearchCriterion(tag, comparator, value);
	}

	/**
	 *
	 * @param tag
	 * @param comparator
	 * @param value
	 * @param combinator
	 * @return
	 */
	@Override
	public SearchCriterion createSearchCriterion(int tag, int comparator,
		String value, int combinator)
	{
		return new XnatSearchCriterion(tag, comparator, value, combinator);
	}

	/**
	 *
	 * @param a
	 * @param b
	 * @return
	 */
	@Override
	public SearchCriterion createSearchCriterion(SearchCriterion a,
		SearchCriterion b)
	{
		return new CompoundSearchCriterion(a, b);
	}

	/**
	 *
	 * @param a
	 * @param b
	 * @param combinator
	 * @return
	 */
	@Override
	public SearchCriterion createSearchCriterion(SearchCriterion a,
		SearchCriterion b, int combinator)
	{
		return new CompoundSearchCriterion(a, b, combinator);
	}

	/**
	 *
	 * @param criteria
	 * @return
	 */
	@Override
	public SearchCriterion createSearchCriterion(List<SearchCriterion> criteria)
	{
		return new CompoundSearchCriterion(criteria);
	}

	/**
	 *
	 * @param criteria
	 * @param combinator
	 * @return
	 */
	@Override
	public SearchCriterion createSearchCriterion(List<SearchCriterion> criteria,
		int combinator)
	{
		return new CompoundSearchCriterion(criteria, combinator);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public SearchSpecification createSearchSpecification()
	{
		return new SearchSpecification();
	}

	@Override
	public XnatServerConnection createServerConnection()
	{
		return xnatFactory.createServerConnection();
	}

	@Override
	public XnatServerConnection createServerConnection(String serverUrl,
		String userId, String password) throws XnatException
	{
		XnatServerConnection xsc = null;
		try
		{
			xsc = xnatFactory.createServerConnection(new URL(serverUrl), userId,
				password);
		}
		catch (MalformedURLException ex)
		{
			throw new XnatException("Malformed URL", XnatCode.MalformedUrl, ex);
		}
		return xsc;
	}

	@Override
	public XnatServerConnection createServerConnection(URL serverUrl,
		String userId, String password) throws XnatException
	{
		return xnatFactory.createServerConnection(serverUrl, userId, password);
	}

}
