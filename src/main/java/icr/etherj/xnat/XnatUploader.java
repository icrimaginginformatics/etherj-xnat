/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.Displayable;
import icr.etherj.xnat.metadata.MetadataComplexType;
import java.util.List;

/**
 * Upload system to upload items to XNAT.
 * @author jamesd
 * @param <T> the type of item to upload
 */
public interface XnatUploader<T>
{
	/**
	 *	Uploads metadata and associated resources into specified project. This
	 * method will block while listener handles pre-existence or ambiguity of
	 * items.
	 * @param projectId the ID of the project to upload to
	 * @param metadataList the list of item and metadata to upload
	 * @param helper the helper for class specific operations
	 * @param handler the handler for events arising during uploading
	 * @return the list of upload results
	 * @throws XnatException
	 */
	List<Result<T>> upload(String projectId, List<UploadMetadata<T>> metadataList,
		Helper<T> helper, EventHandler<T> handler) throws XnatException;

	/**
	 * Helper interface for supplying type-specific operations.
	 * @param <T> the type of item
	 */
	public static interface Helper<T>
	{
		/**
		 *
		 * @param metadata
		 * @return
		 */
		List<MetadataComplexType> createAuxiliaryMctList(UploadMetadata<T> metadata);

		/**
		 * Creates the correct complex type from the XNAT XML schema for
		 * the upload item.
		 * @param metadata the upload item's metadata
		 * @return the complex type
		 */
		MetadataComplexType createComplexType(UploadMetadata<T> metadata);

		/**
		 * Returns whether the item's label exists in the preferred project,
		 * subject, session in the data source.
		 * @param metadata the upload item's metadata
		 * @param dataSource the data source
		 * @return true if the item's label exists
		 * @throws icr.etherj.xnat.XnatException
		 */
		boolean exists(UploadMetadata<T> metadata,
			XnatDataSource dataSource) throws XnatException;

		/**
		 * Returns whether the item's label exists in the preferred project but
		 * outside the preferred subject and/or session in the data source.
		 * @param metadata the upload item's metadata
		 * @param dataSource the data source
		 * @return true if the item's label exists
		 * @throws icr.etherj.xnat.XnatException
		 */
		boolean existsInProject(UploadMetadata<T> metadata,
			XnatDataSource dataSource) throws XnatException;

		/**
		 * Populates the metadata's fields using its item's data.
		 * @param metadata
		 * @return true if fields successfully populated
		 */
		boolean populate(UploadMetadata<T> metadata);

	}

	/**
	 * Handles any events, ambiguity or pre-existence, that arise during an
	 * upload operation. Typically operates on a different thread from the upload
	 * operation itself.
	 * @param <T> the type of item
	 */
	public static interface EventHandler<T>
	{
		/**
		 * Notifies the handler that an upload item has multiple possibly upload
		 * targets. 
		 * @param event
		 */
		void itemAmbiguous(ItemAmbiguousEvent<T> event);

		/**
		 * Notifies the handler that an upload item already exists. 
		 * @param event
		 */
		void itemExists(ItemExistsEvent<T> event);
	}

	/**
	 * Grants access to the upload metadata and resolution actions that apply to
	 * all events.
	 * @param <T> the type of item
	 */
	public static interface Event<T>
	{
		/**
		 * Cancels upload operation, this and any remaining upload items will not
		 * be uploaded. Items already uploaded are not affected.
		 * @throws IllegalStateException if event has already been resolved
		 */
		void cancel() throws IllegalStateException;

		/**
		 * Returns the metadata for the upload item to enable decision making.
		 * @return the metadata
		 */
		UploadMetadata<T> getUploadMetadata();

		/**
		 * Skips the upload of this item.
		 * @throws IllegalStateException if event has already been resolved
		 */
		void skip() throws IllegalStateException;

		/**
		 * Skips the upload of this item and optionally all remaining items that
		 * match the type of event raised, ambiguous or pre-existing.
		 * @param all whether to skip any remaining matching items
		 * @throws IllegalStateException if event has already been resolved
		 */
		void skip(boolean all) throws IllegalStateException;
	}

	/**
	 * Event and resolution actions for items that have multiple upload targets.
	 * @param <T> the type of item
	 */
	public static interface ItemAmbiguousEvent<T> extends Event<T>
	{
		/**
		 * Selects the desired upload target. The list of possible targets is
		 * obtained from the item's metadata.
		 * @param container the selected target
		 * @throws IllegalStateException if event has already been resolved
		 */
		void select(StudyUidContainer container)
			throws IllegalStateException;

		/**
		 * Selects the desired upload target and optionally select the upload
		 * target for any remaining items with this upload target available.
		 * The list of possible targets is obtained from the item's metadata.
		 * @param container the selected target
		 * @param all whether to select this target for any remaining matching
		 * items
		 * @throws IllegalStateException if event has already been resolved
		 */
		void select(StudyUidContainer container, boolean all)
			throws IllegalStateException;
	}

	/**
	 * Event and resolution actions for items that already exist in the upload
	 * target.
	 * @param <T> the type of item
	 */
	public static interface ItemExistsEvent<T> extends Event<T>
	{
		/**
		 * Overwrites the pre-existing item with the current item.
		 * @throws IllegalStateException if event has already been resolved
		 */
		void overwrite() throws IllegalStateException;

		/**
		 * Overwrites the pre-existing item with the current item and optionally
		 * overwrite any remaining, matching items that have pre-existing items.
		 * @param all whether to overwrite the pre-exiting item for any remaining
		 * matching items
		 * @throws IllegalStateException if event has already been resolved
		 */
		void overwrite(boolean all) throws IllegalStateException;
	}

	/**
	 * Result of the upload operation for the item.
	 * @param <T> the type of item
	 */
	public static interface Result<T> extends Displayable
	{
		/** Upload was successful. */
		public static final int Success = 1;
		/** Item has no valid upload targets. */
		public static final int Orphan = 2;
		/** Only some objects referenced by the item are present. */
		public static final int MissingDependencies = 4;
		/** Item was skipped. */
		public static final int Skipped = 8;
		/** Item was cancelled. */
		public static final int Cancelled = 16;
		/** Exception thrown during upload. */
		public static final int Exception = 32;

		/**
		 * Returns the exception thrown during upload or null if no exception
		 * thrown.
		 * @return the exception or null
		 */
		Exception getException();

		/**
		 * Returns the metadata of the item.
		 * @return the metadata
		 */
		UploadMetadata<T> getMetadata();

		/**
		 * Returns the status of the upload result.
		 * @return the status
		 */
		int getStatus();
	}

}
