/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.metadata;

import icr.etherj.XmlBuilder;
import icr.etherj.xnat.schema.Resource;
import java.util.List;

/**
 *
 * @author jamesd
 */
public abstract class XnatImageAssessorDataMCT extends XnatDerivedDataMCT
{
	/**
	 *
	 * @return
	 */
	public abstract List<Resource> getInList();

	/**
	 *
	 * @return
	 */
	public abstract List<Resource> getOutList();

	/**
	 * @return the sessionId
	 */
	public abstract String getSessionId();

	@Override
	public void insertInto(XmlBuilder xmlBuilder)
	{
		super.insertInto(xmlBuilder);
		List<Resource> inList = getInList();
		if ((inList != null) && !inList.isEmpty())
		{
			xmlBuilder.startEntity("in");
			for (Resource resource : inList)
			{
				XnatResourceMCT xrmct = new XnatResourceMCT(resource);
				xrmct.insertInto(xmlBuilder);
			}
			xmlBuilder.endEntity();
		}
		List<Resource> outList = getOutList();
		if ((outList != null) && !outList.isEmpty())
		{
			xmlBuilder.startEntity("out");
			for (Resource resource : outList)
			{
				XnatResourceMCT xrmct = new XnatResourceMCT(resource);
				xrmct.insertInto(xmlBuilder);
			}
			xmlBuilder.endEntity();
		}
		xmlBuilder.startTextEntity("imageSession_ID", getSessionId()).endEntity();
	}

}
