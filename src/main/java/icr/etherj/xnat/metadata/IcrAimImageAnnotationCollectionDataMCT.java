/********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

package icr.etherj.xnat.metadata;

import icr.etherj.Uids;
import icr.etherj.XmlBuilder;
import icr.etherj.aim.AimUtils;
import icr.etherj.aim.Equipment;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.Person;
import icr.etherj.aim.User;
import icr.etherj.xnat.StudyUidContainer;
import icr.etherj.xnat.UploadMetadata;
import icr.etherj.xnat.schema.Provenance;
import icr.etherj.xnat.schema.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class IcrAimImageAnnotationCollectionDataMCT
	extends IcrGenericImageAssessmentDataMCT
{
	private final UploadMetadata<ImageAnnotationCollection> iacMetadata;

	public IcrAimImageAnnotationCollectionDataMCT(
		UploadMetadata<ImageAnnotationCollection> metadata)
	{
		this.iacMetadata = metadata;
	}

	@Override
	public String getDate()
	{
		Date dt = AimUtils.parseDateTime(iacMetadata.getUploadItem().getDateTime());
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(dt);
	}

	@Override
	public String getDicomSubjectName()
	{
		return iacMetadata.getUploadItem().getPerson().getName();
	}

	@Override
	public String getId()
	{
		return iacMetadata.getAccessionId();
	}

	@Override
	public List<Resource> getInList()
	{
		return iacMetadata.getInList();
	}

	@Override
	public String getLabel()
	{
		return iacMetadata.getLabel();
	}

	@Override
	public String getNote()
	{
		return "";
	}

	@Override
	public List<Resource> getOutList()
	{
		return iacMetadata.getOutList();
	}

	@Override
	public String getProjectId()
	{
		return iacMetadata.getPreferredContainer().getProjectId();
	}

	@Override
	public Provenance getProvenance()
	{
		return iacMetadata.getProvenance();
	}

	@Override
	public String getRootElement()
	{
		return "AimImageAnnotationCollection";
	}

	@Override
	public Set<String> getScans()
	{
		StudyUidContainer container = iacMetadata.getPreferredContainer();
		return iacMetadata.getDependencyMaps(container).getSeriesIdSet();
	}

	@Override
	public String getSessionId()
	{
		return iacMetadata.getPreferredContainer().getSessionId();
	}

	@Override
	public String getSubjectId()
	{
		return iacMetadata.getPreferredContainer().getSubjectId();
	}

	@Override
	public String getTime()
	{
		Date dt = AimUtils.parseDateTime(iacMetadata.getUploadItem().getDateTime());
		DateFormat format = new SimpleDateFormat("HH:mm:ss");
		return format.format(dt);
	}

	@Override
	public String getVersion()
	{
		return iacMetadata.getVersion();
	}

	@Override
	public void insertInto(XmlBuilder xmlBuilder)
	{
		super.insertInto(xmlBuilder);
		ImageAnnotationCollection iac = iacMetadata.getUploadItem();
		insertAimGeneralInfo(xmlBuilder, iac);
		insertAnnotations(xmlBuilder, iac);
	}

	private void insertAimGeneralInfo(XmlBuilder xmlBuilder,
		ImageAnnotationCollection iac)
	{
		User user = iac.getUser();
		Equipment eqpt = iac.getEquipment();
		Person person = iac.getPerson();
		xmlBuilder
			.startTextEntity("aimVersion", iac.getAimVersion())
			.endEntity()
			.startTextEntity("aimId", iac.getUid())
			.endEntity()
			.startEntity("aimUser")
				.setAttribute("name", user.getName())
				.setAttribute("loginName", user.getLoginName())
				.setAttribute("roleInClinicalTrial", user.getRoleInTrial())
				.setAttribute("numberWithinRoleOfClinicalTrial",
					user.getNumberWithinRoleOfClinicalTrial())
			.endEntity()
			.startEntity("aimEquipment")
				.setAttribute("manufacturerName", eqpt.getManufacturerName())
				.setAttribute("manufacturerModelName", eqpt.getManufacturerModelName())
				.setAttribute("deviceSerialNumber", eqpt.getDeviceSerialNumber())
				.setAttribute("softwareVersionString", eqpt.getSoftwareVersion())
			.endEntity()
			.startEntity("person")
				.setAttribute("name", person.getName())
				.setAttribute("id", person.getId())
				.setAttribute("birthDate", person.getBirthDate())
				.setAttribute("sex", person.getSex())
				.setAttribute("ethnicGroup", person.getEthnicGroup())
			.endEntity();
	}

	private XmlBuilder insertAnnotations(XmlBuilder xmlBuilder,
		ImageAnnotationCollection iac)
	{
		List<ImageAnnotation> annotations = iac.getAnnotationList();
		xmlBuilder
			.startTextEntity("nImageAnnotation",
				Integer.toString(annotations.size()))
			.endEntity()
			.startTextEntity("associatedRegionSetId",
				"RegionSet_"+Uids.createShortUnique())
			.endEntity();
		xmlBuilder.startEntity("imageAnnotationIds");
		for (ImageAnnotation annotation : annotations)
		{
			xmlBuilder
				.startTextEntity("imageAnnotationId",
					"ImageAnnotation_"+Uids.createShortUnique())
				.endEntity();
		}
		xmlBuilder.endEntity();
		return xmlBuilder;
	}

}
