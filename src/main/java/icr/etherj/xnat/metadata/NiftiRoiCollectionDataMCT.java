/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.metadata;

import icr.etherj.XmlBuilder;
import icr.etherj.nifti.Nifti;
import icr.etherj.xnat.UploadMetadata;
import static icr.etherj.xnat.metadata.RoiCollectionDataMCT.CollectionType;
import static icr.etherj.xnat.metadata.RoiCollectionDataMCT.Name;
import static icr.etherj.xnat.metadata.RoiCollectionDataMCT.References;
import static icr.etherj.xnat.metadata.RoiCollectionDataMCT.SeriesUid;
import static icr.etherj.xnat.metadata.RoiCollectionDataMCT.SubjectId;
import static icr.etherj.xnat.metadata.RoiCollectionDataMCT.Uid;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class NiftiRoiCollectionDataMCT
	extends RoiCollectionDataMCT<Nifti>
{

	public NiftiRoiCollectionDataMCT(UploadMetadata<Nifti> metadata)
	{
		super(metadata);
	}

	@Override
	public String getDate()
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public String getTime()
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	@Override
	public void insertInto(XmlBuilder xmlBuilder)
	{
		super.insertInto(xmlBuilder);
		Nifti nifti = metadata.getUploadItem();
		xmlBuilder
			.startTextEntity(Uid, "").endEntity()
			.startTextEntity(CollectionType, "NIFTI").endEntity()
			.startTextEntity(SubjectId, getSubjectId()).endEntity();
		Set<String> refUids = metadata.getSeriesUids();
		if (!refUids.isEmpty())
		{
			xmlBuilder.startEntity(References);
			for (String uid : refUids)
			{
				xmlBuilder.startTextEntity(SeriesUid, uid).endEntity();
			}
			xmlBuilder.endEntity();
		}
		xmlBuilder.startTextEntity(Name, getName()).endEntity();
	}

}
