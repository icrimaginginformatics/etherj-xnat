/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.metadata;

import icr.etherj.XmlBuilder;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.xnat.UploadMetadata;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class RtStructRoiCollectionDataMCT extends RoiCollectionDataMCT<RtStruct>
{
	public RtStructRoiCollectionDataMCT(UploadMetadata<RtStruct> metadata)
	{
		super(metadata);
	}

	@Override
	public String getDate()
	{
		Date dt = DicomUtils.parseDate(metadata.getUploadItem().getStudyDate());
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(dt);
	}

	@Override
	public String getTime()
	{
		Date dt = DicomUtils.parseTime(metadata.getUploadItem().getStudyTime());
		DateFormat format = new SimpleDateFormat("HH:mm:ss");
		return format.format(dt);
	}

	@Override
	public void insertInto(XmlBuilder xmlBuilder)
	{
		super.insertInto(xmlBuilder);
		RtStruct rtStruct = metadata.getUploadItem();
		xmlBuilder
			.startTextEntity(Uid, rtStruct.getSopInstanceUid()).endEntity()
			.startTextEntity(CollectionType, "RTSTRUCT").endEntity()
			.startTextEntity(SubjectId, getSubjectId()).endEntity();
		Set<String> refUids = metadata.getSeriesUids();
		if (!refUids.isEmpty())
		{
			xmlBuilder.startEntity(References);
			for (String uid : refUids)
			{
				xmlBuilder.startTextEntity(SeriesUid, uid).endEntity();
			}
			xmlBuilder.endEntity();
		}
		xmlBuilder.startTextEntity(Name, getName()).endEntity();
		String desc = rtStruct.getStructureSetModule().getStructureSetDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			xmlBuilder.startTextEntity(Description, desc).endEntity();
		}
	}

}
