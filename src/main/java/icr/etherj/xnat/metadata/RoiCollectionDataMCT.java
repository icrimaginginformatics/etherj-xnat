/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat.metadata;

import icr.etherj.xnat.UploadMetadata;
import icr.etherj.xnat.schema.Provenance;
import icr.etherj.xnat.schema.Resource;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 * @param <T>
 */
public abstract class RoiCollectionDataMCT<T> extends XnatImageAssessorDataMCT
{
	public static final String CollectionType = "collectionType";
	public static final String Description = "description";
	public static final String Name = "name";
	public static final String References = "references";
	public static final String RoiCollection = "RoiCollection";
	public static final String SeriesUid = "seriesUID";
	public static final String SubjectId = "subjectID";
	public static final String Uid = "UID";

	private final static Logger logger = LoggerFactory.getLogger(
		RoiCollectionDataMCT.class);

	protected final UploadMetadata<T> metadata;

	public RoiCollectionDataMCT(UploadMetadata<T> metadata)
	{
		this.metadata = metadata;
	}

	@Override
	public String getId()
	{
		return metadata.getAccessionId();
	}

	@Override
	public List<Resource> getInList()
	{
		return metadata.getInList();
	}

	@Override
	public String getLabel()
	{
		return metadata.getLabel();
	}

	/**
	 *
	 * @return
	 */
	public String getName()
	{
		return metadata.getName();
	}

	@Override
	public String getNote()
	{
		return "";
	}

	@Override
	public List<Resource> getOutList()
	{
		return metadata.getOutList();
	}

	@Override
	public String getProjectId()
	{
		return metadata.getPreferredContainer().getProjectId();
	}

	@Override
	public Provenance getProvenance()
	{
		return metadata.getProvenance();
	}

	@Override
	public String getRootElement()
	{
		return RoiCollection;
	}

	/**
	 *
	 * @return
	 */
	public Set<String> getReferencedSeriesUids()
	{
		return metadata.getSeriesUids();
	}

	@Override
	public String getSessionId()
	{
		return metadata.getPreferredContainer().getSessionId();
	}

	/**
	 *
	 * @return
	 */
	public String getSubjectId()
	{
		return metadata.getPreferredContainer().getSubjectId();
	}

	@Override
	public String getVersion()
	{
		return metadata.getVersion();
	}

}
