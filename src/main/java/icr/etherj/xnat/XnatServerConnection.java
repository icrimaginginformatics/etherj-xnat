/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.xnat;

import icr.etherj.Displayable;
import java.io.Closeable;
import java.io.InputStream;
import java.net.URL;
import org.w3c.dom.Document;

/**
 *
 * @author jamesd
 */
public interface XnatServerConnection extends Closeable, Displayable
{
	@Override
	public void close();

	/**
	 *
	 * @param command
	 * @return
	 * @throws XnatException
	 */
	public InputStream delete(String command) throws XnatException;

	/**
	 *
	 * @param command
	 * @return
	 * @throws icr.etherj.xnat.XnatException
	 */
	public InputStream get(String command) throws XnatException;

	/**
	 *
	 * @param command
	 * @return
	 * @throws XnatException
	 */
	public Document getDocument(String command) throws XnatException;

	/**
	 *
	 * @param command
	 * @param xml
	 * @return
	 * @throws XnatException
	 */
	public Document getDocument(String command, String xml) throws XnatException;

	/**
	 *
	 * @param command
	 * @return
	 * @throws XnatException
	 */
	public XnatResultSet getResultSet(String command) throws XnatException;

	/**
	 *
	 * @param command
	 * @param xml
	 * @return
	 * @throws XnatException
	 */
	public XnatResultSet getResultSet(String command, String xml) throws
		XnatException;

	/**
	 *
	 * @return
	 * @throws XnatException
	 */
	public String getServerVersion() throws XnatException;

	/**
	 *
	 * @return
	 */
	public URL getServerUrl();

	/**
	 *
	 * @return
	 */
	public boolean isOpen();

	/**
	 *
	 * @throws XnatException
	 */
	public void open() throws XnatException;

	/**
	 *
	 * @param command
	 * @return
	 * @throws XnatException
	 */
	public InputStream post(String command) throws XnatException;

	/**
	 *
	 * @param command
	 * @param doc
	 * @return
	 * @throws XnatException
	 */
	public InputStream post(String command, Document doc) throws XnatException;

	/**
	 *
	 * @param command
	 * @param is
	 * @return
	 * @throws XnatException
	 */
	public InputStream post(String command, InputStream is) throws XnatException;

	/**
	 *
	 * @param command
	 * @param xml
	 * @return
	 * @throws XnatException
	 */
	public InputStream post(String command, String xml) throws XnatException;

	/**
	 *
	 * @param command
	 * @return
	 * @throws XnatException
	 */
	public InputStream put(String command) throws XnatException;

	/**
	 *
	 * @param command
	 * @param doc
	 * @return
	 * @throws XnatException
	 */
	public InputStream put(String command, Document doc) throws XnatException;

	/**
	 *
	 * @param command
	 * @param is
	 * @return
	 * @throws XnatException
	 */
	public InputStream put(String command, InputStream is) throws XnatException;

}
